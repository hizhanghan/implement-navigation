﻿using Microsoft.Win32;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System;
using YonYou.U8.IN.Framework;
using U8Login;
using System.Diagnostics;
using Excel;

namespace YonYou.U8.IN.DataCheck
{
    public class Common
    {
        public enum OfficeType
        {
            None = 0,
            Office2000 = 9,
            Office2002 = 10,
            Office2003 = 11,
            Office2007 = 12,
            Office2010 = 14,
            Office2013 = 15
        }

        public static string ConnString;

        public static BizContext context;

        public static UFSoft.U8.Framework.Login.UI.clsLogin login;

        public static U8Login.clsLogin VBlogin;

        public static string DataCheckTemplateAdminConfig;

        public static string senderFile;

        public static string loginDate;

        public static bool GetSql(ReconcileArgs e)
        {
            Debug.WriteLine("Begin GetSql 组件名称:" + e.ComName);
            if (e.ComName != "")
            {
                object obj = null;
                object[] array = null;
                try
                {
                    array = new object[1] { e };
                    Type typeFromProgID = Type.GetTypeFromProgID(e.ComName.Trim());
                    if (typeFromProgID == null)
                    {
                        Debug.WriteLine("不存在以下组件或者以下组件没有正确注册：" + e.ComName.Trim());
                        e.ResultDesc = "不存在组件或者组件没有正确注册：" + e.ComName.Trim();
                        return false;
                    }
                    obj = Activator.CreateInstance(typeFromProgID);
                    obj.GetType().InvokeMember("GetSql", BindingFlags.InvokeMethod, null, obj, array);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("End GetSql , datasource type:" + e.DataSourceType + " " + ex.Message);
                    e.ResultDesc = e.ComName + "执行异常，请检查" + Environment.NewLine + "异常消息为：" + ex.Message + Environment.NewLine + "堆栈信息为：" + ex.StackTrace;
                    return false;
                }
                return true;
            }
            return false;
        }

        public static bool GetSql(MonthEndArgs e)
        {
            Debug.WriteLine("Begin get sql 组件名称:" + e.ComName);
            if (e.ComName != "")
            {
                object obj = null;
                object[] array = null;
                try
                {
                    array = new object[1] { e };
                    Type typeFromProgID = Type.GetTypeFromProgID(e.ComName.Trim());
                    if (typeFromProgID == null)
                    {
                        Debug.WriteLine("不存在以下组件或者以下组件没有正确注册：" + e.ComName.Trim());
                        e.ResultDesc = "不存在组件或者组件没有正确注册：" + e.ComName.Trim();
                        return false;
                    }
                    obj = Activator.CreateInstance(typeFromProgID);
                    obj.GetType().InvokeMember("GetSql", BindingFlags.InvokeMethod, null, obj, array);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("End get sql , datasource type:" + e.DataSourceType + " " + ex.Message);
                    e.ResultDesc = e.ComName + "执行异常，请检查";
                    return false;
                }
                return true;
            }
            return false;
        }

        public static YonYou.U8.IN.DataCheck.Module GetModule(string moduleId)
        {
            string text = "select cMenu_ID,cMenu_Name,IN_ModuleOrder.iOrder from V_IN_SubSys left join IN_ModuleOrder on V_IN_SubSys.cMenu_ID=IN_ModuleOrder.ModuleId where cMenu_Id='" + moduleId + "' order by IN_ModuleOrder.iOrder";
            Debug.WriteLine("数据检查模块信息：" + text);
            System.Data.DataTable dataTable = new System.Data.DataTable();
            YonYou.U8.IN.DataCheck.Module module = new YonYou.U8.IN.DataCheck.Module();
            using (DbContext dbContext = new DbContext(ConnString))
            {
                dataTable = dbContext.QueryDataSet(text).Tables[0];
                if (dataTable.Rows.Count > 0)
                {
                    module.ModuleId = dataTable.Rows[0][0].ToString();
                    module.ModuleName = dataTable.Rows[0][1].ToString();
                    module.iOrder = ((!(dataTable.Rows[0][2].ToString() == "")) ? Convert.ToInt32(dataTable.Rows[0][2].ToString()) : 0);
                }
            }
            return module;
        }

        public static List<YonYou.U8.IN.DataCheck.Module> GetModuleList(string tableName)
        {
            string cmdText = string.Format("select distinct {0}.ModuleId,IN_ModuleOrder.iOrder from {0} left join IN_ModuleOrder on {0}.ModuleId=IN_ModuleOrder.ModuleId order by IN_ModuleOrder.iOrder", tableName);
            System.Data.DataTable dataTable = new System.Data.DataTable();
            List<YonYou.U8.IN.DataCheck.Module> list = new List<YonYou.U8.IN.DataCheck.Module>();
            using (DbContext dbContext = new DbContext(ConnString))
            {
                dataTable = dbContext.QueryDataSet(cmdText).Tables[0];
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    if (IsModuleStart(dataTable.Rows[i][0].ToString()))
                    {
                        list.Add(GetModule(dataTable.Rows[i][0].ToString()));
                    }
                }
            }
            return list;
        }

        public static bool IsModuleStart(string moduleId)
        {
            U8Login.clsLogin clsLogin = new clsLoginClass();
            clsLogin.ConstructLoginEx(login.userToken, login.GetLoginInfo(login.userToken));
            object vValue = moduleId;
            clsLogin.GetAccInfo(201, ref vValue);
            if (vValue != null && vValue.ToString() != "")
            {
                return true;
            }
            return false;
        }

        public static string GetU8Path()
        {
            string name = "Software\\UFSoft\\WF\\V8.700\\Install\\CurrentInstPath";
            string name2 = "";
            try
            {
                using (RegistryKey registryKey = Registry.LocalMachine)
                {
                    using (RegistryKey registryKey2 = registryKey.OpenSubKey(name))
                    {
                        if (registryKey2 != null && registryKey2.GetValue(name2) != null)
                        {
                            return (string)registryKey2.GetValue(name2);
                        }
                    }
                }
                return "";
            }
            catch
            {
                return "";
            }
        }

        public static string GetTabelName()
        {
            bool flag = true;
            string text = "";
            while (flag)
            {
                text = "tmpuf_" + VBlogin.TaskId + "_IN_" + Guid.NewGuid().ToString().Replace("-", "");
                string cmdText = "use tempdb select top 1 * from tempdb..sysobjects where Id=Object_Id('" + text + "')  and type='U'";
                System.Data.DataTable dataTable = new System.Data.DataTable();
                using (DbContext dbContext = new DbContext(ConnString))
                {
                    dataTable = dbContext.QueryDataSet(cmdText).Tables[0];
                }
                if (dataTable.Rows.Count == 0)
                {
                    flag = false;
                }
            }
            return text;
        }

        public static void DropTempTable(List<string> tableNameList)
        {
            if (tableNameList.Count == 0)
            {
                return;
            }
            string text = "";
            foreach (string tableName in tableNameList)
            {
                text = text + " if OBJECT_ID('tempdb.." + tableName + "')is not null drop table tempdb.." + tableName;
                using (DbContext dbContext = new DbContext(ConnString))
                {
                    dbContext.NonQuery(text);
                }
            }
        }

        public static bool InsertTempTable(string tableName, string sql)
        {
            bool result = false;
            string cmdText = "select * into tempdb.." + tableName + " from (" + sql + ") as IN_temp";
            using (DbContext dbContext = new DbContext(ConnString))
            {
                result = dbContext.NonQuery(cmdText) > 0;
            }
            return result;
        }

        private static string GetColValue(System.Data.DataTable dt, string colName, int rowIndex)
        {
            if (!dt.Columns.Contains(colName))
            {
                return "";
            }
            return dt.Rows[rowIndex][colName].ToString();
        }

        private static string GetCellValue(Cell cell, SharedStringTablePart stringTablePart)
        {
            if (cell.ChildElements.Count == 0)
            {
                return "";
            }
            string innerText = cell.CellValue.InnerText;
            if (cell.DataType != null && (CellValues)cell.DataType == CellValues.SharedString)
            {
                innerText = stringTablePart.SharedStringTable.ChildElements[int.Parse(innerText)].InnerText;
            }
            return innerText;
        }

        public static string FillDataToExcel(string path, string tableName, string sheetName)
        {
            string cmdText = "select * from tempdb.." + tableName;
            System.Data.DataTable dataTable = new System.Data.DataTable();
            using (DbContext dbContext = new DbContext(ConnString))
            {
                dataTable = dbContext.QueryDataSet(cmdText).Tables[0];
            }
            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(path, true))
            {
                IEnumerable<Sheet> source = from s in spreadsheetDocument.WorkbookPart.Workbook.Descendants<Sheet>()
                                            where s.Name == sheetName
                                            select s;
                if (source.Count() == 0)
                {
                    return "没有找到名称为[" + sheetName + "]的Sheet页签";
                }
                WorksheetPart worksheetPart = (WorksheetPart)spreadsheetDocument.WorkbookPart.GetPartById(source.First().Id);
                DocumentFormat.OpenXml.Spreadsheet.Worksheet worksheet = worksheetPart.Worksheet;
                SheetData firstChild = worksheet.GetFirstChild<SheetData>();
                WorkbookStylesPart workbookStylesPart = spreadsheetDocument.WorkbookPart.WorkbookStylesPart;
                uint index = 0u;
                AddStylesheet(workbookStylesPart.Stylesheet, ref index);
                IEnumerable<Row> source2 = firstChild.Descendants<Row>();
                Row row = source2.Where((Row s) => (uint)s.RowIndex == 1).FirstOrDefault();
                Cell cell = (from s in row.Descendants<Cell>()
                             where s.CellReference == "A1"
                             select s).FirstOrDefault();
                int num = Convert.ToInt32(cell.CellValue.Text);
                Cell cell2 = (from s in row.Descendants<Cell>()
                              where s.CellReference == "B1"
                              select s).FirstOrDefault();
                int num2 = Convert.ToInt32(cell2.CellValue.Text);
                int colIndex = 2;
                Row row2 = source2.Where((Row s) => (uint)s.RowIndex == (uint)colIndex).FirstOrDefault();
                int num3 = 0;
                int num4 = 0;
                for (int i = num2; i < dataTable.Rows.Count + num2; i++)
                {
                    UInt32Value rowIndex = Convert.ToUInt32(i);
                    Row row3 = new Row();
                    row3.RowIndex = rowIndex;
                    Row row4 = row3;
                    for (int j = 1; j < num + 1; j++)
                    {
                        num4 = j / 26;
                        num3 = j % 26;
                        string prefixChar = "";
                        if (num4 > 0 && num3 != 0)
                        {
                            prefixChar = ((char)(num4 + 64)).ToString();
                        }
                        if (num3 == 0)
                        {
                            num3 = 26;
                        }
                        char strChar = (char)(num3 + 64);
                        Cell cell3 = new Cell();
                        cell3.CellReference = prefixChar + strChar + i;
                        cell3.StyleIndex = index;
                        cell3.DataType = CellValues.String;
                        CellValue cellValue = new CellValue();
                        Cell cell4 = (from s in row2.Descendants<Cell>()
                                      where s.CellReference == prefixChar + strChar + colIndex.ToString()
                                      select s).FirstOrDefault();
                        if (cell4 == null)
                        {
                            spreadsheetDocument.Close();
                            return "模板[" + sheetName + "]格式或者设置出错";
                        }
                        string cellValue2 = GetCellValue(cell4, spreadsheetDocument.WorkbookPart.SharedStringTablePart);
                        cellValue.Text = GetColValue(dataTable, cellValue2, i - num2);
                        cell3.Append(cellValue);
                        row4.Append(cell3);
                    }
                    try
                    {
                        firstChild.Append(row4);
                    }
                    catch (Exception)
                    {
                        spreadsheetDocument.Close();
                        return "添加数据行出错";
                    }
                }
                worksheet.Save();
                spreadsheetDocument.Close();
            }
            Excel.Application application = new ApplicationClass();
            Excel.Workbook workbook;
            try
            {
                workbook = application.Workbooks.Open(path, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            }
            catch (Exception)
            {
                application.Quit();
                application = null;
                return "模板[" + sheetName + "]格式设置错误";
            }
            Excel.Worksheet worksheet2 = workbook.Sheets[1] as Excel.Worksheet;
            ((Range)worksheet2.Rows[1, Missing.Value]).Delete(Missing.Value);
            ((Range)worksheet2.Rows[1, Missing.Value]).Delete(Missing.Value);
            workbook.Close(true, Missing.Value, Missing.Value);
            application.Quit();
            application = null;
            return string.Empty;
        }

        public static bool IsExcelInstalled()
        {
            try
            {
                RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\App Paths\\excel.exe");
                if (registryKey != null)
                {
                    string text = registryKey.GetValue("Path").ToString();
                    text = text.Substring(0, text.LastIndexOf('\\'));
                    string input = text.Substring(text.LastIndexOf('\\') + 1).ToUpper();
                    string pattern = "^OFFICE(?'number'\\d+)";
                    Regex regex = new Regex(pattern);
                    int num = int.Parse(regex.Match(input).Groups["number"].Value);
                    if (num < 12)
                    {
                        return false;
                    }
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public static void CreateSpreadsheetWorkbook(string exportFile, string type)
        {
            File.Delete(exportFile);
            SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(exportFile, SpreadsheetDocumentType.Workbook);
            WorkbookPart workbookPart = spreadsheetDocument.AddWorkbookPart();
            workbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();
            WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            worksheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet();
            DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild(new DocumentFormat.OpenXml.Spreadsheet.Sheets());
            Sheet sheet = new Sheet();
            sheet.Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart);
            sheet.SheetId = 1u;
            sheet.Name = Path.GetFileNameWithoutExtension(exportFile);
            Sheet sheet2 = sheet;
            sheets.Append(sheet2);
            Columns columns = new Columns();
            if (type == "0")
            {
                Column column = new Column();
                column.Min = 1u;
                column.Max = 1u;
                column.Width = 19.0;
                column.CustomWidth = true;
                Column column2 = column;
                columns.Append(column2);
                Column column3 = new Column();
                column3.Min = 2u;
                column3.Max = 2u;
                column3.Width = 25.0;
                column3.CustomWidth = true;
                Column column4 = column3;
                columns.Append(column4);
                Column column5 = new Column();
                column5.Min = 3u;
                column5.Max = 3u;
                column5.Width = 40.0;
                column5.CustomWidth = true;
                Column column6 = column5;
                columns.Append(column6);
            }
            else if (type == "1")
            {
                Column column7 = new Column();
                column7.Min = 1u;
                column7.Max = 1u;
                column7.Width = 19.0;
                column7.CustomWidth = true;
                Column column2 = column7;
                columns.Append(column2);
                Column column8 = new Column();
                column8.Min = 2u;
                column8.Max = 2u;
                column8.Width = 25.0;
                column8.CustomWidth = true;
                Column column4 = column8;
                columns.Append(column4);
                Column column9 = new Column();
                column9.Min = 3u;
                column9.Max = 3u;
                column9.Width = 10.0;
                column9.CustomWidth = true;
                Column column6 = column9;
                columns.Append(column6);
                Column column10 = new Column();
                column10.Min = 4u;
                column10.Max = 4u;
                column10.Width = 30.0;
                column10.CustomWidth = true;
                Column column11 = column10;
                columns.Append(column11);
                Column column12 = new Column();
                column12.Min = 5u;
                column12.Max = 5u;
                column12.Width = 40.0;
                column12.CustomWidth = true;
                Column column13 = column12;
                columns.Append(column13);
            }
            worksheetPart.Worksheet.Append(columns);
            SheetData sheetData = new SheetData();
            worksheetPart.Worksheet.Append(sheetData);
            worksheetPart.Worksheet.Save();
            spreadsheetDocument.Close();
        }

        public static void ExportSummaryExcel(string exportFile, object checkResultList, string type)
        {
            try
            {
                CreateSpreadsheetWorkbook(exportFile, type);
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(exportFile, true))
                {
                    IEnumerable<Sheet> source = from s in spreadsheetDocument.WorkbookPart.Workbook.Descendants<Sheet>()
                                                where (uint)s.SheetId == 1
                                                select s;
                    if (source.Count() == 0)
                    {
                        return;
                    }
                    WorksheetPart worksheetPart = (WorksheetPart)spreadsheetDocument.WorkbookPart.GetPartById(source.First().Id);
                    DocumentFormat.OpenXml.Spreadsheet.Worksheet worksheet = worksheetPart.Worksheet;
                    SheetData firstChild = worksheet.GetFirstChild<SheetData>();
                    WorkbookStylesPart workbookStylesPart = spreadsheetDocument.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                    workbookStylesPart.Stylesheet = CreateStylesheet();
                    workbookStylesPart.Stylesheet.Save();
                    Row row = new Row();
                    row.RowIndex = 1u;
                    Row row2 = row;
                    Cell cell = new Cell();
                    cell.CellReference = "A1";
                    cell.DataType = CellValues.String;
                    CellValue cellValue = new CellValue();
                    cellValue.Text = "检查日期：";
                    cell.Append(cellValue);
                    row2.Append(cell);
                    cell = new Cell();
                    cell.CellReference = "B1";
                    cell.DataType = CellValues.String;
                    cellValue = new CellValue();
                    cellValue.Text = loginDate;
                    cell.Append(cellValue);
                    row2.Append(cell);
                    firstChild.Append(row2);
                    if (type == "0")
                    {
                        List<ReconcileCheckResult> list = checkResultList as List<ReconcileCheckResult>;
                        Row row3 = new Row();
                        row3.RowIndex = 3u;
                        Row row4 = row3;
                        Cell cell2 = new Cell();
                        cell2.CellReference = "A3";
                        cell2.StyleIndex = 1u;
                        cell2.DataType = CellValues.String;
                        CellValue cellValue2 = new CellValue();
                        cellValue2.Text = "分类";
                        cell2.Append(cellValue2);
                        row4.Append(cell2);
                        cell2 = new Cell();
                        cell2.CellReference = "B3";
                        cell2.StyleIndex = 1u;
                        cell2.DataType = CellValues.String;
                        cellValue2 = new CellValue();
                        cellValue2.Text = "对账内容";
                        cell2.Append(cellValue2);
                        row4.Append(cell2);
                        cell2 = new Cell();
                        cell2.CellReference = "C3";
                        cell2.StyleIndex = 1u;
                        cell2.DataType = CellValues.String;
                        cellValue2 = new CellValue();
                        cellValue2.Text = "检查结果";
                        cell2.Append(cellValue2);
                        row4.Append(cell2);
                        firstChild.Append(row4);
                        UInt32Value uInt32Value = 4u;
                        foreach (ReconcileCheckResult item in list)
                        {
                            Row row5 = new Row();
                            row5.RowIndex = uInt32Value;
                            row4 = row5;
                            cell2 = new Cell();
                            cell2.CellReference = "A" + uInt32Value.ToString();
                            cell2.StyleIndex = 2u;
                            cell2.DataType = CellValues.String;
                            cellValue2 = new CellValue();
                            cellValue2.Text = item.Reconcile.ModuleName;
                            cell2.Append(cellValue2);
                            row4.Append(cell2);
                            Cell cell3 = new Cell();
                            cell3.CellReference = "B" + uInt32Value.ToString();
                            cell3.StyleIndex = 2u;
                            cell3.DataType = CellValues.String;
                            CellValue cellValue3 = new CellValue();
                            cellValue3.Text = item.Reconcile.CheckContent;
                            cell3.Append(cellValue3);
                            row4.Append(cell3);
                            Cell cell4 = new Cell();
                            cell4.CellReference = "C" + uInt32Value.ToString();
                            cell4.StyleIndex = 2u;
                            cell4.DataType = CellValues.String;
                            CellValue cellValue4 = new CellValue();
                            cellValue4.Text = item.ResultDesc;
                            cell4.Append(cellValue4);
                            row4.Append(cell4);
                            firstChild.Append(row4);
                            uInt32Value = (uint)uInt32Value + 1;
                        }
                    }
                    else if (type == "1")
                    {
                        List<MonthEndCheckResult> list2 = checkResultList as List<MonthEndCheckResult>;
                        Row row6 = new Row();
                        row6.RowIndex = 3u;
                        row6.Spans = new ListValue<StringValue>
                        {
                            InnerText = "1:5"
                        };
                        Row row4 = row6;
                        Cell cell2 = new Cell();
                        cell2.CellReference = "A3";
                        cell2.StyleIndex = 1u;
                        cell2.DataType = CellValues.String;
                        CellValue cellValue2 = new CellValue();
                        cellValue2.Text = "分类";
                        cell2.Append(cellValue2);
                        row4.Append(cell2);
                        cell2 = new Cell();
                        cell2.CellReference = "B3";
                        cell2.StyleIndex = 1u;
                        cell2.DataType = CellValues.String;
                        cellValue2 = new CellValue();
                        cellValue2.Text = "检查内容";
                        cell2.Append(cellValue2);
                        row4.Append(cell2);
                        cell2 = new Cell();
                        cell2.CellReference = "C3";
                        cell2.StyleIndex = 1u;
                        cell2.DataType = CellValues.String;
                        cellValue2 = new CellValue();
                        cellValue2.Text = "状态";
                        cell2.Append(cellValue2);
                        row4.Append(cell2);
                        cell2 = new Cell();
                        cell2.CellReference = "D3";
                        cell2.StyleIndex = 1u;
                        cell2.DataType = CellValues.String;
                        cellValue2 = new CellValue();
                        cellValue2.Text = "检查结果";
                        cell2.Append(cellValue2);
                        row4.Append(cell2);
                        cell2 = new Cell();
                        cell2.CellReference = "E3";
                        cell2.StyleIndex = 1u;
                        cell2.DataType = CellValues.String;
                        cellValue2 = new CellValue();
                        cellValue2.Text = "操作指导";
                        cell2.Append(cellValue2);
                        row4.Append(cell2);
                        firstChild.Append(row4);
                        UInt32Value uInt32Value = 4u;
                        foreach (MonthEndCheckResult item2 in list2)
                        {
                            Row row7 = new Row();
                            row7.RowIndex = uInt32Value;
                            row7.Spans = new ListValue<StringValue>
                            {
                                InnerText = "1:5"
                            };
                            row7.Height = 20.0;
                            row4 = row7;
                            cell2 = new Cell();
                            cell2.CellReference = "A" + uInt32Value.ToString();
                            cell2.StyleIndex = 2u;
                            cell2.DataType = CellValues.String;
                            cellValue2 = new CellValue();
                            cellValue2.Text = item2.MonthEnd.ModuleName;
                            cell2.Append(cellValue2);
                            row4.Append(cell2);
                            Cell cell3 = new Cell();
                            cell3.CellReference = "B" + uInt32Value.ToString();
                            cell3.StyleIndex = 2u;
                            cell3.DataType = CellValues.String;
                            CellValue cellValue3 = new CellValue();
                            cellValue3.Text = item2.MonthEnd.CheckContent;
                            cell3.Append(cellValue3);
                            row4.Append(cell3);
                            Cell cell4 = new Cell();
                            cell4.CellReference = "C" + uInt32Value.ToString();
                            cell4.StyleIndex = 2u;
                            cell4.DataType = CellValues.String;
                            CellValue cellValue4 = new CellValue();
                            cellValue4.Text = item2.StateName;
                            cell4.Append(cellValue4);
                            row4.Append(cell4);
                            Cell cell5 = new Cell();
                            cell5.CellReference = "D" + uInt32Value.ToString();
                            cell5.StyleIndex = 2u;
                            cell5.DataType = CellValues.String;
                            CellValue cellValue5 = new CellValue();
                            cellValue5.Text = item2.ResultDesc;
                            cell5.Append(cellValue5);
                            row4.Append(cell5);
                            Cell cell6 = new Cell();
                            cell6.CellReference = "E" + uInt32Value.ToString();
                            cell6.StyleIndex = 2u;
                            cell6.DataType = CellValues.String;
                            CellValue cellValue6 = new CellValue();
                            cellValue6.Text = item2.GuidDesc;
                            cell6.Append(cellValue6);
                            row4.Append(cell6);
                            firstChild.Append(row4);
                            uInt32Value = (uint)uInt32Value + 1;
                        }
                    }
                    worksheet.Save();
                    spreadsheetDocument.Close();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("输出检查结果出错，原因是：" + ex.Message);
            }
        }

        public static void ExportModuleExcel(object checkResultList, List<YonYou.U8.IN.DataCheck.Module> moduleList, string type)
        {
            try
            {
                if (type == "0")
                {
                    foreach (YonYou.U8.IN.DataCheck.Module module in moduleList)
                    {
                        string text = string.Concat(GetU8Path(), "\\ImplementNavigation\\DataCheck\\Logs\\", "对账报告" + module.ModuleName + "会计月-" + loginDate + ".xlsx");
                        CreateSpreadsheetWorkbook(text, "0");
                        using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(text, true))
                        {
                            IEnumerable<Sheet> source = from s in spreadsheetDocument.WorkbookPart.Workbook.Descendants<Sheet>()
                                                        where (uint)s.SheetId == 1
                                                        select s;
                            if (source.Count() == 0)
                            {
                                break;
                            }
                            WorksheetPart worksheetPart = (WorksheetPart)spreadsheetDocument.WorkbookPart.GetPartById(source.First().Id);
                            DocumentFormat.OpenXml.Spreadsheet.Worksheet worksheet = worksheetPart.Worksheet;
                            SheetData firstChild = worksheet.GetFirstChild<SheetData>();
                            WorkbookStylesPart workbookStylesPart = spreadsheetDocument.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                            workbookStylesPart.Stylesheet = CreateStylesheet();
                            workbookStylesPart.Stylesheet.Save();
                            Row row = new Row();
                            row.RowIndex = 2u;
                            Row row2 = row;
                            Cell cell = new Cell();
                            cell.CellReference = "A2";
                            cell.DataType = CellValues.String;
                            CellValue cellValue = new CellValue();
                            cellValue.Text = "检查月份：" + VBlogin.cIYear + "年" + VBlogin.iMonth + "月";
                            cell.Append(cellValue);
                            row2.Append(cell);
                            firstChild.Append(row2);
                            Row row3 = new Row();
                            row3.RowIndex = 3u;
                            row2 = row3;
                            cell = new Cell();
                            cell.CellReference = "A3";
                            cell.DataType = CellValues.String;
                            cellValue = new CellValue();
                            cellValue.Text = "模块：" + module.ModuleName;
                            cell.Append(cellValue);
                            row2.Append(cell);
                            cell = new Cell();
                            cell.CellReference = "B3";
                            cell.DataType = CellValues.String;
                            cellValue = new CellValue();
                            cellValue.Text = "操作员：" + login.GetLoginInfo().UserName;
                            cell.Append(cellValue);
                            row2.Append(cell);
                            cell = new Cell();
                            cell.CellReference = "C3";
                            cell.DataType = CellValues.String;
                            cellValue = new CellValue();
                            cellValue.Text = "检查日期：" + loginDate;
                            cell.Append(cellValue);
                            row2.Append(cell);
                            firstChild.Append(row2);
                            Row row4 = new Row();
                            row4.RowIndex = 5u;
                            row2 = row4;
                            cell = new Cell();
                            cell.CellReference = "A5";
                            cell.StyleIndex = 1u;
                            cell.DataType = CellValues.String;
                            cellValue = new CellValue();
                            cellValue.Text = "分类";
                            cell.Append(cellValue);
                            row2.Append(cell);
                            cell = new Cell();
                            cell.CellReference = "B5";
                            cell.StyleIndex = 1u;
                            cell.DataType = CellValues.String;
                            cellValue = new CellValue();
                            cellValue.Text = "检查内容";
                            cell.Append(cellValue);
                            row2.Append(cell);
                            cell = new Cell();
                            cell.CellReference = "C5";
                            cell.StyleIndex = 1u;
                            cell.DataType = CellValues.String;
                            cellValue = new CellValue();
                            cellValue.Text = "检查结果";
                            cell.Append(cellValue);
                            row2.Append(cell);
                            firstChild.Append(row2);
                            List<ReconcileCheckResult> list = new List<ReconcileCheckResult>();
                            UInt32Value uInt32Value = 6u;
                            List<ReconcileCheckResult> list2 = checkResultList as List<ReconcileCheckResult>;
                            foreach (ReconcileCheckResult item in list2)
                            {
                                if (item.Reconcile.ModuleId == module.ModuleId)
                                {
                                    Row row5 = new Row();
                                    row5.RowIndex = uInt32Value;
                                    row2 = row5;
                                    cell = new Cell();
                                    cell.CellReference = "A" + uInt32Value.ToString();
                                    cell.StyleIndex = 2u;
                                    cell.DataType = CellValues.String;
                                    cellValue = new CellValue();
                                    cellValue.Text = item.Reconcile.ModuleName;
                                    cell.Append(cellValue);
                                    row2.Append(cell);
                                    Cell cell2 = new Cell();
                                    cell2.CellReference = "B" + uInt32Value.ToString();
                                    cell2.StyleIndex = 2u;
                                    cell2.DataType = CellValues.String;
                                    CellValue cellValue2 = new CellValue();
                                    cellValue2.Text = item.Reconcile.CheckContent;
                                    cell2.Append(cellValue2);
                                    row2.Append(cell2);
                                    Cell cell3 = new Cell();
                                    cell3.CellReference = "C" + uInt32Value.ToString();
                                    cell3.StyleIndex = 2u;
                                    cell3.DataType = CellValues.String;
                                    CellValue cellValue3 = new CellValue();
                                    cellValue3.Text = item.ResultDesc;
                                    cell3.Append(cellValue3);
                                    row2.Append(cell3);
                                    firstChild.Append(row2);
                                    if (IsExistsData(item.Sql) == 0)
                                    {
                                        list.Add(item);
                                    }
                                    uInt32Value = (uint)uInt32Value + 1;
                                }
                            }
                            worksheet.Save();
                            spreadsheetDocument.Close();
                            foreach (ReconcileCheckResult item2 in list)
                            {
                                string templatePath = item2.Reconcile.TemplatePath;
                                if (!(templatePath == ""))
                                {
                                    string text2 = string.Concat(GetU8Path(), "\\ImplementNavigation\\DataCheck\\Logs\\", item2.Reconcile.CheckContent + ".xlsx");
                                    if (CopyFile(templatePath, text2))
                                    {
                                        FillDataToExcel(text2, item2.Sql, item2.Reconcile.CheckContent);
                                        MergeExcel(text2, text);
                                        File.Delete(text2);
                                    }
                                }
                            }
                        }
                        ReconcileCheck.importFileList.Add(text);
                        senderFile = text;
                    }
                    return;
                }
                if (!(type == "1"))
                {
                    return;
                }
                foreach (YonYou.U8.IN.DataCheck.Module module2 in moduleList)
                {
                    string text = string.Concat(GetU8Path(), "\\ImplementNavigation\\DataCheck\\Logs\\", "月末分析" + module2.ModuleName + "会计月-" + loginDate + ".xlsx");
                    CreateSpreadsheetWorkbook(text, "1");
                    using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(text, true))
                    {
                        IEnumerable<Sheet> source = from s in spreadsheetDocument.WorkbookPart.Workbook.Descendants<Sheet>()
                                                    where (uint)s.SheetId == 1
                                                    select s;
                        if (source.Count() == 0)
                        {
                            break;
                        }
                        WorksheetPart worksheetPart = (WorksheetPart)spreadsheetDocument.WorkbookPart.GetPartById(source.First().Id);
                        DocumentFormat.OpenXml.Spreadsheet.Worksheet worksheet = worksheetPart.Worksheet;
                        SheetData firstChild = worksheet.GetFirstChild<SheetData>();
                        WorkbookStylesPart workbookStylesPart = spreadsheetDocument.WorkbookPart.AddNewPart<WorkbookStylesPart>();
                        workbookStylesPart.Stylesheet = CreateStylesheet();
                        workbookStylesPart.Stylesheet.Save();
                        Row row6 = new Row();
                        row6.RowIndex = 2u;
                        Row row2 = row6;
                        Cell cell = new Cell();
                        cell.CellReference = "A2";
                        cell.DataType = CellValues.String;
                        CellValue cellValue = new CellValue();
                        cellValue.Text = "检查月份：" + VBlogin.cIYear + "年" + VBlogin.iMonth + "月";
                        cell.Append(cellValue);
                        row2.Append(cell);
                        firstChild.Append(row2);
                        Row row7 = new Row();
                        row7.RowIndex = 3u;
                        row2 = row7;
                        cell = new Cell();
                        cell.CellReference = "A3";
                        cell.DataType = CellValues.String;
                        cellValue = new CellValue();
                        cellValue.Text = "模块：" + module2.ModuleName;
                        cell.Append(cellValue);
                        row2.Append(cell);
                        cell = new Cell();
                        cell.CellReference = "B3";
                        cell.DataType = CellValues.String;
                        cellValue = new CellValue();
                        cellValue.Text = "操作员：" + login.GetLoginInfo().UserName;
                        cell.Append(cellValue);
                        row2.Append(cell);
                        cell = new Cell();
                        cell.CellReference = "C3";
                        cell.DataType = CellValues.String;
                        cellValue = new CellValue();
                        cellValue.Text = "检查日期：" + loginDate;
                        cell.Append(cellValue);
                        row2.Append(cell);
                        firstChild.Append(row2);
                        Row row8 = new Row();
                        row8.RowIndex = 5u;
                        row2 = row8;
                        cell = new Cell();
                        cell.CellReference = "A5";
                        cell.StyleIndex = 1u;
                        cell.DataType = CellValues.String;
                        cellValue = new CellValue();
                        cellValue.Text = "分类";
                        cell.Append(cellValue);
                        row2.Append(cell);
                        cell = new Cell();
                        cell.CellReference = "B5";
                        cell.StyleIndex = 1u;
                        cell.DataType = CellValues.String;
                        cellValue = new CellValue();
                        cellValue.Text = "检查内容";
                        cell.Append(cellValue);
                        row2.Append(cell);
                        cell = new Cell();
                        cell.CellReference = "C5";
                        cell.StyleIndex = 1u;
                        cell.DataType = CellValues.String;
                        cellValue = new CellValue();
                        cellValue.Text = "状态";
                        cell.Append(cellValue);
                        row2.Append(cell);
                        cell = new Cell();
                        cell.CellReference = "D5";
                        cell.StyleIndex = 1u;
                        cell.DataType = CellValues.String;
                        cellValue = new CellValue();
                        cellValue.Text = "检查结果";
                        cell.Append(cellValue);
                        row2.Append(cell);
                        cell = new Cell();
                        cell.CellReference = "E5";
                        cell.StyleIndex = 1u;
                        cell.DataType = CellValues.String;
                        cellValue = new CellValue();
                        cellValue.Text = "操作指导";
                        cell.Append(cellValue);
                        row2.Append(cell);
                        firstChild.Append(row2);
                        List<MonthEndCheckResult> list3 = new List<MonthEndCheckResult>();
                        UInt32Value uInt32Value = 6u;
                        List<MonthEndCheckResult> list4 = checkResultList as List<MonthEndCheckResult>;
                        foreach (MonthEndCheckResult item3 in list4)
                        {
                            if (item3.MonthEnd.ModuleId == module2.ModuleId)
                            {
                                Row row9 = new Row();
                                row9.RowIndex = uInt32Value;
                                row2 = row9;
                                cell = new Cell();
                                cell.CellReference = "A" + uInt32Value.ToString();
                                cell.StyleIndex = 2u;
                                cell.DataType = CellValues.String;
                                cellValue = new CellValue();
                                cellValue.Text = item3.MonthEnd.ModuleName;
                                cell.Append(cellValue);
                                row2.Append(cell);
                                Cell cell2 = new Cell();
                                cell2.CellReference = "B" + uInt32Value.ToString();
                                cell2.StyleIndex = 2u;
                                cell2.DataType = CellValues.String;
                                CellValue cellValue2 = new CellValue();
                                cellValue2.Text = item3.MonthEnd.CheckContent;
                                cell2.Append(cellValue2);
                                row2.Append(cell2);
                                Cell cell3 = new Cell();
                                cell3.CellReference = "C" + uInt32Value.ToString();
                                cell3.StyleIndex = 2u;
                                cell3.DataType = CellValues.String;
                                CellValue cellValue3 = new CellValue();
                                cellValue3.Text = item3.StateName;
                                cell3.Append(cellValue3);
                                row2.Append(cell3);
                                Cell cell4 = new Cell();
                                cell4.CellReference = "D" + uInt32Value.ToString();
                                cell4.StyleIndex = 2u;
                                cell4.DataType = CellValues.String;
                                CellValue cellValue4 = new CellValue();
                                cellValue4.Text = item3.ResultDesc;
                                cell4.Append(cellValue4);
                                row2.Append(cell4);
                                Cell cell5 = new Cell();
                                cell5.CellReference = "E" + uInt32Value.ToString();
                                cell5.StyleIndex = 2u;
                                cell5.DataType = CellValues.String;
                                CellValue cellValue5 = new CellValue();
                                cellValue5.Text = item3.GuidDesc;
                                cell5.Append(cellValue5);
                                row2.Append(cell5);
                                firstChild.Append(row2);
                                if (IsExistsData(item3.Sql) == 0)
                                {
                                    list3.Add(item3);
                                }
                                uInt32Value = (uint)uInt32Value + 1;
                            }
                        }
                        worksheet.Save();
                        spreadsheetDocument.Close();
                        foreach (MonthEndCheckResult item4 in list3)
                        {
                            string templatePath = item4.MonthEnd.TemplatePath;
                            if (!(templatePath == ""))
                            {
                                string text2 = string.Concat(GetU8Path(), "\\ImplementNavigation\\DataCheck\\Logs\\", item4.MonthEnd.CheckContent + ".xlsx");
                                if (CopyFile(templatePath, text2))
                                {
                                    FillDataToExcel(text2, item4.Sql, item4.MonthEnd.CheckContent);
                                    MergeExcel(text2, text);
                                    File.Delete(text2);
                                }
                            }
                        }
                    }
                    MonthEndCheck.importFileList.Add(text);
                    senderFile = text;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("输出检查结果出错，原因是：" + ex.Message);
            }
        }

        public static void SaveFile(string origianlFilePath, string defaultFileName)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Excel 工作薄(*.xlsx)|*.xlsx";
            saveFileDialog.DefaultExt = "xlsx";
            saveFileDialog.CreatePrompt = false;
            saveFileDialog.OverwritePrompt = true;
            saveFileDialog.FileName = defaultFileName;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string fileName = saveFileDialog.FileName;
                File.Copy(origianlFilePath, fileName, true);
                MessageBox.Show("输出成功");
            }
        }

        public static void SaveFileList(List<string> importFileList, string defaultFileName)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "";
            saveFileDialog.DefaultExt = "xlsx";
            saveFileDialog.CreatePrompt = false;
            saveFileDialog.OverwritePrompt = true;
            saveFileDialog.FileName = defaultFileName;
            if (saveFileDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            string fileName = saveFileDialog.FileName;
            for (int i = 0; i < importFileList.Count; i++)
            {
                if (i == 0)
                {
                    File.Copy(importFileList[i], fileName, true);
                }
                else
                {
                    File.Copy(importFileList[i], Path.GetDirectoryName(fileName) + "\\" + Path.GetFileName(importFileList[i]), true);
                }
                File.Delete(importFileList[i]);
            }
            MessageBox.Show("输出成功");
        }

        public static void MergeExcel(string path, string exportFile)
        {
            if (!IsExcelInstalled())
            {
                MessageBox.Show("系统检测到当前系统未安装Microsoft Office Excel 2007及以上版本，请安装后重试。");
                return;
            }
            Excel.Application application = new ApplicationClass();
            Excel.Workbook workbook = application.Workbooks.Open(path, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            Excel.Workbook workbook2 = application.Workbooks.Open(exportFile, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
            workbook.Worksheets.Copy(Type.Missing, workbook2.Worksheets[workbook2.Worksheets.Count]);
            (workbook2.Sheets[1] as Excel.Worksheet).Select(Type.Missing);
            workbook.Close(Missing.Value, Missing.Value, Missing.Value);
            workbook2.Close(true, Missing.Value, Missing.Value);
            application.Quit();
            application = null;
        }

        public static Stylesheet CreateStylesheet()
        {
            Stylesheet stylesheet = new Stylesheet();
            stylesheet.Fonts = new Fonts
            {
                Count = 2u
            };
            DocumentFormat.OpenXml.Spreadsheet.Font font = new DocumentFormat.OpenXml.Spreadsheet.Font(new FontSize
            {
                Val = 9.0
            }, new FontName
            {
                Val = "宋体"
            }, new FontFamily
            {
                Val = 2
            }, new FontScheme
            {
                Val = FontSchemeValues.Minor
            });
            stylesheet.Fonts.Append(font);
            DocumentFormat.OpenXml.Spreadsheet.Font font2 = new DocumentFormat.OpenXml.Spreadsheet.Font(new FontSize
            {
                Val = 11.0
            }, new FontName
            {
                Val = "宋体"
            }, new FontFamily
            {
                Val = 2
            }, new FontScheme
            {
                Val = FontSchemeValues.Minor
            });
            stylesheet.Fonts.Append(font2);
            stylesheet.Borders = new DocumentFormat.OpenXml.Spreadsheet.Borders
            {
                Count = 2u
            };
            DocumentFormat.OpenXml.Spreadsheet.Border border = new DocumentFormat.OpenXml.Spreadsheet.Border(new LeftBorder(), new RightBorder(), new TopBorder(), new BottomBorder(), new DiagonalBorder());
            stylesheet.Borders.Append(border);
            DocumentFormat.OpenXml.Spreadsheet.Border border2 = new DocumentFormat.OpenXml.Spreadsheet.Border(new LeftBorder(new Color
            {
                Auto = true
            })
            {
                Style = BorderStyleValues.Thin
            }, new RightBorder(new Color
            {
                Auto = true
            })
            {
                Style = BorderStyleValues.Thin
            }, new TopBorder(new Color
            {
                Auto = true
            })
            {
                Style = BorderStyleValues.Thin
            }, new BottomBorder(new Color
            {
                Auto = true
            })
            {
                Style = BorderStyleValues.Thin
            }, new DiagonalBorder());
            stylesheet.Borders.Append(border2);
            stylesheet.Fills = new Fills
            {
                Count = 3u
            };
            Fill fill = new Fill(new PatternFill
            {
                PatternType = PatternValues.None
            });
            stylesheet.Fills.Append(fill);
            Fill fill2 = new Fill();
            PatternFill patternFill = new PatternFill();
            patternFill.PatternType = PatternValues.Gray125;
            PatternFill patternFill2 = patternFill;
            fill2.Append(patternFill2);
            stylesheet.Fills.Append(fill2);
            Fill fill3 = new Fill();
            PatternFill patternFill3 = new PatternFill(new ForegroundColor
            {
                Rgb = new HexBinaryValue
                {
                    Value = "99CCFF"
                }
            });
            patternFill3.PatternType = PatternValues.Solid;
            PatternFill patternFill4 = patternFill3;
            fill3.Append(patternFill4);
            stylesheet.Fills.Append(fill3);
            stylesheet.CellFormats = new CellFormats();
            stylesheet.CellFormats.Count = 2u;
            CellFormat cellFormat = new CellFormat();
            cellFormat.Alignment = new Alignment();
            cellFormat.NumberFormatId = 0u;
            cellFormat.FontId = 0u;
            cellFormat.BorderId = 0u;
            cellFormat.FillId = 0u;
            cellFormat.ApplyAlignment = true;
            cellFormat.ApplyBorder = true;
            stylesheet.CellFormats.Append(cellFormat);
            CellFormat cellFormat2 = new CellFormat();
            cellFormat2.Alignment = new Alignment();
            cellFormat2.NumberFormatId = 0u;
            cellFormat2.FontId = 1u;
            cellFormat2.BorderId = 1u;
            cellFormat2.FillId = 2u;
            cellFormat2.ApplyAlignment = true;
            cellFormat2.ApplyBorder = true;
            stylesheet.CellFormats.Append(cellFormat2);
            CellFormat cellFormat3 = new CellFormat();
            cellFormat3.Alignment = new Alignment();
            cellFormat3.NumberFormatId = 0u;
            cellFormat3.FontId = 0u;
            cellFormat3.BorderId = 1u;
            cellFormat3.FillId = 0u;
            cellFormat3.ApplyAlignment = true;
            cellFormat3.ApplyBorder = true;
            stylesheet.CellFormats.Append(cellFormat3);
            return stylesheet;
        }

        public static void AddStylesheet(Stylesheet stylesheet, ref uint index)
        {
            uint num = stylesheet.Fonts.Count;
            DocumentFormat.OpenXml.Spreadsheet.Font font = new DocumentFormat.OpenXml.Spreadsheet.Font(new FontSize
            {
                Val = 9.0
            }, new FontName
            {
                Val = "宋体"
            }, new FontFamily
            {
                Val = 2
            }, new FontScheme
            {
                Val = FontSchemeValues.Minor
            });
            stylesheet.Fonts.Count = (uint)stylesheet.Fonts.Count + 1;
            stylesheet.Fonts.Append(font);
            uint num2 = stylesheet.Fonts.Count;
            DocumentFormat.OpenXml.Spreadsheet.Font font2 = new DocumentFormat.OpenXml.Spreadsheet.Font(new FontSize
            {
                Val = 11.0
            }, new FontName
            {
                Val = "宋体"
            }, new FontFamily
            {
                Val = 2
            }, new FontScheme
            {
                Val = FontSchemeValues.Minor
            });
            stylesheet.Fonts.Count = (uint)stylesheet.Fonts.Count + 1;
            stylesheet.Fonts.Append(font2);
            uint num3 = stylesheet.Borders.Count;
            DocumentFormat.OpenXml.Spreadsheet.Border border = new DocumentFormat.OpenXml.Spreadsheet.Border(new LeftBorder(), new RightBorder(), new TopBorder(), new BottomBorder(), new DiagonalBorder());
            stylesheet.Borders.Count = (uint)stylesheet.Borders.Count + 1;
            stylesheet.Borders.Append(border);
            uint num4 = stylesheet.Borders.Count;
            DocumentFormat.OpenXml.Spreadsheet.Border border2 = new DocumentFormat.OpenXml.Spreadsheet.Border(new LeftBorder(new Color
            {
                Auto = true
            })
            {
                Style = BorderStyleValues.Thin
            }, new RightBorder(new Color
            {
                Auto = true
            })
            {
                Style = BorderStyleValues.Thin
            }, new TopBorder(new Color
            {
                Auto = true
            })
            {
                Style = BorderStyleValues.Thin
            }, new BottomBorder(new Color
            {
                Auto = true
            })
            {
                Style = BorderStyleValues.Thin
            }, new DiagonalBorder());
            stylesheet.Borders.Count = stylesheet.Borders.Count;
            stylesheet.Borders.Append(border2);
            uint num5 = stylesheet.Fills.Count;
            Fill fill = new Fill(new PatternFill
            {
                PatternType = PatternValues.None
            });
            stylesheet.Fills.Count = (uint)stylesheet.Fills.Count + 1;
            stylesheet.Fills.Append(fill);
            uint num6 = stylesheet.Fills.Count;
            Fill fill2 = new Fill();
            PatternFill patternFill = new PatternFill();
            patternFill.PatternType = PatternValues.Gray125;
            PatternFill patternFill2 = patternFill;
            fill2.Append(patternFill2);
            stylesheet.Fills.Count = (uint)stylesheet.Fills.Count + 1;
            stylesheet.Fills.Append(fill2);
            uint num7 = stylesheet.Fills.Count;
            Fill fill3 = new Fill();
            PatternFill patternFill3 = new PatternFill(new ForegroundColor
            {
                Rgb = new HexBinaryValue
                {
                    Value = "99CCFF"
                }
            });
            patternFill3.PatternType = PatternValues.Solid;
            PatternFill patternFill4 = patternFill3;
            fill3.Append(patternFill4);
            stylesheet.Fills.Count = (uint)stylesheet.Fills.Count + 1;
            stylesheet.Fills.Append(fill3);
            CellFormat cellFormat = new CellFormat();
            cellFormat.Alignment = new Alignment();
            cellFormat.NumberFormatId = 0u;
            cellFormat.FontId = num;
            cellFormat.BorderId = num4;
            cellFormat.FillId = num5;
            cellFormat.ApplyAlignment = true;
            cellFormat.ApplyBorder = true;
            stylesheet.CellFormats.Count = (uint)stylesheet.CellFormats.Count + 1;
            stylesheet.CellFormats.Append(cellFormat);
            index = (uint)stylesheet.CellFormats.Count - 1;
        }

        public static string GetFile(string fileName)
        {
            string text = "";
            BizFileInfo bizFileInfo = BizFile.Create(context.LoginEntity, fileName);
            if (bizFileInfo != null && bizFileInfo.Exists)
            {
                text = bizFileInfo.LocalFileName;
            }
            return text = "";
        }

        public static bool CopyFile(string fileName, string copyFileName)
        {
            bool result = false;
            string text = "";
            BizFileInfo bizFileInfo = BizFile.Create(context.LoginEntity, fileName);
            if (bizFileInfo != null && bizFileInfo.Exists)
            {
                text = bizFileInfo.LocalFileName;
            }
            if (text != "")
            {
                File.Copy(text, copyFileName, true);
                result = true;
            }
            return result;
        }

        public static int IsExistsData(string tableName)
        {
            string cmdText = "use tempdb select top 1 * from tempdb..sysobjects where Id=Object_Id('" + tableName + "')  and type='U'";
            System.Data.DataTable dataTable = new System.Data.DataTable();
            using (DbContext dbContext = new DbContext(ConnString))
            {
                dataTable = dbContext.QueryDataSet(cmdText).Tables[0];
            }
            if (dataTable.Rows.Count == 0)
            {
                return 1;
            }
            string cmdText2 = "select * from tempdb.." + tableName;
            using (DbContext dbContext = new DbContext(ConnString))
            {
                dataTable = dbContext.QueryDataSet(cmdText2).Tables[0];
            }
            if (dataTable.Rows.Count == 0)
            {
                return 2;
            }
            return 0;
        }
    }
}