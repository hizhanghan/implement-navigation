﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace YonYou.U8.IN.DataAccess
{
    public class DbContext : IDisposable
    {
        private string m_ConnString;

        private SqlConnection m_Connection;

        private SqlTransaction m_Trans;

        private bool m_IsTransDependent = false;

        public bool IsTransDependent
        {
            get
            {
                return m_IsTransDependent;
            }
            set
            {
                m_IsTransDependent = value;
            }
        }

        public DbContext(string connString)
        {
            if (string.IsNullOrEmpty(connString))
            {
                throw new ArgumentNullException("connString");
            }
            m_ConnString = connString;
            m_Connection = new SqlConnection(connString);
            m_Connection.Open();
        }

        public DataSet QueryDataSet(string cmdText, params SqlParameter[] commandParameters)
        {
            try
            {
                if (m_Trans != null)
                {
                    DataSet result = SqlHelper.ExecuteDataset(m_Trans, CommandType.Text, cmdText, commandParameters);
                    if (!m_IsTransDependent)
                    {
                        m_Trans.Commit();
                    }
                    return result;
                }
                return SqlHelper.ExecuteDataset(m_Connection, CommandType.Text, cmdText, commandParameters);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[DbContext->QueryDataSet(string cmdText, params SqlParameter[] commandParameters)]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
            }
            return null;
        }

        public DataSet QueryDataSet(string cmdText)
        {
            try
            {
                if (m_Trans != null)
                {
                    DataSet result = SqlHelper.ExecuteDataset(m_Trans, CommandType.Text, cmdText);
                    if (!m_IsTransDependent)
                    {
                        m_Trans.Commit();
                    }
                    return result;
                }
                return SqlHelper.ExecuteDataset(m_Connection, CommandType.Text, cmdText);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[DbContext->QueryDataSet]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
            }
            return null;
        }

        public SqlDataReader QueryDataReader(string cmdText, params SqlParameter[] commandParameters)
        {
            try
            {
                return SqlHelper.ExecuteReader(m_Connection, CommandType.Text, cmdText, commandParameters);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[DbContext->QueryDataReader(string cmdText, params SqlParameter[] commandParameters)]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
            }
            return null;
        }

        public SqlDataReader QueryDataReader(string cmdText)
        {
            try
            {
                return SqlHelper.ExecuteReader(m_Connection, CommandType.Text, cmdText);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[DbContext->QueryDataReader]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
            }
            return null;
        }

        public int NonQuery(string cmdText, params SqlParameter[] commandParameters)
        {
            try
            {
                if (m_Trans != null)
                {
                    int result = SqlHelper.ExecuteNonQuery(m_Trans, CommandType.Text, cmdText, commandParameters);
                    if (!m_IsTransDependent)
                    {
                        m_Trans.Commit();
                    }
                    return result;
                }
                return SqlHelper.ExecuteNonQuery(m_Connection, CommandType.Text, cmdText, commandParameters);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[DbContext->NonQuery(string cmdText, params SqlParameter[] commandParameters)]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
                if (m_Trans != null)
                {
                    m_Trans.Rollback();
                }
            }
            return -1;
        }

        public int NonQuery(string cmdText)
        {
            try
            {
                if (m_Trans != null)
                {
                    int result = SqlHelper.ExecuteNonQuery(m_Trans, CommandType.Text, cmdText);
                    if (!m_IsTransDependent)
                    {
                        m_Trans.Commit();
                    }
                    return result;
                }
                return SqlHelper.ExecuteNonQuery(m_Connection, CommandType.Text, cmdText);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[DbContext->NonQuery]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
                if (m_Trans != null)
                {
                    if (m_IsTransDependent)
                    {
                        throw ex;
                    }
                    m_Trans.Rollback();
                }
            }
            return -1;
        }

        public int QueryScalar(string cmdText)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(m_Connection, CommandType.Text, cmdText);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[DbContext->NonQuery]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
            }
            return -1;
        }

        public int QueryScalar(string cmdText, params SqlParameter[] commandParameters)
        {
            try
            {
                return (int)SqlHelper.ExecuteScalar(m_Connection, CommandType.Text, cmdText, commandParameters);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[DbContext->NonQuery]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
            }
            return -1;
        }

        public DataSet QueryDataSet(string spName, params object[] parameterValues)
        {
            try
            {
                return SqlHelper.ExecuteDataset(m_Connection, spName, parameterValues);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[DbContext->QueryDataSet(string spName, params object[] parameterValues)]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
            }
            return null;
        }

        public SqlDataReader QueryDataReader(string spName, params object[] parameterValues)
        {
            try
            {
                return SqlHelper.ExecuteReader(m_Connection, spName, parameterValues);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[DbContext->QueryDataReader(string spName, params object[] parameterValues)]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
            }
            return null;
        }

        public int NonQuery(string spName, params object[] parameterValues)
        {
            try
            {
                if (m_Trans != null)
                {
                    int result = SqlHelper.ExecuteNonQuery(m_Trans, spName, parameterValues);
                    if (!m_IsTransDependent)
                    {
                        m_Trans.Commit();
                    }
                    return result;
                }
                return SqlHelper.ExecuteNonQuery(m_Connection, spName, parameterValues);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[DbContext->NonQuery(string spName, params object[] parameterValues)]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
                if (m_Trans != null)
                {
                    if (m_IsTransDependent)
                    {
                        throw ex;
                    }
                    m_Trans.Rollback();
                }
            }
            return -1;
        }

        public void BeginTransaction()
        {
            if (m_Trans == null)
            {
                m_Trans = m_Connection.BeginTransaction();
            }
        }

        public void EndTransaction()
        {
            if (m_Trans != null)
            {
                m_Trans = null;
            }
        }

        public void Commit()
        {
            if (m_Trans != null)
            {
                m_Trans.Commit();
            }
        }

        public void Rollback()
        {
            if (m_Trans != null)
            {
                m_Trans.Rollback();
            }
        }

        public void Dispose()
        {
            if (m_Connection != null && m_Connection.State != 0)
            {
                m_Connection.Close();
            }
        }
    }
}
