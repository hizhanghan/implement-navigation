﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YonYou.U8.IN.Framework;

namespace YonYou.U8.IN.Project.View
{
    public class BizProjectLoader : NetLoader
    {
        public override bool IsEmbed
        {
            get
            {
                return true;
            }
        }

        public override bool Load(BizContext context, string cMenuId, string subMenuId)
        {
            try
            {
                Debug.WriteLine("[实施导航]-[BizProjectLoader.cs->Load]-[Begin:{0}]", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss:fff"));
                //ProjectContext.Instance.LoginEntity = context.LoginEntity;
                //ProjectListControl control = new ProjectListControl();
                //ShowEmbedControl(control, cMenuId, true);
                return false;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[BizProjectLoader.cs->Load]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
                throw ex;
            }
            finally
            {
                try
                {
                    Debug.WriteLine("[实施导航]-[BizProjectLoader.cs->Load]-[End:{0}]", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss:fff"));
                }
                catch
                {
                }
            }
        }
    }
}
