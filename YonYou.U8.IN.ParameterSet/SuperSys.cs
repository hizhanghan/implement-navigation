﻿using System.Collections.Generic;

namespace YonYou.U8.IN.ParameterSet
{
    public class SuperSys
    {
        private string _super_ID;

        private string _super_Name;

        private List<SubSys> _subsyss;

        public string Super_ID
        {
            get
            {
                return _super_ID;
            }
            set
            {
                _super_ID = value;
            }
        }

        public string Super_Name
        {
            get
            {
                return _super_Name;
            }
            set
            {
                _super_Name = value;
            }
        }

        public List<SubSys> Subsyss
        {
            get
            {
                return _subsyss;
            }
            set
            {
                _subsyss = value;
            }
        }
    }
}