﻿namespace YonYou.U8.IN.ParameterSet
{
    public class SubSys
    {
        private string _sub_ID;

        private string _sub_Name;

        public string Sub_ID
        {
            get
            {
                return _sub_ID;
            }
            set
            {
                _sub_ID = value;
            }
        }

        public string Sub_Name
        {
            get
            {
                return _sub_Name;
            }
            set
            {
                _sub_Name = value;
            }
        }
    }
}