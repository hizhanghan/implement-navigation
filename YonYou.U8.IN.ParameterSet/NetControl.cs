﻿using System.ComponentModel.Design;
using System.Windows.Forms;
using YonYou.U8.IN.Framework;
using YonYou.U8.IN.Model;

namespace YonYou.U8.IN.ParameterSet
{
    internal class NetControl : INetControl
    {
        public bool ShowStatusBar
        {
            get
            {
                return false;
            }
        }

        public Control CreateControl(BizContext context)
        {
            LoginEntity loginEntity = new LoginEntity(context.LoginObject);
            DataService.ConnString = loginEntity.UserData.ConnString.Replace("PROVIDER=SQLOLEDB;", "");
            DataService.login = loginEntity.LoginObject;
            return new Designer();
        }

    }
}