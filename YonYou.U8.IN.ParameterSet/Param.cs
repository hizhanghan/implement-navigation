﻿namespace YonYou.U8.IN.ParameterSet
{
    public class Param
    {
        public string ParamId { get; set; }

        public string ParamName { get; set; }

        public string ClassCode { get; set; }

        public string ModuleId { get; set; }

        public string Order { get; set; }

        public bool CheckValue { get; set; }

        public string Groupcode { get; set; }

        public bool ManualChange { get; set; }

        public bool IsHide { get; set; }
    }
}