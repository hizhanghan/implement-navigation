﻿namespace YonYou.U8.IN.ParameterSet
{
    public class ParamRule
    {
        public string ParamId { get; set; }

        public string RuleId { get; set; }

        public string RuleName { get; set; }

        public int ValueType { get; set; }

        public string Sql { get; set; }

        public int Type { get; set; }

        public string AssemblyClass { get; set; }

        public string AssemblyPath { get; set; }

        public string EntryPoint { get; set; }

        public string MenuId { get; set; }

        public string GuideDesc { get; set; }

        public string Order { get; set; }

        public string ParamName { get; set; }

        public string ClassName { get; set; }

        public string AccId { get; set; }
    }
}