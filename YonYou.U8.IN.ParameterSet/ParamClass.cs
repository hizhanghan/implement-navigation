﻿namespace YonYou.U8.IN.ParameterSet
{
    public class ParamClass
    {
        private string _classCode;

        private string _className;

        private int _order;

        public string ClassCode
        {
            get
            {
                return _classCode;
            }
            set
            {
                _classCode = value;
            }
        }

        public string ClassName
        {
            get
            {
                return _className;
            }
            set
            {
                _className = value;
            }
        }

        public int Order
        {
            get
            {
                return _order;
            }
            set
            {
                _order = value;
            }
        }
    }

}