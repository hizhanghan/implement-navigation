﻿namespace YonYou.U8.IN.PMPPortal
{
    partial class PMPForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PMPForm));
            this.panelPMP = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // panelPMP
            // 
            this.panelPMP.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelPMP.BackgroundImage")));
            this.panelPMP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelPMP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPMP.Location = new System.Drawing.Point(0, 0);
            this.panelPMP.Name = "panelPMP";
            this.panelPMP.Size = new System.Drawing.Size(929, 586);
            this.panelPMP.TabIndex = 0;
            // 
            // PMPForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(929, 586);
            this.Controls.Add(this.panelPMP);
            this.Name = "PMPForm";
            this.Text = "PMPForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelPMP;
    }
}