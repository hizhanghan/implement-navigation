﻿using System.Windows.Forms;
using YonYou.U8.IN.Framework;

namespace YonYou.U8.IN.PMPPortal
{
    public class NetControl : INetControl
    {
        private static Control _tmCtrl;

        public bool ShowStatusBar
        {
            get
            {
                return false;
            }
        }

        public Control CreateControl(BizContext context)
        {
            if (_tmCtrl == null)
            {
                _tmCtrl = new PMPForm
                {
                    Dock = DockStyle.Fill
                };
            }
            return _tmCtrl;
        }
    }
}