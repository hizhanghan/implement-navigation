﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YonYou.U8.IN.Model
{
    public class BizMenu
    {
        public string Id { get; set; }

        public string ParentId { get; set; }

        public string AuthId { get; set; }

        public string Name { get; set; }

        public int Level { get; set; }

        public int Order { get; set; }

        public bool IsEndGrade { get; set; }

        public string Class { get; set; }

        public string Assembly { get; set; }

        public ComponentType ComponentType { get; set; }

        public string Entrypoint { get; set; }

        public string ImagePath { get; set; }

        public string Reserved { get; set; }

        public override string ToString()
        {
            return string.Format("{0}-{1}-{2}", Name, Level, Order);
        }
    }
}
