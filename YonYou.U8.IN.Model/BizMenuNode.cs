﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YonYou.U8.IN.Model
{
	public class BizMenuNode : BizNode<BizMenu>
	{
		public BizMenuNode()
		{
		}

		public BizMenuNode(BizMenu bizMenu) : base(bizMenu)
		{
		}

		public BizMenuNode(BizMenu bizMenu, BizMenuNode children) : base(bizMenu, children)
		{
		}

		public BizMenuNode(BizMenu bizMenu, BizNodeCollection<BizMenu> childs) : base(bizMenu, childs)
		{
		}

		public BizMenuNode(BizMenu bizMenu, BizMenuNode[] childs) : base(bizMenu, childs)
		{
		}

		public BizMenuNode(BizMenu bizMenu, IEnumerable<BizMenuNode> childs) : base(bizMenu, childs as IEnumerable<BizNode<BizMenu>>)
		{
		}

		public virtual bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		public virtual void Append(IEnumerable<BizMenuNode> childs)
		{
			if (childs != null)
			{
				base.Append(childs as IEnumerable<BizNode<BizMenu>>);
			}
		}

		public override BizNode<BizMenu> CloneElement(bool deep)
		{
			return this.CloneImp<BizMenuNode>(deep);
		}

		protected override void PutCache(BizNode<BizMenu> node)
		{
			if (!NodeCache.ContainsKey(base.Data.Id))
			{
                NodeCache.Add(base.Data.Id, node);
			}
		}

		public BizMenuNode QueryNode(string cMenuId)
		{
			if (NodeCache.ContainsKey(cMenuId))
			{
				BizMenuNode bizMenuNode = NodeCache[cMenuId] as BizMenuNode;
				if (bizMenuNode != null)
				{
					return ReadOnly(bizMenuNode);
				}
			}
			return null;
		}

		protected override void RemoveFromCache(BizNode<BizMenu> node)
		{
			if (NodeCache.ContainsKey(base.Data.Id))
			{
                NodeCache.Remove(base.Data.Id);
			}
		}

		public static BizMenuNode ReadOnly(BizMenuNode node)
		{
			if (node == null)
			{
				throw new ArgumentNullException("list");
			}
			return new ReadOnlyBizMenuNode(node);
		}

		private class ReadOnlyBizMenuNode : BizMenuNode
		{
			internal ReadOnlyBizMenuNode(BizMenuNode node)
			{
				this._node = node;
			}

			public override bool IsReadOnly
			{
				get
				{
					return true;
				}
			}

			public override void Append(BizNode<BizMenu> child)
			{
				throw new NotSupportedException("NotSupported_ReadOnlyBizMenuNode");
			}

			public override void Append(BizNodeCollection<BizMenu> childs)
			{
				throw new NotSupportedException("NotSupported_ReadOnlyBizMenuNode");
			}

			public override void Append(params BizNode<BizMenu>[] childs)
			{
				throw new NotSupportedException("NotSupported_ReadOnlyBizMenuNode");
			}

			public override void Append(IEnumerable<BizMenuNode> childs)
			{
				throw new NotSupportedException("NotSupported_ReadOnlyBizMenuNode");
			}

			public override void Remove()
			{
				throw new NotSupportedException("NotSupported_ReadOnlyBizMenuNode");
			}

			public override void RemoveAllChild()
			{
				throw new NotSupportedException("NotSupported_ReadOnlyBizMenuNode");
			}

			public override BizNode<BizMenu> RemoveChild(BizNode<BizMenu> oldChild)
			{
				throw new NotSupportedException("NotSupported_ReadOnlyBizMenuNode");
			}

			public override BizNode<BizMenu> CloneElement(bool deep)
			{
				throw new NotSupportedException("NotSupported_ReadOnlyBizMenuNode");
			}

			private BizMenuNode _node;
		}
	}
}
