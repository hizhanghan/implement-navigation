﻿using System.Collections;

namespace YonYou.U8.IN.Model
{
	public class BizNodeCache<T> : Hashtable where T : new()
	{
		public BizNode<T> this[string key]
		{
			get
			{
				if (this.ContainsKey(key))
				{
					return base[key] as BizNode<T>;
				}
				return null;
			}
		}
	}
}