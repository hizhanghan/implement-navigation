﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UFSoft.U8.Framework.Login.UI;
using UFSoft.U8.Framework.LoginContext;

namespace YonYou.U8.IN.Model
{
	public class LoginEntity
	{
		public static string ufconnstr = "Data Source=192.168.1.90;DataBase=UFDATA_002_2016;User ID=sa;PWD=123";
		public static string ufsysconnstr = "Data Source=192.168.1.90;DataBase=UFsystem;User ID=sa;PWD=123";
		public LoginEntity()
		{
		}

		public LoginEntity(clsLogin loginObject)
		{
			this._LoginObject = loginObject;
		}

		public string UFSysConnectionStr
		{
			get
			{
				string text = this.UserData.SecondConnString["DAC"].ToString();
				return text.Replace("PROVIDER=SQLOLEDB;", "");
			}
		}

		public UserData UserData
		{
			get
			{
				if (this._UserData == null)
				{
					this._UserData = this._LoginObject.GetLoginInfo(this.UserToken);
				}
				return this._UserData;
			}
		}

		public IDBServerInfo DBServerInfo
		{
			get
			{
				if (this._DbServerInfo == null)
				{
					this._DbServerInfo = this._LoginObject.GetDBServerInfo(this.UserData.ConnString);
				}
				return this._DbServerInfo;
			}
		}

		public string UserToken
		{
			get
			{
				return this._LoginObject.userToken;
			}
		}

		public clsLogin LoginObject
		{
			get
			{
				return this._LoginObject;
			}
		}

		public string LanguageID
		{
			get
			{
				if (this.UserData != null)
				{
					return this.UserData.LanguageID;
				}
				return "zh-CN";
			}
		}

		public bool IsAdmin
		{
			get
			{
				return this.UserData != null && this.UserData.IsAdmin;
			}
		}

		public string FileServerInfo(bool isWeb)
		{
			return this._LoginObject.GetFileServerInfo(this._LoginObject.userToken, isWeb);
		}

		private clsLogin _LoginObject;

		private UserData _UserData;

		private IDBServerInfo _DbServerInfo;
	}
}
