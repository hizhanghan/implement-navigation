﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using ImplementNavigation;
using YonYou.U8.IN.Framework;

namespace YonYou.U8.IN.Setup
{
    internal static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                AssemblyResolve asR = new AssemblyResolve();
                WorkBench.RunApplication(args);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[Program->Main]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
            }
        }

    }
}
