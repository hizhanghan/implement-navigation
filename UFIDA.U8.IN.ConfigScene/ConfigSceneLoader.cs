﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YonYou.U8.IN.Framework;

namespace UFIDA.U8.IN.ConfigScene
{
    public class ConfigSceneLoader : NetLoader
    {
        public override bool IsEmbed
        {
            get
            {
                return true;
            }
        }

        public override bool Load(BizContext context, string cMenuId, string subMenuId)
        {
            NetLoaderControl control = new NetLoaderControl();
            ShowEmbedControl(control, cMenuId, true);
            return false;
        }
    }

}
