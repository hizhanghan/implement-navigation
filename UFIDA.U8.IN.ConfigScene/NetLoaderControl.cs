﻿using System.Windows.Forms;
using YonYou.U8.IN.Framework;

namespace UFIDA.U8.IN.ConfigScene
{
    public class NetLoaderControl : INetControl
    {
        private static Control _NetControl = null;

        public bool ShowStatusBar
        {
            get
            {
                return false;
            }
        }

        public Control CreateControl(BizContext context)
        {
            if (_NetControl == null)
            {
                ConfigSceneForm configSceneForm = new ConfigSceneForm(context.LoginEntity.UFSysConnectionStr);
                configSceneForm.ShowInTaskbar = false;
                configSceneForm.TopLevel = false;
                configSceneForm.FormBorderStyle = FormBorderStyle.None;
                configSceneForm.Dock = DockStyle.Fill;
                _NetControl = configSceneForm;
            }
            return _NetControl;
        }
    }
}