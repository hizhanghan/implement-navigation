﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YonYou.U8.IN.Framework;

namespace YonYou.U8.IN.ParameterCheck
{
    public class ParameterCheckLoader : NetLoader
    {
        public override bool IsEmbed
        {
            get
            {
                return true;
            }
        }

        public override bool Load(BizContext context, string cMenuId, string subMenuId)
        {
            INetControl control = new NetControl();
            ShowEmbedControl(control, cMenuId, true);
            return true;
        }
    }

}
