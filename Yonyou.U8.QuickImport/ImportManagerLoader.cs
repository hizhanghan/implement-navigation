﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-05-27 18:22:58
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YonYou.U8.IN.Framework;
using U8Login;

namespace Yonyou.U8.QuickImport
{
    public class ImportManagerLoader : NetLoader
    {
        public override bool IsEmbed
        {
            get
            {
                return true;
            }
        }

        public override bool Load(BizContext context, string cMenuId, string subMenuId)
        {
            ConstructLogin(context);
            ImportManagerLoaderControl control = new ImportManagerLoaderControl();
            ShowEmbedControl(control, cMenuId, true);
            return false;
        }

        private void ConstructLogin(BizContext context)
        {
            clsImport.QILogin = context.LoginObject;
            if (clsImport.U8login == null)
            {
                clsLoginClass clsLoginClass = new clsLoginClass();
                if (!clsLoginClass.ConstructLogin(context.LoginObject.userToken))
                {
                    throw new ApplicationException(clsLoginClass.ShareString);
                }
                string pSubId = "QI";
                string pAccId = string.Empty;
                string pYearId = string.Empty;
                string pUserId = string.Empty;
                string pPassword = "";
                string pDate = string.Empty;
                string cSrv = string.Empty;
                string cSerial = "";
                if (!clsLoginClass.Login(ref pSubId, ref pAccId, ref pYearId, ref pUserId, ref pPassword, ref pDate, ref cSrv, ref cSerial))
                {
                    throw new ApplicationException(clsLoginClass.ShareString);
                }
                clsImport.U8login = clsLoginClass;
            }
        }
    }
}
