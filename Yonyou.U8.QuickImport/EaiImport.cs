﻿using System.Reflection;
using System.Windows.Forms;
using System.Xml;
using System;
using U8Login;

namespace Yonyou.U8.QuickImport
{
    public class EaiImport
    {
        public string ImportToDB(string xml, clsLogin u8login)
        {
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(xml);
                XmlElement documentElement = xmlDocument.DocumentElement;
                string innerText = documentElement.Attributes["roottag"].InnerText;
                string filename = Context.QIPath + "\\Xml\\Distribute.xml";
                XmlDocument xmlDocument2 = new XmlDocument();
                xmlDocument2.Load(filename);
                XmlNode xmlNode = xmlDocument2.SelectSingleNode("/U8Interface/" + innerText);
                if (xmlNode == null)
                {
                    MessageBox.Show("distribute.xml文件不包含此档案的配置信息");
                    return null;
                }
                string innerText2 = xmlNode.InnerText;
                object[] array = new object[2];
                Type typeFromProgID = Type.GetTypeFromProgID(innerText2);
                if (typeFromProgID == null)
                {
                    MessageBox.Show("系统未注册组件" + innerText2 + "，该组件和档案" + innerText + "匹配");
                    return null;
                }
                object target = typeFromProgID.InvokeMember(null, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.CreateInstance, null, null, null);
                array[0] = xml;
                array[1] = u8login;
                object obj = typeFromProgID.InvokeMember("Transact", BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.InvokeMethod, null, target, array);
                string result = "";
                if (obj != null)
                {
                    result = obj.ToString();
                }
                return result;
            }
            catch (XmlException ex)
            {
                MessageBox.Show("eai导入错误：" + ex.Message);
                return null;
            }
            catch (Exception ex2)
            {
                MessageBox.Show("eai导入错误：" + ex2.Message);
                return null;
            }
        }
    }
}