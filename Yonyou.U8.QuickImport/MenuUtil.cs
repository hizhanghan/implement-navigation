﻿using System.Data.SqlClient;
using System.Data;
using System;
using U8Login;

namespace Yonyou.U8.QuickImport
{
    public class MenuUtil
    {
        private const string DataBaseName = "UFSystem";

        private const string SQL_CMD = "(Select [cMenuId],[cSupMenuId],[cMenuName],[iGrade],[bEndGrade],[iOrder],[cComType],[cComName],[cComFileName],[cFunName],[sXMLParam],[cAuthId] from UFSystem..UA_ServiceToolMenu where {0}) order by igrade ASC,iOrder ASC,cMenuId ASC";

        public static bool HasSupAuth(clsLoginClass vbLogin, string supAuthId)
        {
            if (vbLogin == null)
            {
                throw new ArgumentNullException("vbLogin");
            }
            SqlConnection sqlConnection = null;
            try
            {
                string text = "SELECT Count(*) FROM UFSYSTEM..UA_HoldAuth (NOLOCK) WHERE cAcc_Id='" + vbLogin.get_cAcc_Id() + "' AND iYear='" + vbLogin.cBeginYear + "' AND cUser_Id IN ('" + vbLogin.cUserId + "'" + ((vbLogin.Roles.Length > 0) ? ("," + vbLogin.Roles) : "") + ") ";
                if (vbLogin.IsAdmin)
                {
                    return true;
                }
                text += string.Format("AND cAuth_Id in (select cAuth_Id from UFSYSTEM..UA_Auth where cSub_id='QI' and cSupAuth_Id = '{0}')", supAuthId);
                sqlConnection = new SqlConnection(vbLogin.UFMetaConnstringForNet);
                sqlConnection.Open();
                object obj = SqlHelper.ExecuteScalar(sqlConnection, CommandType.Text, text);
                if (obj != null)
                {
                    int num = (int)obj;
                    if (num > 0)
                    {
                        return true;
                    }
                }
            }
            catch
            {
            }
            finally
            {
                if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
                sqlConnection = null;
            }
            return false;
        }

        public static bool IsEndAuth(clsLoginClass vbLogin, string authId)
        {
            if (vbLogin == null)
            {
                throw new ArgumentNullException("vbLogin");
            }
            if (vbLogin.IsAdmin)
            {
                return false;
            }
            SqlConnection sqlConnection = null;
            SqlDataReader sqlDataReader = null;
            bool result = false;
            try
            {
                string format = "select bEndGrade from UFSystem..UA_Auth_Base where cAuth_Id='{0}'";
                string commandText = string.Format(format, authId);
                sqlConnection = new SqlConnection(vbLogin.UFMetaConnstringForNet);
                sqlConnection.Open();
                sqlConnection.ChangeDatabase("UFSystem");
                sqlDataReader = SqlHelper.ExecuteReader(sqlConnection, CommandType.Text, commandText);
                if (sqlDataReader.Read())
                {
                    return sqlDataReader.GetBoolean(0);
                }
                return result;
            }
            catch
            {
                return result;
            }
            finally
            {
                if (sqlDataReader != null && !sqlDataReader.IsClosed)
                {
                    sqlDataReader.Close();
                }
                sqlDataReader = null;
                if (sqlConnection != null && sqlConnection.State == ConnectionState.Open)
                {
                    sqlConnection.Close();
                }
                sqlConnection = null;
            }
        }
    }
}