﻿namespace Yonyou.U8.QuickImport
{
    partial class ChoseModel
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.MetroListView = new System.Windows.Forms.ListView();
            this.TopToolBarContainer = new YonYou.U8.IN.Forms.MetroPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.btnRestoreModel = new System.Windows.Forms.Button();
            this.btnOpenFile = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.TopToolBarContainer.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.MetroListView);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(640, 432);
            this.panel1.TabIndex = 1;
            // 
            // MetroListView
            // 
            this.MetroListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MetroListView.HideSelection = false;
            this.MetroListView.Location = new System.Drawing.Point(0, 0);
            this.MetroListView.Name = "MetroListView";
            this.MetroListView.Size = new System.Drawing.Size(640, 432);
            this.MetroListView.TabIndex = 0;
            this.MetroListView.UseCompatibleStateImageBehavior = false;
            // 
            // TopToolBarContainer
            // 
            this.TopToolBarContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(243)))), ((int)(((byte)(254)))));
            this.TopToolBarContainer.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(195)))), ((int)(((byte)(217)))));
            this.TopToolBarContainer.BorderDrawPosition = YonYou.U8.IN.Forms.MetroPanel.BorderPosition.Bottom;
            this.TopToolBarContainer.Controls.Add(this.flowLayoutPanel1);
            this.TopToolBarContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopToolBarContainer.Location = new System.Drawing.Point(0, 0);
            this.TopToolBarContainer.Margin = new System.Windows.Forms.Padding(0);
            this.TopToolBarContainer.Name = "TopToolBarContainer";
            this.TopToolBarContainer.Size = new System.Drawing.Size(640, 44);
            this.TopToolBarContainer.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.btnRestoreModel);
            this.flowLayoutPanel1.Controls.Add(this.btnOpenFile);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 9, 16, 9);
            this.flowLayoutPanel1.Size = new System.Drawing.Size(640, 44);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(530, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(91, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "数据录入规则";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btnRestoreModel
            // 
            this.btnRestoreModel.Location = new System.Drawing.Point(449, 12);
            this.btnRestoreModel.Name = "btnRestoreModel";
            this.btnRestoreModel.Size = new System.Drawing.Size(75, 23);
            this.btnRestoreModel.TabIndex = 1;
            this.btnRestoreModel.Text = "恢复模板";
            this.btnRestoreModel.UseVisualStyleBackColor = true;
            // 
            // btnOpenFile
            // 
            this.btnOpenFile.Location = new System.Drawing.Point(368, 12);
            this.btnOpenFile.Name = "btnOpenFile";
            this.btnOpenFile.Size = new System.Drawing.Size(75, 23);
            this.btnOpenFile.TabIndex = 2;
            this.btnOpenFile.Text = "打开文件";
            this.btnOpenFile.UseVisualStyleBackColor = true;
            // 
            // ChoseModel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.TopToolBarContainer);
            this.Name = "ChoseModel";
            this.Size = new System.Drawing.Size(640, 476);
            this.panel1.ResumeLayout(false);
            this.TopToolBarContainer.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private YonYou.U8.IN.Forms.MetroPanel TopToolBarContainer;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView MetroListView;
        private System.Windows.Forms.Button btnRestoreModel;
        private System.Windows.Forms.Button btnOpenFile;
        private System.Windows.Forms.Button button1;
    }
}
