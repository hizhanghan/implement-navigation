﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;

namespace Yonyou.U8.QuickImport
{
    public class AccountSync : ISynchronize
    {
        public void Synchronize(Context context, params object[] args)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            ModelSync(context);
            ImportStrategySync(context);
        }

        private string GetFileName(string filePath)
        {
            string[] array = filePath.Split("\\".ToArray());
            return array[array.Length - 1];
        }

        private void ModelSync(Context context)
        {
            if (string.IsNullOrEmpty(context.ExcelModelPath) || !Directory.Exists(context.ExcelModelPath))
            {
                return;
            }
            if (!Directory.Exists(context.QICurrentModelPath))
            {
                Directory.CreateDirectory(context.QICurrentModelPath);
            }
            List<string> list = CommonUtil.ModelListFilter(context);
            foreach (string item in list)
            {
                if (item.ToLower().EndsWith(".xls"))
                {
                    string text = Path.Combine(context.QICurrentModelPath, item);
                    string text2 = Path.Combine(context.ExcelModelPath, item);
                    if (File.Exists(text2) && !File.Exists(text))
                    {
                        File.Copy(text2, text);
                    }
                }
            }
        }

        private void ImportStrategySync(Context context)
        {
            if (Directory.Exists(context.QICurImportStrategy))
            {
                return;
            }
            if (!Directory.Exists(context.QICurImportStrategy))
            {
                Directory.CreateDirectory(context.QICurImportStrategy);
            }
            string[] files = Directory.GetFiles(context.QIImportStrategy);
            string[] array = files;
            foreach (string text in array)
            {
                string fileName = GetFileName(text);
                if (!IsGlobalConfig(fileName))
                {
                    string text2 = Path.Combine(context.QICurImportStrategy, fileName);
                    if (!File.Exists(text2))
                    {
                        File.Copy(text, text2);
                    }
                }
            }
            if (!Directory.Exists(context.QICurImportStrategyFilePath))
            {
                Directory.CreateDirectory(context.QICurImportStrategyFilePath);
            }
            files = Directory.GetFiles(context.QIImportStrategyFilePath);
            string[] array2 = files;
            foreach (string text3 in array2)
            {
                string fileName2 = GetFileName(text3);
                string text4 = Path.Combine(context.QICurImportStrategyFilePath, fileName2);
                if (!File.Exists(text4))
                {
                    File.Copy(text3, text4);
                }
            }
        }

        private bool IsGlobalConfig(string fileName)
        {
            switch (fileName)
            {
                case "ChildArch.xml":
                    return true;
                case "默认方案.xml":
                    return true;
                case "ImportParameter.xml":
                    return true;
                case "ImportStrategy.xml":
                    return true;
                default:
                    return false;
            }
        }
    }
}