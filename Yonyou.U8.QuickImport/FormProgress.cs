﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yonyou.U8.QuickImport
{
    public partial class FormProgress : Form
    {
        private int DocCount;
        private int NowDocNum;
        private int NowDocMax;
        private int SumStepValue;

        public FormProgress()
        {
            InitializeComponent();
        }

        public void Init(int IntDocCount)
        {
            if (IntDocCount <= 0)
            {
                DocCount = 0;
            }
            else
            {
                DocCount = IntDocCount;
            }
            proBar.Maximum = 100;
            proBar.Minimum = 0;
            SumStepValue = 0;
        }

        public void InitSingleDoc(int IntMax, int IntDocNum, string StrCaption)
        {
            if (IntMax > 0 && IntDocNum > 0 && IntDocNum <= DocCount)
            {
                NowDocMax = IntMax;
                NowDocNum = IntDocNum;
                Text = StrCaption;
            }
        }

        public void StepForward(int IntStep, double coeeff)
        {
            if (NowDocMax > 0)
            {
                double num = 1.0 * (double)NowDocNum / (double)DocCount * 100.0;
                SumStepValue += IntStep;
                double num2 = 1.0 * (double)SumStepValue / (double)NowDocMax / (double)DocCount * coeeff * 100.0;
                double num3 = (double)proBar.Maximum * (1.0 - coeeff) * 1.0 + num2;
                if (num3 <= (double)(int)num)
                {
                    proBar.Value = (int)num3;
                }
                else
                {
                    proBar.Value = (int)num;
                }
            }
        }

        public void ExSetValue(int IntValue, double coeeff)
        {
            if (NowDocNum >= 1 && NowDocMax > 0)
            {
                if (IntValue <= NowDocMax)
                {
                    double num = (1.0 * (double)(NowDocNum - 1) / (double)DocCount + 1.0 * (double)IntValue / (double)NowDocMax / (double)DocCount * coeeff) * 100.0;
                    proBar.Value = (int)num;
                }
                else
                {
                    double num = 1.0 * (double)NowDocNum / (double)DocCount * coeeff * 100.0;
                    proBar.Value = (int)num;
                }
            }
        }

        public void ClosePro()
        {
            if (!proBar.IsDisposed)
            {
                Close();
            }
        }

    }
}
