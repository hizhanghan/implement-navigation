﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YonYou.U8.IN.Framework;

namespace Yonyou.U8.QuickImport
{
    public  class ImportManagerLoaderControl : INetControl
    {
        private static Control _qiCtrl;

        public bool ShowStatusBar
        {
            get
            {
                return false;
            }
        }

        public Control CreateControl(BizContext context)
        {
            if (_qiCtrl == null)
            {
                ISynchronize synchronize = new AccountSync();
                Context context2 = new Context(clsImport.U8login);
                synchronize.Synchronize(context2);
                ImportPanel importPanel = new ImportPanel();
                importPanel.Dock = DockStyle.Fill;
                _qiCtrl = importPanel;
            }
            return _qiCtrl;
        }
    }
}
