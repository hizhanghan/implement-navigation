﻿namespace Yonyou.U8.QuickImport
{
    partial class ToolsManager
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToolsManager));
            this.label1 = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnInventoryClear = new Yonyou.U8.QuickImport.GlassButton();
            this.btnBatchClear = new Yonyou.U8.QuickImport.GlassButton();
            this.glassButton7 = new Yonyou.U8.QuickImport.GlassButton();
            this.glassButton8 = new Yonyou.U8.QuickImport.GlassButton();
            this.glassButton1 = new Yonyou.U8.QuickImport.GlassButton();
            this.glassButton2 = new Yonyou.U8.QuickImport.GlassButton();
            this.glassButton3 = new Yonyou.U8.QuickImport.GlassButton();
            this.glassButton4 = new Yonyou.U8.QuickImport.GlassButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.glassButton5 = new Yonyou.U8.QuickImport.GlassButton();
            this.glassButton6 = new Yonyou.U8.QuickImport.GlassButton();
            this.glassButton9 = new Yonyou.U8.QuickImport.GlassButton();
            this.glassButton10 = new Yonyou.U8.QuickImport.GlassButton();
            this.glassButton11 = new Yonyou.U8.QuickImport.GlassButton();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.label1.Location = new System.Drawing.Point(28, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "数据清理";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.lblTitle.Location = new System.Drawing.Point(28, 23);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(89, 12);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "数据监控与优化";
            this.lblTitle.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.label2.Location = new System.Drawing.Point(33, 401);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "加密证书管理";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.label3.Location = new System.Drawing.Point(29, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "其他";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(30, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(387, 92);
            this.panel1.TabIndex = 4;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.glassButton1);
            this.flowLayoutPanel1.Controls.Add(this.glassButton2);
            this.flowLayoutPanel1.Controls.Add(this.glassButton3);
            this.flowLayoutPanel1.Controls.Add(this.glassButton4);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(387, 92);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.flowLayoutPanel2);
            this.panel2.Location = new System.Drawing.Point(30, 46);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(387, 92);
            this.panel2.TabIndex = 5;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.btnInventoryClear);
            this.flowLayoutPanel2.Controls.Add(this.btnBatchClear);
            this.flowLayoutPanel2.Controls.Add(this.glassButton7);
            this.flowLayoutPanel2.Controls.Add(this.glassButton8);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(387, 92);
            this.flowLayoutPanel2.TabIndex = 0;
            // 
            // btnInventoryClear
            // 
            this.btnInventoryClear.AuthId = "";
            this.btnInventoryClear.BackColor = System.Drawing.Color.Transparent;
            this.btnInventoryClear.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btnInventoryClear.Icon = global::Yonyou.U8.QuickImport.Properties.Resources.qi_group2_1;
            this.btnInventoryClear.Location = new System.Drawing.Point(3, 3);
            this.btnInventoryClear.Name = "btnInventoryClear";
            this.btnInventoryClear.Size = new System.Drawing.Size(81, 81);
            this.btnInventoryClear.TabIndex = 0;
            this.btnInventoryClear.Text = "存货清理";
            // 
            // btnBatchClear
            // 
            this.btnBatchClear.AuthId = "";
            this.btnBatchClear.BackColor = System.Drawing.Color.Transparent;
            this.btnBatchClear.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.btnBatchClear.Icon = global::Yonyou.U8.QuickImport.Properties.Resources.qi_group2_2;
            this.btnBatchClear.Location = new System.Drawing.Point(90, 3);
            this.btnBatchClear.Name = "btnBatchClear";
            this.btnBatchClear.Size = new System.Drawing.Size(81, 81);
            this.btnBatchClear.TabIndex = 1;
            this.btnBatchClear.Text = "批次清理";
            // 
            // glassButton7
            // 
            this.glassButton7.AuthId = "";
            this.glassButton7.BackColor = System.Drawing.Color.Transparent;
            this.glassButton7.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.glassButton7.Icon = global::Yonyou.U8.QuickImport.Properties.Resources.qi_group2_3;
            this.glassButton7.Location = new System.Drawing.Point(177, 3);
            this.glassButton7.Name = "glassButton7";
            this.glassButton7.Size = new System.Drawing.Size(81, 81);
            this.glassButton7.TabIndex = 2;
            this.glassButton7.Text = "HR考勤数据清理";
            // 
            // glassButton8
            // 
            this.glassButton8.AuthId = "";
            this.glassButton8.BackColor = System.Drawing.Color.Transparent;
            this.glassButton8.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.glassButton8.Icon = global::Yonyou.U8.QuickImport.Properties.Resources.qi_group2_4;
            this.glassButton8.Location = new System.Drawing.Point(264, 3);
            this.glassButton8.Name = "glassButton8";
            this.glassButton8.Size = new System.Drawing.Size(81, 81);
            this.glassButton8.TabIndex = 3;
            this.glassButton8.Text = "操作员清理";
            // 
            // glassButton1
            // 
            this.glassButton1.AuthId = "";
            this.glassButton1.BackColor = System.Drawing.Color.Transparent;
            this.glassButton1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.glassButton1.Icon = global::Yonyou.U8.QuickImport.Properties.Resources.glassButton1;
            this.glassButton1.Location = new System.Drawing.Point(3, 3);
            this.glassButton1.Name = "glassButton1";
            this.glassButton1.Size = new System.Drawing.Size(81, 81);
            this.glassButton1.TabIndex = 0;
            this.glassButton1.Text = "数据库管理";
            // 
            // glassButton2
            // 
            this.glassButton2.AuthId = "";
            this.glassButton2.BackColor = System.Drawing.Color.Transparent;
            this.glassButton2.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.glassButton2.Icon = global::Yonyou.U8.QuickImport.Properties.Resources.glassButton2;
            this.glassButton2.Location = new System.Drawing.Point(90, 3);
            this.glassButton2.Name = "glassButton2";
            this.glassButton2.Size = new System.Drawing.Size(81, 81);
            this.glassButton2.TabIndex = 1;
            this.glassButton2.Text = "业务数据量查看";
            // 
            // glassButton3
            // 
            this.glassButton3.AuthId = "";
            this.glassButton3.BackColor = System.Drawing.Color.Transparent;
            this.glassButton3.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.glassButton3.Icon = global::Yonyou.U8.QuickImport.Properties.Resources.glassButton3;
            this.glassButton3.Location = new System.Drawing.Point(177, 3);
            this.glassButton3.Name = "glassButton3";
            this.glassButton3.Size = new System.Drawing.Size(81, 81);
            this.glassButton3.TabIndex = 2;
            this.glassButton3.Text = "数据库日志收缩";
            // 
            // glassButton4
            // 
            this.glassButton4.AuthId = "";
            this.glassButton4.BackColor = System.Drawing.Color.Transparent;
            this.glassButton4.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.glassButton4.Icon = global::Yonyou.U8.QuickImport.Properties.Resources.qi_group1_4;
            this.glassButton4.Location = new System.Drawing.Point(264, 3);
            this.glassButton4.Name = "glassButton4";
            this.glassButton4.Size = new System.Drawing.Size(81, 81);
            this.glassButton4.TabIndex = 3;
            this.glassButton4.Text = "数据库优化";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.flowLayoutPanel3);
            this.panel3.Location = new System.Drawing.Point(30, 420);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(566, 94);
            this.panel3.TabIndex = 6;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.glassButton11);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(566, 94);
            this.flowLayoutPanel3.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.flowLayoutPanel4);
            this.panel4.Location = new System.Drawing.Point(30, 180);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(800, 98);
            this.panel4.TabIndex = 7;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.glassButton5);
            this.flowLayoutPanel4.Controls.Add(this.glassButton6);
            this.flowLayoutPanel4.Controls.Add(this.glassButton9);
            this.flowLayoutPanel4.Controls.Add(this.glassButton10);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(800, 98);
            this.flowLayoutPanel4.TabIndex = 0;
            // 
            // glassButton5
            // 
            this.glassButton5.AuthId = "";
            this.glassButton5.BackColor = System.Drawing.Color.Transparent;
            this.glassButton5.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.glassButton5.Icon = ((System.Drawing.Bitmap)(resources.GetObject("glassButton5.Icon")));
            this.glassButton5.Location = new System.Drawing.Point(3, 3);
            this.glassButton5.Name = "glassButton5";
            this.glassButton5.Size = new System.Drawing.Size(81, 81);
            this.glassButton5.TabIndex = 0;
            this.glassButton5.Text = "glassButton5";
            // 
            // glassButton6
            // 
            this.glassButton6.AuthId = "";
            this.glassButton6.BackColor = System.Drawing.Color.Transparent;
            this.glassButton6.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.glassButton6.Icon = ((System.Drawing.Bitmap)(resources.GetObject("glassButton6.Icon")));
            this.glassButton6.Location = new System.Drawing.Point(90, 3);
            this.glassButton6.Name = "glassButton6";
            this.glassButton6.Size = new System.Drawing.Size(81, 81);
            this.glassButton6.TabIndex = 1;
            this.glassButton6.Text = "glassButton6";
            // 
            // glassButton9
            // 
            this.glassButton9.AuthId = "";
            this.glassButton9.BackColor = System.Drawing.Color.Transparent;
            this.glassButton9.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.glassButton9.Icon = ((System.Drawing.Bitmap)(resources.GetObject("glassButton9.Icon")));
            this.glassButton9.Location = new System.Drawing.Point(177, 3);
            this.glassButton9.Name = "glassButton9";
            this.glassButton9.Size = new System.Drawing.Size(81, 81);
            this.glassButton9.TabIndex = 2;
            this.glassButton9.Text = "glassButton9";
            // 
            // glassButton10
            // 
            this.glassButton10.AuthId = "";
            this.glassButton10.BackColor = System.Drawing.Color.Transparent;
            this.glassButton10.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.glassButton10.Icon = ((System.Drawing.Bitmap)(resources.GetObject("glassButton10.Icon")));
            this.glassButton10.Location = new System.Drawing.Point(264, 3);
            this.glassButton10.Name = "glassButton10";
            this.glassButton10.Size = new System.Drawing.Size(81, 81);
            this.glassButton10.TabIndex = 3;
            this.glassButton10.Text = "glassButton10";
            // 
            // glassButton11
            // 
            this.glassButton11.AuthId = "";
            this.glassButton11.BackColor = System.Drawing.Color.Transparent;
            this.glassButton11.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.glassButton11.Icon = ((System.Drawing.Bitmap)(resources.GetObject("glassButton11.Icon")));
            this.glassButton11.Location = new System.Drawing.Point(3, 3);
            this.glassButton11.Name = "glassButton11";
            this.glassButton11.Size = new System.Drawing.Size(81, 81);
            this.glassButton11.TabIndex = 0;
            this.glassButton11.Text = "glassButton11";
            // 
            // ToolsManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTitle);
            this.Name = "ToolsManager";
            this.Size = new System.Drawing.Size(820, 520);
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.flowLayoutPanel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.flowLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private GlassButton glassButton1;
        private GlassButton glassButton2;
        private GlassButton glassButton3;
        private GlassButton glassButton4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private GlassButton btnInventoryClear;
        private GlassButton btnBatchClear;
        private GlassButton glassButton7;
        private GlassButton glassButton8;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private GlassButton glassButton11;
        private GlassButton glassButton5;
        private GlassButton glassButton6;
        private GlassButton glassButton9;
        private GlassButton glassButton10;
    }
}
