﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Yonyou.U8.QuickImport
{
    public partial class DeleteData : ControlBase
    {

        private XmlDocument ModelList;
        public DeleteData(Context context) : base(context)
        {
            InitializeComponent();
            InitialModel();
            float num = (float)Screen.PrimaryScreen.Bounds.Width / 800f;
            panel2.Width = (int)((float)panel2.Width * num);
            radList.Visible = false;
            radDetail.Visible = false;
            this.Load += QueryLog_Load;
        }

        private void InitialModel()
        {
            try
            {
                ModelList = new XmlDocument();
                ModelList.Load(base.QIContext.ExcelModelPath + "\\ModelList.xml");
                CommonUtil.ModelListFilter(ModelList);
                SuspendLayout();
                for (int i = 0; i < ModelList.DocumentElement.ChildNodes.Count; i++)
                {
                    for (int j = 0; j < ModelList.DocumentElement.ChildNodes.Item(i).ChildNodes.Count; j++)
                    {
                        ArrayList arrayList = ReadDeleteServer();
                        if (arrayList.Contains(ModelList.DocumentElement.ChildNodes.Item(i).ChildNodes.Item(j).Attributes.GetNamedItem("id").Value))
                        {
                            chkListBox.Items.Add(ModelList.DocumentElement.ChildNodes.Item(i).ChildNodes.Item(j).Attributes.GetNamedItem("name").Value);
                        }
                    }
                }
                ResumeLayout(false);
            }
            catch (Exception)
            {
            }
        }

        private ArrayList ReadDeleteServer()
        {
            ArrayList arrayList = new ArrayList();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(Context.QIPath + "\\Xml\\ImportConfig.xml");
            XmlNodeList xmlNodeList = xmlDocument.SelectNodes("//ServerRegion/*[@delete='true']");
            foreach (XmlNode item in xmlNodeList)
            {
                arrayList.Add(item.Name);
            }
            return arrayList;
        }


        private void QueryLog_Load(object sender, EventArgs e)
        {
            string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Context.QIPath + "\\log\\loglist.mdb;";
            OleDbConnection oleDbConnection = new OleDbConnection(connectionString);
            oleDbConnection.Open();
            OleDbCommand selectCommand = new OleDbCommand("SELECT DISTINCT username FROM log ", oleDbConnection);
            OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter();
            oleDbDataAdapter.SelectCommand = selectCommand;
            DataSet dataSet = new DataSet();
            dataSet.DataSetName = "ufinterface";
            oleDbDataAdapter.Fill(dataSet, "LogList");
            DataTable dataTable = dataSet.Tables["LogList"];
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                cboName.Items.Insert(i, dataTable.Rows[i].ItemArray[0].ToString());
            }
            cboName.Items.Insert(dataTable.Rows.Count, "");
            cboName.SelectedIndex = dataTable.Rows.Count;
            oleDbConnection.Close();
            startDate.Value = DateTime.Parse(DateTime.Now.ToString(startDate.CustomFormat));
            endDate.Value = startDate.Value;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml("<data><time start=\"\" end=\"\"/><user name=\"\"/><log type=\"\"/></data>");
            string text = "";
            if (startDate.Value.CompareTo(endDate.Value) > 0)
            {
                MessageBox.Show("开始日期不能大于结束日期，请重新设置日期区间!");
                return;
            }
            DateTime value = startDate.Value;
            text = "where importtime>='" + startDate.Value.ToString(startDate.CustomFormat) + "'";
            if (endDate != null)
            {
                text = text + " and importtime<='" + endDate.Value.ToString(endDate.CustomFormat) + "'";
            }
            if (cboName.SelectedItem != null && cboName.SelectedItem.ToString().Trim() != "")
            {
                text = text + " and username='" + cboName.SelectedItem.ToString().Trim() + "'";
            }
            int queryType = (radList.Checked ? 1 : 2);
            if (chkListBox.CheckedItems.Count != 0)
            {
                text += " and type in(";
                for (int i = 0; i <= chkListBox.CheckedItems.Count - 1; i++)
                {
                    if (ModelList.SelectSingleNode("//menu[@name='" + chkListBox.CheckedItems[i].ToString() + "']") != null)
                    {
                        XmlNode xmlNode = ModelList.SelectSingleNode("//menu[@name='" + chkListBox.CheckedItems[i].ToString() + "']");
                        text = text + "'" + xmlNode.Attributes.GetNamedItem("id").Value + "',";
                    }
                }
                text = text.Substring(0, text.Length - 1);
                text += ") ";
            }
            ArrayList arrayList = ReadDeleteServer();
            if (arrayList != null)
            {
                string text2 = "('";
                for (int j = 0; j < arrayList.Count; j++)
                {
                    text2 = text2 + arrayList[j].ToString() + "','";
                }
                if (arrayList.Count > 0)
                {
                    text2 = text2.Substring(0, text2.Length - 2);
                    text2 += ")";
                    text = text + "  and type in " + text2;
                }
            }
            text += " and ImportType in ('add','edit') and DeleteTag = 0";
            text += " order by 编号,token,username,type";
            TabPage tapPage = null;
            DeleteList deleteList = null;
            if (!base.QIContext.CheckTab("tpDeleteLogList", out tapPage))
            {
                tapPage = new TabPage("导入删除(日志列表)");
                tapPage.Name = "tpDeleteLogList";
                deleteList = new DeleteList(base.QIContext);
                deleteList.Tag = base.Parent;
                deleteList.Dock = DockStyle.Fill;
                tapPage.Controls.Add(deleteList);
                base.QIContext.TabContainer.TabPages.Add(tapPage);
            }
            else
            {
                deleteList = tapPage.Controls[0] as DeleteList;
                if (deleteList != null)
                {
                    deleteList.SetQueryXML(text, queryType);
                    deleteList.InitialList();
                }
            }
            if (tapPage != null)
            {
                base.QIContext.TabContainer.SelectedTab = tapPage;
            }
        }
    }
}
