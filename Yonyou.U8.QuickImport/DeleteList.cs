﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using ET;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace Yonyou.U8.QuickImport
{
    public partial class DeleteList : ControlBase
    {
        private System.Data.DataTable LogList;
        private string strSQL;
        private int intQueryType;

        public DeleteList(Context context) : base(context)
        {
            InitializeComponent();
        }

        private void ListLog_Load(object sender, EventArgs e)
        {
            InitialList();
        }

        public void InitialList()
        {
            try
            {
                string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Context.QIPath + "\\log\\loglist.mdb;";
                OleDbConnection oleDbConnection = new OleDbConnection(connectionString);
                oleDbConnection.Open();
                string text = "SELECT 0 as checked,编号 as num,importtime,type,name,filename,succeeded,failed,username,token,importtype,deletetag FROM log ";
                OleDbCommand selectCommand = new OleDbCommand(text + strSQL, oleDbConnection);
                OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter();
                oleDbDataAdapter.SelectCommand = selectCommand;
                DataSet dataSet = new DataSet();
                dataSet.DataSetName = "ufinterface";
                oleDbDataAdapter.Fill(dataSet, "LogList");
                LogList = dataSet.Tables["LogList"];
                foreach (DataRow row in LogList.Rows)
                {
                    row["importtype"] = ConvertStrValue(row["importtype"].ToString());
                }
                logGrid.DataSource = LogList.DefaultView;
                oleDbConnection.Close();
                if (intQueryType == 2)
                {
                    ChoseModel choseModel = new ChoseModel();
                    choseModel.OpenFileList(LogList);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private string ConvertStrValue(string strValue)
        {
            switch (strValue)
            {
                case "True":
                    return "是";
                case "False":
                    return "否";
                case "add":
                    return "追加";
                case "edit":
                    return "覆盖";
                case "delete":
                    return "删除";
                case "empty":
                    return "清空";
                default:
                    return strValue;
            }
        }


        public void SetQueryXML(string strQuery, int QueryType)
        {
            strSQL = strQuery;
            intQueryType = QueryType;
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            TabPage tapPage;
            if (base.QIContext.CheckTab("tpDeleteLogList", out tapPage) && tapPage != null)
            {
                base.QIContext.TabContainer.TabPages.Remove(tapPage);
            }
            TabPage tabPage = base.Tag as TabPage;
            if (tabPage != null)
            {
                base.QIContext.TabContainer.SelectedTab = tabPage;
            }
        }

        private void cmdDeleteData_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(this, "是否确定执行删除操作?", "删除", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            string xml = "<?xml version='1.0'?><data><class id='' name=''/><import type='' logfilepath='' alerttype=''/></data>";
            XmlDocument xmlDocument = new XmlDocument();
            try
            {
                xmlDocument.LoadXml(xml);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            for (int i = 0; i < logGrid.Rows.Count; i++)
            {
                if (logGrid.Rows[i].Cells["ColumnCheck"].Value.ToString() == "1")
                {
                    string innerText = LogList.Rows[i]["type"].ToString();
                    string innerText2 = LogList.Rows[i]["name"].ToString();
                    string innerText3 = LogList.Rows[i]["filename"].ToString();
                    string innerText4 = LogList.Rows[i]["num"].ToString();
                    XmlNode xmlNode = xmlDocument.SelectSingleNode("//class");
                    XmlNode xmlNode2 = xmlDocument.CreateElement("", "menu", "");
                    XmlAttribute xmlAttribute = xmlDocument.CreateAttribute("id");
                    xmlAttribute.InnerText = innerText;
                    XmlAttribute xmlAttribute2 = xmlDocument.CreateAttribute("name");
                    xmlAttribute2.InnerText = innerText2;
                    XmlAttribute xmlAttribute3 = xmlDocument.CreateAttribute("filename");
                    xmlAttribute3.InnerText = innerText3;
                    XmlAttribute xmlAttribute4 = xmlDocument.CreateAttribute("number");
                    xmlAttribute4.InnerText = innerText4;
                    xmlNode2.Attributes.Append(xmlAttribute);
                    xmlNode2.Attributes.Append(xmlAttribute2);
                    xmlNode2.Attributes.Append(xmlAttribute3);
                    xmlNode2.Attributes.Append(xmlAttribute4);
                    xmlNode.AppendChild(xmlNode2);
                }
            }
            XmlDocument xmlDocument2 = new XmlDocument();
            xmlDocument2.Load(Context.QIPath + "\\ImportStrategy\\ImportParameter.xml");
            XmlNode xmlNode3 = xmlDocument.SelectSingleNode("//import");
            xmlNode3.Attributes.Item(0).Value = "3";
            xmlNode3.Attributes.Item(1).Value = xmlDocument2.DocumentElement.FirstChild.Attributes.Item(1).Value;
            xmlNode3.Attributes.Item(2).Value = xmlDocument2.DocumentElement.FirstChild.Attributes.Item(2).Value;
            base.QIContext.DeleteData(xmlDocument.OuterXml);
        }

        private void logGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex <= logGrid.Rows.Count - 1 && e.ColumnIndex >= 0 && e.ColumnIndex <= logGrid.Columns.Count - 1)
            {
                string text = LogList.Rows[e.RowIndex].ItemArray[5].ToString();
                if (string.Compare(Path.GetExtension(text), ".txt", true) == 0 || string.Compare(Path.GetExtension(text), ".csv", true) == 0)
                {
                    Process.Start("notepad", text);
                    return;
                }
                OpenLogFile openLogFile = new OpenLogFile(LogList.Rows[e.RowIndex].ItemArray[5].ToString());
                openLogFile.OpenLog(base.QIContext);
            }
        }

        private void gridContainer_ClientSizeChanged(object sender, EventArgs e)
        {
            logGrid.Height = gridContainer.ClientSize.Height;
            logGrid.Width = gridContainer.ClientSize.Width - logGrid.Location.X;
            int num = logGrid.Width - 20 - logGrid.Columns["ColumnCheck"].Width;
            int num2 = CountGridRownValidWidth();
            int num3 = 0;
            DataGridViewColumn dataGridViewColumn = null;
            foreach (DataGridViewColumn column in logGrid.Columns)
            {
                if (!column.Name.Equals("ColumnCheck") && column.Visible)
                {
                    column.Width = (int)((double)column.Width * 1.0 / (double)num2 * (double)num);
                    num3 += column.Width;
                    dataGridViewColumn = column;
                }
            }
            dataGridViewColumn.Width += num - num3;
        }

        private int CountGridRownValidWidth()
        {
            int num = 0;
            foreach (DataGridViewColumn column in logGrid.Columns)
            {
                if (!column.Name.Equals("ColumnCheck") && column.Visible)
                {
                    num += column.Width;
                }
            }
            return num;
        }

    }
}
