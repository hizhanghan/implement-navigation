﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yonyou.U8.QuickImport
{
    public partial class ControlBase : UserControl
    {
        protected Context _Context;

        public Context QIContext
        {
            get
            {
                return _Context;
            }
        }
        public ControlBase(Context context)
        {
            InitializeComponent();
            if (context == null || context.Login == null)
            {
                throw new InvalidOperationException("value");
            }
            _Context = context;
        }
        public ControlBase()
        {
            InitializeComponent();
        }
    }
}
