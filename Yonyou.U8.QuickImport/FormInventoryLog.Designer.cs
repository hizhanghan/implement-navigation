﻿namespace Yonyou.U8.QuickImport
{
    partial class FormInventoryLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgLog = new System.Windows.Forms.DataGridView();
            this.ColInvCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Log = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgLog)).BeginInit();
            this.SuspendLayout();
            // 
            // dgLog
            // 
            this.dgLog.AllowUserToAddRows = false;
            this.dgLog.AllowUserToDeleteRows = false;
            this.dgLog.BackgroundColor = System.Drawing.Color.White;
            this.dgLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColInvCode,
            this.Log});
            this.dgLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgLog.GridColor = System.Drawing.Color.Gray;
            this.dgLog.Location = new System.Drawing.Point(0, 0);
            this.dgLog.Name = "dgLog";
            this.dgLog.RowTemplate.Height = 23;
            this.dgLog.Size = new System.Drawing.Size(528, 301);
            this.dgLog.TabIndex = 0;
            // 
            // ColInvCode
            // 
            this.ColInvCode.DataPropertyName = "cInvCode";
            this.ColInvCode.HeaderText = "存货编码";
            this.ColInvCode.Name = "ColInvCode";
            this.ColInvCode.ReadOnly = true;
            // 
            // Log
            // 
            this.Log.DataPropertyName = "Log";
            this.Log.HeaderText = "描述";
            this.Log.Name = "Log";
            this.Log.ReadOnly = true;
            this.Log.Width = 400;
            // 
            // FormInventoryLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 301);
            this.Controls.Add(this.dgLog);
            this.Name = "FormInventoryLog";
            this.Text = "存货清除结果";
            ((System.ComponentModel.ISupportInitialize)(this.dgLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColInvCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Log;
    }
}