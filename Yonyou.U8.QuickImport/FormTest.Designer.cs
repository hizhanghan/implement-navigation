﻿namespace Yonyou.U8.QuickImport
{
    partial class FormTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormTest));
            this.glassButton1 = new Yonyou.U8.QuickImport.GlassButton();
            this.SuspendLayout();
            // 
            // glassButton1
            // 
            this.glassButton1.AuthId = "";
            this.glassButton1.BackColor = System.Drawing.Color.Transparent;
            this.glassButton1.Font = new System.Drawing.Font("微软雅黑", 9F);
            this.glassButton1.Icon = ((System.Drawing.Bitmap)(resources.GetObject("glassButton1.Icon")));
            this.glassButton1.Location = new System.Drawing.Point(41, 57);
            this.glassButton1.Name = "glassButton1";
            this.glassButton1.Size = new System.Drawing.Size(81, 81);
            this.glassButton1.TabIndex = 0;
            this.glassButton1.Text = "glassButton1";
            // 
            // FormTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(576, 366);
            this.Controls.Add(this.glassButton1);
            this.Name = "FormTest";
            this.Text = "FormTest";
            this.ResumeLayout(false);

        }

        #endregion

        private GlassButton glassButton1;
    }
}