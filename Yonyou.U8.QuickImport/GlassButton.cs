﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-18 00:59:06
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.AxHost;
using Yonyou.U8.QuickImport.Properties;

namespace Yonyou.U8.QuickImport
{
    public class GlassButton : Control
    {
        private const int bmp_Top = 5;
        private const int bmp_Size = 45;
        private int bmp_Left;
        private State state;
        private string _authid = string.Empty;
        private Bitmap _icon = Resources.icon;
        private string _text;

        public GlassButton()
        {
            base.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer | ControlStyles.OptimizedDoubleBuffer, true);
            base.SetStyle(ControlStyles.UserPaint, true);
            base.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            base.SetStyle(ControlStyles.Selectable, true);
            base.SetStyle(ControlStyles.ResizeRedraw, true);
            base.Size = new Size(81, 81);
            this.Font = new Font("微软雅黑", 9f);
            this.BackColor = Color.Transparent;
        }

        [Description("获取或设置权限号")]
        public string AuthId
        {
            get
            {
                return this._authid;
            }
            set
            {
                this._authid = value;
            }
        }

        [Description("获取或设置控件显示的图标")]
        public Bitmap Icon
        {
            get
            {
                return this._icon;
            }
            set
            {
                this._icon = value;
                base.Invalidate(false);
            }
        }

        [Description("与控件关联的文本。")]
        [DefaultValue(null)]
        public new string Text
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
                base.Invalidate(false);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Graphics graphics = e.Graphics;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            switch (this.state)
            {
                case State.MouseEnter:
                case State.MouseDown:
                    graphics.DrawImage(Resources.u695_normal, base.ClientRectangle);
                    break;
            }
            Rectangle bmpRegion;
            this.DrawIcon(graphics, out bmpRegion);
            this.DrawText(graphics, bmpRegion);
        }

        protected override void OnResize(EventArgs e)
        {
            base.Width = 81;
            base.Height = 81;
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            this.state = State.MouseEnter;
            base.Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            this.state = State.Normal;
            base.Invalidate();
        }

        protected override void OnClick(EventArgs e)
        {
            MouseEventArgs mouseEventArgs = (MouseEventArgs)e;
            if (mouseEventArgs.Button == MouseButtons.Left)
            {
                base.OnClick(e);
                base.Focus();
            }
        }

        private void DrawSelected(Graphics g)
        {
            g.DrawImage(Resources.down, base.ClientRectangle);
        }

        private void DrawIcon(Graphics g, out Rectangle bmpRegion)
        {
            bmpRegion = base.ClientRectangle;
            if (this._icon != null)
            {
                Bitmap bitmap = this.ScaleZoom(this._icon);
                this.bmp_Left = (base.Width - bitmap.Width) / 2;
                bmpRegion = new Rectangle(this.bmp_Left, 5, bitmap.Width, bitmap.Height);
                g.DrawImage(bitmap, bmpRegion);
            }
        }

        private void DrawText(Graphics g, Rectangle bmpRegion)
        {
            Rectangle r = new Rectangle(base.ClientRectangle.X, bmpRegion.Y + bmpRegion.Height - base.ClientRectangle.Y, base.ClientRectangle.Width, base.ClientRectangle.Height - bmpRegion.Height);
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Center;
            stringFormat.LineAlignment = StringAlignment.Center;
            g.DrawString(this._text, this.Font, new SolidBrush(this.ForeColor), r, stringFormat);
        }

        public Bitmap ScaleZoom(Bitmap bmp)
        {
            if (bmp == null || (bmp.Width <= 45 && bmp.Height <= 45))
            {
                return bmp;
            }
            double num = (double)bmp.Width / (double)bmp.Height;
            double num2 = 1.0;
            double num3;
            if (num > num2)
            {
                num3 = 45.0 / (double)bmp.Width;
                return this.BitMapZoom(bmp, 45, (int)((double)bmp.Height * num3));
            }
            num3 = 45.0 / (double)bmp.Height;
            return this.BitMapZoom(bmp, (int)((double)bmp.Width * num3), 45);
        }

        public Bitmap BitMapZoom(Bitmap bmpSource, int bmpWidth, int bmpHeight)
        {
            try
            {
                Bitmap bitmap = new Bitmap(bmpSource);
                Bitmap bitmap2 = new Bitmap(bmpWidth, bmpHeight);
                Graphics graphics = Graphics.FromImage(bitmap2);
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.DrawImage(bitmap, new Rectangle(0, 0, bmpWidth, bmpHeight), new Rectangle(0, 0, bitmap.Width, bitmap.Height), GraphicsUnit.Pixel);
                graphics.Dispose();
                return bitmap2;
            }
            catch
            {
            }
            finally
            {
                GC.Collect();
            }
            return null;
        }








    }

    public enum State
    {
        Normal,
        MouseEnter,
        MouseDown
    }
}
