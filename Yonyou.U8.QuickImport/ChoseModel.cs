﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Yonyou.U8.QuickImport
{
    public partial class ChoseModel : ControlBase
    {
        private string curFileName;
        public string strURL = "::/2.使用指南/1.1打开模版.htm";
        private ExcelBase _Excel;

        public ChoseModel()
        {
            InitializeComponent();
            Init();
        }
        
        public ChoseModel(Context context) : base(context)

        {
            InitializeComponent();
            Init();
        }

        public void Init()
        {
            this.InitialModel();
            if (!CommonUtil.CheckAuthValid(this._Context.Login, "QI0102"))
            {
                this.btnOpenFile.Visible = false;
            }
            if (!CommonUtil.CheckAuthValid(this._Context.Login, "QI0103"))
            {
                this.btnRestoreModel.Visible = false;
            }
        }

        private void InitialModel()
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(base.QIContext.ExcelModelPath + "\\ModelList.xml");
            //CommonUtil.ModelListFilter(xmlDocument);
            //ItemGroupCollection itemGroupCollection = new ItemGroupCollection();
            //foreach (XmlElement childNode in xmlDocument.DocumentElement.ChildNodes)
            //{
            //    ListItemGroup listItemGroup = new ListItemGroup();
            //    listItemGroup.Name = childNode.GetAttribute("id");
            //    listItemGroup.Text = childNode.GetAttribute("name");
            //    ListItemGroup listItemGroup2 = listItemGroup;
            //    foreach (XmlElement childNode2 in childNode.ChildNodes)
            //    {
            //        ListItem listItem = new ListItem();
            //        listItem.Name = childNode2.GetAttribute("id");
            //        listItem.Text = childNode2.GetAttribute("name");
            //        listItem.Tag = childNode2;
            //        ListItem value = listItem;
            //        listItemGroup2.ItemCollection.Add(value);
            //    }
            //    itemGroupCollection.Add(listItemGroup2);
            //}
            //MetroListView.InitListView(itemGroupCollection);
            //MetroListView.ItemClick += MetroListView_ItemClick;
        }
        public void OpenFileList(System.Data.DataTable LogList)
        {
            IList<string> list = new List<string>();
            for (int i = 0; i < LogList.Rows.Count; i++)
            {
                list.Add(LogList.Rows[i].ItemArray[4].ToString());
            }
            _Excel.OpenExcels(list);
        }


    }
}
