﻿namespace Yonyou.U8.QuickImport
{
    partial class DataImportDesign2
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.GridContainer = new System.Windows.Forms.Panel();
            this.itemGrid = new System.Windows.Forms.DataGridView();
            this.ColumnCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnItem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnPath = new System.Windows.Forms.DataGridViewLinkColumn();
            this.voucherClassContainer = new System.Windows.Forms.Panel();
            this.voucherClasses = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.toolBarContainer = new System.Windows.Forms.Panel();
            this.toolBarInternalContainer = new YonYou.U8.IN.Forms.MetroPanel();
            this.tBoxFocus = new System.Windows.Forms.TextBox();
            this.toolBarRightContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.btnRestore = new System.Windows.Forms.Button();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnOption = new System.Windows.Forms.Button();
            this.toolBarLeftContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.cBoxSolution = new System.Windows.Forms.CheckBox();
            this.rBoxSolution = new Yonyou.U8.QuickImport.MetroReferBox();
            this.btnSolutionManager = new System.Windows.Forms.Button();
            this.openFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.panel2.SuspendLayout();
            this.GridContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemGrid)).BeginInit();
            this.voucherClassContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.toolBarContainer.SuspendLayout();
            this.toolBarInternalContainer.SuspendLayout();
            this.toolBarRightContainer.SuspendLayout();
            this.toolBarLeftContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.GridContainer);
            this.panel2.Controls.Add(this.voucherClassContainer);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 44);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(784, 489);
            this.panel2.TabIndex = 1;
            // 
            // GridContainer
            // 
            this.GridContainer.BackColor = System.Drawing.Color.White;
            this.GridContainer.Controls.Add(this.itemGrid);
            this.GridContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridContainer.Location = new System.Drawing.Point(0, 44);
            this.GridContainer.Name = "GridContainer";
            this.GridContainer.Size = new System.Drawing.Size(784, 445);
            this.GridContainer.TabIndex = 3;
            this.GridContainer.ClientSizeChanged += new System.EventHandler(this.GridContainer_ClientSizeChanged);
            // 
            // itemGrid
            // 
            this.itemGrid.AllowUserToAddRows = false;
            this.itemGrid.AllowUserToDeleteRows = false;
            this.itemGrid.AllowUserToResizeColumns = false;
            this.itemGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.itemGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.itemGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(237)))), ((int)(((byte)(237)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(119)))), ((int)(((byte)(119)))), ((int)(((byte)(119)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.itemGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.itemGrid.ColumnHeadersHeight = 30;
            this.itemGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.itemGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnCheck,
            this.ColumnItem,
            this.ColumnPath});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.itemGrid.DefaultCellStyle = dataGridViewCellStyle3;
            this.itemGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.itemGrid.Location = new System.Drawing.Point(36, 0);
            this.itemGrid.Margin = new System.Windows.Forms.Padding(0);
            this.itemGrid.Name = "itemGrid";
            this.itemGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.itemGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.itemGrid.RowHeadersVisible = false;
            this.itemGrid.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.itemGrid.RowTemplate.Height = 25;
            this.itemGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.itemGrid.Size = new System.Drawing.Size(685, 134);
            this.itemGrid.TabIndex = 2;
            this.itemGrid.VirtualMode = true;
            this.itemGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.itemGrid_CellContentDoubleClick);
            // 
            // ColumnCheck
            // 
            this.ColumnCheck.DataPropertyName = "Selected";
            this.ColumnCheck.HeaderText = "";
            this.ColumnCheck.Name = "ColumnCheck";
            this.ColumnCheck.Width = 22;
            // 
            // ColumnItem
            // 
            this.ColumnItem.DataPropertyName = "Name";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.ColumnItem.DefaultCellStyle = dataGridViewCellStyle2;
            this.ColumnItem.FillWeight = 47.49843F;
            this.ColumnItem.HeaderText = "数据项";
            this.ColumnItem.Name = "ColumnItem";
            this.ColumnItem.ReadOnly = true;
            // 
            // ColumnPath
            // 
            this.ColumnPath.DataPropertyName = "Path";
            this.ColumnPath.FillWeight = 243.4729F;
            this.ColumnPath.HeaderText = "数据表路径";
            this.ColumnPath.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(107)))), ((int)(((byte)(187)))));
            this.ColumnPath.Name = "ColumnPath";
            this.ColumnPath.ReadOnly = true;
            this.ColumnPath.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ColumnPath.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.ColumnPath.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(107)))), ((int)(((byte)(187)))));
            this.ColumnPath.Width = 500;
            // 
            // voucherClassContainer
            // 
            this.voucherClassContainer.Controls.Add(this.voucherClasses);
            this.voucherClassContainer.Controls.Add(this.pictureBox1);
            this.voucherClassContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.voucherClassContainer.Location = new System.Drawing.Point(0, 0);
            this.voucherClassContainer.Name = "voucherClassContainer";
            this.voucherClassContainer.Padding = new System.Windows.Forms.Padding(34, 0, 0, 0);
            this.voucherClassContainer.Size = new System.Drawing.Size(784, 44);
            this.voucherClassContainer.TabIndex = 2;
            // 
            // voucherClasses
            // 
            this.voucherClasses.Dock = System.Windows.Forms.DockStyle.Fill;
            this.voucherClasses.Location = new System.Drawing.Point(34, 0);
            this.voucherClasses.Name = "voucherClasses";
            this.voucherClasses.Padding = new System.Windows.Forms.Padding(0, 12, 0, 11);
            this.voucherClasses.Size = new System.Drawing.Size(750, 44);
            this.voucherClasses.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Yonyou.U8.QuickImport.Properties.Resources.pictureBox1;
            this.pictureBox1.Location = new System.Drawing.Point(3, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(21, 16);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // toolBarContainer
            // 
            this.toolBarContainer.Controls.Add(this.toolBarInternalContainer);
            this.toolBarContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarContainer.Location = new System.Drawing.Point(0, 0);
            this.toolBarContainer.Name = "toolBarContainer";
            this.toolBarContainer.Padding = new System.Windows.Forms.Padding(16, 0, 16, 0);
            this.toolBarContainer.Size = new System.Drawing.Size(784, 44);
            this.toolBarContainer.TabIndex = 2;
            // 
            // toolBarInternalContainer
            // 
            this.toolBarInternalContainer.BackColor = System.Drawing.Color.White;
            this.toolBarInternalContainer.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.toolBarInternalContainer.BorderDrawPosition = YonYou.U8.IN.Forms.MetroPanel.BorderPosition.Bottom;
            this.toolBarInternalContainer.Controls.Add(this.tBoxFocus);
            this.toolBarInternalContainer.Controls.Add(this.toolBarRightContainer);
            this.toolBarInternalContainer.Controls.Add(this.toolBarLeftContainer);
            this.toolBarInternalContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolBarInternalContainer.Location = new System.Drawing.Point(16, 0);
            this.toolBarInternalContainer.Margin = new System.Windows.Forms.Padding(0);
            this.toolBarInternalContainer.Name = "toolBarInternalContainer";
            this.toolBarInternalContainer.Size = new System.Drawing.Size(752, 44);
            this.toolBarInternalContainer.TabIndex = 0;
            // 
            // tBoxFocus
            // 
            this.tBoxFocus.Location = new System.Drawing.Point(438, 10);
            this.tBoxFocus.Name = "tBoxFocus";
            this.tBoxFocus.Size = new System.Drawing.Size(0, 21);
            this.tBoxFocus.TabIndex = 2;
            // 
            // toolBarRightContainer
            // 
            this.toolBarRightContainer.Controls.Add(this.btnRestore);
            this.toolBarRightContainer.Controls.Add(this.btnImport);
            this.toolBarRightContainer.Controls.Add(this.btnOption);
            this.toolBarRightContainer.Dock = System.Windows.Forms.DockStyle.Right;
            this.toolBarRightContainer.Location = new System.Drawing.Point(500, 0);
            this.toolBarRightContainer.Name = "toolBarRightContainer";
            this.toolBarRightContainer.Padding = new System.Windows.Forms.Padding(0, 9, 0, 9);
            this.toolBarRightContainer.Size = new System.Drawing.Size(252, 44);
            this.toolBarRightContainer.TabIndex = 1;
            // 
            // btnRestore
            // 
            this.btnRestore.Location = new System.Drawing.Point(3, 12);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(96, 26);
            this.btnRestore.TabIndex = 2;
            this.btnRestore.Text = "恢复默认路径";
            this.btnRestore.UseVisualStyleBackColor = true;
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(105, 12);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(59, 26);
            this.btnImport.TabIndex = 1;
            this.btnImport.Text = "导入";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnOption
            // 
            this.btnOption.Location = new System.Drawing.Point(170, 12);
            this.btnOption.Name = "btnOption";
            this.btnOption.Size = new System.Drawing.Size(46, 26);
            this.btnOption.TabIndex = 0;
            this.btnOption.Text = "选项";
            this.btnOption.UseVisualStyleBackColor = true;
            // 
            // toolBarLeftContainer
            // 
            this.toolBarLeftContainer.Controls.Add(this.cBoxSolution);
            this.toolBarLeftContainer.Controls.Add(this.rBoxSolution);
            this.toolBarLeftContainer.Controls.Add(this.btnSolutionManager);
            this.toolBarLeftContainer.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolBarLeftContainer.Location = new System.Drawing.Point(0, 0);
            this.toolBarLeftContainer.Name = "toolBarLeftContainer";
            this.toolBarLeftContainer.Padding = new System.Windows.Forms.Padding(20, 9, 0, 9);
            this.toolBarLeftContainer.Size = new System.Drawing.Size(438, 44);
            this.toolBarLeftContainer.TabIndex = 0;
            // 
            // cBoxSolution
            // 
            this.cBoxSolution.AutoSize = true;
            this.cBoxSolution.Location = new System.Drawing.Point(20, 12);
            this.cBoxSolution.Margin = new System.Windows.Forms.Padding(0, 3, 4, 0);
            this.cBoxSolution.Name = "cBoxSolution";
            this.cBoxSolution.Size = new System.Drawing.Size(84, 16);
            this.cBoxSolution.TabIndex = 0;
            this.cBoxSolution.Text = "按方案导入";
            // 
            // rBoxSolution
            // 
            this.rBoxSolution.BackColor = System.Drawing.Color.Transparent;
            this.rBoxSolution.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(170)))), ((int)(((byte)(203)))));
            this.rBoxSolution.Location = new System.Drawing.Point(108, 9);
            this.rBoxSolution.Margin = new System.Windows.Forms.Padding(0, 0, 6, 0);
            this.rBoxSolution.Name = "rBoxSolution";
            this.rBoxSolution.ReadOnly = true;
            this.rBoxSolution.Size = new System.Drawing.Size(181, 26);
            this.rBoxSolution.TabIndex = 1;
            // 
            // btnSolutionManager
            // 
            this.btnSolutionManager.Location = new System.Drawing.Point(295, 9);
            this.btnSolutionManager.Margin = new System.Windows.Forms.Padding(0);
            this.btnSolutionManager.Name = "btnSolutionManager";
            this.btnSolutionManager.Size = new System.Drawing.Size(70, 26);
            this.btnSolutionManager.TabIndex = 2;
            this.btnSolutionManager.Text = "方案管理";
            this.btnSolutionManager.UseVisualStyleBackColor = true;
            // 
            // openFileDlg
            // 
            this.openFileDlg.FileName = "openFileDialog1";
            // 
            // DataImportDesign2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.toolBarContainer);
            this.Name = "DataImportDesign2";
            this.Size = new System.Drawing.Size(784, 533);
            this.panel2.ResumeLayout(false);
            this.GridContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.itemGrid)).EndInit();
            this.voucherClassContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.toolBarContainer.ResumeLayout(false);
            this.toolBarInternalContainer.ResumeLayout(false);
            this.toolBarInternalContainer.PerformLayout();
            this.toolBarRightContainer.ResumeLayout(false);
            this.toolBarLeftContainer.ResumeLayout(false);
            this.toolBarLeftContainer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel toolBarContainer;
        private YonYou.U8.IN.Forms.MetroPanel toolBarInternalContainer;
        private System.Windows.Forms.TextBox tBoxFocus;
        private System.Windows.Forms.FlowLayoutPanel toolBarRightContainer;
        private System.Windows.Forms.Button btnRestore;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Button btnOption;
        private System.Windows.Forms.FlowLayoutPanel toolBarLeftContainer;
        private System.Windows.Forms.CheckBox cBoxSolution;
        private MetroReferBox rBoxSolution;
        private System.Windows.Forms.Panel GridContainer;
        private System.Windows.Forms.DataGridView itemGrid;
        private System.Windows.Forms.Panel voucherClassContainer;
        private System.Windows.Forms.FlowLayoutPanel voucherClasses;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnSolutionManager;
        private System.Windows.Forms.OpenFileDialog openFileDlg;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnItem;
        private System.Windows.Forms.DataGridViewLinkColumn ColumnPath;
    }
}
