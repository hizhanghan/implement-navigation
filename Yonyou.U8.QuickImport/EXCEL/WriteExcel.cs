﻿using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Xml;
using System;
using Excel;
using ET;

namespace Yonyou.U8.QuickImport
{
    public class WriteExcel
    {
        public void Write(string fullPath, string fileName, string[,] strVal, int EndPos, string[] rxmlstr, string roottype, string proctype)
        {
            switch (ExcelManager.GetExcelType())
            {
                case ExcelType.OfficeExcel:
                    WriteByOfficeExcel(fullPath, fileName, strVal, EndPos, rxmlstr, roottype, proctype);
                    break;
                case ExcelType.WPSExcel:
                    WriteByWpsExcel(fullPath, fileName, strVal, EndPos, rxmlstr, roottype, proctype);
                    break;
                default:
                    MessageHelper.ShowMessage("日志写失败，请检查是否正确安装了OFFICE Excel 或者 WPS Excel!");
                    break;
            }
        }

        private void WriteByOfficeExcel(string fullPath, string fileName, string[,] strVal, int EndPos, string[] rxmlstr, string roottype, string proctype)
        {
            try
            {
                string[,] array = new string[strVal.GetLength(0), strVal.GetLength(1)];
                for (int i = 0; i < strVal.GetLength(0); i++)
                {
                    for (int j = 0; j < strVal.GetLength(1); j++)
                    {
                        int num = strVal[i, j].Length;
                        if (num > 900)
                        {
                            num = 900;
                        }
                        array[i, j] = strVal[i, j].Substring(0, num);
                    }
                }
                Excel.Application application = new Excel.ApplicationClass();
                object value = Missing.Value;
                application.Application.Workbooks.Open(fullPath, value, value, value, value, value, value, value, value, value, value, value, value);
                Workbook workbook = application.Workbooks[1];
                workbook.Keywords = "Ufida.Eai.QuickImport.LogFile";
                Excel.Worksheet worksheet = (Excel.Worksheet)workbook.Sheets["sheet1"];
                worksheet.Protect("ufsoft", false, false, false, false);
                Excel.Range range = ((Excel._Worksheet)worksheet).get_Range(worksheet.Cells[1, 1], worksheet.Cells[EndPos, 1]);
                Excel.Range entireColumn = range.EntireColumn;
                if (proctype != "3")
                {
                    entireColumn.Insert(Excel.XlInsertShiftDirection.xlShiftToRight);
                    entireColumn.Hidden = false;
                }
                Excel.Range range2 = ((Excel._Worksheet)worksheet).get_Range(worksheet.Cells[1, 1], worksheet.Cells[EndPos, 1]);
                range2.Value = array;
                if (rxmlstr != null && rxmlstr.Length > 0)
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    string filename = Context.QIPath + "\\Xml\\KeywordsConfig.xml";
                    try
                    {
                        xmlDocument.Load(filename);
                        XmlNode xmlNode = xmlDocument.SelectSingleNode("/U8QuickImport/keywords/" + roottype);
                        string text;
                        string text2;
                        if (xmlNode != null)
                        {
                            text = xmlNode.Attributes["key"].Value;
                            text2 = xmlNode.Attributes["checked"].Value;
                        }
                        else
                        {
                            text = "";
                            text2 = "";
                        }
                        if (text2 == "true")
                        {
                            int num2 = 1;
                            int num3 = 1;
                            Hashtable hashTable = GetHashTable(rxmlstr);
                            Excel.Range range3 = ((Excel._Worksheet)worksheet).get_Range(worksheet.Cells[num2, num3], worksheet.Cells[num2, num3]);
                            while (range3.Text.ToString().Trim().Length > 0 && !(range3.Text.ToString().Trim() == text))
                            {
                                num3++;
                                range3 = ((Excel._Worksheet)worksheet).get_Range(worksheet.Cells[num2, num3], worksheet.Cells[num2, num3]);
                            }
                            if (hashTable != null && range3.Text.ToString().Trim().Length > 0)
                            {
                                for (num2 = 2; num2 <= worksheet.UsedRange.Rows.Count; num2++)
                                {
                                    range3 = ((Excel._Worksheet)worksheet).get_Range(worksheet.Cells[num2, num3], worksheet.Cells[num2, num3]);
                                    if (hashTable.ContainsKey(range3.Text.ToString().Trim()))
                                    {
                                        worksheet.Cells[num2, 1] = hashTable[range3.Text.ToString().Trim()].ToString().Trim();
                                    }
                                }
                            }
                        }
                    }
                    catch (XmlException)
                    {
                        MessageBox.Show("KeywordsConfig.xml文件格式错误");
                    }
                }
                application.ActiveWorkbook.Save();
                application.ActiveWorkbook.Close(false, fileName, Type.Missing);
                application.Quit();
            }
            catch (Exception ex2)
            {
                MessageBox.Show(ex2.Message);
            }
        }

        private void WriteByWpsExcel(string fullPath, string fileName, string[,] strVal, int EndPos, string[] rxmlstr, string roottype, string proctype)
        {
            try
            {
                string[,] array = new string[strVal.GetLength(0), strVal.GetLength(1)];
                for (int i = 0; i < strVal.GetLength(0); i++)
                {
                    for (int j = 0; j < strVal.GetLength(1); j++)
                    {
                        int num = strVal[i, j].Length;
                        if (num > 900)
                        {
                            num = 900;
                        }
                        array[i, j] = strVal[i, j].Substring(0, num);
                    }
                }
                ET.Application application = new ET.ApplicationClass();
                object value = Missing.Value;
                application.Application.Workbooks.Open(fullPath, value, value, value, value, value, value, value, value, value, value, value, value);
                ET._Workbook workbook = application.Workbooks[1];
                ET.Worksheet worksheet = (ET.Worksheet)workbook.Sheets["sheet1"];
                worksheet.Unprotect("ufsoft");
                ET.Range range = ((ET._Worksheet)worksheet).get_Range(worksheet.Cells[1, 1], worksheet.Cells[EndPos, 1]);
                ET.Range entireColumn = range.EntireColumn;
                if (proctype != "3")
                {
                    entireColumn.Insert(ET.XlInsertShiftDirection.xlShiftToRight, Missing.Value);
                    entireColumn.Hidden = false;
                }
                ET.Range range2 = ((ET._Worksheet)worksheet).get_Range(worksheet.Cells[1, 1], worksheet.Cells[EndPos, 1]);
                range2.set_Value(ETRangeValueDataType.etRangeValueDefault, (object)array);
                if (rxmlstr != null && rxmlstr.Length > 0)
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    string filename = Context.QIPath + "\\Xml\\KeywordsConfig.xml";
                    try
                    {
                        xmlDocument.Load(filename);
                        XmlNode xmlNode = xmlDocument.SelectSingleNode("/U8QuickImport/keywords/" + roottype);
                        string text;
                        string text2;
                        if (xmlNode != null)
                        {
                            text = xmlNode.Attributes["key"].Value;
                            text2 = xmlNode.Attributes["checked"].Value;
                        }
                        else
                        {
                            text = "";
                            text2 = "";
                        }
                        if (text2 == "true")
                        {
                            int num2 = 1;
                            int num3 = 1;
                            Hashtable hashTable = GetHashTable(rxmlstr);
                            ET.Range range3 = ((ET._Worksheet)worksheet).get_Range(worksheet.Cells[num2, num3], worksheet.Cells[num2, num3]);
                            while (range3.Text.ToString().Trim().Length > 0 && !(range3.Text.ToString().Trim() == text))
                            {
                                num3++;
                                range3 = ((ET._Worksheet)worksheet).get_Range(worksheet.Cells[num2, num3], worksheet.Cells[num2, num3]);
                            }
                            if (hashTable != null && range3.Text.ToString().Trim().Length > 0)
                            {
                                for (num2 = 2; num2 <= worksheet.UsedRange.Rows.Count; num2++)
                                {
                                    range3 = ((ET._Worksheet)worksheet).get_Range(worksheet.Cells[num2, num3], worksheet.Cells[num2, num3]);
                                    if (hashTable.ContainsKey(range3.Text.ToString().Trim()))
                                    {
                                        worksheet.Cells[num2, 1] = hashTable[range3.Text.ToString().Trim()].ToString().Trim();
                                    }
                                }
                            }
                        }
                        worksheet.Protect("ufsoft", false, false, false, false, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                    }
                    catch (XmlException)
                    {
                        MessageBox.Show("KeywordsConfig.xml文件格式错误");
                    }
                }
                application.ActiveWorkbook.Save();
                application.ActiveWorkbook.Close(false, fileName, Type.Missing);
                application.Quit();
                Marshal.ReleaseComObject(application);
            }
            catch (Exception ex2)
            {
                MessageBox.Show(ex2.Message);
            }
        }

        private Hashtable GetHashTable(string[] rxmlstr)
        {
            ArrayList arrayList = new ArrayList();
            XmlDocument xmlDocument = new XmlDocument();
            Hashtable hashtable = new Hashtable();
            for (int i = 0; i < rxmlstr.Length; i++)
            {
                xmlDocument.LoadXml(rxmlstr[i]);
                XmlNodeList xmlNodeList = xmlDocument.SelectNodes("/ufinterface/item");
                if (xmlNodeList == null || xmlNodeList.Count == 0)
                {
                    continue;
                }
                foreach (XmlNode item in xmlNodeList)
                {
                    XmlNode xmlNode2 = item.Attributes["dsc"];
                    string value = ((xmlNode2 == null) ? "" : xmlNode2.Value);
                    XmlNode xmlNode3 = item.Attributes["key"];
                    if (xmlNode3 == null)
                    {
                        continue;
                    }
                    string value2 = xmlNode3.Value;
                    if (value2 != "")
                    {
                        if (!hashtable.ContainsKey(value2))
                        {
                            hashtable.Add(value2, value);
                        }
                        else if (!arrayList.Contains(value2))
                        {
                            arrayList.Add(value2);
                        }
                    }
                }
            }
            foreach (string item2 in arrayList)
            {
                if (hashtable.Contains(item2))
                {
                    hashtable.Remove(item2);
                }
            }
            return hashtable;
        }

        private Hashtable GetAudiHashtable(string[] rxmlstr)
        {
            return null;
        }
    }

}