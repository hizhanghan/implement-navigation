﻿using Microsoft.Win32;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using System;
using ET;

namespace Yonyou.U8.QuickImport
{
    internal class WpsExcel : ExcelBase, IDisposable
    {
        private bool _IsDisposed;

        public override bool IsExcelInstalled()
        {
            RegistryKey registryKey = Registry.ClassesRoot.OpenSubKey("CLSID\\{00020906-0000-4b30-A977-D214852036FE}");
            if (registryKey == null)
            {
                return false;
            }
            return true;
        }

        public override void OpenExcel(string fullName)
        {
            if (!IsExcelInstalled())
            {
                MessageHelper.ShowMessage("请正确安装WPS！");
                return;
            }
            try
            {
                OnBeforeOpen(new ExcelEventArgs
                {
                    FileName = fullName
                });
                if (base.XlsApplacation == null)
                {
                    base.XlsApplacation = new ApplicationClass();
                }
                Application application = base.XlsApplacation as Application;
                application.Visible = true;
                object value = Missing.Value;
                application.Workbooks.Open(fullName, value, value, value, value, value, value, value, value, value, value, value, value);
                OnBeforeOpen(new ExcelEventArgs
                {
                    FileName = fullName
                });
            }
            catch
            {
                MessageHelper.ShowMessage("Excel打开过程中出现错误，请检查该文件是否被其他进程占用！");
            }
        }

        public override void OpenExcelByOpenDialog(string fullName)
        {
            if (!IsExcelInstalled())
            {
                MessageHelper.ShowMessage("请正确安装WPS EXCEL！");
                return;
            }
            Application application = null;
            try
            {
                application = new ApplicationClass();
                application.Visible = true;
                object value = Missing.Value;
                application.Workbooks.Open(fullName, value, value, value, value, value, value, value, value, value, value, value, value);
            }
            catch
            {
                application.Quit();
                Marshal.ReleaseComObject(application);
                MessageHelper.ShowMessage(string.Format("打开{0}的过程中出现错误，请检查该文件是否被其他进程占用！", GetFileName(fullName)));
            }
        }

        public override void OpenExcels(IList<string> names)
        {
            try
            {
                int num = 0;
                Application application = base.XlsApplacation as Application;
                int i = 0;
                for (int count = names.Count; i < count; i++)
                {
                    if (i == 0)
                    {
                        num = application.Workbooks.Count;
                        application.Visible = true;
                        object value = Missing.Value;
                        application.Workbooks.Open(names[i], value, value, value, value, value, value, value, value, value, value, value, value);
                    }
                    else
                    {
                        application.Application.Workbooks.Add(names[i]);
                        Worksheet worksheet = (Worksheet)application.Workbooks[num + 2].Sheets[1];
                        worksheet.Copy(Type.Missing, application.Workbooks[num + 1].Sheets[i]);
                        application.Application.Workbooks[num + 2].Close(false, "", 1);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageHelper.ShowMessage(ex.Message);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!_IsDisposed)
            {
                if (base.XlsApplacation != null)
                {
                    Application application = base.XlsApplacation as Application;
                    application.Quit();
                    Marshal.ReleaseComObject(application);
                    application = null;
                }
                _IsDisposed = true;
            }
        }

        ~WpsExcel()
        {
            Dispose(false);
        }
    }
}