﻿using Microsoft.Win32;

namespace Yonyou.U8.QuickImport
{
    internal class ExcelManager
    {
        private ExcelType _Type;

        public static ExcelBase CreatExcel()
        {
            ExcelBase result = null;
            switch (GetExcelType())
            {
                case ExcelType.OfficeExcel:
                    result = new OfficeExcel();
                    break;
                case ExcelType.WPSExcel:
                    result = new WpsExcel();
                    break;
                default:
                    MessageHelper.ShowMessage("错误提示", "请确认是否正确的安装了Microsoft Excel 或 WPS！");
                    break;
            }
            return result;
        }

        public static ExcelType GetExcelType()
        {
            RegistryKey registryKey = null;
            registryKey = Registry.ClassesRoot.OpenSubKey("CLSID\\{00024500-0000-0000-C000-000000000046}");
            if (registryKey != null)
            {
                return ExcelType.OfficeExcel;
            }
            if (registryKey == null)
            {
                registryKey = Registry.ClassesRoot.OpenSubKey("CLSID\\{00020906-0000-4b30-A977-D214852036FE}");
            }
            if (registryKey != null)
            {
                return ExcelType.WPSExcel;
            }
            return ExcelType.Nothing;
        }
    }
}