﻿using System.Collections.Generic;
using System;

namespace Yonyou.U8.QuickImport
{
    internal delegate void ExcelEventHandle(object sender, ExcelEventArgs e);
    internal class ExcelEventArgs : EventArgs
    {
        public string FileName { get; set; }
    }

    internal abstract class ExcelBase : IDisposable
    {
        private object _XlsApplacation;

        private bool _IsDisposed;

        protected object XlsApplacation
        {
            get
            {
                return _XlsApplacation;
            }
            set
            {
                _XlsApplacation = value;
            }
        }

        private event ExcelEventHandle _BeforeOpenEventHandler;

        private event ExcelEventHandle _AfterOpenEventHandler;

        public event ExcelEventHandle BeforeOpen
        {
            add
            {
                _BeforeOpenEventHandler += value;
            }
            remove
            {
                _BeforeOpenEventHandler -= value;
            }
        }

        public event ExcelEventHandle AfterOpen
        {
            add
            {
                _AfterOpenEventHandler += value;
            }
            remove
            {
                _AfterOpenEventHandler -= value;
            }
        }

        protected string GetFileName(string fullName)
        {
            string[] array = fullName.Split('\\');
            return array[array.Length - 1];
        }

        protected virtual void OnBeforeOpen(ExcelEventArgs e)
        {
            if (this._BeforeOpenEventHandler != null)
            {
                this._BeforeOpenEventHandler(this, e);
            }
        }

        protected virtual void OnAfterOpen(ExcelEventArgs e)
        {
            if (this._AfterOpenEventHandler != null)
            {
                this._AfterOpenEventHandler(this, e);
            }
        }

        public abstract bool IsExcelInstalled();

        public abstract void OpenExcel(string fullName);

        public abstract void OpenExcelByOpenDialog(string fullName);

        public abstract void OpenExcels(IList<string> names);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_IsDisposed)
            {
                _IsDisposed = true;
            }
        }
    }

}