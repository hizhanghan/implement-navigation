﻿using Microsoft.Win32;
using System.Collections.Generic;
using System.Reflection;
using System;
using Excel;

namespace Yonyou.U8.QuickImport
{
    internal class OfficeExcel : ExcelBase, IDisposable
    {
        private bool _IsDisposed;

        public override bool IsExcelInstalled()
        {
            RegistryKey registryKey = Registry.ClassesRoot.OpenSubKey("CLSID\\{00024500-0000-0000-C000-000000000046}");
            if (registryKey == null)
            {
                return false;
            }
            return true;
        }

        public override void OpenExcel(string fullName)
        {
            if (!IsExcelInstalled())
            {
                MessageHelper.ShowMessage("请正确安装OFFICE！");
                return;
            }
            try
            {
                OnBeforeOpen(new ExcelEventArgs
                {
                    FileName = fullName
                });
                Application application = new ApplicationClass();
                application.Visible = true;
                object value = Missing.Value;
                application.Workbooks.Open(fullName, value, value, value, value, value, value, value, value, value, value, value, value);
                OnAfterOpen(new ExcelEventArgs
                {
                    FileName = fullName
                });
            }
            catch
            {
                MessageHelper.ShowMessage("Excel打开过程中出现错误，请检查该文件是否被其他进程占用！");
            }
        }

        public override void OpenExcelByOpenDialog(string fullName)
        {
            if (!IsExcelInstalled())
            {
                MessageHelper.ShowMessage("请正确安装OFFICE EXCEL！");
                return;
            }
            try
            {
                Application application = new ApplicationClass();
                application.Visible = true;
                object value = Missing.Value;
                application.Workbooks.Open(fullName, value, value, value, value, value, value, value, value, value, value, value, value);
            }
            catch
            {
                MessageHelper.ShowMessage(string.Format("打开{0}的过程中出现错误，请检查该文件是否被其他进程占用！", GetFileName(fullName)));
            }
        }

        public override void OpenExcels(IList<string> names)
        {
            try
            {
                int num = 0;
                Application application = new ApplicationClass();
                int i = 0;
                for (int count = names.Count; i < count; i++)
                {
                    if (i == 0)
                    {
                        num = application.Workbooks.Count;
                        application.Visible = true;
                        object value = Missing.Value;
                        application.Workbooks.Open(names[i], value, value, value, value, value, value, value, value, value, value, value, value);
                    }
                    else
                    {
                        application.Application.Workbooks.Add(names[i]);
                        Worksheet worksheet = (Worksheet)application.Workbooks[num + 2].Sheets[1];
                        worksheet.Copy(Type.Missing, application.Workbooks[num + 1].Sheets[i]);
                        application.Application.Workbooks[num + 2].Close(false, "", 1);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageHelper.ShowMessage(ex.Message);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!_IsDisposed)
            {
                _IsDisposed = true;
            }
        }

        ~OfficeExcel()
        {
            Dispose(false);
        }
    }
}