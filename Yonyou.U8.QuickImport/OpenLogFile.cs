﻿using System.Data.OleDb;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;
using System;

namespace Yonyou.U8.QuickImport
{
    public class OpenLogFile
    {
        private string m_LogFileName;

        public OpenLogFile(string strFileName)
        {
            m_LogFileName = strFileName;
        }

        public void OpenLog(Context context)
        {
            OpenExcelFile(context);
        }

        private void OpenExcelFile(Context context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            try
            {
                string text = "";
                string text2 = ReadType(m_LogFileName);
                XmlDocument xmlDocument = CommonUtil.ReadStrategy(false, context);
                if (xmlDocument == null)
                {
                    XmlDocument xmlDocument2 = new XmlDocument();
                    xmlDocument2.Load(context.ExcelModelPath + "\\ModelList.xml");
                    CommonUtil.ModelListFilter(xmlDocument2);
                    XmlNode xmlNode = xmlDocument2.SelectSingleNode("//Menu[@id='" + text2 + "']");
                    text = context.QICurrentModelPath + "\\" + xmlNode.Attributes["filename"].ToString();
                }
                else
                {
                    string proctype;
                    string logpath;
                    string altertype;
                    XmlNodeList nodelist;
                    DataImport.parseXml(xmlDocument.OuterXml, out proctype, out logpath, out altertype, out nodelist);
                    foreach (XmlNode item in nodelist)
                    {
                        string innerText = item.Attributes["id"].InnerText;
                        if (string.Compare(innerText, text2, true) == 0)
                        {
                            text = item.Attributes["filename"].InnerText;
                            break;
                        }
                    }
                }
                object[] array = new object[2];
                string text3 = "ExcelLog.OpenExcel";
                Type typeFromProgID = Type.GetTypeFromProgID(text3);
                if (typeFromProgID == null)
                {
                    MessageBox.Show("系统未注册组件" + text3, "日志", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return;
                }
                SetExcelRegistryKey.SetValue();
                object target = typeFromProgID.InvokeMember(null, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.CreateInstance, null, null, null);
                array[0] = m_LogFileName;
                array[1] = text;
                typeFromProgID.InvokeMember("Show", BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.InvokeMethod, null, target, array);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private string ReadType(string filename)
        {
            string result = null;
            string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Context.QIPath + "\\log\\loglist.mdb;";
            OleDbConnection oleDbConnection = new OleDbConnection(connectionString);
            oleDbConnection.Open();
            string cmdText = "select [type] from [log] where filename = '" + filename + "'";
            OleDbCommand oleDbCommand = new OleDbCommand(cmdText, oleDbConnection);
            OleDbDataReader oleDbDataReader = oleDbCommand.ExecuteReader();
            if (oleDbDataReader.Read())
            {
                result = oleDbDataReader.GetString(0);
            }
            oleDbDataReader.Close();
            oleDbConnection.Close();
            return result;
        }
    }

}