﻿namespace Yonyou.U8.QuickImport
{
    partial class SolutionManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode105 = new System.Windows.Forms.TreeNode("节点0");
            System.Windows.Forms.TreeNode treeNode106 = new System.Windows.Forms.TreeNode("节点1");
            System.Windows.Forms.TreeNode treeNode107 = new System.Windows.Forms.TreeNode("节点3");
            System.Windows.Forms.TreeNode treeNode108 = new System.Windows.Forms.TreeNode("节点4");
            System.Windows.Forms.TreeNode treeNode109 = new System.Windows.Forms.TreeNode("节点5");
            System.Windows.Forms.TreeNode treeNode110 = new System.Windows.Forms.TreeNode("节点6");
            System.Windows.Forms.TreeNode treeNode111 = new System.Windows.Forms.TreeNode("节点7");
            System.Windows.Forms.TreeNode treeNode112 = new System.Windows.Forms.TreeNode("节点8");
            System.Windows.Forms.TreeNode treeNode113 = new System.Windows.Forms.TreeNode("节点9");
            System.Windows.Forms.TreeNode treeNode114 = new System.Windows.Forms.TreeNode("节点11");
            System.Windows.Forms.TreeNode treeNode115 = new System.Windows.Forms.TreeNode("节点12");
            System.Windows.Forms.TreeNode treeNode116 = new System.Windows.Forms.TreeNode("节点13");
            System.Windows.Forms.TreeNode treeNode117 = new System.Windows.Forms.TreeNode("节点14");
            System.Windows.Forms.TreeNode treeNode118 = new System.Windows.Forms.TreeNode("节点15");
            System.Windows.Forms.TreeNode treeNode119 = new System.Windows.Forms.TreeNode("节点16");
            System.Windows.Forms.TreeNode treeNode120 = new System.Windows.Forms.TreeNode("节点17");
            System.Windows.Forms.TreeNode treeNode121 = new System.Windows.Forms.TreeNode("节点18");
            System.Windows.Forms.TreeNode treeNode122 = new System.Windows.Forms.TreeNode("节点10", new System.Windows.Forms.TreeNode[] {
            treeNode114,
            treeNode115,
            treeNode116,
            treeNode117,
            treeNode118,
            treeNode119,
            treeNode120,
            treeNode121});
            System.Windows.Forms.TreeNode treeNode123 = new System.Windows.Forms.TreeNode("节点2", new System.Windows.Forms.TreeNode[] {
            treeNode107,
            treeNode108,
            treeNode109,
            treeNode110,
            treeNode111,
            treeNode112,
            treeNode113,
            treeNode122});
            System.Windows.Forms.TreeNode treeNode124 = new System.Windows.Forms.TreeNode("节点0");
            System.Windows.Forms.TreeNode treeNode125 = new System.Windows.Forms.TreeNode("节点1");
            System.Windows.Forms.TreeNode treeNode126 = new System.Windows.Forms.TreeNode("节点2");
            System.Windows.Forms.TreeNode treeNode127 = new System.Windows.Forms.TreeNode("节点3");
            System.Windows.Forms.TreeNode treeNode128 = new System.Windows.Forms.TreeNode("节点4");
            System.Windows.Forms.TreeNode treeNode129 = new System.Windows.Forms.TreeNode("节点5");
            System.Windows.Forms.TreeNode treeNode130 = new System.Windows.Forms.TreeNode("节点6");
            System.Windows.Forms.TreeNode treeNode131 = new System.Windows.Forms.TreeNode("节点7");
            System.Windows.Forms.TreeNode treeNode132 = new System.Windows.Forms.TreeNode("节点8");
            System.Windows.Forms.TreeNode treeNode133 = new System.Windows.Forms.TreeNode("节点9");
            System.Windows.Forms.TreeNode treeNode134 = new System.Windows.Forms.TreeNode("节点10");
            System.Windows.Forms.TreeNode treeNode135 = new System.Windows.Forms.TreeNode("节点12");
            System.Windows.Forms.TreeNode treeNode136 = new System.Windows.Forms.TreeNode("节点13");
            System.Windows.Forms.TreeNode treeNode137 = new System.Windows.Forms.TreeNode("节点14");
            System.Windows.Forms.TreeNode treeNode138 = new System.Windows.Forms.TreeNode("节点19");
            System.Windows.Forms.TreeNode treeNode139 = new System.Windows.Forms.TreeNode("节点20");
            System.Windows.Forms.TreeNode treeNode140 = new System.Windows.Forms.TreeNode("节点25");
            System.Windows.Forms.TreeNode treeNode141 = new System.Windows.Forms.TreeNode("节点26");
            System.Windows.Forms.TreeNode treeNode142 = new System.Windows.Forms.TreeNode("节点27");
            System.Windows.Forms.TreeNode treeNode143 = new System.Windows.Forms.TreeNode("节点28");
            System.Windows.Forms.TreeNode treeNode144 = new System.Windows.Forms.TreeNode("节点29");
            System.Windows.Forms.TreeNode treeNode145 = new System.Windows.Forms.TreeNode("节点30");
            System.Windows.Forms.TreeNode treeNode146 = new System.Windows.Forms.TreeNode("节点31");
            System.Windows.Forms.TreeNode treeNode147 = new System.Windows.Forms.TreeNode("节点32");
            System.Windows.Forms.TreeNode treeNode148 = new System.Windows.Forms.TreeNode("节点21", new System.Windows.Forms.TreeNode[] {
            treeNode140,
            treeNode141,
            treeNode142,
            treeNode143,
            treeNode144,
            treeNode145,
            treeNode146,
            treeNode147});
            System.Windows.Forms.TreeNode treeNode149 = new System.Windows.Forms.TreeNode("节点22");
            System.Windows.Forms.TreeNode treeNode150 = new System.Windows.Forms.TreeNode("节点23");
            System.Windows.Forms.TreeNode treeNode151 = new System.Windows.Forms.TreeNode("节点24");
            System.Windows.Forms.TreeNode treeNode152 = new System.Windows.Forms.TreeNode("节点15", new System.Windows.Forms.TreeNode[] {
            treeNode138,
            treeNode139,
            treeNode148,
            treeNode149,
            treeNode150,
            treeNode151});
            System.Windows.Forms.TreeNode treeNode153 = new System.Windows.Forms.TreeNode("节点16");
            System.Windows.Forms.TreeNode treeNode154 = new System.Windows.Forms.TreeNode("节点17");
            System.Windows.Forms.TreeNode treeNode155 = new System.Windows.Forms.TreeNode("节点18");
            System.Windows.Forms.TreeNode treeNode156 = new System.Windows.Forms.TreeNode("节点11", new System.Windows.Forms.TreeNode[] {
            treeNode135,
            treeNode136,
            treeNode137,
            treeNode152,
            treeNode153,
            treeNode154,
            treeNode155});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SolutionManager));
            this.LayoutContainer = new System.Windows.Forms.Panel();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.metroTreeView1 = new YonYou.U8.IN.Forms.MetroTreeView();
            this.GridContainer = new System.Windows.Forms.Panel();
            this.itemGrid = new System.Windows.Forms.DataGridView();
            this.voucherClassContainer = new System.Windows.Forms.Panel();
            this.voucherClasses = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BottomContianer = new YonYou.U8.IN.Forms.MetroPanel();
            this.TopContainer = new YonYou.U8.IN.Forms.MetroPanel();
            this.metroToolStrip1 = new YonYou.U8.IN.Forms.MetroToolStrip();
            this.btnAdd = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.LayoutContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.GridContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.itemGrid)).BeginInit();
            this.voucherClassContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.TopContainer.SuspendLayout();
            this.metroToolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LayoutContainer
            // 
            this.LayoutContainer.Controls.Add(this.splitContainer);
            this.LayoutContainer.Controls.Add(this.BottomContianer);
            this.LayoutContainer.Controls.Add(this.TopContainer);
            this.LayoutContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutContainer.Location = new System.Drawing.Point(1, 30);
            this.LayoutContainer.Margin = new System.Windows.Forms.Padding(4);
            this.LayoutContainer.Name = "LayoutContainer";
            this.LayoutContainer.Size = new System.Drawing.Size(904, 556);
            this.LayoutContainer.TabIndex = 0;
            // 
            // splitContainer
            // 
            this.splitContainer.BackColor = System.Drawing.Color.White;
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.IsSplitterFixed = true;
            this.splitContainer.Location = new System.Drawing.Point(0, 30);
            this.splitContainer.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.metroTreeView1);
            this.splitContainer.Panel1.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.GridContainer);
            this.splitContainer.Panel2.Controls.Add(this.voucherClassContainer);
            this.splitContainer.Panel2.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.splitContainer.Size = new System.Drawing.Size(904, 482);
            this.splitContainer.SplitterDistance = 156;
            this.splitContainer.TabIndex = 2;
            // 
            // metroTreeView1
            // 
            this.metroTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroTreeView1.HideScrollBarType = YonYou.U8.IN.Forms.MetroTreeView.MetroHideScrollBar.HorizontalScroll;
            this.metroTreeView1.HideSelection = false;
            this.metroTreeView1.Location = new System.Drawing.Point(0, 6);
            this.metroTreeView1.Name = "metroTreeView1";
            treeNode105.Name = "节点0";
            treeNode105.Text = "节点0";
            treeNode106.Name = "节点1";
            treeNode106.Text = "节点1";
            treeNode107.Name = "节点3";
            treeNode107.Text = "节点3";
            treeNode108.Name = "节点4";
            treeNode108.Text = "节点4";
            treeNode109.Name = "节点5";
            treeNode109.Text = "节点5";
            treeNode110.Name = "节点6";
            treeNode110.Text = "节点6";
            treeNode111.Name = "节点7";
            treeNode111.Text = "节点7";
            treeNode112.Name = "节点8";
            treeNode112.Text = "节点8";
            treeNode113.Name = "节点9";
            treeNode113.Text = "节点9";
            treeNode114.Name = "节点11";
            treeNode114.Text = "节点11";
            treeNode115.Name = "节点12";
            treeNode115.Text = "节点12";
            treeNode116.Name = "节点13";
            treeNode116.Text = "节点13";
            treeNode117.Name = "节点14";
            treeNode117.Text = "节点14";
            treeNode118.Name = "节点15";
            treeNode118.Text = "节点15";
            treeNode119.Name = "节点16";
            treeNode119.Text = "节点16";
            treeNode120.Name = "节点17";
            treeNode120.Text = "节点17";
            treeNode121.Name = "节点18";
            treeNode121.Text = "节点18";
            treeNode122.Name = "节点10";
            treeNode122.Text = "节点10";
            treeNode123.Name = "节点2";
            treeNode123.Text = "节点2";
            treeNode124.Name = "节点0";
            treeNode124.Text = "节点0";
            treeNode125.Name = "节点1";
            treeNode125.Text = "节点1";
            treeNode126.Name = "节点2";
            treeNode126.Text = "节点2";
            treeNode127.Name = "节点3";
            treeNode127.Text = "节点3";
            treeNode128.Name = "节点4";
            treeNode128.Text = "节点4";
            treeNode129.Name = "节点5";
            treeNode129.Text = "节点5";
            treeNode130.Name = "节点6";
            treeNode130.Text = "节点6";
            treeNode131.Name = "节点7";
            treeNode131.Text = "节点7";
            treeNode132.Name = "节点8";
            treeNode132.Text = "节点8";
            treeNode133.Name = "节点9";
            treeNode133.Text = "节点9";
            treeNode134.Name = "节点10";
            treeNode134.Text = "节点10";
            treeNode135.Name = "节点12";
            treeNode135.Text = "节点12";
            treeNode136.Name = "节点13";
            treeNode136.Text = "节点13";
            treeNode137.Name = "节点14";
            treeNode137.Text = "节点14";
            treeNode138.Name = "节点19";
            treeNode138.Text = "节点19";
            treeNode139.Name = "节点20";
            treeNode139.Text = "节点20";
            treeNode140.Name = "节点25";
            treeNode140.Text = "节点25";
            treeNode141.Name = "节点26";
            treeNode141.Text = "节点26";
            treeNode142.Name = "节点27";
            treeNode142.Text = "节点27";
            treeNode143.Name = "节点28";
            treeNode143.Text = "节点28";
            treeNode144.Name = "节点29";
            treeNode144.Text = "节点29";
            treeNode145.Name = "节点30";
            treeNode145.Text = "节点30";
            treeNode146.Name = "节点31";
            treeNode146.Text = "节点31";
            treeNode147.Name = "节点32";
            treeNode147.Text = "节点32";
            treeNode148.Name = "节点21";
            treeNode148.Text = "节点21";
            treeNode149.Name = "节点22";
            treeNode149.Text = "节点22";
            treeNode150.Name = "节点23";
            treeNode150.Text = "节点23";
            treeNode151.Name = "节点24";
            treeNode151.Text = "节点24";
            treeNode152.Name = "节点15";
            treeNode152.Text = "节点15";
            treeNode153.Name = "节点16";
            treeNode153.Text = "节点16";
            treeNode154.Name = "节点17";
            treeNode154.Text = "节点17";
            treeNode155.Name = "节点18";
            treeNode155.Text = "节点18";
            treeNode156.Name = "节点11";
            treeNode156.Text = "节点11";
            this.metroTreeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode105,
            treeNode106,
            treeNode123,
            treeNode124,
            treeNode125,
            treeNode126,
            treeNode127,
            treeNode128,
            treeNode129,
            treeNode130,
            treeNode131,
            treeNode132,
            treeNode133,
            treeNode134,
            treeNode156});
            this.metroTreeView1.Size = new System.Drawing.Size(156, 476);
            this.metroTreeView1.TabIndex = 0;
            // 
            // GridContainer
            // 
            this.GridContainer.Controls.Add(this.itemGrid);
            this.GridContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridContainer.Location = new System.Drawing.Point(5, 53);
            this.GridContainer.Name = "GridContainer";
            this.GridContainer.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.GridContainer.Size = new System.Drawing.Size(739, 429);
            this.GridContainer.TabIndex = 0;
            // 
            // itemGrid
            // 
            this.itemGrid.BackgroundColor = System.Drawing.Color.White;
            this.itemGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.itemGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.itemGrid.Location = new System.Drawing.Point(3, 4);
            this.itemGrid.Name = "itemGrid";
            this.itemGrid.RowTemplate.Height = 23;
            this.itemGrid.Size = new System.Drawing.Size(733, 421);
            this.itemGrid.TabIndex = 0;
            // 
            // voucherClassContainer
            // 
            this.voucherClassContainer.Controls.Add(this.voucherClasses);
            this.voucherClassContainer.Controls.Add(this.pictureBox1);
            this.voucherClassContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.voucherClassContainer.Location = new System.Drawing.Point(5, 0);
            this.voucherClassContainer.Margin = new System.Windows.Forms.Padding(0);
            this.voucherClassContainer.Name = "voucherClassContainer";
            this.voucherClassContainer.Padding = new System.Windows.Forms.Padding(14, 0, 0, 0);
            this.voucherClassContainer.Size = new System.Drawing.Size(739, 53);
            this.voucherClassContainer.TabIndex = 1;
            // 
            // voucherClasses
            // 
            this.voucherClasses.Location = new System.Drawing.Point(14, 0);
            this.voucherClasses.Margin = new System.Windows.Forms.Padding(0);
            this.voucherClasses.Name = "voucherClasses";
            this.voucherClasses.Padding = new System.Windows.Forms.Padding(0, 15, 0, 13);
            this.voucherClasses.Size = new System.Drawing.Size(727, 53);
            this.voucherClasses.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(21, 19);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(21, 19);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // BottomContianer
            // 
            this.BottomContianer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(243)))), ((int)(((byte)(254)))));
            this.BottomContianer.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(195)))), ((int)(((byte)(217)))));
            this.BottomContianer.BorderDrawPosition = YonYou.U8.IN.Forms.MetroPanel.BorderPosition.Bottom;
            this.BottomContianer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomContianer.Location = new System.Drawing.Point(0, 512);
            this.BottomContianer.Name = "BottomContianer";
            this.BottomContianer.Size = new System.Drawing.Size(904, 44);
            this.BottomContianer.TabIndex = 1;
            // 
            // TopContainer
            // 
            this.TopContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(243)))), ((int)(((byte)(254)))));
            this.TopContainer.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(195)))), ((int)(((byte)(217)))));
            this.TopContainer.BorderDrawPosition = YonYou.U8.IN.Forms.MetroPanel.BorderPosition.Bottom;
            this.TopContainer.Controls.Add(this.metroToolStrip1);
            this.TopContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopContainer.Location = new System.Drawing.Point(0, 0);
            this.TopContainer.Name = "TopContainer";
            this.TopContainer.Size = new System.Drawing.Size(904, 30);
            this.TopContainer.TabIndex = 0;
            // 
            // metroToolStrip1
            // 
            this.metroToolStrip1.AutoSize = false;
            this.metroToolStrip1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(59)))), ((int)(((byte)(59)))));
            this.metroToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.metroToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAdd,
            this.toolStripSeparator1});
            this.metroToolStrip1.Location = new System.Drawing.Point(0, 0);
            this.metroToolStrip1.MinimumSize = new System.Drawing.Size(20, 20);
            this.metroToolStrip1.Name = "metroToolStrip1";
            this.metroToolStrip1.Padding = new System.Windows.Forms.Padding(0);
            this.metroToolStrip1.ShowItemToolTips = false;
            this.metroToolStrip1.Size = new System.Drawing.Size(904, 31);
            this.metroToolStrip1.TabIndex = 0;
            this.metroToolStrip1.Text = "metroToolStrip1";
            this.metroToolStrip1.ToolScripBackColor = System.Drawing.Color.White;
            this.metroToolStrip1.ToolScripBorderColor = System.Drawing.Color.White;
            this.metroToolStrip1.TransBackground = false;
            // 
            // btnAdd
            // 
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAdd.Margin = new System.Windows.Forms.Padding(0);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(80, 22);
            this.btnAdd.Text = "新增方案";
            this.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // SolutionManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 587);
            this.Controls.Add(this.LayoutContainer);
            this.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.Name = "SolutionManager";
            this.Text = "方案管理";
            this.LayoutContainer.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.GridContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.itemGrid)).EndInit();
            this.voucherClassContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.TopContainer.ResumeLayout(false);
            this.metroToolStrip1.ResumeLayout(false);
            this.metroToolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LayoutContainer;
        private YonYou.U8.IN.Forms.MetroPanel TopContainer;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Panel GridContainer;
        private System.Windows.Forms.DataGridView itemGrid;
        private System.Windows.Forms.Panel voucherClassContainer;
        private System.Windows.Forms.FlowLayoutPanel voucherClasses;
        private System.Windows.Forms.PictureBox pictureBox1;
        private YonYou.U8.IN.Forms.MetroPanel BottomContianer;
        private YonYou.U8.IN.Forms.MetroToolStrip metroToolStrip1;
        private System.Windows.Forms.ToolStripButton btnAdd;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private YonYou.U8.IN.Forms.MetroTreeView metroTreeView1;
    }
}