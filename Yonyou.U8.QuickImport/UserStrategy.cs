﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Yonyou.U8.QuickImport
{
    public partial class UserStrategy : Form
    {
        public UserStrategy()
        {
            InitializeComponent();
        }


        public static void ResetLogPath()
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(Context.QIPath + "\\ImportStrategy\\ImportParameter.xml");
            XmlNode xmlNode = xmlDocument.SelectSingleNode("//import");
            if (!Directory.Exists(Context.QIPath + "\\Logs"))
            {
                Directory.CreateDirectory(Context.QIPath + "\\Logs");
            }
            xmlNode.Attributes.Item(1).Value = Context.QIPath + "\\Logs\\";
            XmlTextWriter xmlTextWriter = new XmlTextWriter(Context.QIPath + "\\ImportStrategy\\ImportParameter.xml", null);
            xmlDocument.Save(xmlTextWriter);
            xmlTextWriter.Flush();
            xmlTextWriter.Close();
        }
    }
}
