﻿namespace Yonyou.U8.QuickImport
{
    partial class DeleteList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TopContainer = new System.Windows.Forms.Panel();
            this.cmdDeleteData = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.gridContainer = new System.Windows.Forms.Panel();
            this.logGrid = new System.Windows.Forms.DataGridView();
            this.ColumnCheck = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ColumnNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSucceed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnUser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnToken = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImportType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDelete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TopContainer.SuspendLayout();
            this.gridContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // TopContainer
            // 
            this.TopContainer.Controls.Add(this.cmdDeleteData);
            this.TopContainer.Controls.Add(this.cmdCancel);
            this.TopContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopContainer.Location = new System.Drawing.Point(0, 0);
            this.TopContainer.Name = "TopContainer";
            this.TopContainer.Size = new System.Drawing.Size(715, 46);
            this.TopContainer.TabIndex = 0;
            // 
            // cmdDeleteData
            // 
            this.cmdDeleteData.Location = new System.Drawing.Point(574, 11);
            this.cmdDeleteData.Name = "cmdDeleteData";
            this.cmdDeleteData.Size = new System.Drawing.Size(64, 23);
            this.cmdDeleteData.TabIndex = 1;
            this.cmdDeleteData.Text = "删除";
            this.cmdDeleteData.UseVisualStyleBackColor = true;
            this.cmdDeleteData.Click += new System.EventHandler(this.cmdDeleteData_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Location = new System.Drawing.Point(644, 11);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(64, 23);
            this.cmdCancel.TabIndex = 0;
            this.cmdCancel.Text = "退出";
            this.cmdCancel.UseVisualStyleBackColor = true;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // gridContainer
            // 
            this.gridContainer.Controls.Add(this.logGrid);
            this.gridContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridContainer.Location = new System.Drawing.Point(0, 46);
            this.gridContainer.Name = "gridContainer";
            this.gridContainer.Size = new System.Drawing.Size(715, 384);
            this.gridContainer.TabIndex = 1;
            this.gridContainer.ClientSizeChanged += new System.EventHandler(this.gridContainer_ClientSizeChanged);
            // 
            // logGrid
            // 
            this.logGrid.AllowUserToAddRows = false;
            this.logGrid.AllowUserToDeleteRows = false;
            this.logGrid.BackgroundColor = System.Drawing.SystemColors.Window;
            this.logGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.logGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnCheck,
            this.ColumnNum,
            this.ColumnTime,
            this.ColumnType,
            this.ColumnName,
            this.ColumnFile,
            this.ColumnSucceed,
            this.ColumnFail,
            this.ColumnUser,
            this.ColumnToken,
            this.ImportType,
            this.ColumnDelete});
            this.logGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logGrid.GridColor = System.Drawing.Color.Gray;
            this.logGrid.Location = new System.Drawing.Point(0, 0);
            this.logGrid.MultiSelect = false;
            this.logGrid.Name = "logGrid";
            this.logGrid.RowHeadersVisible = false;
            this.logGrid.RowHeadersWidth = 10;
            this.logGrid.RowTemplate.Height = 23;
            this.logGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.logGrid.Size = new System.Drawing.Size(715, 384);
            this.logGrid.TabIndex = 0;
            // 
            // ColumnCheck
            // 
            this.ColumnCheck.FalseValue = "0";
            this.ColumnCheck.HeaderText = "";
            this.ColumnCheck.IndeterminateValue = "0";
            this.ColumnCheck.Name = "ColumnCheck";
            this.ColumnCheck.Width = 25;
            // 
            // ColumnNum
            // 
            this.ColumnNum.DataPropertyName = "num";
            this.ColumnNum.HeaderText = "编号";
            this.ColumnNum.Name = "ColumnNum";
            this.ColumnNum.Width = 80;
            // 
            // ColumnTime
            // 
            this.ColumnTime.DataPropertyName = "importtime";
            this.ColumnTime.HeaderText = "导入时间";
            this.ColumnTime.Name = "ColumnTime";
            // 
            // ColumnType
            // 
            this.ColumnType.DataPropertyName = "type";
            this.ColumnType.HeaderText = "type";
            this.ColumnType.Name = "ColumnType";
            this.ColumnType.ReadOnly = true;
            this.ColumnType.Visible = false;
            // 
            // ColumnName
            // 
            this.ColumnName.DataPropertyName = "name";
            this.ColumnName.HeaderText = "数据项";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.ReadOnly = true;
            this.ColumnName.Width = 150;
            // 
            // ColumnFile
            // 
            this.ColumnFile.DataPropertyName = "filename";
            this.ColumnFile.HeaderText = "filename";
            this.ColumnFile.Name = "ColumnFile";
            this.ColumnFile.ReadOnly = true;
            this.ColumnFile.Visible = false;
            this.ColumnFile.Width = 200;
            // 
            // ColumnSucceed
            // 
            this.ColumnSucceed.DataPropertyName = "succeeded";
            this.ColumnSucceed.HeaderText = "导入成功记录数";
            this.ColumnSucceed.Name = "ColumnSucceed";
            this.ColumnSucceed.ReadOnly = true;
            this.ColumnSucceed.Width = 120;
            // 
            // ColumnFail
            // 
            this.ColumnFail.DataPropertyName = "failed";
            this.ColumnFail.HeaderText = "导入不成功记录数";
            this.ColumnFail.Name = "ColumnFail";
            this.ColumnFail.ReadOnly = true;
            this.ColumnFail.Width = 130;
            // 
            // ColumnUser
            // 
            this.ColumnUser.DataPropertyName = "username";
            this.ColumnUser.HeaderText = "导入人";
            this.ColumnUser.Name = "ColumnUser";
            this.ColumnUser.ReadOnly = true;
            // 
            // ColumnToken
            // 
            this.ColumnToken.DataPropertyName = "token";
            this.ColumnToken.HeaderText = "token";
            this.ColumnToken.Name = "ColumnToken";
            this.ColumnToken.ReadOnly = true;
            this.ColumnToken.Visible = false;
            // 
            // ImportType
            // 
            this.ImportType.DataPropertyName = "ImportType";
            this.ImportType.HeaderText = "导入类型";
            this.ImportType.Name = "ImportType";
            this.ImportType.ReadOnly = true;
            // 
            // ColumnDelete
            // 
            this.ColumnDelete.HeaderText = "已删除";
            this.ColumnDelete.Name = "ColumnDelete";
            this.ColumnDelete.ReadOnly = true;
            // 
            // DeleteList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridContainer);
            this.Controls.Add(this.TopContainer);
            this.Name = "DeleteList";
            this.Size = new System.Drawing.Size(715, 430);
            this.Load += new System.EventHandler(this.ListLog_Load);
            this.TopContainer.ResumeLayout(false);
            this.gridContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.logGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel TopContainer;
        private System.Windows.Forms.Button cmdDeleteData;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Panel gridContainer;
        private System.Windows.Forms.DataGridView logGrid;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ColumnCheck;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSucceed;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFail;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnToken;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImportType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDelete;
    }
}