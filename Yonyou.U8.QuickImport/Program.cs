﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace Yonyou.U8.QuickImport
{
    internal static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                //Application.Run(new SolutionManager());
                //Application.Run(new DataImportDesign2());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[Program->Main]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
            }
        }

    }
}
