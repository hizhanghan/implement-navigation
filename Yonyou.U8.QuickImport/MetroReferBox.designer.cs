﻿using System.Windows.Forms;

namespace Yonyou.U8.QuickImport
{
    partial class MetroReferBox
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tlpBackgroud = new System.Windows.Forms.TableLayoutPanel();
            this.tlpLayout = new System.Windows.Forms.TableLayoutPanel();
            this.tlpItemLayout = new System.Windows.Forms.TableLayoutPanel();
            this.tboxInput = new System.Windows.Forms.TextBox();
            this.pBoxLocation = new System.Windows.Forms.PictureBox();
            this.tlpBackgroud.SuspendLayout();
            this.tlpLayout.SuspendLayout();
            this.tlpItemLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxLocation)).BeginInit();
            this.SuspendLayout();
            // 
            // tlpBackgroud
            // 
            this.tlpBackgroud.ColumnCount = 1;
            this.tlpBackgroud.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpBackgroud.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpBackgroud.Controls.Add(this.tlpLayout, 0, 0);
            this.tlpBackgroud.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpBackgroud.Location = new System.Drawing.Point(0, 0);
            this.tlpBackgroud.Margin = new System.Windows.Forms.Padding(0);
            this.tlpBackgroud.Name = "tlpBackgroud";
            this.tlpBackgroud.RowCount = 1;
            this.tlpBackgroud.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpBackgroud.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpBackgroud.Size = new System.Drawing.Size(181, 29);
            this.tlpBackgroud.TabIndex = 0;
            // 
            // tlpLayout
            // 
            this.tlpLayout.ColumnCount = 1;
            this.tlpLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpLayout.Controls.Add(this.tlpItemLayout, 0, 0);
            this.tlpLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpLayout.Location = new System.Drawing.Point(1, 1);
            this.tlpLayout.Margin = new System.Windows.Forms.Padding(1);
            this.tlpLayout.Name = "tlpLayout";
            this.tlpLayout.RowCount = 1;
            this.tlpLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpLayout.Size = new System.Drawing.Size(179, 27);
            this.tlpLayout.TabIndex = 0;
            // 
            // tlpItemLayout
            // 
            this.tlpItemLayout.ColumnCount = 5;
            this.tlpItemLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.tlpItemLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpItemLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.tlpItemLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 14F));
            this.tlpItemLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.tlpItemLayout.Controls.Add(this.tboxInput, 1, 1);
            this.tlpItemLayout.Controls.Add(this.pBoxLocation, 3, 1);
            this.tlpItemLayout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tlpItemLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpItemLayout.Location = new System.Drawing.Point(1, 1);
            this.tlpItemLayout.Margin = new System.Windows.Forms.Padding(1);
            this.tlpItemLayout.Name = "tlpItemLayout";
            this.tlpItemLayout.RowCount = 3;
            this.tlpItemLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpItemLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 14F));
            this.tlpItemLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpItemLayout.Size = new System.Drawing.Size(177, 25);
            this.tlpItemLayout.TabIndex = 0;
            // 
            // tboxInput
            // 
            this.tboxInput.BackColor = System.Drawing.Color.White;
            this.tboxInput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tboxInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxInput.Location = new System.Drawing.Point(2, 5);
            this.tboxInput.Margin = new System.Windows.Forms.Padding(0);
            this.tboxInput.Name = "tboxInput";
            this.tboxInput.Size = new System.Drawing.Size(157, 14);
            this.tboxInput.TabIndex = 0;
            // 
            // pBoxLocation
            // 
            this.pBoxLocation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pBoxLocation.Image = global::Yonyou.U8.QuickImport.Properties.Resources.reftool;
            this.pBoxLocation.Location = new System.Drawing.Point(162, 6);
            this.pBoxLocation.Margin = new System.Windows.Forms.Padding(1);
            this.pBoxLocation.Name = "pBoxLocation";
            this.pBoxLocation.Size = new System.Drawing.Size(12, 12);
            this.pBoxLocation.TabIndex = 1;
            this.pBoxLocation.TabStop = false;
            this.pBoxLocation.Click += new System.EventHandler(this.Locate);
            // 
            // MetroReferBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.tlpBackgroud);
            this.Name = "MetroReferBox";
            this.Size = new System.Drawing.Size(181, 29);
            this.tlpBackgroud.ResumeLayout(false);
            this.tlpLayout.ResumeLayout(false);
            this.tlpItemLayout.ResumeLayout(false);
            this.tlpItemLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pBoxLocation)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpBackgroud;
        private System.Windows.Forms.TableLayoutPanel tlpLayout;
        private System.Windows.Forms.TableLayoutPanel tlpItemLayout;
        private System.Windows.Forms.TextBox tboxInput;
        private System.Windows.Forms.PictureBox pBoxLocation;
    }
}
