﻿namespace Yonyou.U8.QuickImport
{
    partial class NewSolution
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tboxSolution = new System.Windows.Forms.TextBox();
            this.bottomContainer = new YonYou.U8.IN.Forms.MetroPanel();
            this.buttonEx2 = new YonYou.U8.IN.Forms.ButtonEx();
            this.buttonEx1 = new YonYou.U8.IN.Forms.ButtonEx();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.bottomContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.bottomContainer);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(1, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(335, 79);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 6F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tboxSolution, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(335, 43);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "方案名称";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxSolution
            // 
            this.tboxSolution.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tboxSolution.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxSolution.Location = new System.Drawing.Point(73, 10);
            this.tboxSolution.Margin = new System.Windows.Forms.Padding(0);
            this.tboxSolution.Name = "tboxSolution";
            this.tboxSolution.Size = new System.Drawing.Size(256, 23);
            this.tboxSolution.TabIndex = 1;
            // 
            // bottomContainer
            // 
            this.bottomContainer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(243)))), ((int)(((byte)(254)))));
            this.bottomContainer.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(195)))), ((int)(((byte)(217)))));
            this.bottomContainer.BorderDrawPosition = YonYou.U8.IN.Forms.MetroPanel.BorderPosition.Top;
            this.bottomContainer.Controls.Add(this.buttonEx2);
            this.bottomContainer.Controls.Add(this.buttonEx1);
            this.bottomContainer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomContainer.Location = new System.Drawing.Point(0, 43);
            this.bottomContainer.Name = "bottomContainer";
            this.bottomContainer.Size = new System.Drawing.Size(335, 36);
            this.bottomContainer.TabIndex = 1;
            // 
            // buttonEx2
            // 
            this.buttonEx2.BackColor = System.Drawing.Color.Transparent;
            this.buttonEx2.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(237)))), ((int)(((byte)(241)))));
            this.buttonEx2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(170)))), ((int)(((byte)(203)))));
            this.buttonEx2.Description = "";
            this.buttonEx2.ForeColor = System.Drawing.Color.Black;
            this.buttonEx2.Location = new System.Drawing.Point(229, 7);
            this.buttonEx2.Name = "buttonEx2";
            this.buttonEx2.ProviderStyle = YonYou.U8.IN.Forms.ProviderStyle.Normal;
            this.buttonEx2.RoundStyle = YonYou.U8.IN.Forms.RoundStyle.None;
            this.buttonEx2.Size = new System.Drawing.Size(75, 23);
            this.buttonEx2.Skin = null;
            this.buttonEx2.SkinStyle = YonYou.U8.IN.Forms.ButtonSkinStyle.None;
            this.buttonEx2.TabIndex = 1;
            this.buttonEx2.Text = "取消";
            this.buttonEx2.UseVisualStyleBackColor = false;
            // 
            // buttonEx1
            // 
            this.buttonEx1.BackColor = System.Drawing.Color.Transparent;
            this.buttonEx1.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(237)))), ((int)(((byte)(241)))));
            this.buttonEx1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(137)))), ((int)(((byte)(170)))), ((int)(((byte)(203)))));
            this.buttonEx1.Description = "";
            this.buttonEx1.ForeColor = System.Drawing.Color.Black;
            this.buttonEx1.Location = new System.Drawing.Point(124, 6);
            this.buttonEx1.Name = "buttonEx1";
            this.buttonEx1.ProviderStyle = YonYou.U8.IN.Forms.ProviderStyle.Normal;
            this.buttonEx1.RoundStyle = YonYou.U8.IN.Forms.RoundStyle.None;
            this.buttonEx1.Size = new System.Drawing.Size(75, 23);
            this.buttonEx1.Skin = null;
            this.buttonEx1.SkinStyle = YonYou.U8.IN.Forms.ButtonSkinStyle.None;
            this.buttonEx1.TabIndex = 0;
            this.buttonEx1.Text = "确定";
            this.buttonEx1.UseVisualStyleBackColor = false;
            // 
            // NewSolution
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(337, 110);
            this.Controls.Add(this.panel1);
            this.Name = "NewSolution";
            this.Text = "新增方案";
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.bottomContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private YonYou.U8.IN.Forms.MetroPanel bottomContainer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tboxSolution;
        private YonYou.U8.IN.Forms.ButtonEx buttonEx2;
        private YonYou.U8.IN.Forms.ButtonEx buttonEx1;
    }
}