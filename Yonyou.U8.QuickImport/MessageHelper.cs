﻿using System.Windows.Forms;

namespace Yonyou.U8.QuickImport
{
    internal class MessageHelper
    {
        internal static void ShowMessage(string msg)
        {
            MessageBox.Show(msg, "提示信息", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        internal static void ShowMessage(string title, string msg)
        {
            MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

        public static bool ShowTipMessage(string content, string title)
        {
            if (MessageBox.Show(content, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return false;
            }
            return true;
        }
    }
}