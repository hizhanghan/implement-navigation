﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using U8Login;

namespace Yonyou.U8.QuickImport
{
    public partial class ImportPanel : UserControl
    {
        private Context context;

        public ImportPanel()
        {
            InitializeComponent();
            clsLoginClass clsLoginClass = clsImport.U8login as clsLoginClass;
            if (clsLoginClass == null)
            {
                throw new ApplicationException("clsImport.U8login is Null");
            }
            context = new Context(clsLoginClass);
            context.TabContainer = tabControl1;
            tabControl1.Selected += tabControl1_Selected;
            SuspendLayout();
            if (CommonUtil.CheckAuthValid(clsLoginClass, "QI0101"))
            {
                TabPage tabPage = new TabPage
                {
                    BackColor = Color.White,
                    Location = new Point(0, 0),
                    Name = "tpOpenModel",
                    Padding = new Padding(0),
                    Size = new Size(492, 405),
                    TabIndex = 0,
                    Text = "打开模板"
                };
                ChoseModel value = new ChoseModel(context)
                {
                    Dock = DockStyle.Fill
                };
                tabPage.Controls.Add(value);
                tabControl1.TabPages.Add(tabPage);
            }
            if (MenuUtil.HasSupAuth(clsLoginClass, "QI0104"))
            {
                TabPage tabPage2 = new TabPage
                {
                    BackColor = Color.White,
                    Location = new Point(0, 0),
                    Name = "tpDataImport",
                    Padding = new Padding(0),
                    Size = new Size(492, 405),
                    TabIndex = 1,
                    Text = "数据导入"
                };
                DataImportDesign2 value2 = new DataImportDesign2(context)
                {
                    Padding = new Padding(0),
                    Dock = DockStyle.Fill
                };
                tabPage2.Controls.Add(value2);
                tabControl1.TabPages.Add(tabPage2);
            }
            if (CommonUtil.CheckAuthValid(clsLoginClass, "QI0106"))
            {
                TabPage tabPage3 = new TabPage
                {
                    BackColor = Color.White,
                    Location = new Point(0, 0),
                    Name = "tpImportDelete",
                    Padding = new Padding(0),
                    Size = new Size(492, 405),
                    TabIndex = 2,
                    Text = "导入删除"
                };
                DeleteData value3 = new DeleteData(context)
                {
                    Dock = DockStyle.Fill
                };
                tabPage3.Controls.Add(value3);
                tabControl1.TabPages.Add(tabPage3);
            }
            if (CommonUtil.CheckAuthValid(clsLoginClass, "QI0107"))
            {
                TabPage tabPage4 = new TabPage
                {
                    BackColor = Color.White,
                    Location = new Point(0, 0),
                    Name = "tpImportLog",
                    Padding = new Padding(0),
                    Size = new Size(492, 405),
                    TabIndex = 3,
                    Text = "导入日志"
                };
                QueryLog value4 = new QueryLog(context)
                {
                    Dock = DockStyle.Fill
                };
                tabPage4.Controls.Add(value4);
                tabControl1.TabPages.Add(tabPage4);
            }
            ResumeLayout(false);
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            TabPage tabPage = e.TabPage;
            if (tabPage.Controls.Count <= 0)
            {
                switch (tabPage.Name)
                {
                    case "tpDataImport":
                        {
                            DataImportDesign dataImportDesign = new DataImportDesign(context);
                            dataImportDesign.Padding = new Padding(0);
                            dataImportDesign.Dock = DockStyle.Fill;
                            tabPage.Controls.Add(dataImportDesign);
                            break;
                        }
                    case "tpImportDelete":
                        {
                            DeleteData deleteData = new DeleteData(context);
                            deleteData.Dock = DockStyle.Fill;
                            tabPage.Controls.Add(deleteData);
                            break;
                        }
                    case "tpImportLog":
                        {
                            QueryLog queryLog = new QueryLog(context);
                            queryLog.Dock = DockStyle.Fill;
                            tabPage.Controls.Add(queryLog);
                            break;
                        }
                }
            }
        }

        internal static Control Query(TabPage page, Context context)
        {
            if (page.Controls.Count == 0)
            {
                switch (page.Name)
                {
                    case "tpDataImport":
                        {
                            DataImportDesign2 dataImportDesign = new DataImportDesign2(context);
                            dataImportDesign.Padding = new Padding(0);
                            dataImportDesign.Dock = DockStyle.Fill;
                            page.Controls.Add(dataImportDesign);
                            break;
                        }
                    case "tpImportDelete":
                        {
                            DeleteData deleteData = new DeleteData(context);
                            deleteData.Dock = DockStyle.Fill;
                            page.Controls.Add(deleteData);
                            break;
                        }
                    case "tpImportLog":
                        {
                            QueryLog queryLog = new QueryLog(context);
                            queryLog.Dock = DockStyle.Fill;
                            page.Controls.Add(queryLog);
                            break;
                        }
                }
            }
            if (page.Controls.Count > 0)
            {
                return page.Controls[0];
            }
            return null;
        }


    }
}
