﻿using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System;
using YonYou.U8.IN.Framework;

namespace Yonyou.U8.QuickImport
{
    internal class clsImport
    {
        public static string g_InstallPath;

        public Form pForm;

        internal static object QILogin;

        internal static object U8login;

        private int MAX_PATH = 260;

        public static clsImport mainObj;

        private static HelpProvider helpProvider;

        private Context _Context;

        //public static _ILocalID localSrv;

        //public static ResourceServiceWrapper resSrv;

        //public static ResourceServiceFactoryClass resFactory;

        private bool bInitRes;

        public Context Context
        {
            get
            {
                if (_Context == null)
                {
                    _Context = new Context(U8login);
                }
                return _Context;
            }
        }

        static clsImport()
        {
            g_InstallPath = "";
            mainObj = null;
            //localSrv = null;
            //resSrv = null;
            //resFactory = null;
            try
            {
                InitWnd();
                UserStrategy.ResetLogPath();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void ResumeModel(TabControl tab)
        {
            if (MessageBox.Show("请关闭所有打开的模板。\n恢复模板将删除现有模板中的所有数据，是否继续？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }
            IntPtr intPtr = IntPtr.Zero;
            IntPtr ppidl = IntPtr.Zero;
            try
            {
                int nFolder = 17;
                Win32Api.SHGetSpecialFolderLocation(tab.Handle, nFolder, out ppidl);
                Win32Api.BROWSEINFO bi = default(Win32Api.BROWSEINFO);
                IntPtr intPtr2 = Marshal.AllocHGlobal(MAX_PATH);
                bi.pidlRoot = ppidl;
                bi.hwndOwner = tab.Handle;
                bi.pszDisplayName = intPtr2;
                bi.lpszTitle = "请选择Excel模板的备份路径";
                bi.ulFlags = 64;
                intPtr = Win32Api.SHBrowseForFolder(ref bi);
                Marshal.FreeHGlobal(intPtr2);
                StringBuilder stringBuilder = new StringBuilder(MAX_PATH);
                if (Win32Api.SHGetPathFromIDList(intPtr, stringBuilder) == 0)
                {
                    return;
                }
                string text = stringBuilder.ToString();
                if (string.Equals(text, Context.QICurrentModelPath, StringComparison.OrdinalIgnoreCase) || string.Equals(text, Context.ExcelModelPath, StringComparison.OrdinalIgnoreCase))
                {
                    MessageHelper.ShowMessage("非法路径选择错误", "Excel模板文件备份路径非法,请重新选择其他文件夹作为备份路径！");
                    return;
                }
                string[] files = Directory.GetFiles(Context.QICurrentModelPath, "*.xls");
                for (int i = 0; i < files.Length; i++)
                {
                    File.Copy(Context.QICurrentModelPath + "\\" + Path.GetFileName(files[i]), text + "\\" + Path.GetFileName(files[i]), true);
                }
                files = Directory.GetFiles(Context.ExcelModelPath, "*.xls");
                for (int j = 0; j < files.Length; j++)
                {
                    File.Copy(Context.ExcelModelPath + "\\" + Path.GetFileName(files[j]), Context.QICurrentModelPath + "\\" + Path.GetFileName(files[j]), true);
                }
                MessageBox.Show("模板已成功恢复", "提示", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Win32Api.IMalloc ppMalloc;
                Win32Api.SHGetMalloc(out ppMalloc);
                ppMalloc.Free(ppidl);
                if (intPtr != IntPtr.Zero)
                {
                    ppMalloc.Free(intPtr);
                }
            }
        }

        private static bool InitWnd()
        {
            try
            {
                helpProvider = new HelpProvider();
                helpProvider.HelpNamespace = Application.StartupPath + "\\Help\\U8实施与维护工具.chm";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return true;
        }

        private void InitMultiLangRes()
        {
            //if (bInitRes)
            //{
            //    return;
            //}
            //try
            //{
            //    if (resSrv == null)
            //    {
            //        resSrv = new ResourceServiceWrapper();
            //    }
            //    if (resFactory == null)
            //    {
            //        resFactory = new ResourceServiceFactoryClass();
            //    }
            //    resSrv.Init("");
            //    int num = 0;
            //    resFactory.SetResourceType(ref num);
            //    string text = "zh-CN";
            //    localSrv = resFactory.GetLocalService();
            //    localSrv.SetProcessLocalID(ref text);
            //    bInitRes = true;
            //}
            //catch (Exception)
            //{
            //    throw new Exception("初始化U8多语服务失败，请确认正确安装了U8ERP！");
            //}
        }

        public bool CheckForm(string strfrmName)
        {
            Form[] mdiChildren = pForm.MdiChildren;
            foreach (Form form in mdiChildren)
            {
                if (form.Name == strfrmName)
                {
                    form.Activate();
                    form.Show();
                    form.WindowState = FormWindowState.Maximized;
                    return true;
                }
            }
            return false;
        }
    }
}