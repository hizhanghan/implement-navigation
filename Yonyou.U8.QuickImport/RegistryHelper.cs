﻿using Microsoft.Win32;
using System.Globalization;
using System.IO;
using System.Security.AccessControl;
using System;

namespace Yonyou.U8.QuickImport
{
    public class RegistryHelper
    {
        public const string IIsRegistry = "SOFTWARE\\Microsoft\\Internet Explorer\\MAIN\\FeatureControl\\FEATURE_IGNORE_ZONES_INITIALIZATION_FAILURE_KB945701";

        public const string IUSR_HKCU_Registry = "HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings\\Zones";

        public const string IUSR_HKLM_Registry = "HKLM\\Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings\\Zones";

        public const string IUSR_HKU_Registry = "HKU\\S-1-5-20\\Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings\\Zones";

        public const string U8RegPath = "Software\\UFSoft\\WF\\V8.700\\Install\\CurrentInstPath";

        public const string U8Version = "Software\\UFSoft\\WF\\V8.700\\Version";

        public static string GetRegValue(string szRegPath)
        {
            string text = ReadRegValue(szRegPath);
            if (text != null && text.Trim().Length > 0)
            {
                return text.Trim();
            }
            return null;
        }

        public static bool GetRegValue<T>(string key, string value, out T data)
        {
            return GetRegValue<T>(Registry.LocalMachine, key, value, RegistryValueKind.DWord, out data);
        }

        public static bool GetRegValue<T>(RegistryKey root, string key, string value, RegistryValueKind kind, out T data)
        {
            bool result = false;
            data = default(T);
            using (RegistryKey registryKey = root)
            {
                using (RegistryKey registryKey2 = registryKey.OpenSubKey(key, RegistryKeyPermissionCheck.ReadSubTree))
                {
                    if (registryKey2 != null)
                    {
                        try
                        {
                            if (registryKey2.GetValueKind(value) == kind)
                            {
                                object value2 = registryKey2.GetValue(value, null);
                                if (value2 != null)
                                {
                                    data = (T)Convert.ChangeType(value2, typeof(T), CultureInfo.InvariantCulture);
                                    result = true;
                                }
                            }
                        }
                        catch (IOException)
                        {
                        }
                    }
                    return result;
                }
            }
        }

        public static string GetU8EAIPath()
        {
            string u8Path = GetU8Path();
            if (u8Path != null)
            {
                return Path.Combine(u8Path, "EAI");
            }
            return null;
        }

        public static string GetU8Path()
        {
            return ReadRegValue("Software\\UFSoft\\WF\\V8.700\\Install\\CurrentInstPath", "");
        }

        public static string GetU8Version()
        {
            return ReadRegValue("Software\\UFSoft\\WF\\V8.700\\Version", "");
        }

        public static string ReadRegValue(string subKey)
        {
            try
            {
                using (RegistryKey registryKey = Registry.LocalMachine)
                {
                    using (RegistryKey registryKey2 = registryKey.OpenSubKey(subKey))
                    {
                        if (registryKey2 != null && registryKey2.GetValue(null) != null)
                        {
                            return (string)registryKey2.GetValue(null);
                        }
                    }
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public static string ReadRegValue(string subKey, string sValue)
        {
            try
            {
                using (RegistryKey registryKey = Registry.LocalMachine)
                {
                    using (RegistryKey registryKey2 = registryKey.OpenSubKey(subKey))
                    {
                        if (registryKey2 != null && registryKey2.GetValue(sValue) != null)
                        {
                            return (string)registryKey2.GetValue(sValue);
                        }
                    }
                }
                return null;
            }
            catch
            {
                return null;
            }
        }

        public static void SetRegistryRight(string user, string subKey)
        {
            if (string.IsNullOrEmpty(user) || string.IsNullOrEmpty(subKey))
            {
                return;
            }
            try
            {
                RegistrySecurity registrySecurity = new RegistrySecurity();
                RegistryAccessRule rule = new RegistryAccessRule(user, RegistryRights.ExecuteKey | RegistryRights.SetValue | RegistryRights.CreateSubKey | RegistryRights.Delete, InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow);
                registrySecurity.AddAccessRule(rule);
                rule = new RegistryAccessRule(user, RegistryRights.ChangePermissions, InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit | PropagationFlags.InheritOnly, AccessControlType.Allow);
                registrySecurity.AddAccessRule(rule);
                int num = subKey.IndexOf('\\');
                RegistryKey registryKey = null;
                switch (subKey.Substring(0, num).ToUpper())
                {
                    case "HKLM":
                        registryKey = Registry.LocalMachine;
                        break;
                    case "HKU":
                        registryKey = Registry.Users;
                        break;
                    case "HKCU":
                        registryKey = Registry.CurrentUser;
                        break;
                    case "HKCC":
                        registryKey = Registry.CurrentConfig;
                        break;
                    case "HKCR":
                        registryKey = Registry.ClassesRoot;
                        break;
                }
                if (registryKey != null)
                {
                    RegistryKey registryKey2 = registryKey.OpenSubKey(subKey.Substring(num + 1), true);
                    if (registryKey2 != null)
                    {
                        registryKey2.SetAccessControl(registrySecurity);
                        registryKey2.Close();
                    }
                    registryKey.Close();
                }
            }
            catch
            {
            }
        }

        public static void WriteRegValue(string subKey, string sName, string sValue)
        {
            WriteRegValue(subKey, sName, sValue, RegistryValueKind.DWord);
        }

        public static void WriteRegValue(string subKey, string sName, string sValue, RegistryValueKind kind)
        {
            try
            {
                using (RegistryKey registryKey = Registry.LocalMachine)
                {
                    using (RegistryKey registryKey2 = registryKey.OpenSubKey(subKey))
                    {
                        if (registryKey2 != null && sName != null && sValue != null)
                        {
                            registryKey2.SetValue(sName, sValue, kind);
                        }
                    }
                }
            }
            catch
            {
            }
        }
    }

}