﻿using Microsoft.Win32;

namespace Yonyou.U8.QuickImport
{
    public class SetExcelRegistryKey
    {
        public static void SetValue()
        {
            try
            {
                using (RegistryKey registryKey = Registry.LocalMachine)
                {
                    long voValue = -2147481088L;
                    string voKeyName = "BrowserFlags";
                    string[] array = new string[4] { "SOFTWARE\\Classes\\Excel.Sheet.8", "SOFTWARE\\Classes\\Excel.Sheet.12", "SOFTWARE\\Classes\\Excel.SheetMacroEnabled.12", "SOFTWARE\\Classes\\Excel.SheetBinaryMacroEnabled.12" };
                    foreach (string voKeyPath in array)
                    {
                        SetValue(registryKey, voKeyPath, voKeyName, voValue);
                    }
                    registryKey.Close();
                }
            }
            catch
            {
            }
        }

        public static void SetValue(RegistryKey voParentKey, string voKeyPath, string voKeyName, long voValue)
        {
            try
            {
                using (RegistryKey registryKey = voParentKey.OpenSubKey(voKeyPath, true))
                {
                    bool flag = false;
                    string[] valueNames = registryKey.GetValueNames();
                    foreach (string text in valueNames)
                    {
                        if (text == voKeyName)
                        {
                            flag = true;
                            break;
                        }
                    }
                    if (flag && registryKey.GetValue(voKeyName, "").ToString() != "-2147481088")
                    {
                        registryKey.SetValue(voKeyName, voValue, RegistryValueKind.DWord);
                    }
                    registryKey.Close();
                }
            }
            catch
            {
            }
        }
    }

}