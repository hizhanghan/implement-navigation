﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Yonyou.U8.QuickImport
{
    public partial class QueryLog : ControlBase
    {
        private XmlDocument ModelList;

        public QueryLog(Context context) : base(context)
        {
            InitializeComponent();
            InitialModel();
            float num = (float)Screen.PrimaryScreen.Bounds.Width / 800f;
            panel2.Width = (int)((float)panel2.Width * num);
            radList.Visible = false;
            radDetail.Visible = false;
            this.Load += QueryLog_Load;
            btnSave.Click += btnOk_Click;
        }

        private void InitialModel()
        {
            try
            {
                ModelList = new XmlDocument();
                ModelList.Load(base.QIContext.ExcelModelPath + "\\ModelList.xml");
                CommonUtil.ModelListFilter(ModelList);
                SuspendLayout();
                XmlNodeList xmlNodeList = ModelList.DocumentElement.SelectNodes("class");
                for (int i = 0; i < xmlNodeList.Count; i++)
                {
                    for (int j = 0; j < xmlNodeList.Item(i).ChildNodes.Count; j++)
                    {
                        chkListBox.Items.Add(xmlNodeList.Item(i).ChildNodes.Item(j).Attributes.GetNamedItem("name").Value);
                    }
                }
                ResumeLayout(false);
            }
            catch (Exception)
            {
            }
        }

        public void QueryToken(string strSQL)
        {
            TabPage tapPage;
            if (!base.QIContext.CheckTab("tpListLog", out tapPage))
            {
                tapPage = new TabPage("日志列表");
                tapPage.Name = "tpListLog";
                ListLog listLog = new ListLog(base.QIContext);
                listLog.Name = "ListLog";
                listLog.Tag = base.Parent;
                listLog.Dock = DockStyle.Fill;
                listLog.SetQueryXML(strSQL, 1);
                tapPage.Controls.Add(listLog);
                base.QIContext.TabContainer.TabPages.Add(tapPage);
            }
            else if (tapPage != null)
            {
                ListLog listLog2 = tapPage.Controls[0] as ListLog;
                listLog2.SetQueryXML(strSQL, 1);
            }
            if (tapPage != null)
            {
                base.QIContext.TabContainer.SelectedTab = tapPage;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml("<data><time start=\"\" end=\"\"/><user name=\"\"/><log type=\"\"/></data>");
            string text = "";
            if (startDate.Value.CompareTo(endDate.Value) > 0)
            {
                MessageBox.Show("开始日期不能大于结束日期，请重新设置日期区间!");
                return;
            }
            DateTime value = startDate.Value;
            text = "where importtime>='" + startDate.Value.ToString(startDate.CustomFormat) + "'";
            DateTime value2 = startDate.Value;
            text = text + " and importtime<='" + endDate.Value.ToString(endDate.CustomFormat) + "'";
            if (cboName.SelectedItem != null && cboName.SelectedItem.ToString().Trim() != "")
            {
                text = text + " and username='" + cboName.SelectedItem.ToString().Trim() + "'";
            }
            int queryType = (radList.Checked ? 1 : 2);
            if (chkListBox.CheckedItems.Count != 0)
            {
                text += " and type in(";
                for (int i = 0; i <= chkListBox.CheckedItems.Count - 1; i++)
                {
                    if (ModelList.SelectSingleNode("//menu[@name='" + chkListBox.CheckedItems[i].ToString() + "']") != null)
                    {
                        XmlNode xmlNode = ModelList.SelectSingleNode("//menu[@name='" + chkListBox.CheckedItems[i].ToString() + "']");
                        text = text + "'" + xmlNode.Attributes.GetNamedItem("id").Value + "',";
                    }
                }
                text = text.Substring(0, text.Length - 1);
                text += ") ";
            }
            text += " order by 编号,token,username,type";
            TabPage tapPage;
            if (!base.QIContext.CheckTab("tpListLog", out tapPage))
            {
                tapPage = new TabPage("日志列表");
                tapPage.Name = "tpListLog";
                ListLog listLog = new ListLog(base.QIContext);
                listLog.Name = "ListLog";
                listLog.Tag = base.Parent;
                listLog.Dock = DockStyle.Fill;
                listLog.SetQueryXML(text, queryType);
                tapPage.Controls.Add(listLog);
                base.QIContext.TabContainer.TabPages.Add(tapPage);
            }
            else if (tapPage != null)
            {
                ListLog listLog2 = tapPage.Controls[0] as ListLog;
                listLog2.SetQueryXML(text, 1);
            }
            if (tapPage != null)
            {
                base.QIContext.TabContainer.SelectedTab = tapPage;
            }
        }

        private void ShowThis(object sender, EventArgs e)
        {
            base.Visible = true;
        }

        private void QueryLog_Load(object sender, EventArgs e)
        {
            string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Context.QIPath + "\\log\\loglist.mdb;";
            OleDbConnection oleDbConnection = new OleDbConnection(connectionString);
            oleDbConnection.Open();
            OleDbCommand selectCommand = new OleDbCommand("SELECT DISTINCT username FROM log ", oleDbConnection);
            OleDbDataAdapter oleDbDataAdapter = new OleDbDataAdapter();
            oleDbDataAdapter.SelectCommand = selectCommand;
            DataSet dataSet = new DataSet();
            dataSet.DataSetName = "ufinterface";
            oleDbDataAdapter.Fill(dataSet, "LogList");
            DataTable dataTable = dataSet.Tables["LogList"];
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                cboName.Items.Insert(i, dataTable.Rows[i].ItemArray[0].ToString());
            }
            cboName.Items.Insert(dataTable.Rows.Count, "");
            cboName.SelectedIndex = dataTable.Rows.Count;
            oleDbConnection.Close();
            startDate.Value = DateTime.Parse(DateTime.Now.ToString(startDate.CustomFormat));
            endDate.Value = startDate.Value;
        }

        private ArrayList ReadDeleteServer()
        {
            ArrayList arrayList = new ArrayList();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(Context.QIPath + "\\Xml\\ImportConfig.xml");
            XmlNodeList xmlNodeList = xmlDocument.SelectNodes("//ServerRegion/*[@delete='true']");
            foreach (XmlNode item in xmlNodeList)
            {
                arrayList.Add(item.Name);
            }
            return arrayList;
        }

    }
}
