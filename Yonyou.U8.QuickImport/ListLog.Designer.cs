﻿namespace Yonyou.U8.QuickImport
{
    partial class ListLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdPrint = new System.Windows.Forms.Button();
            this.cmdQuery = new System.Windows.Forms.Button();
            this.cmdDelete = new System.Windows.Forms.Button();
            this.gridContainer = new System.Windows.Forms.Panel();
            this.logGrid = new System.Windows.Forms.DataGridView();
            this.ColumnNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnSucceed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFail = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnUser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnToken = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnOperateType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDelete = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.panel1.SuspendLayout();
            this.gridContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmdCancel);
            this.panel1.Controls.Add(this.cmdPrint);
            this.panel1.Controls.Add(this.cmdQuery);
            this.panel1.Controls.Add(this.cmdDelete);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 382);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(704, 48);
            this.panel1.TabIndex = 0;
            // 
            // cmdCancel
            // 
            this.cmdCancel.Location = new System.Drawing.Point(646, 12);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(64, 22);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "退出";
            this.cmdCancel.UseVisualStyleBackColor = true;
            // 
            // cmdPrint
            // 
            this.cmdPrint.Location = new System.Drawing.Point(434, 12);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(64, 22);
            this.cmdPrint.TabIndex = 2;
            this.cmdPrint.Text = "打印";
            this.cmdPrint.UseVisualStyleBackColor = true;
            // 
            // cmdQuery
            // 
            this.cmdQuery.Location = new System.Drawing.Point(505, 12);
            this.cmdQuery.Name = "cmdQuery";
            this.cmdQuery.Size = new System.Drawing.Size(64, 22);
            this.cmdQuery.TabIndex = 1;
            this.cmdQuery.Text = "查询";
            this.cmdQuery.UseVisualStyleBackColor = true;
            // 
            // cmdDelete
            // 
            this.cmdDelete.Location = new System.Drawing.Point(575, 12);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(64, 22);
            this.cmdDelete.TabIndex = 0;
            this.cmdDelete.Text = "删除";
            this.cmdDelete.UseVisualStyleBackColor = true;
            // 
            // gridContainer
            // 
            this.gridContainer.Controls.Add(this.logGrid);
            this.gridContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridContainer.Location = new System.Drawing.Point(0, 0);
            this.gridContainer.Name = "gridContainer";
            this.gridContainer.Size = new System.Drawing.Size(704, 382);
            this.gridContainer.TabIndex = 1;
            // 
            // logGrid
            // 
            this.logGrid.AllowUserToAddRows = false;
            this.logGrid.AllowUserToDeleteRows = false;
            this.logGrid.BackgroundColor = System.Drawing.Color.White;
            this.logGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.logGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnNum,
            this.ColumnTime,
            this.ColumnType,
            this.ColumnName,
            this.ColumnFile,
            this.ColumnSucceed,
            this.ColumnFail,
            this.ColumnUser,
            this.ColumnToken,
            this.ColumnOperateType,
            this.ColumnDelete});
            this.logGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logGrid.Location = new System.Drawing.Point(0, 0);
            this.logGrid.Name = "logGrid";
            this.logGrid.RowTemplate.Height = 23;
            this.logGrid.Size = new System.Drawing.Size(704, 382);
            this.logGrid.TabIndex = 0;
            // 
            // ColumnNum
            // 
            this.ColumnNum.DataPropertyName = "num";
            this.ColumnNum.HeaderText = "编号";
            this.ColumnNum.Name = "ColumnNum";
            this.ColumnNum.ReadOnly = true;
            this.ColumnNum.Width = 80;
            // 
            // ColumnTime
            // 
            this.ColumnTime.DataPropertyName = "importtime";
            this.ColumnTime.HeaderText = "操作时间";
            this.ColumnTime.Name = "ColumnTime";
            this.ColumnTime.ReadOnly = true;
            // 
            // ColumnType
            // 
            this.ColumnType.DataPropertyName = "type";
            this.ColumnType.HeaderText = "type";
            this.ColumnType.Name = "ColumnType";
            this.ColumnType.ReadOnly = true;
            this.ColumnType.Visible = false;
            // 
            // ColumnName
            // 
            this.ColumnName.DataPropertyName = "name";
            this.ColumnName.HeaderText = "数据项";
            this.ColumnName.Name = "ColumnName";
            this.ColumnName.ReadOnly = true;
            this.ColumnName.Width = 150;
            // 
            // ColumnFile
            // 
            this.ColumnFile.HeaderText = "filename";
            this.ColumnFile.Name = "ColumnFile";
            this.ColumnFile.ReadOnly = true;
            this.ColumnFile.Visible = false;
            this.ColumnFile.Width = 200;
            // 
            // ColumnSucceed
            // 
            this.ColumnSucceed.DataPropertyName = "succeeded";
            this.ColumnSucceed.HeaderText = "操作成功记录数";
            this.ColumnSucceed.Name = "ColumnSucceed";
            this.ColumnSucceed.ReadOnly = true;
            this.ColumnSucceed.Width = 120;
            // 
            // ColumnFail
            // 
            this.ColumnFail.DataPropertyName = "failed";
            this.ColumnFail.HeaderText = "操作不成功记录数";
            this.ColumnFail.Name = "ColumnFail";
            this.ColumnFail.ReadOnly = true;
            this.ColumnFail.Width = 130;
            // 
            // ColumnUser
            // 
            this.ColumnUser.DataPropertyName = "username";
            this.ColumnUser.HeaderText = "操作员";
            this.ColumnUser.Name = "ColumnUser";
            this.ColumnUser.ReadOnly = true;
            // 
            // ColumnToken
            // 
            this.ColumnToken.DataPropertyName = "token";
            this.ColumnToken.HeaderText = "token";
            this.ColumnToken.Name = "ColumnToken";
            this.ColumnToken.ReadOnly = true;
            this.ColumnToken.Visible = false;
            // 
            // ColumnOperateType
            // 
            this.ColumnOperateType.DataPropertyName = "ImportType";
            this.ColumnOperateType.HeaderText = "操作类型";
            this.ColumnOperateType.Name = "ColumnOperateType";
            this.ColumnOperateType.ReadOnly = true;
            // 
            // ColumnDelete
            // 
            this.ColumnDelete.DataPropertyName = "DeleteTag";
            this.ColumnDelete.HeaderText = "数据已删除";
            this.ColumnDelete.Name = "ColumnDelete";
            this.ColumnDelete.ReadOnly = true;
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // ListLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 430);
            this.Controls.Add(this.gridContainer);
            this.Controls.Add(this.panel1);
            this.Name = "ListLog";
            this.Text = "日志列表";
            this.panel1.ResumeLayout(false);
            this.gridContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.logGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdPrint;
        private System.Windows.Forms.Button cmdQuery;
        private System.Windows.Forms.Button cmdDelete;
        private System.Windows.Forms.Panel gridContainer;
        private System.Windows.Forms.DataGridView logGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnSucceed;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFail;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnToken;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnOperateType;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDelete;
        private System.Windows.Forms.PrintDialog printDialog1;
    }
}