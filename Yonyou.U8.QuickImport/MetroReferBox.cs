﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Yonyou.U8.QuickImport
{
    public partial class MetroReferBox : UserControl
    {
        private string filterType;
        private bool readOnly = true;
        public EventHandler<ReferBoxEventArgs> _RefClick;
       
        //private TableLayoutPanel tlpBackgroud;
        //private TableLayoutPanel tlpLayout;
        //private TableLayoutPanel tlpItemLayout;
        //private TextBox tboxInput;
        //private PictureBox pBoxLocation;
        public event EventHandler<ReferBoxEventArgs> RefClick
        {
            add
            {
                this._RefClick = (EventHandler<ReferBoxEventArgs>)Delegate.Combine(this._RefClick, value);
            }
            remove
            {
                this._RefClick = (EventHandler<ReferBoxEventArgs>)Delegate.Remove(this._RefClick, value);
            }
        }
        [Category("Metro")]
        public Color BorderColor
        {
            get
            {
                return tlpBackgroud.BackColor;
            }
            set
            {
                tlpBackgroud.BackColor = value;
                base.Invalidate();
            }
        }
        [Category("Metro")]
        public override string Text
        {
            get
            {
                return tboxInput.Text;
            }
            set
            {
                tboxInput.Text = value;
            }
        }
        [Category("Metro")]
        public string FilterType
        {
            get
            {
                return filterType;
            }
        }
        [Category("Metro")]
        public bool ReadOnly
        {
            get
            {
                return this.readOnly;
            }
            set
            {
                this.readOnly = value;
                if (value)
                {
                    this.tboxInput.ReadOnly = true;
                    return;
                }
                this.tboxInput.ReadOnly = false;
            }
        }
        public MetroReferBox()
        {
            this.InitializeComponent();
            this.SetDefaultProperty();
            this.tboxInput.Text = string.Empty;
        }
        public MetroReferBox(string filterType)
            : this()
        {
            this.filterType = filterType;
            this.tboxInput.Text = string.Empty;
        }
        private void SetDefaultProperty()
        {
            this.tlpBackgroud.BackColor = Color.FromArgb(77, 96, 130);
            this.tlpItemLayout.BackColor = Color.White;
            this.tlpLayout.BackColor = Color.White;
            base.Height = 24;
        }
        private void Locate(object sender, EventArgs e)
        {
            ReferBoxEventArgs referBoxEventArgs = new ReferBoxEventArgs();
            this.OnRefClick(referBoxEventArgs);
            this.tboxInput.Text = referBoxEventArgs.Text;
            base.Tag = referBoxEventArgs.Tag;
        }
        protected virtual void OnRefClick(ReferBoxEventArgs e)
        {
            if (this._RefClick != null)
            {
                this._RefClick(this, e);
            }
        }
        //private void InitializeComponent()
        //{
            //this.tlpBackgroud = new TableLayoutPanel();
            //this.tlpLayout = new TableLayoutPanel();
            //this.tlpItemLayout = new TableLayoutPanel();
            //this.tboxInput = new TextBox();
            //this.pBoxLocation = new PictureBox();
            //this.tlpBackgroud.SuspendLayout();
            //this.tlpLayout.SuspendLayout();
            //this.tlpItemLayout.SuspendLayout();
            //((ISupportInitialize)this.pBoxLocation).BeginInit();
            //base.SuspendLayout();
            //this.tlpBackgroud.ColumnCount = 1;
            //this.tlpBackgroud.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
            //this.tlpBackgroud.Controls.Add(this.tlpLayout, 0, 0);
            //this.tlpBackgroud.Dock = DockStyle.Fill;
            //this.tlpBackgroud.Location = new Point(0, 0);
            //this.tlpBackgroud.Margin = new Padding(0);
            //this.tlpBackgroud.Name = "tlpBackgroud";
            //this.tlpBackgroud.RowCount = 1;
            //this.tlpBackgroud.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
            //this.tlpBackgroud.Size = new Size(181, 29);
            //this.tlpBackgroud.TabIndex = 0;
            //this.tlpLayout.ColumnCount = 1;
            //this.tlpLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
            //this.tlpLayout.Controls.Add(this.tlpItemLayout, 0, 0);
            //this.tlpLayout.Dock = DockStyle.Fill;
            //this.tlpLayout.Location = new Point(1, 1);
            //this.tlpLayout.Margin = new Padding(1);
            //this.tlpLayout.Name = "tlpLayout";
            //this.tlpLayout.RowCount = 1;
            //this.tlpLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
            //this.tlpLayout.Size = new Size(179, 27);
            //this.tlpLayout.TabIndex = 0;
            //this.tlpItemLayout.ColumnCount = 5;
            //this.tlpItemLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 2f));
            //this.tlpItemLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
            //this.tlpItemLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 2f));
            //this.tlpItemLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 14f));
            //this.tlpItemLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 2f));
            //this.tlpItemLayout.Controls.Add(this.tboxInput, 1, 1);
            //this.tlpItemLayout.Controls.Add(this.pBoxLocation, 3, 1);
            //this.tlpItemLayout.Cursor = Cursors.Hand;
            //this.tlpItemLayout.Dock = DockStyle.Fill;
            //this.tlpItemLayout.Location = new Point(1, 1);
            //this.tlpItemLayout.Margin = new Padding(1);
            //this.tlpItemLayout.Name = "tlpItemLayout";
            //this.tlpItemLayout.RowCount = 3;
            //this.tlpItemLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
            //this.tlpItemLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 14f));
            //this.tlpItemLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
            //this.tlpItemLayout.Size = new Size(177, 25);
            //this.tlpItemLayout.TabIndex = 0;
            //this.tboxInput.BackColor = Color.White;
            //this.tboxInput.BorderStyle = BorderStyle.None;
            //this.tboxInput.Dock = DockStyle.Fill;
            //this.tboxInput.Location = new Point(2, 5);
            //this.tboxInput.Margin = new Padding(0);
            //this.tboxInput.Name = "tboxInput";
            //this.tboxInput.Size = new Size(157, 14);
            //this.tboxInput.TabIndex = 0;
            //this.pBoxLocation.Dock = DockStyle.Fill;
            //this.pBoxLocation.Image = Resources.reftool;
            //this.pBoxLocation.Location = new Point(162, 6);
            //this.pBoxLocation.Margin = new Padding(1);
            //this.pBoxLocation.Name = "pBoxLocation";
            //this.pBoxLocation.Size = new Size(12, 12);
            //this.pBoxLocation.TabIndex = 1;
            //this.pBoxLocation.TabStop = false;
            //this.pBoxLocation.Click += new EventHandler(this.Locate);
            //base.AutoScaleDimensions = new SizeF(6f, 12f);
            //base.AutoScaleMode = AutoScaleMode.Font;
            //this.BackColor = Color.Transparent;
            //base.Controls.Add(this.tlpBackgroud);
            //base.Name = "MetroReferBox";
            //base.Size = new Size(181, 29);
            //this.tlpBackgroud.ResumeLayout(false);
            //this.tlpLayout.ResumeLayout(false);
            //this.tlpItemLayout.ResumeLayout(false);
            //this.tlpItemLayout.PerformLayout();
            //((ISupportInitialize)this.pBoxLocation).EndInit();
            //base.ResumeLayout(false);
        //}
    }
}
