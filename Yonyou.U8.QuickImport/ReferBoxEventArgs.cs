﻿using System;

namespace Yonyou.U8.QuickImport
{
    public class ReferBoxEventArgs : EventArgs
    {
        public string Text { get; set; }
        public object Tag { get; set; }
    }
}