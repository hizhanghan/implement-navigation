﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-05-27 18:23:07
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System.Windows.Forms;
using YonYou.U8.IN.Framework;

namespace Yonyou.U8.QuickImport
{
    public class ToolsManagerLoaderControl : INetControl
    {
        private static Control _tmCtrl;

        public bool ShowStatusBar
        {
            get
            {
                return false;
            }
        }

        public Control CreateControl(BizContext context)
        {
            if (_tmCtrl == null)
            {
                ToolsManagerFrm toolsManagerFrm = new ToolsManagerFrm();
                toolsManagerFrm.Dock = DockStyle.Fill;
                _tmCtrl = toolsManagerFrm;
            }
            return _tmCtrl;
        }
    }
}