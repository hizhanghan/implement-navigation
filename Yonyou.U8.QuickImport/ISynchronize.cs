﻿using System.Runtime.Remoting.Contexts;

namespace Yonyou.U8.QuickImport
{
    public interface ISynchronize
    {
        void Synchronize(Context context, params object[] args);
    }

}