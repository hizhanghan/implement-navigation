﻿using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using System;
using YonYou.U8.IN.Framework;
using U8Login;

namespace Yonyou.U8.QuickImport
{
    public class Context
    {
        private string _AccountNo;

        private clsLoginClass _Login;

        private string _QICurImportStrategy;

        private string _QICurImportStrategyFilePath;

        private string _QICurrentModel;

        private static string _QIPath = string.Empty;

        private static string _U8Path = string.Empty;

        public string AccountNo
        {
            get
            {
                if (string.IsNullOrEmpty(_AccountNo) && _Login != null)
                {
                    string[] array = _Login.DataSource.Split("@".ToArray());
                    _AccountNo = array[array.Length - 1];
                }
                return _AccountNo;
            }
        }

        public string ExcelModelPath
        {
            get
            {
                return Path.Combine(U8Path, "ExcelModel");
            }
        }

        public Form FrmMdiParent { get; set; }

        public clsLoginClass Login
        {
            get
            {
                return _Login;
            }
            set
            {
                _Login = value;
            }
        }

        public string QICurImportStrategy
        {
            get
            {
                if (string.IsNullOrEmpty(_QICurImportStrategy))
                {
                    string text = (_QICurImportStrategy = Path.Combine(Path.Combine(Path.Combine(QIModel, Server), AccountNo), "ImportStrategy"));
                }
                return _QICurImportStrategy;
            }
        }

        public string QICurImportStrategyFilePath
        {
            get
            {
                if (string.IsNullOrEmpty(_QICurImportStrategyFilePath))
                {
                    _QICurImportStrategyFilePath = Path.Combine(QICurImportStrategy, "FilePath");
                }
                return _QICurImportStrategyFilePath;
            }
        }

        public string QICurrentModelPath
        {
            get
            {
                if (string.IsNullOrEmpty(_QICurrentModel))
                {
                    string path = Path.Combine(Path.Combine(QIModel, Server), AccountNo);
                    _QICurrentModel = Path.Combine(path, "Excel");
                }
                return _QICurrentModel;
            }
        }

        public string QIImportStrategy
        {
            get
            {
                return Path.Combine(QIPath, "ImportStrategy");
            }
        }

        public string QIImportStrategyFilePath
        {
            get
            {
                return Path.Combine(QIImportStrategy, "FilePath");
            }
        }

        private string QIModel
        {
            get
            {
                return Path.Combine(QIPath, "Model");
            }
        }

        public static string QIPath
        {
            get
            {
                if (string.IsNullOrEmpty(_QIPath))
                {
                    _QIPath = Path.Combine(U8Path, "QuickImport");
                }
                return _QIPath;
            }
        }

        public string Server
        {
            get
            {
                string result = string.Empty;
                if (_Login != null)
                {
                    result = _Login.dbServerName;
                }
                return result;
            }
        }

        public TabControl TabContainer { get; set; }

        public static string U8Path
        {
            get
            {
                if (string.IsNullOrEmpty(_U8Path))
                {
                    _U8Path = RegistryHelper.GetU8Path();
                }
                return _U8Path;
            }
        }

        public string UserId
        {
            get
            {
                string result = string.Empty;
                if (_Login != null)
                {
                    result = _Login.cUserId;
                }
                return result;
            }
        }

        public string SolutionNameInUsed { get; set; }

        public Form WorkBenchForm
        {
            get
            {
                return ViewAdvisor.Instance.MainForm;
            }
        }

        private Context()
        {
            _AccountNo = string.Empty;
            _QICurImportStrategy = string.Empty;
            _QICurrentModel = string.Empty;
            _QICurImportStrategyFilePath = string.Empty;
        }

        public Context(object loginObj)
            : this()
        {
            clsLoginClass clsLoginClass = loginObj as clsLoginClass;
            if (clsLoginClass == null)
            {
                throw new ArgumentNullException("login");
            }
            _Login = clsLoginClass;
        }

        public Context(clsLoginClass login)
            : this()
        {
            if (login == null)
            {
                throw new ArgumentNullException("login");
            }
            _Login = login;
        }

        public bool CheckTab(string strfrmName, out TabPage tapPage)
        {
            tapPage = null;
            foreach (TabPage tabPage in TabContainer.TabPages)
            {
                if (tabPage.Name == strfrmName)
                {
                    tapPage = tabPage;
                    return true;
                }
            }
            return false;
        }

        public void DeleteData(string strategyXml)
        {
            string querytoken = "";
            bool flag = new DataImport(this).transimport(strategyXml, Login, ref querytoken, FrmMdiParent);
            string caption = "";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            TabPage tapPage;
            if (!CheckTab("tpImportLog", out tapPage))
            {
                return;
            }
            DialogResult dialogResult;
            if (flag)
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(strategyXml);
                XmlNodeList xmlNodeList = xmlDocument.SelectNodes("//menu");
                OleDbConnection oleDbConnection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + QIPath + "\\log\\loglist.mdb;");
                oleDbConnection.Open();
                OleDbCommand oleDbCommand = new OleDbCommand();
                oleDbCommand.Connection = oleDbConnection;
                OleDbCommand oleDbCommand2 = oleDbCommand;
                foreach (XmlNode item in xmlNodeList)
                {
                    oleDbCommand2.CommandText = "update log set deletetag = 1 where 编号=" + item.Attributes["number"].InnerText;
                    oleDbCommand2.ExecuteNonQuery();
                }
                oleDbConnection.Close();
                dialogResult = MessageBox.Show(TabContainer, "本次删除全部成功，是否查看本次的删除日志?", caption, buttons, MessageBoxIcon.Question);
            }
            else
            {
                dialogResult = MessageBox.Show(TabContainer, "本次删除有部分错误，是否查看本次的删除日志?", caption, buttons, MessageBoxIcon.Question);
            }
            if (dialogResult != DialogResult.Yes)
            {
                return;
            }
            QueryLog queryLog = null;
            if (tapPage != null)
            {
                if (tapPage.Controls.Count > 0)
                {
                    queryLog = tapPage.Controls[0] as QueryLog;
                }
                else
                {
                    QueryLog queryLog2 = new QueryLog(this);
                    queryLog2.Dock = DockStyle.Fill;
                    queryLog = queryLog2;
                    tapPage.Controls.Add(queryLog);
                }
            }
            else
            {
                tapPage = new TabPage();
                QueryLog queryLog3 = new QueryLog(this);
                queryLog3.Dock = DockStyle.Fill;
                queryLog = queryLog3;
                tapPage.Controls.Add(queryLog);
                TabContainer.TabPages.Add(tapPage);
            }
            if (tapPage != null)
            {
                TabContainer.SelectedTab = tapPage;
            }
            if (queryLog != null)
            {
                queryLog.QueryToken(" where token='" + querytoken + "'");
            }
        }
    }
}