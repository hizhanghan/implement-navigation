﻿namespace Yonyou.U8.QuickImport
{
    public delegate void InitialRows(int IntInitialRows, string StrDocName);
    public delegate void SetValue(int IntValue);

    public interface IDataImport
    {
        bool ArchIsEmpty { get; }

        event InitialRows InitSetup;

        event SetValue ValueSet;

        string[] transform(string type, string filename, string proctype);

        void logWrite(string logfilename, string[,] strLog, string[] rxmlstr, string id, string procType);
    }
}