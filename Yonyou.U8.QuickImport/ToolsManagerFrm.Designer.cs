﻿namespace Yonyou.U8.QuickImport
{
    partial class ToolsManagerFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolsManager = new Yonyou.U8.QuickImport.ToolsManager();
            this.SuspendLayout();
            // 
            // toolsManager
            // 
            this.toolsManager.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolsManager.Location = new System.Drawing.Point(0, 0);
            this.toolsManager.Name = "toolsManager";
            this.toolsManager.Size = new System.Drawing.Size(776, 400);
            this.toolsManager.TabIndex = 0;
            // 
            // ToolsManagerFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 364);
            this.Controls.Add(this.toolsManager);
            this.Name = "ToolsManagerFrm";
            this.Text = "ToolsManagerFrm";
            this.ResumeLayout(false);

        }

        #endregion

        private ToolsManager toolsManager;
    }
}