﻿using System.Collections.Generic;
using System.ComponentModel;
using System;
using YonYou.U8.IN.Framework;
using System.Windows.Forms;
using System.Diagnostics;

namespace YonYou.U8.IN.BizNavigationDesigner
{
    public class NetLoaderControl : INetControl
    {
        private static Control _NetControl;

        public bool ShowStatusBar
        {
            get
            {
                return false;
            }
        }

        public Control CreateControl(BizContext context)
        {
            if (_NetControl == null)
            {
                RequestPatchServer();
            }
            Designer designer = new Designer();
            designer.ShowInTaskbar = false;
            designer.TopLevel = false;
            designer.FormBorderStyle = FormBorderStyle.None;
            designer.Dock = DockStyle.Fill;
            _NetControl = designer;
            ViewAdvisor.Instance.Exiting += Instance_Exiting;
            ViewAdvisor.Instance.PageSwitching += Instance_PageSwitching;
            return _NetControl;
        }

        private void RequestPatchServer()
        {
            try
            {
                //PatchAsyncClientService patchAsyncClientService = new PatchAsyncClientService();
                //patchAsyncClientService.RunPatchProcessor(BizNavContext.Instance.LoginEntity.UserToken);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[业务向导设计]-[NetLoaderControl->RequestPatchServer]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
            }
        }

        private void Instance_Exiting(object sender, CancelEventArgs e)
        {
            //if (BizNavContext.Instance.NoAuthSet.Contains(AuthType.EdieScene) || (!BizNavContext.Instance.IsHtmlModify && !BizNavContext.Instance.IsVisioChanged))
            //{
            //    return;
            //}
            //if (MessageBox.Show("是否保存已修改内容", "保存", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            //{
            //    if (Convert.ToBoolean(new SaveCommand().Execute(null)))
            //    {
            //        e.Cancel = false;
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //    }
            //}
            //else
            //{
            //    e.Cancel = false;
            //}
        }

        private void Instance_PageSwitching(object sender, CancelEventArgs e)
        {
            //if (BizNavContext.Instance.NoAuthSet.Contains(AuthType.EdieScene) || (!BizNavContext.Instance.IsHtmlModify && !BizNavContext.Instance.IsVisioChanged))
            //{
            //    return;
            //}
            //if (MessageBox.Show("是否保存已修改内容", "保存", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            //{
            //    if (Convert.ToBoolean(new SaveCommand().Execute(null)))
            //    {
            //        e.Cancel = false;
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //    }
            //    return;
            //}
            //SceneEntity sceneEntity = BizNavContext.Instance.SceneEntity;
            //BizNavContext.Instance.IsVisioChanged = false;
            //BizNavContext.Instance.IsHtmlModify = false;
            //if (sceneEntity != null)
            //{
            //    IEditor htmlEditor = BizNavContext.Instance.HtmlEditor;
            //    IEditor visioEditor = BizNavContext.Instance.VisioEditor;
            //    BizFileInfo htmlFileInfo = (sceneEntity.Html = htmlEditor.ExcuteCommand<LoadHtmlCommand>(new object[1] { sceneEntity }) as BizFileInfo);
            //    BizNavContext.Instance.HtmlFileInfo = htmlFileInfo;
            //    if (sceneEntity.Type == SceneType.Separate)
            //    {
            //        htmlFileInfo = (sceneEntity.Visio = visioEditor.ExcuteCommand<LoadVisioCommand>(new object[1] { sceneEntity }) as BizFileInfo);
            //        BizNavContext.Instance.VisioFileInfo = htmlFileInfo;
            //        SceneStateControl.Separate();
            //    }
            //    else
            //    {
            //        BizNavContext.Instance.VisioFileInfo = null;
            //        SceneStateControl.Entirety();
            //    }
            //    BizNavContext.Instance.SceneEntity = sceneEntity;
            //    LinkRepository linkRepository = new LinkRepository(BizNavContext.Instance.SystemConnectionStr);
            //    if (sceneEntity.LinkCache == null && !string.IsNullOrEmpty(sceneEntity.VisioId))
            //    {
            //        sceneEntity.LinkCache = linkRepository.QueryByFileID(sceneEntity.VisioId);
            //    }
            //    if (sceneEntity.LinkCache != null)
            //    {
            //        BizNavContext.Instance.LinkCache = new List<LinkEntity>(sceneEntity.LinkCache);
            //    }
            //    else
            //    {
            //        BizNavContext.Instance.LinkCache = new List<LinkEntity>();
            //    }
            //    BookMarkRepository bookMarkRepository = new BookMarkRepository(BizNavContext.Instance.SystemConnectionStr);
            //    if (sceneEntity.BookMarkCache == null && !string.IsNullOrEmpty(sceneEntity.VisioId))
            //    {
            //        sceneEntity.BookMarkCache = bookMarkRepository.Query(sceneEntity.VisioId);
            //    }
            //    if (sceneEntity.BookMarkCache != null)
            //    {
            //        BizNavContext.Instance.BookMarkCache = new List<BookMarkEntity>(sceneEntity.BookMarkCache);
            //    }
            //    else
            //    {
            //        BizNavContext.Instance.BookMarkCache = new List<BookMarkEntity>();
            //    }
            //    e.Cancel = false;
            //}
        }
    }
}