﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YonYou.U8.IN.Framework;

namespace YonYou.U8.IN.BizNavigationDesigner
{
    public class BizWizardLoader : NetLoader
    {
        public override bool IsEmbed
        {
            get
            {
                return true;
            }
        }

        public override bool Load(BizContext context, string cMenuId, string subMenuId)
        {
            //BizNavContext.Instance.LoginEntity = context.LoginEntity;
            NetLoaderControl control = new NetLoaderControl();
            ShowEmbedControl(control, cMenuId, true);
            return false;
        }
    }

}
