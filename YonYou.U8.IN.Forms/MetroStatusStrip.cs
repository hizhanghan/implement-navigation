﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms
{
	/// <summary>
	/// 状态栏
	/// </summary>
	public class MetroStatusStrip : StatusStrip
	{
		public MetroStatusStrip()
		{
			this.AutoSize = false;
			base.Height = 22;
			base.Renderer = new StatusRenderer();
			base.GripStyle = ToolStripGripStyle.Hidden;
			base.SizingGrip = false;
			base.BackColor = Color.FromArgb(0, 122, 204);
		}
	}
}
