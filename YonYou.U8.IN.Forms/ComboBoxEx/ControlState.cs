﻿namespace YonYou.U8.IN.Forms
{
	public enum ControlState
	{
		Normal,
		Hover,
		Pressed,
		Focused
	}
}