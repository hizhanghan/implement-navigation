using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Drawing;

namespace Wade.Forms
{
    /// <summary>
    /// Summary description for MultiColumnComboBox.
    /// </summary>
    public delegate void AfterSelectEventHandler();
	public class MultiColumnComboBox : ComboBoxEx
    {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;
		private DataRow selectedRow = null;
		private string displayMember = "";
		private string displayValue = "";
		private DataTable dataTable = null;
		private DataRow[] dataRows = null;
		private string[] columnsToDisplay = null;
		public event AfterSelectEventHandler AfterSelectEvent;

		public MultiColumnComboBox(IContainer container)
		{
			/// <summary>
			/// Required for Windows.Forms Class Composition Designer support
			/// </summary>
			container.Add(this);
			InitializeComponent();
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public MultiColumnComboBox()
		{
			InitializeComponent();
		}

		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new Container();
		}
		#endregion

		protected override void OnDropDown(EventArgs e){
			Form parent = this.FindForm();
			if(this.dataTable != null || this.dataRows!= null){
				MultiColumnComboPopup popup = new MultiColumnComboPopup(this.dataTable,ref this.selectedRow,columnsToDisplay);
				popup.AfterRowSelectEvent+=new AfterRowSelectEventHandler(MultiColumnComboBox_AfterSelectEvent);
				popup.Location = new Point(parent.Left + this.Left + 4 ,parent.Top + this.Bottom + this.Height);
				popup.Show();
				if(popup.SelectedRow!=null){
					try{
						this.selectedRow = popup.SelectedRow;
						this.displayValue = popup.SelectedRow[this.displayMember].ToString();
						this.Text = this.displayValue;
					}
					catch(Exception ex) 
					{

						MessageBox.Show(ex.Message,"Error");	
					}
				}
				if(AfterSelectEvent!=null)
					AfterSelectEvent();
			}
			base.OnDropDown(e);
		}

		private void MultiColumnComboBox_AfterSelectEvent(object sender, DataRow drow){
			try{
				if(drow!=null){
					this.Text = drow[displayMember].ToString();
				}
			}catch(Exception exp){
				MessageBox.Show(this,exp.Message,"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
		}
		[Category("Wade")]
		public DataRow SelectedRow{
			get{
				return selectedRow;
			}
		}
		
		[Category("Wade")]
		public string DisplayValue{
			get{
				return displayValue;
			}
		}
		
		[Category("Wade")]
		public new string DisplayMember{
			set{
				displayMember = value;
			}
		}

		[Category("Wade")]
		public DataTable Table{
			set{
				dataTable = value;
				if(dataTable==null)
					return;
				selectedRow=dataTable.NewRow();
			}
		}

		[Category("Wade")]
		public DataRow[] Rows{
			set{
				dataRows = value;
			}
		}

		[Category("Wade")]
		[Description("��ʾ����")]
		public string[] ColumnsToDisplay{
			set{
				columnsToDisplay = value;
			}
		}
	}
}
