﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms
{
	public class SubWindow : NativeWindow
	{
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern int MapWindowPoints(IntPtr hWndFrom, IntPtr hWndTo, [In][Out] SubWindow.POINT pt, int cPoints);

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int SendMessage(IntPtr hWnd, int msg, int wParam, int lParam);

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr SendMessage(IntPtr hWnd, int msg, int wParam, StringBuilder lParam);
		public SubWindow()
		{
			this.m_Index = -1;
			this.toolTip = new ToolTip();
		}
		public Control Owner
		{
			get
			{
				return this.m_Owner;
			}
			set
			{
				this.m_Owner = value;
			}
		}
		protected override void WndProc(ref Message m)
		{
			if (m.Msg == 512)
			{
				Point position = Cursor.Position;
                POINT point = new POINT(position.X, position.Y);
                MapWindowPoints(IntPtr.Zero, base.Handle, point, 1);
				int num = SendMessage(m.HWnd, 425, 0, point.y << 16 | (point.x & 65535));
				if ((num >> 16 & 65535) == 0)
				{
					num &= 65535;
					if (this.m_Index != num || !this.toolTip.Active)
					{
						int num2 = SendMessage(base.Handle, 394, num, 0);
						StringBuilder stringBuilder = new StringBuilder(num2 + 1);
                        SendMessage(base.Handle, 393, num, stringBuilder);
						Point point2 = this.Owner.PointToClient(position);
						this.toolTip.RemoveAll();
						this.toolTip.Show(stringBuilder.ToString(), this.Owner, point2.X + 10, point2.Y + 10, 1000);
						this.m_Index = num;
					}
				}
			}
			base.WndProc(ref m);
		}
		public void ResetIndex()
		{
			this.m_Index = -1;
			this.toolTip.Hide(this.Owner);
		}
		private int m_Index;
		private ToolTip toolTip;
		private Control m_Owner;
		[StructLayout(LayoutKind.Sequential)]
		public class POINT
		{
			public POINT(int x, int y)
			{
				this.x = x;
				this.y = y;
			}

			public int x;

			public int y;
		}
	}
}
