﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-18 00:34:25
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.StyleToolStrips;
using YonYou.U8.IN.Forms.Win32;

namespace YonYou.U8.IN.Forms
{
    public class MetroToolStrip : ToolStrip
    {
        private const int IMAGE_RIGHT_SPACE = 3;
        private const int TEXT_RIGHT_SPACE = 5;
        private const int IMAGE_WIDTH = 16;
        private const int ITEM_HEIGHT = 22;
        private const int SMALL_BTN_IMAGE_WIDHT = 16;
        private const int SMALL_BTN_HEIGHT = 22;
        private Color ToolStripForeColor = Color.FromArgb(59, 59, 59);
        private Color _ToolScripBackColor = Color.White;
        private Color _ToolScripBorderColor = Color.White;
        private ToolTipHelper toolTipHelper;
        private Padding itemPadding = new Padding(0, 0, 0, 0);
        private bool transBackground = false;

        public MetroToolStrip()
        {
            base.Renderer = new MetroStyleRenderer();
            this.AutoSize = false;
            this.MinimumSize = new Size(20, 20);
            base.Padding = new Padding(0, 0, 0, 0);
            base.GripStyle = ToolStripGripStyle.Hidden;
            base.Height = 31;
            this.toolTipHelper = new ToolTipHelper(this);
            base.ForeColor = this.ToolStripForeColor;
        }
        protected override void WndProc(ref Message msg)
        {
            if (msg.Msg == 33)
            {
                NativeMethods.SetActiveWindow(NativeMethods.GetAncestor(base.Handle, 2));
            }
            base.WndProc(ref msg);
        }
        protected override void OnMouseMove(MouseEventArgs mea)
        {
            base.OnMouseMove(mea);
            this.toolTipHelper.ShowToolTip(mea);
        }

        public Color ToolScripBackColor
        {
            get
            {
                return this._ToolScripBackColor;
            }
            set
            {
                this._ToolScripBackColor = value;
                base.Invalidate();
            }
        }

        public Color ToolScripBorderColor
        {
            get
            {
                return this._ToolScripBorderColor;
            }
            set
            {
                this._ToolScripBorderColor = value;
                base.Invalidate();
            }
        }

        protected override void OnLayout(LayoutEventArgs e)
        {
            using (Graphics graphics = base.CreateGraphics())
            {
                this.SetToolStripItemSize(graphics);
            }
            base.OnLayout(e);
        }

        private void SetToolStripItemSize(Graphics g)
        {
            foreach (object obj in this.Items)
            {
                ToolStripItem toolStripItem = (ToolStripItem)obj;
                if (toolStripItem is ToolStripControlHost || toolStripItem is ToolStripSeparator)
                {
                    toolStripItem.Height = 22;
                }
                //else if (toolStripItem is ToolStripComboBoxButton)
                //{
                //    toolStripItem.AutoSize = false;
                //    toolStripItem.Height = 22;
                //}
                else if (toolStripItem is ToolStripCmbButton)
                {
                    toolStripItem.AutoSize = false;
                    toolStripItem.Height = 22;
                    toolStripItem.Width = 112;
                    toolStripItem.Image = null;
                    toolStripItem.TextAlign = ContentAlignment.MiddleLeft;
                    toolStripItem.ImageAlign = ContentAlignment.MiddleLeft;
                }
                else
                {
                    int num = TextRenderer.MeasureText(toolStripItem.Text, this.Font).Width + ((toolStripItem.Image != null) ? 16 : 0) + 3 + 5;
                    int num2 = num;
                    ToolStripSplitButton toolStripSplitButton = toolStripItem as ToolStripSplitButton;
                    if (toolStripSplitButton != null)
                    {
                        num2 += toolStripSplitButton.DropDownButtonBounds.Width;
                    }
                    System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton = toolStripItem as System.Windows.Forms.ToolStripDropDownButton;
                    if (toolStripDropDownButton != null)
                    {
                        num2 += 11;
                    }
                    toolStripItem.AutoSize = false;
                    toolStripItem.Size = new Size(num2, 22);
                    toolStripItem.TextAlign = ContentAlignment.MiddleLeft;
                    toolStripItem.ImageAlign = ContentAlignment.MiddleLeft;
                    toolStripItem.Margin = this.itemPadding;
                }
            }
        }

        public Padding ItemPadding
        {
            set
            {
                this.itemPadding = value;
            }
        }

        public bool TransBackground
        {
            get
            {
                return this.transBackground;
            }
            set
            {
                this.transBackground = value;
            }
        }

    }
}
