﻿namespace YonYou.U8.IN.Forms
{
	public enum RoundStyle
	{
		None,
		All,
		Left,
		Right,
		Top,
		Bottom,
		BottomLeft,
		BottomRight
	}
}