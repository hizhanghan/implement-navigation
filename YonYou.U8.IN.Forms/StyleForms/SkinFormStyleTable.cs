﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using YonYou.U8.IN.Forms.StyleForms;

namespace YonYou.U8.IN.Forms.StyleForms
{
	public class SkinFormStyleTable
	{
		public static Color StatusBarBackColor
		{
			get
			{
				return _statusBarBackColor;
			}
		}

		public static Color NavBarBackColor
		{
			get
			{
				return _navBarBackColor;
			}
		}

		public static Color DotNetTabTitleBackColor
		{
			get
			{
				return _dotNetTabTitleBackColor;
			}
			internal set
			{
                _dotNetTabTitleBackColor = value;
			}
		}

		public ColorBlend ControlBoxColorBlend
		{
			get
			{
				if (_controlBoxColorBlend == null)
				{
                    _controlBoxColorBlend = new ColorBlend
					{
						Colors = new Color[]
						{
							Color.FromArgb(255, 247, 249, 251),
							Color.FromArgb(255, 193, 213, 233)
						},
						Positions = new float[]
						{
							0f,
							1f
						}
					};
				}
				return _controlBoxColorBlend;
			}
		}

		/// <summary>
		/// 标题栏背景色
		/// </summary>
		public ColorBlend HeaderColorBlend
		{
			get
			{
				if (_headerColorBlend == null)
				{
                    _headerColorBlend = new ColorBlend
					{
						Colors = new Color[]
						{
							this._CaptionbackStart,
							this._CaptionbackEnd
						},
						Positions = new float[]
						{
							0f,
							1f
						}
					};
				}
				return _headerColorBlend;
			}
		}

		public virtual Color CaptionActive
		{
			get
			{
				return this._captionActive;
			}
			set
			{
				this._captionActive = value;
			}
		}

		public virtual Color CaptionDeactive
		{
			get
			{
				return this._captionDeactive;
			}
			set
			{
				this._captionDeactive = value;
			}
		}

		public virtual Color CaptionText
		{
			get
			{
				return this._captionText;
			}
			set
			{
				this._captionText = value;
			}
		}

		public virtual Color Border
		{
			get
			{
				return this._border;
			}
			set
			{
				this._border = value;
			}
		}

		public virtual Color InnerBorder
		{
			get
			{
				return this._innerBorder;
			}
			set
			{
				this._innerBorder = value;
			}
		}

		public virtual Color Back
		{
			get
			{
				return this._back;
			}
			set
			{
				this._back = value;
			}
		}

		public virtual Color ControlBoxActive
		{
			get
			{
				return this._controlBoxActive;
			}
			set
			{
				this._controlBoxActive = value;
			}
		}

		public virtual Color ControlBoxDeactive
		{
			get
			{
				return this._controlBoxDeactive;
			}
			set
			{
				this._controlBoxDeactive = value;
			}
		}

		public virtual Color ControlBoxHover
		{
			get
			{
				return this._controlBoxHover;
			}
			set
			{
				this._controlBoxHover = value;
			}
		}

		public virtual Color ControlBoxPressed
		{
			get
			{
				return this._controlBoxPressed;
			}
			set
			{
				this._controlBoxPressed = value;
			}
		}

		public virtual Color ControlCloseBoxHover
		{
			get
			{
				return this._controlCloseBoxHover;
			}
			set
			{
				this._controlCloseBoxHover = value;
			}
		}

		public virtual Color ControlCloseBoxPressed
		{
			get
			{
				return this._controlCloseBoxPressed;
			}
			set
			{
				this._controlCloseBoxPressed = value;
			}
		}

		public virtual Color ControlBoxInnerBorder
		{
			get
			{
				return this._controlBoxInnerBorder;
			}
			set
			{
				this._controlBoxInnerBorder = value;
			}
		}

		public static Color TitleBarSpliterColor
		{
			get
			{
				return titleBarSpliterColor;
			}
			set
			{
                titleBarSpliterColor = value;
			}
		}

		public static WindowStyle Style
		{
			get
			{
				return style;
			}
			set
			{
                style = value;
			}
		}

		private Color _captionActive = Color.FromArgb(206, 222, 236);

		private Color _captionDeactive = Color.FromArgb(183, 203, 221);

		private Color _captionText = Color.FromArgb(255, 255, 255);

		private Color _border = Color.FromArgb(105, 163, 232);

		private Color _innerBorder = Color.FromArgb(200, 250, 250);

		private Color _back = Color.FromArgb(254, 254, 254);
		private Color _CaptionbackStart = Color.FromArgb(73, 146, 230);

		private Color _CaptionbackEnd = Color.FromArgb(60, 128, 211);

		private Color _controlBoxActive = Color.FromArgb(102, 102, 102);

		private Color _controlBoxDeactive = Color.FromArgb(102, 102, 102);

		private Color _controlBoxHover = Color.FromArgb(155, 255, 255);

		private Color _controlBoxPressed = Color.FromArgb(25, 5, 255);

        private Color _controlCloseBoxHover = Color.FromArgb(213, 66, 22);

        private Color _controlCloseBoxPressed = Color.FromArgb(171, 53, 17);

		private Color _controlBoxInnerBorder = Color.FromArgb(128, 250, 250);

		private static ColorBlend _headerColorBlend;

		private static ColorBlend _controlBoxColorBlend;

		private static Color _statusBarBackColor = Color.FromArgb(70, 130, 200);

		private static Color _navBarBackColor = Color.FromArgb(70, 130, 200);

		private static Color _dotNetTabTitleBackColor = Color.FromArgb(70, 130, 200);

		public static readonly Size TilteBarSpliterSize = new Size(1, 24);

		private static Color titleBarSpliterColor = Color.FromArgb(117, 117, 117);

		private static WindowStyle style = WindowStyle.Flat;

		public static readonly Size StatusBarSpliterSize = new Size(1, 24);

		public static readonly Color StatusBarSpliterColor = Color.FromArgb(57, 109, 178);
	}
}
