﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace YonYou.U8.IN.Forms
{
	public class RenderHelper
	{
		public static void RenderBackgroundInternal(Graphics g, Rectangle rect, Color baseColor, Color borderColor, Color innerBorderColor, RoundStyle style, bool drawBorder, bool drawGlass, LinearGradientMode mode)
		{
            RenderBackgroundInternal(g, rect, baseColor, borderColor, innerBorderColor, style, 8, drawBorder, drawGlass, mode);
		}

		public static void RenderBackgroundInternal(Graphics g, Rectangle rect, Color baseColor, Color borderColor, Color innerBorderColor, RoundStyle style, int roundWidth, bool drawBorder, bool drawGlass, LinearGradientMode mode)
		{
            RenderBackgroundInternal(g, rect, baseColor, borderColor, innerBorderColor, style, 8, 0.45f, drawBorder, drawGlass, mode);
		}

		public static void RenderBackgroundInternal(Graphics g, Rectangle rect, Color baseColor, Color borderColor, Color innerBorderColor, RoundStyle style, int roundWidth, float basePosition, bool drawBorder, bool drawGlass, LinearGradientMode mode)
		{
			if (drawBorder)
			{
				rect.Width--;
				rect.Height--;
			}
			using (LinearGradientBrush linearGradientBrush = new LinearGradientBrush(rect, Color.Transparent, Color.Transparent, mode))
			{
				Color[] colors = new Color[]
				{
                    GetColor(baseColor, 0, 35, 24, 9),
                    GetColor(baseColor, 0, 13, 8, 3),
					baseColor,
                    GetColor(baseColor, 0, 35, 24, 9)
				};
				linearGradientBrush.InterpolationColors = new ColorBlend
				{
					Positions = new float[]
					{
						0f,
						basePosition,
						basePosition + 0.05f,
						1f
					},
					Colors = colors
				};
				if (style != RoundStyle.None)
				{
					using (GraphicsPath graphicsPath = GraphicsPathHelper.CreatePath(rect, roundWidth, style, false))
					{
						g.FillPath(linearGradientBrush, graphicsPath);
					}
					if (baseColor.A > 80)
					{
						Rectangle rect2 = rect;
						if (mode == LinearGradientMode.Vertical)
						{
							rect2.Height = (int)(rect2.Height * basePosition);
						}
						else
						{
							rect2.Width = (int)(rect.Width * basePosition);
						}
						using (GraphicsPath graphicsPath2 = GraphicsPathHelper.CreatePath(rect2, roundWidth, RoundStyle.Top, false))
						{
							using (SolidBrush solidBrush = new SolidBrush(Color.FromArgb(128, 255, 255, 255)))
							{
								g.FillPath(solidBrush, graphicsPath2);
							}
						}
					}
					if (drawGlass)
					{
						RectangleF glassRect = rect;
						if (mode == LinearGradientMode.Vertical)
						{
							glassRect.Y = rect.Y + rect.Height * basePosition;
							glassRect.Height = (rect.Height - rect.Height * basePosition) * 2f;
						}
						else
						{
							glassRect.X = rect.X + rect.Width * basePosition;
							glassRect.Width = (rect.Width - rect.Width * basePosition) * 2f;
						}
						ControlPaintEx.DrawGlass(g, glassRect, 170, 0);
					}
					if (drawBorder)
					{
						using (GraphicsPath graphicsPath = GraphicsPathHelper.CreatePath(rect, roundWidth, style, false))
						{
							using (Pen pen = new Pen(borderColor))
							{
								g.DrawPath(pen, graphicsPath);
							}
						}
						rect.Inflate(-1, -1);
						using (GraphicsPath graphicsPath = GraphicsPathHelper.CreatePath(rect, roundWidth, style, false))
						{
							using (Pen pen = new Pen(innerBorderColor))
							{
								g.DrawPath(pen, graphicsPath);
							}
						}
					}
				}
				else
				{
					g.FillRectangle(linearGradientBrush, rect);
					if (baseColor.A > 80)
					{
						Rectangle rect2 = rect;
						if (mode == LinearGradientMode.Vertical)
						{
							rect2.Height = (int)(rect2.Height * basePosition);
						}
						else
						{
							rect2.Width = (int)(rect.Width * basePosition);
						}
						using (SolidBrush solidBrush = new SolidBrush(Color.FromArgb(128, 255, 255, 255)))
						{
							g.FillRectangle(solidBrush, rect2);
						}
					}
					if (drawGlass)
					{
						RectangleF glassRect = rect;
						if (mode == LinearGradientMode.Vertical)
						{
							glassRect.Y = rect.Y + rect.Height * basePosition;
							glassRect.Height = (rect.Height - rect.Height * basePosition) * 2f;
						}
						else
						{
							glassRect.X = rect.X + rect.Width * basePosition;
							glassRect.Width = (rect.Width - rect.Width * basePosition) * 2f;
						}
						ControlPaintEx.DrawGlass(g, glassRect, 200, 0);
					}
					if (drawBorder)
					{
						using (Pen pen = new Pen(borderColor))
						{
							g.DrawRectangle(pen, rect);
						}
						rect.Inflate(-1, -1);
						using (Pen pen = new Pen(innerBorderColor))
						{
							g.DrawRectangle(pen, rect);
						}
					}
				}
			}
		}
        /// <summary>
        /// 绘制标题
        /// </summary>
        /// <param name="g"></param>
        /// <param name="rect"></param>
        /// <param name="blend"></param>
        /// <param name="borderColor"></param>
        /// <param name="innerBorderColor"></param>
        /// <param name="style">圆角样式 上下左右全部无</param>
        /// <param name="roundWidth">圆角角度</param>
        /// <param name="drawBorder">是否标题划边框</param>
        /// <param name="mode">标题颜色渐变</param>
		public static void RenderBackgroundInternal(Graphics g, Rectangle rect, ColorBlend blend,
            Color borderColor, Color innerBorderColor, RoundStyle style, int roundWidth, bool drawBorder, LinearGradientMode mode)
		{
			if (drawBorder)
			{
				rect.Width--;
				rect.Height--;
			}
			using (LinearGradientBrush linearGradientBrush = new LinearGradientBrush(rect, Color.Transparent, Color.Transparent, mode))
			{
				//设置多色渐变颜色
				linearGradientBrush.InterpolationColors = blend;
				if (style != RoundStyle.None)
				{
                    using (GraphicsPath graphicsPath = GraphicsPathHelper.CreatePath(rect, roundWidth, style, false))
					{
                        //g.SmoothingMode = SmoothingMode.HighQuality; //高质量
                        //g.PixelOffsetMode = PixelOffsetMode.HighQuality; //高像素偏移质量
                        g.SmoothingMode = SmoothingMode.AntiAlias; //抗锯齿
						g.FillPath(linearGradientBrush, graphicsPath);
					}
					if (drawBorder)
					{
						using (GraphicsPath graphicsPath = GraphicsPathHelper.CreatePath(rect, roundWidth, style, false))
						{
							using (Pen pen = new Pen(borderColor))
							{
								g.DrawPath(pen, graphicsPath);
							}
						}
						rect.Inflate(-1, -1);
						using (GraphicsPath graphicsPath = GraphicsPathHelper.CreatePath(rect, roundWidth, style, false))
						{
							using (Pen pen = new Pen(innerBorderColor))
							{
								g.DrawPath(pen, graphicsPath);
							}
						}
					}
				}
				else
				{
					g.FillRectangle(linearGradientBrush, rect);
					if (drawBorder)
					{
						using (Pen pen = new Pen(borderColor))
						{
							g.DrawRectangle(pen, rect);
						}
						rect.Inflate(-1, -1);
						using (Pen pen = new Pen(innerBorderColor))
						{
							g.DrawRectangle(pen, rect);
						}
					}
				}
			}
		}

		public static void RenderAlphaImage(Graphics g, Image image, Rectangle imageRect, float alpha)
		{
			using (ImageAttributes imageAttributes = new ImageAttributes())
			{
				ColorMap[] map = new ColorMap[]
				{
					new ColorMap
					{
						OldColor = Color.FromArgb(255, 0, 255, 0),
						NewColor = Color.FromArgb(0, 0, 0, 0)
					}
				};
				imageAttributes.SetRemapTable(map, ColorAdjustType.Bitmap);
				float[][] array = new float[5][];
				float[][] array2 = array;
				int num = 0;
				float[] array3 = new float[5];
				array3[0] = 1f;
				array2[num] = array3;
				float[][] array4 = array;
				int num2 = 1;
				array3 = new float[5];
				array3[1] = 1f;
				array4[num2] = array3;
				float[][] array5 = array;
				int num3 = 2;
				array3 = new float[5];
				array3[2] = 1f;
				array5[num3] = array3;
				float[][] array6 = array;
				int num4 = 3;
				array3 = new float[5];
				array3[3] = alpha;
				array6[num4] = array3;
				array[4] = new float[]
				{
					0f,
					0f,
					0f,
					0f,
					1f
				};
				float[][] newColorMatrix = array;
				ColorMatrix newColorMatrix2 = new ColorMatrix(newColorMatrix);
				imageAttributes.SetColorMatrix(newColorMatrix2, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
				g.DrawImage(image, imageRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, imageAttributes);
			}
		}

		public static Color GetColor(Color colorBase, int a, int r, int g, int b)
		{
			int a2 = colorBase.A;
			int r2 = colorBase.R;
			int g2 = colorBase.G;
			int b2 = colorBase.B;
			if (a + a2 > 255)
			{
				a = 255;
			}
			else
			{
				a = Math.Max(0, a + a2);
			}
			if (r + r2 > 255)
			{
				r = 255;
			}
			else
			{
				r = Math.Max(0, r + r2);
			}
			if (g + g2 > 255)
			{
				g = 255;
			}
			else
			{
				g = Math.Max(0, g + g2);
			}
			if (b + b2 > 255)
			{
				b = 255;
			}
			else
			{
				b = Math.Max(0, b + b2);
			}
			return Color.FromArgb(a, r, g, b);
		}
	}
}