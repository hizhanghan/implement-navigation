﻿namespace YonYou.U8.IN.Forms
{
	public enum SysButtonType
	{
		None,
		CloseButton,
		MinButton,
		MaxButton,
		RestoreButton
	}
}