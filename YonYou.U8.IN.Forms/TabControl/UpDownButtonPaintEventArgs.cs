﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-05-28 01:10:47
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System.Drawing;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms
{
    public class UpDownButtonPaintEventArgs : PaintEventArgs
    {
        private bool _mouseOver;

        private bool _mousePress;

        private bool _mouseInUpButton;

        public bool MouseOver
        {
            get
            {
                return _mouseOver;
            }
        }

        public bool MousePress
        {
            get
            {
                return _mousePress;
            }
        }

        public bool MouseInUpButton
        {
            get
            {
                return _mouseInUpButton;
            }
        }

        public UpDownButtonPaintEventArgs(Graphics graphics, Rectangle clipRect, bool mouseOver, bool mousePress, bool mouseInUpButton)
            : base(graphics, clipRect)
        {
            _mouseOver = mouseOver;
            _mousePress = mousePress;
            _mouseInUpButton = mouseInUpButton;
        }
    }
}