﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-05-28 01:10:47
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.Win32;

namespace YonYou.U8.IN.Forms
{
    public class MetroTabControl : TabControl
    {
        private class UpDownButtonNativeWindow : NativeWindow, IDisposable
        {
            private MetroTabControl _owner;

            private bool _bPainting;

            public UpDownButtonNativeWindow(MetroTabControl owner)
            {
                _owner = owner;
                AssignHandle(owner.UpDownButtonHandle);
            }

            private bool LeftKeyPressed()
            {
                if (SystemInformation.MouseButtonsSwapped)
                {
                    return NativeMethods.GetKeyState(2) < 0;
                }
                return NativeMethods.GetKeyState(1) < 0;
            }

            private void DrawUpDownButton()
            {
                bool flag = false;
                bool mousePress = LeftKeyPressed();
                bool flag2 = false;
                RECT r = default(RECT);
                GetClientRect(base.Handle, ref r);
                Rectangle clipRect = Rectangle.FromLTRB(r.Top, r.Left, r.Right, r.Bottom);
                Point lpPoint = default(Point);
                GetCursorPos(ref lpPoint);
                GetWindowRect(base.Handle, ref r);
                flag = PtInRect(ref r, lpPoint);
                lpPoint.X -= r.Left;
                lpPoint.Y -= r.Top;
                flag2 = lpPoint.X < clipRect.Width / 2;
                using (Graphics graphics = Graphics.FromHwnd(base.Handle))
                {
                    UpDownButtonPaintEventArgs e = new UpDownButtonPaintEventArgs(graphics, clipRect, flag, mousePress, flag2);
                    _owner.OnPaintUpDownButton(e);
                }
            }
            [DllImport("user32.dll")]
            public static extern bool GetClientRect(IntPtr hWnd, ref RECT r);
            [DllImport("user32.dll")]
            public static extern bool GetCursorPos(ref Point lpPoint);

            [DllImport("user32.dll")]
            public static extern bool PtInRect([In] ref RECT lprc, Point pt);

            [DllImport("user32.dll")]
            public static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

            protected override void WndProc(ref Message m)
            {
                int msg = m.Msg;
                if (msg == 15)
                {
                    if (!_bPainting)
                    {
                        NativeMethods.PAINTSTRUCT ps = default(NativeMethods.PAINTSTRUCT);
                        _bPainting = true;
                        NativeMethods.BeginPaint(m.HWnd, ref ps);
                        DrawUpDownButton();
                        NativeMethods.EndPaint(m.HWnd, ref ps);
                        _bPainting = false;
                        m.Result = NativeMethods.TRUE;
                    }
                    else
                    {
                        base.WndProc(ref m);
                    }
                }
                else
                {
                    base.WndProc(ref m);
                }
            }

            public void Dispose()
            {
                _owner = null;
                base.ReleaseHandle();
            }
        }

        private const string UpDownButtonClassName = "msctls_updown32";

        private UpDownButtonNativeWindow _upDownButtonNativeWindow;

        private Color _baseColor = Color.FromArgb(240, 246, 249);

        private Color _backColor = Color.FromArgb(255, 255, 255);

        private Color _borderColor = Color.FromArgb(171, 171, 171);

        private Color _arrowColor = Color.FromArgb(0, 79, 125);

        private Color _headerBackColor = Color.FromArgb(200, 229, 241);

        private Color _ActiveBackColor = Color.FromArgb(255, 255, 255);

        private Color _FontColor = Color.FromArgb(46, 106, 155);

        private Color _ActiveFontColor = Color.FromArgb(35, 35, 35);

        private static readonly int Radius = 8;

        private static readonly object EventPaintUpDownButton = new object();

        public MetroTabControl()
        {
            SetStyles();
            SetDefaultProperty();
        }

        private void SetStyles()
        {
            SetStyle(ControlStyles.UserPaint | ControlStyles.ResizeRedraw 
                | ControlStyles.SupportsTransparentBackColor 
                | ControlStyles.AllPaintingInWmPaint 
                | ControlStyles.OptimizedDoubleBuffer, true);
            UpdateStyles();
        }

        private void SetDefaultProperty()
        {
            base.Padding = new Point(18, 6);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button != MouseButtons.Left)
            {
                return;
            }
            for (int i = 0; i < base.TabCount; i++)
            {
                TabPage tabPage = base.TabPages[i];
                Rectangle tabRect = GetTabRect(i);
                tabRect.X -= 4 * i;
                Point pt = PointToClient(Control.MousePosition);
                if (tabRect.Contains(pt))
                {
                    base.SelectedIndex = i;
                    break;
                }
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            Invalidate();
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            if (UpDownButtonHandle != IntPtr.Zero && _upDownButtonNativeWindow == null)
            {
                _upDownButtonNativeWindow = new UpDownButtonNativeWindow(this);
            }
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            if (UpDownButtonHandle != IntPtr.Zero && _upDownButtonNativeWindow == null)
            {
                _upDownButtonNativeWindow = new UpDownButtonNativeWindow(this);
            }
        }

        protected override void OnHandleDestroyed(EventArgs e)
        {
            base.OnHandleDestroyed(e);
            if (_upDownButtonNativeWindow != null)
            {
                _upDownButtonNativeWindow.Dispose();
                _upDownButtonNativeWindow = null;
            }
        }

        protected override void OnControlAdded(ControlEventArgs e)
        {
            base.OnControlAdded(e);
            if (UpDownButtonHandle != IntPtr.Zero && _upDownButtonNativeWindow == null)
            {
                _upDownButtonNativeWindow = new UpDownButtonNativeWindow(this);
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            if (UpDownButtonHandle != IntPtr.Zero && _upDownButtonNativeWindow == null)
            {
                _upDownButtonNativeWindow = new UpDownButtonNativeWindow(this);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            DrawTabContrl(e.Graphics);
        }

        private void DrawTabContrl(Graphics g)
        {
            g.SmoothingMode = SmoothingMode.AntiAlias;
            DrawDrawBackgroundAndHeader(g);
            DrawTabPages(g);
        }

        private void DrawDrawBackgroundAndHeader(Graphics g)
        {
            int num = 0;
            int num2 = 0;
            int num3 = 0;
            int num4 = 0;
            switch (base.Alignment)
            {
                case TabAlignment.Top:
                    num = 0;
                    num2 = 0;
                    num3 = base.ClientRectangle.Width;
                    num4 = base.ClientRectangle.Height - DisplayRectangle.Height - 6;
                    break;
                case TabAlignment.Bottom:
                    num = 0;
                    num2 = DisplayRectangle.Height + 6;
                    num3 = base.ClientRectangle.Width;
                    num4 = base.ClientRectangle.Height - DisplayRectangle.Height;
                    break;
                case TabAlignment.Left:
                    num = 0;
                    num2 = 0;
                    num3 = base.ClientRectangle.Width - DisplayRectangle.Width;
                    num4 = base.ClientRectangle.Height;
                    break;
                case TabAlignment.Right:
                    num = DisplayRectangle.Width;
                    num2 = 0;
                    num3 = base.ClientRectangle.Width - DisplayRectangle.Width;
                    num4 = base.ClientRectangle.Height;
                    break;
            }
            Color color = (base.Enabled ? _backColor : Color.White);
            using (SolidBrush brush = new SolidBrush(color))
            {
                g.FillRectangle(brush, base.ClientRectangle);
                using (Pen pen = new Pen(brush))
                {
                    g.DrawRectangle(pen, base.ClientRectangle);
                }
            }
            Rectangle rect = new Rectangle(num, num2, num3, num4);
            using (SolidBrush brush = new SolidBrush(_headerBackColor))
            {
                g.FillRectangle(brush, rect);
                using (Pen pen = new Pen(brush))
                {
                    g.DrawRectangle(pen, rect);
                }
            }
        }

        private void DrawTabPages(Graphics g)
        {
            Point pt = PointToClient(Control.MousePosition);
            bool flag = false;
            IntPtr upDownButtonHandle = UpDownButtonHandle;
            if (upDownButtonHandle != IntPtr.Zero && IsWindowVisible(upDownButtonHandle))
            {
                RECT lpRect = default(RECT);
                GetWindowRect(upDownButtonHandle, ref lpRect);
                Rectangle r = Rectangle.FromLTRB(lpRect.Left, lpRect.Top, lpRect.Right, lpRect.Bottom);
                r = RectangleToClient(r);
                switch (base.Alignment)
                {
                    case TabAlignment.Top:
                        r.Y = 0;
                        break;
                    case TabAlignment.Bottom:
                        r.Y = base.ClientRectangle.Height - DisplayRectangle.Height;
                        break;
                }
                r.Height = base.ClientRectangle.Height;
                g.SetClip(r, CombineMode.Exclude);
                flag = true;
            }
            for (int i = 0; i < base.TabCount; i++)
            {
                TabPage tabPage = base.TabPages[i];
                Rectangle tabRect = GetTabRect(i);
                tabRect.X -= 4 * i;
                bool flag2 = tabRect.Contains(pt);
                bool flag3;
                bool isGradient = (flag3 = base.SelectedIndex == i);
                Color baseColor = _baseColor;
                Color borderColor = _borderColor;
                Color foreColor = _FontColor;
                if (flag3)
                {
                    baseColor = _ActiveBackColor;
                    foreColor = _ActiveFontColor;
                }
                RenderTabBackground(g, tabRect, baseColor, borderColor, isGradient, LinearGradientMode.Vertical);
                bool flag4 = false;
                g.InterpolationMode = InterpolationMode.HighQualityBilinear;
                if (base.ImageList != null)
                {
                    Image image = null;
                    if (tabPage.ImageIndex != -1)
                    {
                        image = base.ImageList.Images[tabPage.ImageIndex];
                    }
                    else if (tabPage.ImageKey != null)
                    {
                        image = base.ImageList.Images[tabPage.ImageKey];
                    }
                    if (image != null)
                    {
                        flag4 = true;
                        g.DrawImage(image, new Rectangle(tabRect.X + Radius / 2 + 2, tabRect.Y + 2, tabRect.Height - 4, tabRect.Height - 4), 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);
                    }
                }
                Rectangle bounds = tabRect;
                if (flag4)
                {
                    bounds.X = tabRect.X + Radius / 2 + tabRect.Height - 2;
                    bounds.Width = tabRect.Width - Radius - tabRect.Height;
                }
                TextRenderer.DrawText(g, tabPage.Text, tabPage.Font, bounds, foreColor);
            }
            if (flag)
            {
                g.ResetClip();
            }
        }

        internal void RenderTabBackground(Graphics g, Rectangle rect, Color baseColor, Color borderColor, bool isGradient, LinearGradientMode mode)
        {
            using (GraphicsPath graphicsPath = CreateTabPath(rect))
            {
                if (isGradient)
                {
                    using (LinearGradientBrush linearGradientBrush = new LinearGradientBrush(rect, Color.Transparent, Color.Transparent, mode))
                    {
                        Color[] array = new Color[4];
                        ColorBlend colorBlend = new ColorBlend();
                        if (base.Alignment == TabAlignment.Top)
                        {
                            array[0] = GetColor(baseColor, 0, -27, -27, -27);
                            array[1] = baseColor;
                            array[2] = baseColor;
                            array[3] = baseColor;
                            colorBlend.Positions = new float[4] { 0f, 0.27f, 0.25f, 1f };
                        }
                        else if (base.Alignment == TabAlignment.Bottom)
                        {
                            array[0] = baseColor;
                            array[1] = baseColor;
                            array[2] = baseColor;
                            array[3] = GetColor(baseColor, 0, -27, -27, -27);
                            colorBlend.Positions = new float[4] { 0f, 0.27f, 0.73f, 1f };
                        }
                        colorBlend.Colors = array;
                        linearGradientBrush.InterpolationColors = colorBlend;
                        g.FillPath(linearGradientBrush, graphicsPath);
                    }
                }
                rect.Inflate(-1, -1);
                using (Brush brush = new SolidBrush(baseColor))
                {
                    if (!isGradient)
                    {
                        g.FillPath(brush, graphicsPath);
                    }
                    using (Pen pen = new Pen(borderColor))
                    {
                        g.DrawLines(pen, graphicsPath.PathPoints);
                    }
                    using (Pen pen = new Pen(brush))
                    {
                        if (base.Alignment == TabAlignment.Top)
                        {
                            g.DrawLine(pen, rect.X + Radius / 2, rect.Bottom + 1, rect.Right - Radius / 2, rect.Bottom + 1);
                        }
                        else if (base.Alignment == TabAlignment.Bottom)
                        {
                            g.DrawLine(pen, rect.X + Radius / 2, rect.Y - 1, rect.Right - Radius / 2, rect.Y - 1);
                        }
                    }
                }
            }
        }

        private Color GetColor(Color colorBase, int a, int r, int g, int b)
        {
            int a2 = colorBase.A;
            int r2 = colorBase.R;
            int g2 = colorBase.G;
            int b2 = colorBase.B;
            a = ((a + a2 <= 255) ? Math.Max(a + a2, 0) : 255);
            r = ((r + r2 <= 255) ? Math.Max(r + r2, 0) : 255);
            g = ((g + g2 <= 255) ? Math.Max(g + g2, 0) : 255);
            b = ((b + b2 <= 255) ? Math.Max(b + b2, 0) : 255);
            return Color.FromArgb(a, r, g, b);
        }

        private GraphicsPath CreateTabPath(Rectangle rect)
        {
            GraphicsPath graphicsPath = new GraphicsPath();
            switch (base.Alignment)
            {
                case TabAlignment.Top:
                    graphicsPath.AddLine(rect.X + Radius / 2, rect.Bottom, rect.X + Radius / 2, rect.Y + Radius / 2);
                    graphicsPath.AddArc(rect.X + Radius / 2, rect.Y, Radius, Radius, 180f, 90f);
                    graphicsPath.AddArc(rect.Right - Radius - Radius / 2, rect.Y, Radius, Radius, 270f, 90f);
                    graphicsPath.AddLine(rect.Right - Radius / 2, rect.Y + Radius / 2, rect.Right - Radius / 2, rect.Bottom);
                    break;
                case TabAlignment.Bottom:
                    graphicsPath.AddLine(rect.X + Radius / 2, rect.Y, rect.X + Radius / 2, rect.Bottom - Radius / 2);
                    graphicsPath.AddArc(rect.X + Radius / 2, rect.Bottom - Radius, Radius, Radius, 180f, -90f);
                    graphicsPath.AddArc(rect.Right - Radius - Radius / 2, rect.Bottom - Radius, Radius, Radius, 90f, -90f);
                    graphicsPath.AddLine(rect.Right - Radius / 2, rect.Bottom - Radius / 2, rect.Right - Radius / 2, rect.Y);
                    break;
            }
            graphicsPath.CloseFigure();
            return graphicsPath;
        }

        protected virtual void OnPaintUpDownButton(UpDownButtonPaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Rectangle clipRectangle = e.ClipRectangle;
            Color baseColor = _baseColor;
            Color borderColor = _borderColor;
            Color arrowColor = _arrowColor;
            Color baseColor2 = _baseColor;
            Color borderColor2 = _borderColor;
            Color arrowColor2 = _arrowColor;
            Rectangle rect = clipRectangle;
            rect.X += 4;
            rect.Y += 4;
            rect.Width = clipRectangle.Width / 2 - 8;
            rect.Height -= 8;
            Rectangle rect2 = clipRectangle;
            rect2.X = rect.Right + 2;
            rect2.Y += 4;
            rect2.Width = clipRectangle.Width / 2 - 8;
            rect2.Height -= 8;
            if (base.Enabled)
            {
                if (e.MouseOver)
                {
                    if (e.MousePress)
                    {
                        if (e.MouseInUpButton)
                        {
                            baseColor = GetColor(HeaderBackColor, 0, -35, -24, -9);
                        }
                        else
                        {
                            baseColor2 = GetColor(HeaderBackColor, 0, -35, -24, -9);
                        }
                    }
                    else if (e.MouseInUpButton)
                    {
                        baseColor = GetColor(HeaderBackColor, 0, 35, 24, 9);
                    }
                    else
                    {
                        baseColor2 = GetColor(HeaderBackColor, 0, 35, 24, 9);
                    }
                }
            }
            else
            {
                baseColor = SystemColors.Control;
                borderColor = SystemColors.ControlDark;
                arrowColor = SystemColors.ControlDark;
                baseColor2 = SystemColors.Control;
                borderColor2 = SystemColors.ControlDark;
                arrowColor2 = SystemColors.ControlDark;
            }
            graphics.SmoothingMode = SmoothingMode.AntiAlias;
            Color color = (base.Enabled ? HeaderBackColor : SystemColors.Control);
            using (SolidBrush brush = new SolidBrush(HeaderBackColor))
            {
                clipRectangle.Inflate(1, 1);
                graphics.FillRectangle(brush, clipRectangle);
            }
            RenderButton(graphics, rect, baseColor, borderColor, arrowColor, ArrowDirection.Left);
            RenderButton(graphics, rect2, baseColor2, borderColor2, arrowColor2, ArrowDirection.Right);
            UpDownButtonPaintEventHandler upDownButtonPaintEventHandler = base.Events[EventPaintUpDownButton] as UpDownButtonPaintEventHandler;
            if (upDownButtonPaintEventHandler != null)
            {
                upDownButtonPaintEventHandler(this, e);
            }
        }

        internal void RenderButton(Graphics g, Rectangle rect, Color baseColor, Color borderColor, Color arrowColor, ArrowDirection direction)
        {
            RenderBackgroundInternal(g, rect, baseColor, borderColor, 0.45f, true, LinearGradientMode.Vertical);
            using (SolidBrush brush = new SolidBrush(arrowColor))
            {
                RenderArrowInternal(g, rect, direction, brush);
            }
        }
        internal void RenderBackgroundInternal(Graphics g, Rectangle rect, Color baseColor, Color borderColor, float basePosition, bool drawBorder, LinearGradientMode mode)
        {
            using (LinearGradientBrush linearGradientBrush = new LinearGradientBrush(rect, Color.Transparent, Color.Transparent, mode))
            {
                Color[] colors = new Color[4]
                {
                GetColor(baseColor, 0, 35, 24, 9),
                GetColor(baseColor, 0, 13, 8, 3),
                baseColor,
                GetColor(baseColor, 0, 68, 69, 54)
                };
                ColorBlend colorBlend = new ColorBlend();
                colorBlend.Positions = new float[4]
                {
                0f,
                basePosition,
                basePosition + 0.05f,
                1f
                };
                colorBlend.Colors = colors;
                linearGradientBrush.InterpolationColors = colorBlend;
                g.FillRectangle(linearGradientBrush, rect);
            }
            if (baseColor.A > 80)
            {
                Rectangle rect2 = rect;
                if (mode == LinearGradientMode.Vertical)
                {
                    rect2.Height = (int)((float)rect2.Height * basePosition);
                }
                else
                {
                    rect2.Width = (int)((float)rect.Width * basePosition);
                }
                using (SolidBrush brush = new SolidBrush(Color.FromArgb(80, 255, 255, 255)))
                {
                    g.FillRectangle(brush, rect2);
                }
            }
            if (drawBorder)
            {
                using (Pen pen = new Pen(borderColor))
                {
                    g.DrawRectangle(pen, rect);
                }
            }
        }

        internal void RenderArrowInternal(Graphics g, Rectangle dropDownRect, ArrowDirection direction, Brush brush)
        {
            Point point = new Point(dropDownRect.Left + dropDownRect.Width / 2, dropDownRect.Top + dropDownRect.Height / 2);
            Point[] array = null;
            switch (direction)
            {
                case ArrowDirection.Left:
                    array = new Point[3]
                    {
                new Point(point.X + 1, point.Y - 4),
                new Point(point.X + 1, point.Y + 4),
                new Point(point.X - 2, point.Y)
                    };
                    break;
                case ArrowDirection.Up:
                    array = new Point[3]
                    {
                new Point(point.X - 3, point.Y + 1),
                new Point(point.X + 3, point.Y + 1),
                new Point(point.X, point.Y - 1)
                    };
                    break;
                case ArrowDirection.Right:
                    array = new Point[3]
                    {
                new Point(point.X - 1, point.Y - 4),
                new Point(point.X - 1, point.Y + 4),
                new Point(point.X + 2, point.Y)
                    };
                    break;
                default:
                    array = new Point[3]
                    {
                new Point(point.X - 3, point.Y - 1),
                new Point(point.X + 3, point.Y - 1),
                new Point(point.X, point.Y + 1)
                    };
                    break;
            }
            g.FillPolygon(brush, array);
        }

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool IsWindowVisible(IntPtr hwnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);

        #region 属性
        public delegate void UpDownButtonPaintEventHandler(object sender, UpDownButtonPaintEventArgs e);
        public event UpDownButtonPaintEventHandler PaintUpDownButton
        {
            add
            {
                base.Events.AddHandler(EventPaintUpDownButton, value);
            }
            remove
            {
                base.Events.RemoveHandler(EventPaintUpDownButton, value);
            }
        }

        [DefaultValue(typeof(Color), "166, 222, 255")]
        public Color BaseColor
        {
            get
            {
                return _baseColor;
            }
            set
            {
                _baseColor = value;
                Invalidate(true);
            }
        }

        [DefaultValue(typeof(Color), "234, 247, 254")]
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public override Color BackColor
        {
            get
            {
                return _backColor;
            }
            set
            {
                _backColor = value;
                Invalidate(true);
            }
        }

        [Browsable(true)]
        [DefaultValue(typeof(Color), "255, 255, 255")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public Color ActiveBackColor
        {
            get
            {
                return _ActiveBackColor;
            }
            set
            {
                _ActiveBackColor = value;
                Invalidate(true);
            }
        }

        [DefaultValue(typeof(Color), "46, 106, 155")]
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public Color FontColor
        {
            get
            {
                return _FontColor;
            }
            set
            {
                _FontColor = value;
                Invalidate(true);
            }
        }

        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [DefaultValue(typeof(Color), "35, 35, 35")]
        public Color ActiveFontColor
        {
            get
            {
                return _ActiveFontColor;
            }
            set
            {
                _ActiveFontColor = value;
                Invalidate(true);
            }
        }

        [DefaultValue(typeof(Color), "171, 171, 171")]
        public Color BorderColor
        {
            get
            {
                return _borderColor;
            }
            set
            {
                _borderColor = value;
                Invalidate(true);
            }
        }

        [DefaultValue(typeof(Color), "0, 95, 152")]
        public Color ArrowColor
        {
            get
            {
                return _arrowColor;
            }
            set
            {
                _arrowColor = value;
                Invalidate(true);
            }
        }

        [DefaultValue(typeof(Color), "200, 229, 241")]
        public Color HeaderBackColor
        {
            get
            {
                return _headerBackColor;
            }
            set
            {
                _headerBackColor = value;
                Invalidate(true);
            }
        }

        internal IntPtr UpDownButtonHandle
        {
            get
            {
                return FindUpDownButton();
            }
        }

        private IntPtr FindUpDownButton()
        {
            return NativeMethods.FindWindowEx(base.Handle, IntPtr.Zero, "msctls_updown32", null);
        }

        #endregion

        public struct RECT
        {
            internal int Left;

            internal int Top;

            internal int Right;

            internal int Bottom;

            internal RECT(int X, int Y, int Width, int Height)
            {
                Left = X;
                Top = Y;
                Right = Width;
                Bottom = Height;
            }
        }
    }

}
