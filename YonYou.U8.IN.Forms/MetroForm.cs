﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.Properties;

namespace YonYou.U8.IN.Forms
{
    public partial class MetroForm : FormBase
    {
        public MetroForm()
        {
            InitializeComponent();
            this.Text = "UFMetroForm";
            this.Font = new Font("微软雅黑", 9f, FontStyle.Regular, GraphicsUnit.Point, 134);
            this.ForeColor = Color.White;
            this.ShowIcon = false;
        }
		[Category("Wade")]
		[Description("系统控制按钮与右边框之间的距离")]
		public int SysButtonPos
		{
			get
			{
				return this.m_SysButtonPos;
			}
			set
			{
				this.m_SysButtonPos = value;
				base.Invalidate(this.SysBtnRect);
			}
		}

		protected override Rectangle CloseRect
		{
			get
			{
				Rectangle sysBtnRect = this.SysBtnRect;
				Rectangle result;
				if (this._sysButton == SysButton.Normal)
				{
					int x = sysBtnRect.X + this.m_SysButtonHoverBackGroundImage.Width * 2;
					int y = sysBtnRect.Y;
					int width = this.m_SysButtonHoverBackGroundImage.Width;
					int height = this.m_SysButtonHoverBackGroundImage.Height;
					result = new Rectangle(x, y, width, height);
				}
				else if (this._sysButton == SysButton.CloseAndMini)
				{
					int x = sysBtnRect.X + this.m_SysButtonHoverBackGroundImage.Width;
					int y = sysBtnRect.Y;
					int width = this.m_SysButtonHoverBackGroundImage.Width;
					int height = this.m_SysButtonHoverBackGroundImage.Height;
					result = new Rectangle(x, y, width, height);
				}
				else
				{
					int x = sysBtnRect.X;
					int y = sysBtnRect.Y;
					int width = this.m_SysButtonHoverBackGroundImage.Width;
					int height = this.m_SysButtonHoverBackGroundImage.Height;
					result = new Rectangle(x, y, width, height);
				}
				return result;
			}
		}

		protected override Rectangle MaxRect
		{
			get
			{
				Rectangle sysBtnRect = this.SysBtnRect;
				Rectangle result;
				if (this._sysButton == SysButton.Normal)
				{
					int x = sysBtnRect.X + this.m_SysButtonHoverBackGroundImage.Width;
					int y = sysBtnRect.Y;
					int width = this.m_SysButtonHoverBackGroundImage.Width;
					int height = this.m_SysButtonHoverBackGroundImage.Height;
					result = new Rectangle(x, y, width, height);
				}
				else if (this._sysButton == SysButton.CloseAndMini)
				{
					result = Rectangle.Empty;
				}
				else
				{
					result = Rectangle.Empty;
				}
				return result;
			}
		}

		protected override Rectangle MiniRect
		{
			get
			{
				Rectangle sysBtnRect = this.SysBtnRect;
				Rectangle result;
				if (this._sysButton == SysButton.Normal)
				{
					int x = sysBtnRect.X;
					int y = sysBtnRect.Y;
					int width = this.m_SysButtonHoverBackGroundImage.Width;
					int height = this.m_SysButtonHoverBackGroundImage.Height;
					result = new Rectangle(x, y, width, height);
				}
				else if (this._sysButton == SysButton.CloseAndMini)
				{
					int x = sysBtnRect.X;
					int y = sysBtnRect.Y;
					int width = this.m_SysButtonHoverBackGroundImage.Width;
					int height = this.m_SysButtonHoverBackGroundImage.Height;
					result = new Rectangle(x, y, width, height);
				}
				else
				{
					result = Rectangle.Empty;
				}
				return result;
			}
		}

		protected override Rectangle SysBtnRect
		{
			get
			{
				Rectangle result;
				if (this._sysButton == SysButton.Normal)
				{
					int x = base.Width - this.m_SysButtonHoverBackGroundImage.Width * 3 - this.SysButtonPos;
					int y = base.ClientRectangle.Y;
					int width = this.m_SysButtonHoverBackGroundImage.Width * 3;
					int height = this.m_SysButtonHoverBackGroundImage.Height;
					result = new Rectangle(x, y, width, height);
				}
				else if (this._sysButton == SysButton.CloseAndMini)
				{
					int x = base.Width - this.m_SysButtonHoverBackGroundImage.Width * 2 - this.SysButtonPos;
					int y = base.ClientRectangle.Y;
					int width = this.m_SysButtonHoverBackGroundImage.Width * 2;
					int height = this.m_SysButtonHoverBackGroundImage.Height;
					result = new Rectangle(x, y, width, height);
				}
				else
				{
					int x = base.Width - this.m_SysButtonHoverBackGroundImage.Width - this.SysButtonPos;
					int y = base.ClientRectangle.Y;
					int width = this.m_SysButtonHoverBackGroundImage.Width;
					int height = this.m_SysButtonHoverBackGroundImage.Height;
					result = new Rectangle(x, y, width, height);
				}
				return result;
			}
		}
		[Category("Wade")]
		[Description("边框颜色")]
		public Color BorderColor
		{
			get
			{
				return this.m_BorderColor;
			}
			set
			{
				this.m_BorderColor = value;
				base.Invalidate();
			}
		}

		private void DrawSysButton(Graphics g, SysButtonType buttonType, Rectangle rect, Image image, MouseState state)
		{
			switch (state)
			{
				case MouseState.Normal:
				case MouseState.Leave:
					g.DrawImage(this.m_SysButtonNormalBackGroundImage, rect);
					break;
				case MouseState.Move:
				case MouseState.Up:
					if (buttonType != SysButtonType.CloseButton)
					{
						g.DrawImage(this.m_SysButtonHoverBackGroundImage, rect);
					}
					else
					{
						g.DrawImage(this.m_SysCloseButtonHoverBackGroundImage, rect);
					}
					break;
				case MouseState.Down:
					if (buttonType != SysButtonType.CloseButton)
					{
						g.DrawImage(this.m_SysButtonPushedBackGroundImage, rect);
					}
					else
					{
						g.DrawImage(this.m_SysCloseButtonPushedBackGroundImage, rect);
					}
					break;
			}
			Rectangle rect2 = default(Rectangle);
			rect2.X = rect.X + (rect.Width - image.Width) / 2;
			if (buttonType == SysButtonType.MinButton)
			{
				rect2.Y = rect.Y + rect.Height - 7 - image.Height;
			}
			else
			{
				rect2.Y = rect.Y + rect.Height - 5 - image.Height;
			}
			rect2.Size = image.Size;
			g.DrawImage(image, rect2);
		}

		private void DrawFrameBorder(Graphics g)
		{
			Rectangle clientRectangle = base.ClientRectangle;
			SmoothingMode smoothingMode = g.SmoothingMode;
			g.SmoothingMode = SmoothingMode.AntiAlias;
			if (base.WindowState != FormWindowState.Maximized)
			{
				Pen pen = new Pen(this.m_BorderColor, 1f);
				Rectangle rect = new Rectangle(clientRectangle.X, clientRectangle.Y, clientRectangle.Width - 1, clientRectangle.Height - 1);
				g.DrawRectangle(pen, rect);
				pen.Dispose();
			}
			g.SmoothingMode = smoothingMode;
		}

		protected override void OnWindowState()
		{
			if (base.SysButton == SysButton.Normal)
			{
				base.OnWindowState();
			}
		}

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			Graphics graphics = e.Graphics;
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
			Rectangle clientRectangle = base.ClientRectangle;
			switch (base.SysButton)
			{
				case SysButton.Normal:
					if (base.CloseState == MouseState.Up || base.CloseState == MouseState.Down)
					{
						this.DrawSysButton(graphics, SysButtonType.CloseButton, this.CloseRect, this.m_CloseHoverImage, base.CloseState);
					}
					else
					{
						this.DrawSysButton(graphics, SysButtonType.CloseButton, this.CloseRect, this.m_CloseImage, base.CloseState);
					}
					if (base.WindowState != FormWindowState.Maximized)
					{
						this.DrawSysButton(graphics, SysButtonType.MaxButton, this.MaxRect, this.m_MaxImage, base.MaxState);
					}
					else
					{
						this.DrawSysButton(graphics, SysButtonType.RestoreButton, this.MaxRect, this.m_RestoreImage, base.MaxState);
					}
					this.DrawSysButton(graphics, SysButtonType.MinButton, this.MiniRect, this.m_MinImage, base.MinState);
					break;
				case SysButton.Close:
					if (base.CloseState == MouseState.Up || base.CloseState == MouseState.Down)
					{
						this.DrawSysButton(graphics, SysButtonType.CloseButton, this.CloseRect, this.m_CloseHoverImage, base.CloseState);
					}
					else
					{
						this.DrawSysButton(graphics, SysButtonType.CloseButton, this.CloseRect, this.m_CloseImage, base.CloseState);
					}
					break;
				case SysButton.CloseAndMini:
					if (base.CloseState == MouseState.Up || base.CloseState == MouseState.Down)
					{
						this.DrawSysButton(graphics, SysButtonType.CloseButton, this.CloseRect, this.m_CloseHoverImage, base.CloseState);
					}
					else
					{
						this.DrawSysButton(graphics, SysButtonType.CloseButton, this.CloseRect, this.m_CloseImage, base.CloseState);
					}
					this.DrawSysButton(graphics, SysButtonType.MinButton, this.MiniRect, this.m_MinImage, base.MinState);
					break;
			}
		}
		public new DialogResult ShowDialog(IWin32Window owner)
		{
			DialogResult result = base.ShowDialog(owner);
			Form form = owner as Form;
			if (form != null)
			{
				form.Activate();
			}
			return result;
		}

		public new DialogResult ShowDialog()
		{
			Form owner = base.Owner;
			DialogResult result = base.ShowDialog();
			if (owner != null)
			{
				owner.Activate();
			}
			return result;
		}

		private Image m_CloseImage = Resources.sys_button_close;//ImageUtils.GetImageFromAssembly("Wade.Forms.img.sys_button_close.png");
		private Image m_CloseHoverImage = Resources.sys_button_close_hover;//ImageUtils.GetImageFromAssembly("dom.Forms.img.sys_button_close_hover.png");
		private Image m_MinImage = Resources.sys_button_min;//ImageUtils.GetImageFromAssembly("Wade.Forms.img.sys_button_min.png");

		private Image m_MaxImage = Resources.sys_button_max;//ImageUtils.GetImageFromAssembly("Wade.Forms.img.sys_button_max.png");

		private Image m_RestoreImage = Resources.sys_button_restore;//ImageUtils.GetImageFromAssembly("Wade.Forms.img.sys_button_restore.png");

		private Image m_SysButtonHoverBackGroundImage = Resources.sys_button_hover_background;//ImageUtils.GetImageFromAssembly("Wade.Forms.img.sys_button_hover_background.png");

		private Image m_SysCloseButtonHoverBackGroundImage = Resources.sys_button_close_hover_background;//ImageUtils.GetImageFromAssembly("Wade.Forms.img.sys_button_close_hover_background.png");

		private Image m_SysButtonNormalBackGroundImage = Resources.sys_button_normal_background;//ImageUtils.GetImageFromAssembly("Wade.Forms.img.sys_button_normal_background.png");

		private Image m_SysCloseButtonPushedBackGroundImage = Resources.sys_button_close_pushed_background;//ImageUtils.GetImageFromAssembly("Wade.Forms.img.sys_button_close_pushed_background.png");

		private Image m_SysButtonPushedBackGroundImage = Resources.sys_button_pushed_background;//ImageUtils.GetImageFromAssembly("Wade.Forms.img.sys_button_pushed_background.png");

		private int m_SysButtonPos = 4;

		private Color m_BorderColor = ColorTranslator.FromHtml("#007ACC");

		private Bitmap _BufferImage = null;

		private bool _IsEnableDradow;

	}
}
