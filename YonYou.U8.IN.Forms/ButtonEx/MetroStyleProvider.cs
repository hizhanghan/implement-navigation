﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-17 16:18:53
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System.Drawing.Drawing2D;
using System.Drawing;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms
{
    class MetroStyleProvider : AbstractStyleProvider
    {
        protected override void OnDraw(Graphics g, Rectangle clientRect, ButtonEx button)
        {
            Rectangle destRect;
            Rectangle bounds;
            this.CalculateRect(out destRect, out bounds, button);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            Color color = Color.FromArgb(200, 255, 255, 255);
            Color foreColor = button.ForeColor;
            Color backColor;
            Color borderColor;
            if (button.Enabled)
            {
                switch (button.ControlState)
                {
                    case ControlState.Hover:
                        backColor = button.Skin.HoverBackColor;
                        borderColor = button.Skin.HoverBorderColor;
                        foreColor = button.Skin.HoverForeColor;
                        break;
                    case ControlState.Pressed:
                        backColor = button.Skin.PressedBackColor;
                        borderColor = button.Skin.PressedBorderColor;
                        foreColor = button.Skin.PressedForeColor;
                        break;
                    default:
                        backColor = button.Skin.BackColor;
                        borderColor = button.Skin.BorderColor;
                        foreColor = button.Skin.ForeColor;
                        break;
                }
            }
            else
            {
                backColor = button.Skin.DisableBackColor;
                borderColor = button.Skin.DisableBorderColor;
                foreColor = base.DisableForeColor;
            }
            base.RenderBackground(g, button.ClientRectangle, backColor, borderColor, true);
            if (button.Image != null)
            {
                g.InterpolationMode = InterpolationMode.HighQualityBilinear;
                g.DrawImage(button.Image, destRect, 0, 0, button.Image.Width, button.Image.Height, GraphicsUnit.Pixel);
            }
            if (button.Enabled)
            {
                TextRenderer.DrawText(g, button.Text, button.Font, bounds, foreColor, AbstractStyleProvider.GetTextFormatFlags(button.TextAlign, button.RightToLeft == RightToLeft.Yes));
            }
            else
            {
                TextRenderer.DrawText(g, button.Text, button.Font, new Rectangle(bounds.X + 1, bounds.Y + 1, bounds.Width, bounds.Height), Color.White, AbstractStyleProvider.GetTextFormatFlags(button.TextAlign, button.RightToLeft == RightToLeft.Yes));
                TextRenderer.DrawText(g, button.Text, button.Font, bounds, Color.Gray, AbstractStyleProvider.GetTextFormatFlags(button.TextAlign, button.RightToLeft == RightToLeft.Yes));
            }
        }
    }
}