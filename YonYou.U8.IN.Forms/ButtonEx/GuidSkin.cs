﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-17 16:18:53
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System.ComponentModel;
using System.Drawing;

namespace YonYou.U8.IN.Forms
{
    public class GuidSkin : Component, IButtonSkin
    {
        public GuidSkin()
        {
            this.InitializeComponent();
        }

        public GuidSkin(IContainer container)
        {
            container.Add(this);
            this.InitializeComponent();
        }

        public Color BackColor
        {
            get
            {
                return this._BackColor;
            }
            set
            {
                this._BackColor = value;
            }
        }

        public Color ForeColor
        {
            get
            {
                return this._ForeColor;
            }
            set
            {
                this._ForeColor = value;
            }
        }

        public Color BorderColor
        {
            get
            {
                return this._BorderColor;
            }
            set
            {
                this._BorderColor = value;
            }
        }

        public Color HoverBackColor
        {
            get
            {
                return this._HoverBackColor;
            }
            set
            {
                this._HoverBackColor = value;
            }
        }

        public Color HoverForeColor
        {
            get
            {
                return this._HoverForeColor;
            }
            set
            {
                this._HoverForeColor = value;
            }
        }

        public Color HoverBorderColor
        {
            get
            {
                return this._HoverBorderColor;
            }
            set
            {
                this._HoverBorderColor = value;
            }
        }

        public Color PressedBackColor
        {
            get
            {
                return this._PressedBackColor;
            }
            set
            {
                this._PressedBackColor = value;
            }
        }

        public Color PressedForeColor
        {
            get
            {
                return this._PressedForeColor;
            }
            set
            {
                this._PressedForeColor = value;
            }
        }

        public Color PressedBorderColor
        {
            get
            {
                return this._PressedBorderColor;
            }
            set
            {
                this._PressedBorderColor = value;
            }
        }

        public Color DisableBackColor
        {
            get
            {
                return this._DisableBackColor;
            }
            set
            {
                this._DisableBackColor = value;
            }
        }

        public Color DisableBorderColor
        {
            get
            {
                return this._DisableBorderColor;
            }
            set
            {
                this._DisableBorderColor = value;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
        }

        private Color _BackColor = ColorTranslator.FromHtml("#5DA3DA");

        private Color _BorderColor = ColorTranslator.FromHtml("#4491CE");

        private Color _ForeColor = Color.White;

        private Color _HoverBackColor = ColorTranslator.FromHtml("#61B3F2");

        private Color _HoverBorderColor = ColorTranslator.FromHtml("#3C81BB");

        private Color _HoverForeColor = Color.White;

        private Color _PressedBackColor = ColorTranslator.FromHtml("#4A99D7");

        private Color _PressedBorderColor = ColorTranslator.FromHtml("#3A80B7");

        private Color _PressedForeColor = Color.White;

        private Color _DisableBackColor = ColorTranslator.FromHtml("#C2C2C2");

        private Color _DisableBorderColor = ColorTranslator.FromHtml("#AAAAAA");

        private IContainer components = null;
    }
}