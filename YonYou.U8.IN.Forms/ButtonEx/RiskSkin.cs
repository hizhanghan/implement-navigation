﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-17 16:18:53
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System.ComponentModel;
using System.Drawing;

namespace YonYou.U8.IN.Forms
{
    public class RiskSkin : Component, IButtonSkin
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
        }

        public RiskSkin()
        {
            this.InitializeComponent();
        }

        public RiskSkin(IContainer container)
        {
            container.Add(this);
            this.InitializeComponent();
        }

        public Color BackColor
        {
            get
            {
                return this._BackColor;
            }
            set
            {
                this._BackColor = value;
            }
        }

        public Color ForeColor
        {
            get
            {
                return this._ForeColor;
            }
            set
            {
                this._ForeColor = value;
            }
        }

        public Color BorderColor
        {
            get
            {
                return this._BorderColor;
            }
            set
            {
                this._BorderColor = value;
            }
        }

        public Color HoverBackColor
        {
            get
            {
                return this._HoverBackColor;
            }
            set
            {
                this._HoverBackColor = value;
            }
        }

        public Color HoverForeColor
        {
            get
            {
                return this._HoverForeColor;
            }
            set
            {
                this._HoverForeColor = value;
            }
        }

        public Color HoverBorderColor
        {
            get
            {
                return this._HoverBorderColor;
            }
            set
            {
                this._HoverBorderColor = value;
            }
        }

        public Color PressedBackColor
        {
            get
            {
                return this._PressedBackColor;
            }
            set
            {
                this._PressedBackColor = value;
            }
        }

        public Color PressedForeColor
        {
            get
            {
                return this._PressedForeColor;
            }
            set
            {
                this._PressedForeColor = value;
            }
        }

        public Color PressedBorderColor
        {
            get
            {
                return this._PressedBorderColor;
            }
            set
            {
                this._PressedBorderColor = value;
            }
        }

        public Color DisableBackColor
        {
            get
            {
                return this._DisableBackColor;
            }
            set
            {
                this._DisableBackColor = value;
            }
        }

        public Color DisableBorderColor
        {
            get
            {
                return this._DisableBorderColor;
            }
            set
            {
                this._DisableBorderColor = value;
            }
        }

        private IContainer components = null;

        private Color _BackColor = ColorTranslator.FromHtml("#EEF1F3");

        private Color _BorderColor = ColorTranslator.FromHtml("#C7C7C7");

        private Color _ForeColor = ColorTranslator.FromHtml("#535353");

        private Color _HoverBackColor = ColorTranslator.FromHtml("#ED5855");

        private Color _HoverBorderColor = ColorTranslator.FromHtml("#CE403F");

        private Color _HoverForeColor = Color.White;

        private Color _PressedBackColor = ColorTranslator.FromHtml("#C5504F");

        private Color _PressedBorderColor = ColorTranslator.FromHtml("#933A39");

        private Color _PressedForeColor = Color.White;

        private Color _DisableBackColor = ColorTranslator.FromHtml("#EDF2F3");

        private Color _DisableBorderColor = ColorTranslator.FromHtml("#C7C7C7");
    }
}