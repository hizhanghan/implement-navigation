﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-17 16:18:53
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System.Drawing;

namespace YonYou.U8.IN.Forms
{
    public interface IButtonSkin
    {
        Color BackColor { get; set; }

        Color ForeColor { get; set; }

        Color BorderColor { get; set; }

        Color HoverBackColor { get; set; }

        Color HoverForeColor { get; set; }

        Color HoverBorderColor { get; set; }

        Color PressedBackColor { get; set; }

        Color PressedForeColor { get; set; }

        Color PressedBorderColor { get; set; }

        Color DisableBackColor { get; set; }

        Color DisableBorderColor { get; set; }
    }
}