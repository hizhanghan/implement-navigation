﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-17 16:18:53
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms
{
    public class ButtonEx : Button
    {
        public ButtonEx()
        {
            this.BackColor = Color.Transparent;
            base.SetStyle(ControlStyles.UserPaint 
                | ControlStyles.ResizeRedraw 
                | ControlStyles.SupportsTransparentBackColor 
                | ControlStyles.AllPaintingInWmPaint 
                | ControlStyles.OptimizedDoubleBuffer, true);
        }

        [DefaultValue(typeof(Color), "51, 161, 224")]
        public Color BaseColor
        {
            get
            {
                return this._baseColor;
            }
            set
            {
                this._baseColor = value;
                base.Invalidate();
            }
        }

        [DefaultValue(typeof(Color), "51, 161, 224")]
        public Color BorderColor
        {
            get
            {
                return this._borderColor;
            }
            set
            {
                this._borderColor = value;
                base.Invalidate();
            }
        }

        [DefaultValue(18)]
        public int ImageWidth
        {
            get
            {
                return this._imageWidth;
            }
            set
            {
                if (value != this._imageWidth)
                {
                    this._imageWidth = ((value < 12) ? 12 : value);
                    base.Invalidate();
                }
            }
        }

        [DefaultValue(typeof(RoundStyle), "1")]
        public RoundStyle RoundStyle
        {
            get
            {
                return this._roundStyle;
            }
            set
            {
                if (this._roundStyle != value)
                {
                    this._roundStyle = value;
                    base.Invalidate();
                }
            }
        }

        [DefaultValue(4)]
        public int Radius
        {
            get
            {
                return this._radius;
            }
            set
            {
                if (this._radius != value)
                {
                    this._radius = ((value < 4) ? 4 : value);
                    base.Invalidate();
                }
            }
        }

        public IButtonSkin Skin
        {
            get
            {
                return this._Skin;
            }
            set
            {
                if (this._SkinStyle == ButtonSkinStyle.Customer)
                {
                    this._Skin = value;
                    base.Invalidate();
                }
            }
        }

        public ButtonSkinStyle SkinStyle
        {
            get
            {
                return this._SkinStyle;
            }
            set
            {
                if (value != this._SkinStyle)
                {
                    this.CreateSkinByStyle(value);
                    this._SkinStyle = value;
                    base.Invalidate();
                }
            }
        }

        public ProviderStyle ProviderStyle
        {
            get
            {
                return this._ProviderStyle;
            }
            set
            {
                if (value != this._ProviderStyle)
                {
                    this.CreateProviderByStyle(value);
                    this._ProviderStyle = value;
                    base.Invalidate();
                }
            }
        }

        internal ControlState ControlState
        {
            get
            {
                return this._controlState;
            }
            set
            {
                if (this._controlState != value)
                {
                    this._controlState = value;
                    base.Invalidate();
                }
            }
        }

        public string Description
        {
            get
            {
                return this._Desc;
            }
            set
            {
                if (this._Desc != value)
                {
                    this._Desc = value;
                    base.Invalidate();
                }
            }
        }

        private void CreateSkinByStyle(ButtonSkinStyle style)
        {
            switch (style)
            {
                case ButtonSkinStyle.None:
                    this._Skin = null;
                    break;
                case ButtonSkinStyle.Normal:
                    this._Skin = new NormalSkin();
                    break;
                case ButtonSkinStyle.Giud:
                    this._Skin = new GuidSkin();
                    break;
                case ButtonSkinStyle.Risk:
                    this._Skin = new RiskSkin();
                    break;
                case ButtonSkinStyle.Check:
                    this._Skin = new CheckSkin();
                    break;
            }
        }

        private void CreateProviderByStyle(ProviderStyle style)
        {
            switch (style)
            {
                case ProviderStyle.Normal:
                    this._Provider = new NormalStyleProvider();
                    break;
                case ProviderStyle.Metro:
                    this._Provider = new MetroStyleProvider();
                    break;
                case ProviderStyle.Check:
                    this._Provider = new CheckStyleProvider();
                    break;
            }
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.OnMouseEnter(e);
            this.ControlState = ControlState.Hover;
            base.Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            this.ControlState = ControlState.Normal;
            base.Invalidate();
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Button == MouseButtons.Left && e.Clicks == 1)
            {
                this.ControlState = ControlState.Pressed;
            }
            base.Invalidate();
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (e.Button == MouseButtons.Left && e.Clicks == 1)
            {
                if (this.ControlState == ControlState.Pressed)
                {
                    this.ControlState = ControlState.Hover;
                }
                else
                {
                    this.ControlState = ControlState.Normal;
                }
                base.Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            base.OnPaintBackground(e);
            this._Provider.Draw(e, this);
        }

        private Color _baseColor = Color.FromArgb(229, 237, 241);

        private Color _borderColor = Color.FromArgb(137, 170, 203);

        private ControlState _controlState;

        private int _imageWidth = 18;

        private RoundStyle _roundStyle = RoundStyle.All;

        private int _radius = 4;

        private Color _disableFontColor = SystemColors.Control;

        private IButtonSkin _Skin;

        private ButtonSkinStyle _SkinStyle = ButtonSkinStyle.None;

        private string _Desc = string.Empty;

        private ProviderStyle _ProviderStyle = ProviderStyle.Metro;

        private IStyleProvider _Provider = new MetroStyleProvider();

    }
}
