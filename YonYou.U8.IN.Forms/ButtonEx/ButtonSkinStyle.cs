﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-17 16:18:53
// Email:hanlilong2004@163.com
// Description:
**************************************/

namespace YonYou.U8.IN.Forms
{
    public enum ButtonSkinStyle
    {
        None,
        Customer,
        Normal,
        Giud,
        Risk,
        Check
    }
}