﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-17 16:18:53
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System.Windows.Forms;

namespace YonYou.U8.IN.Forms
{
    public interface IStyleProvider
    {
        void Draw(PaintEventArgs e, ButtonEx button);
    }
}