﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-17 16:18:53
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System.Drawing.Drawing2D;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms
{
    class CheckStyleProvider : AbstractStyleProvider
    {
        protected override void OnDraw(Graphics g, Rectangle clientRect, ButtonEx button)
        {
            Rectangle destRect;
            Rectangle rectangle;
            this.CalculateRect(out destRect, out rectangle, button);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            CheckSkin checkSkin = button.Skin as CheckSkin;
            Color color = Color.FromArgb(200, 255, 255, 255);
            Color foreColor = button.ForeColor;
            Color foreColor2 = checkSkin.DescForeColor;
            Color backColor;
            Color borderColor;
            if (button.Enabled)
            {
                switch (button.ControlState)
                {
                    case ControlState.Hover:
                        backColor = button.Skin.HoverBackColor;
                        borderColor = button.Skin.HoverBorderColor;
                        foreColor = button.Skin.HoverForeColor;
                        foreColor2 = button.Skin.HoverForeColor;
                        break;
                    case ControlState.Pressed:
                        backColor = button.Skin.PressedBackColor;
                        borderColor = button.Skin.PressedBorderColor;
                        foreColor = button.Skin.PressedForeColor;
                        foreColor2 = button.Skin.PressedForeColor;
                        break;
                    default:
                        backColor = button.Skin.BackColor;
                        borderColor = button.Skin.BorderColor;
                        foreColor = button.Skin.ForeColor;
                        foreColor2 = checkSkin.DescForeColor;
                        break;
                }
            }
            else
            {
                backColor = button.Skin.DisableBackColor;
                borderColor = button.Skin.DisableBorderColor;
                foreColor = base.DisableForeColor;
            }
            base.RenderBackground(g, button.ClientRectangle, backColor, borderColor, true);
            if (button.Image != null)
            {
                g.InterpolationMode = InterpolationMode.HighQualityBilinear;
                g.DrawImage(button.Image, destRect, 0, 0, button.Image.Width, button.Image.Height, GraphicsUnit.Pixel);
            }
            if (button.Enabled)
            {
                TextFormatFlags flags = TextFormatFlags.NoPrefix | TextFormatFlags.VerticalCenter | TextFormatFlags.WordBreak | TextFormatFlags.NoPadding;
                TextRenderer.DrawText(g, button.Text, checkSkin.Font, new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height / 2), foreColor, flags);
                this.DrawText(g, button.Description, checkSkin.DescFont, new Rectangle(rectangle.X, rectangle.Y + rectangle.Height / 2, rectangle.Width, rectangle.Height / 2), foreColor2, 1, 6);
            }
            else
            {
                TextRenderer.DrawText(g, button.Text, button.Font, new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height / 2), Color.White, AbstractStyleProvider.GetTextFormatFlags(button.TextAlign, button.RightToLeft == RightToLeft.Yes));
                TextRenderer.DrawText(g, button.Text, button.Font, new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height / 2), Color.Gray, AbstractStyleProvider.GetTextFormatFlags(button.TextAlign, button.RightToLeft == RightToLeft.Yes));
                if (checkSkin != null)
                {
                    TextFormatFlags flags = TextFormatFlags.NoPrefix | TextFormatFlags.WordBreak | TextFormatFlags.NoPadding;
                    TextRenderer.DrawText(g, button.Description, checkSkin.DescFont, new Rectangle(rectangle.X + 1, rectangle.Y + rectangle.Height / 2 + 1, rectangle.Width, rectangle.Height / 2), Color.White, flags);
                    TextRenderer.DrawText(g, button.Description, checkSkin.DescFont, new Rectangle(rectangle.X, rectangle.Y + rectangle.Height / 2, rectangle.Width, rectangle.Height / 2), Color.Gray, flags);
                }
            }
        }

        protected override void CalculateRect(out Rectangle imageRect, out Rectangle textRect, ButtonEx button)
        {
            int width = 0;
            int num = 0;
            if (button.Image != null)
            {
                width = button.Image.Width;
                num = button.Image.Height;
            }
            imageRect = Rectangle.Empty;
            textRect = Rectangle.Empty;
            if (button.Image == null)
            {
                textRect = new Rectangle(3, 0, button.Width - 4, button.Height);
            }
            else
            {
                int x = 16;
                imageRect = new Rectangle(x, (button.Height - num) / 2, width, num);
                textRect = new Rectangle(imageRect.Right + 2, imageRect.Y, button.ClientRectangle.Right - imageRect.Right - 2 - 20, num);
                if (button.RightToLeft == RightToLeft.Yes)
                {
                    imageRect.X = button.Width - imageRect.Right;
                    textRect.X = button.Width - textRect.Right;
                }
            }
        }

        private void DrawText(Graphics g, string text, Font font, Rectangle bounds, Color foreColor, int gapX, int gapY)
        {
            PointF point = new PointF((float)(bounds.X - 2), (float)bounds.Y);
            SizeF sizeF = SizeF.Empty;
            char[] array = text.ToCharArray();
            using (Brush brush = new SolidBrush(foreColor))
            {
                StringBuilder stringBuilder = new StringBuilder();
                foreach (char value in array)
                {
                    stringBuilder.Append(value);
                    sizeF = g.MeasureString(stringBuilder.ToString(), font);
                    if (sizeF.Width > (float)bounds.Width)
                    {
                        g.DrawString(stringBuilder.ToString(), font, brush, point);
                        point.Y += sizeF.Height;
                        stringBuilder = new StringBuilder();
                    }
                }
                if (stringBuilder.Length > 0)
                {
                    g.DrawString(stringBuilder.ToString(), font, brush, point);
                }
            }
        }
    }
}