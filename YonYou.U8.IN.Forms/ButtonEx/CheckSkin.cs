﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-17 16:18:53
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System.ComponentModel;
using System.Drawing;

namespace YonYou.U8.IN.Forms
{
    public class CheckSkin : Component, IButtonSkin
    {
        public CheckSkin()
        {
            this.InitializeComponent();
        }

        public CheckSkin(IContainer container)
        {
            container.Add(this);
            this.InitializeComponent();
        }

        public Color BackColor
        {
            get
            {
                return this._BackColor;
            }
            set
            {
                this._BackColor = value;
            }
        }

        public Color ForeColor
        {
            get
            {
                return this._ForeColor;
            }
            set
            {
                this._ForeColor = value;
            }
        }

        public Color BorderColor
        {
            get
            {
                return this._BorderColor;
            }
            set
            {
                this._BorderColor = value;
            }
        }

        public Color HoverBackColor
        {
            get
            {
                return this._HoverBackColor;
            }
            set
            {
                this._HoverBackColor = value;
            }
        }

        public Color HoverForeColor
        {
            get
            {
                return this._HoverForeColor;
            }
            set
            {
                this._HoverForeColor = value;
            }
        }

        public Color HoverBorderColor
        {
            get
            {
                return this._HoverBorderColor;
            }
            set
            {
                this._HoverBorderColor = value;
            }
        }

        public Color PressedBackColor
        {
            get
            {
                return this._PressedBackColor;
            }
            set
            {
                this._PressedBackColor = value;
            }
        }

        public Color PressedForeColor
        {
            get
            {
                return this._PressedForeColor;
            }
            set
            {
                this._PressedForeColor = value;
            }
        }

        public Color PressedBorderColor
        {
            get
            {
                return this._PressedBorderColor;
            }
            set
            {
                this._PressedBorderColor = value;
            }
        }

        public Color DisableBackColor
        {
            get
            {
                return this._DisableBackColor;
            }
            set
            {
                this._DisableBackColor = value;
            }
        }

        public Color DisableBorderColor
        {
            get
            {
                return this._DisableBorderColor;
            }
            set
            {
                this._DisableBorderColor = value;
            }
        }

        public Font Font
        {
            get
            {
                return this._Font;
            }
            set
            {
                this._Font = value;
            }
        }

        public Font DescFont
        {
            get
            {
                return this._DescFont;
            }
            set
            {
                this._DescFont = value;
            }
        }

        public Color DescForeColor
        {
            get
            {
                return this._DescForeColor;
            }
            set
            {
                this._DescForeColor = value;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.components = new Container();
        }

        private Color _BackColor = ColorTranslator.FromHtml("#EEEEEE");

        private Color _BorderColor = ColorTranslator.FromHtml("#DBDBDB");

        private Color _ForeColor = Color.Black;

        private Color _HoverBackColor = ColorTranslator.FromHtml("#82BEEB");

        private Color _HoverBorderColor = ColorTranslator.FromHtml("#70ACDB");

        private Color _HoverForeColor = Color.White;

        private Color _PressedBackColor = ColorTranslator.FromHtml("#78B1E1");

        private Color _PressedBorderColor = ColorTranslator.FromHtml("#5B9ACB");

        private Color _PressedForeColor = Color.White;

        private Color _DisableBackColor = ColorTranslator.FromHtml("#C2C2C2");

        private Color _DisableBorderColor = ColorTranslator.FromHtml("#AAAAAA");

        private Font _Font = new Font("宋体", 14f, FontStyle.Bold, GraphicsUnit.Pixel);

        private Font _DescFont = new Font("宋体", 12f, FontStyle.Regular, GraphicsUnit.Pixel);

        private Color _DescForeColor = ColorTranslator.FromHtml("#767676");

        private IContainer components = null;
    }
}