﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace YonYou.U8.IN.Forms
{
    public class SelectedItemEventArgs : CancelEventArgs
    {
        public MetroToolItem SelectedItem { get; private set; }

        public int SelectedIndex { get; private set; }

        public SelectedItemEventArgs(MetroToolItem item, int index)
        {
            this.SelectedItem = item;
            this.SelectedIndex = index;
        }
    }
}
