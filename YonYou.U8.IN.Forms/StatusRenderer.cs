﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.Properties;

namespace YonYou.U8.IN.Forms
{
	public class StatusRenderer : ToolStripRenderer
	{
		protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
		{
			base.OnRenderToolStripBackground(e);
			Graphics graphics = e.Graphics;
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			StatusStrip statusStrip = e.ToolStrip as StatusStrip;
			if (statusStrip != null)
			{
			}
		}

		protected override void OnRenderSeparator(ToolStripSeparatorRenderEventArgs e)
		{
			base.OnRenderSeparator(e);
			if (e.Item is ToolStripSeparator)
			{
				Graphics graphics = e.Graphics;
				int y = 0;
				int x = 0;
				int width = Resources.cut_v.Width;
				int height = Resources.cut_v.Height;
				graphics.DrawImage(Resources.cut_v, x, y, width, height);
			}
		}
	}
}