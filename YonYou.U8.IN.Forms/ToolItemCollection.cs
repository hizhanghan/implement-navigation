﻿using System.Collections.Generic;
using System.ComponentModel;

namespace YonYou.U8.IN.Forms
{
    [ListBindable(false)]
    public class ToolItemCollection : List<MetroToolItem>
    {
        private MetroToolBar _owner = null;

        public ToolItemCollection(MetroToolBar owner)
        {
            this._owner = owner;
        }

        public int GetIndexOfRange(MetroToolItem item)
        {
            int num = -1;
            for (int index = 0; index < this.Count; ++index)
            {
                if (item == this[index])
                {
                    num = index;
                    break;
                }
            }
            return num;
        }
    }
}