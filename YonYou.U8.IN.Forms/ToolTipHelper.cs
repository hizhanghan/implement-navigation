﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-18 00:34:25
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System.Reflection;
using System.Windows.Forms;
using System;
using System.Drawing;
using System.Diagnostics;

namespace YonYou.U8.IN.Forms
{
    public class ToolTipHelper
    {
        private ToolStrip owner;
        private MethodInfo methodSetTrackPosition = null;
        private MethodInfo methodSetTool = null;
        private MethodInfo methodStartTimer = null;
        private ToolStripItem lastActivetem = null;
        private ToolTip tooltip = null;
        public bool useCustomTooltip = true;
        public ToolTipHelper(ToolStrip toolStrip)
        {
            this.owner = toolStrip;
            this.owner.ShowItemToolTips = false;
        }

        public void ShowToolTip(MouseEventArgs mea)
        {
            if (this.useCustomTooltip)
            {
                ToolStripItem itemAt = this.owner.GetItemAt(mea.X, mea.Y);
                if (itemAt != null && itemAt != this.lastActivetem)
                {
                    this.UpdateToolTip(itemAt);
                }
                this.lastActivetem = itemAt;
            }
        }

        public void clear()
        {
            this.owner = null;
        }

        public void HideToolTip()
        {
            if (this.tooltip != null)
            {
                try
                {
                    this.tooltip.Hide(this.owner);
                }
                catch
                {
                }
            }
        }

        private void UpdateToolTip(ToolStripItem item)
        {
            if (this.tooltip == null)
            {
                PropertyInfo property = this.owner.GetType().GetProperty("ToolTip", BindingFlags.Instance | BindingFlags.NonPublic);
                this.tooltip = (property.GetValue(this.owner, null) as ToolTip);
                Type type = this.tooltip.GetType();
                if (this.methodSetTrackPosition == null)
                {
                    this.methodSetTrackPosition = type.GetMethod("SetTrackPosition", BindingFlags.Instance | BindingFlags.NonPublic);
                }
                if (this.methodSetTool == null)
                {
                    this.methodSetTool = type.GetMethod("SetTool", BindingFlags.Instance | BindingFlags.NonPublic);
                }
                if (this.methodStartTimer == null)
                {
                    this.methodStartTimer = type.GetMethod("StartTimer", BindingFlags.Instance | BindingFlags.NonPublic);
                }
            }
            if (this.tooltip != null)
            {
                try
                {
                    this.tooltip.Hide(this.owner);
                }
                catch
                {
                }
                try
                {
                    this.tooltip.Active = true;
                    Point position = Cursor.Position;
                    position.Y += this.owner.Cursor.Size.Height - this.owner.Cursor.HotSpot.Y;
                    int x = position.X;
                    int y = position.Y;
                    this.methodSetTrackPosition.Invoke(this.tooltip, new object[]
                    {
                        x,
                        y
                    });
                    this.methodSetTool.Invoke(this.tooltip, new object[]
                    {
                        this.owner,
                        item.ToolTipText,
                        2,
                        position
                    });
                    this.methodStartTimer.Invoke(this.tooltip, new object[]
                    {
                        this.owner,
                        this.tooltip.AutomaticDelay * 2
                    });
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }
            }
        }


    }
}