﻿using System.ComponentModel;
using System.Drawing;

namespace YonYou.U8.IN.Forms
{
    public class MetroToolItem
    {
        public Image Image { get; set; }

        public object Tag { get; set; }

        [DefaultValue("toolItem")]
        public string Text { get; set; }

        internal Rectangle Rectangle { get; set; }

        internal MouseState MouseState { get; set; }
    }
}
