﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-18 00:34:25
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System.Drawing;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.StyleToolStrips;

namespace YonYou.U8.IN.Forms
{
    public class MetroStyleRenderer : StyleRenderer
    {
        protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
        {
            MetroToolStrip metroToolStrip = e.ToolStrip as MetroToolStrip;
            if (metroToolStrip != null)
            {
                if (!metroToolStrip.TransBackground)
                {
                    Rectangle rect = new Rectangle(0, 0, e.ToolStrip.Width, e.ToolStrip.Height);
                    using (Brush brush = new SolidBrush(metroToolStrip.ToolScripBackColor))
                    {
                        e.Graphics.FillRectangle(brush, rect);
                        using (Pen pen = new Pen(metroToolStrip.ToolScripBorderColor))
                        {
                            e.Graphics.DrawLine(pen, rect.X, rect.Bottom - 1, rect.Right, rect.Bottom - 1);
                        }
                    }
                }
            }
            else
            {
                base.OnRenderToolStripBackground(e);
            }
        }
    }
}