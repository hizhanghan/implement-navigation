﻿namespace YonYou.U8.IN.Forms
{
	public interface IEditor
	{
		CommandTable CommandTypes { get; }

		void Initialize();

		void RegisteCommandType<T>(string key) where T : ICommand;

		void UnregisteCommandType<T>(string key) where T : ICommand;

		void ExcuteAction(IAction action);

		object ExcuteCommand(string commandKey, params object[] args);

		object ExcuteCommand<T>(params object[] args) where T : ICommand;

		void InitActions();
	}
}