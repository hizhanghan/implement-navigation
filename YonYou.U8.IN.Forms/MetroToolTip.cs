﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms
{
    public class MetroToolTip : ToolTip
    {
		private Color m_BorderColor = ColorTranslator.FromHtml("#767676");
		private static readonly int g_Height = 25;
		private Font m_Font;
		public MetroToolTip()
		{
			this.OwnerDraw = true;
			this.Font = new Font("微软雅黑", 9f, FontStyle.Regular, GraphicsUnit.Point, 134);
			this.BackColor = ColorTranslator.FromHtml("#F1F2F7");
			this.ForeColor = ColorTranslator.FromHtml("#575757");
			base.Popup += this.OnPopup;
			base.Draw += this.OnDraw;
		}

		[Category("Wade")]
		[Description("提示框边框颜色.")]
		public Color BorderColor
		{
			get
			{
				return this.m_BorderColor;
			}
			set
			{
				this.m_BorderColor = value;
			}
		}

		[Category("Wade")]
		[Description("获取或设置一个值，用来指示是否采用系统绘制或用户自定义绘制提示框样式.")]
		public new bool OwnerDraw
		{
			get
			{
				return base.OwnerDraw;
			}
			set
			{
				if (value)
				{
					this.ToolTipIcon = ToolTipIcon.None;
					this.ToolTipTitle = string.Empty;
				}
				base.OwnerDraw = value;
			}
		}

		[Description("获取或设置提示信息的图标.")]
		[Category("Wade")]
		public new ToolTipIcon ToolTipIcon
		{
			get
			{
				return base.ToolTipIcon;
			}
			set
			{
				if (!this.OwnerDraw)
				{
					base.ToolTipIcon = value;
				}
			}
		}

		[Description("获取或设置提示信息，只在OwnerDraw为True的情况下才有效.")]
		[Category("Wade")]
		public new string ToolTipTitle
		{
			get
			{
				return base.ToolTipTitle;
			}
			set
			{
				base.ToolTipTitle = value;
			}
		}

		[Description("获取或设置提示框的背景色.")]
		[Category("Wade")]
		[DefaultValue("#F1F2F7")]
		public new Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}

		[Category("Wade")]
		[Description("获取或设置提示信息的前景色.")]
		[DefaultValue("#575757")]
		public new Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}

		[Description("获取或设置提示信息的字体.")]
		[Category("Wade")]
		[DefaultValue("#575757")]
		public Font Font
		{
			get
			{
				return this.m_Font;
			}
			set
			{
				this.m_Font = value;
			}
		}
		protected virtual void OnDraw(object sender, DrawToolTipEventArgs e)
		{
			if (!string.IsNullOrEmpty(e.ToolTipText))
			{
				e.Graphics.CompositingQuality = CompositingQuality.HighQuality;
				Rectangle rect = default(Rectangle);
				rect.Size = new Size(e.Bounds.Width, MetroToolTip.g_Height);
				e.Graphics.Clear(Color.Transparent);
				this.OnDrawBackGroud(e.Graphics, rect);
				this.OnDrawBorder(e.Graphics, rect);
				this.OnDrawText(e.Graphics, rect, e.ToolTipText);
			}
		}
		protected virtual void OnDrawBorder(Graphics g, Rectangle rect)
		{
			using (Pen pen = new Pen(this.BorderColor))
			{
				Rectangle rect2 = new Rectangle(rect.X, rect.Y, rect.Width - 1, rect.Height - 1);
				g.DrawRectangle(pen, rect2);
			}
		}
		protected virtual void OnDrawBackGroud(Graphics g, Rectangle rect)
		{
			using (Brush brush = new SolidBrush(this.BackColor))
			{
				g.FillRectangle(brush, rect);
			}
		}
		protected virtual void OnDrawText(Graphics g, Rectangle rect, string text)
		{
			using (StringFormat stringFormat = new StringFormat())
			{
				SizeF sizeF = g.MeasureString(text, this.Font);
				stringFormat.FormatFlags = StringFormatFlags.LineLimit;
				stringFormat.Alignment = StringAlignment.Near;
				stringFormat.LineAlignment = StringAlignment.Center;
				stringFormat.Trimming = StringTrimming.None;
				Rectangle r = Rectangle.Inflate(rect, 0, 0);
				r.X += (r.Width - (int)sizeF.Width) / 2;
				r.Width -= 12;
				using (Brush brush = new SolidBrush(this.ForeColor))
				{
					g.DrawString(text, this.Font, brush, r, stringFormat);
				}
			}
		}
		public new void SetToolTip(Control control, string caption)
		{
			this.ToolTipTitle = caption;
			base.SetToolTip(control, caption);
		}
		public virtual void OnPopup(object sender, PopupEventArgs e)
		{
			try
			{
				if (this.OwnerDraw)
				{
					SizeF sizeF = TextRenderer.MeasureText(this.ToolTipTitle, this.Font);
					Size toolTipSize = e.ToolTipSize;
					toolTipSize.Height = MetroToolTip.g_Height;
					toolTipSize.Width = (int)sizeF.Width + 12;
					e.ToolTipSize = toolTipSize;
				}
			}
			catch (Exception ex)
			{
				string message = "Exception in CustomizedToolTip.CustomizedToolTip_Popup (object, PopupEventArgs) " + ex.ToString();
				Trace.TraceError(message);
				throw;
			}
		}

	}
}
