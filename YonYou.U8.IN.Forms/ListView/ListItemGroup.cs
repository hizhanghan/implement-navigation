﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-05-28 02:05:01
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YonYou.U8.IN.Forms
{
    public class ListItemGroup
    {
        public class ListViewItemCollection : IList, ICollection, IEnumerable
        {
            private int lastAccessedIndex = -1;

            private List<ListItem> listViewitems;

            private ListItemGroup owner;

            [Browsable(false)]
            public int Count
            {
                get
                {
                    return listViewitems.Count;
                }
            }

            public bool IsReadOnly
            {
                get
                {
                    return false;
                }
            }

            public virtual ListItem this[int index]
            {
                get
                {
                    return listViewitems[index];
                }
                set
                {
                    listViewitems.Insert(index, value);
                }
            }

            public virtual ListItem this[string key]
            {
                get
                {
                    if (!string.IsNullOrEmpty(key))
                    {
                        int index = IndexOfKey(key);
                        if (IsValidIndex(index))
                        {
                            return this[index];
                        }
                    }
                    return null;
                }
            }

            bool ICollection.IsSynchronized
            {
                get
                {
                    return false;
                }
            }

            object ICollection.SyncRoot
            {
                get
                {
                    return this;
                }
            }

            bool IList.IsFixedSize
            {
                get
                {
                    return false;
                }
            }

            object IList.this[int index]
            {
                get
                {
                    return this[index];
                }
                set
                {
                    if (!(value is ListItem))
                    {
                        throw new ArgumentException("value");
                    }
                    this[index] = (ListItem)value;
                }
            }

            public ListViewItemCollection(ListItemGroup owner)
            {
                if (owner == null)
                {
                    throw new ArgumentNullException("owner");
                }
                listViewitems = new List<ListItem>();
                this.owner = owner;
            }

            public void Add(string text)
            {
                ListItem listItem = new ListItem();
                listItem.Text = text;
                ListItem item = listItem;
                listViewitems.Add(item);
            }

            public void Add(ListItem value)
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                listViewitems.Add(value);
            }

            public void Add(string key, string text)
            {
                ListItem listItem = new ListItem();
                listItem.Name = key;
                listItem.Text = text;
                ListItem value = listItem;
                Add(value);
            }

            public void Add(string key, string text, object userData)
            {
                ListItem listItem = new ListItem();
                listItem.Name = key;
                listItem.Text = text;
                listItem.Tag = userData;
                ListItem value = listItem;
                Add(value);
            }

            public void AddRange(ListItem[] items)
            {
                if (items == null)
                {
                    throw new ArgumentNullException("items");
                }
                foreach (ListItem value in items)
                {
                    Add(value);
                }
            }

            public virtual void Clear()
            {
                listViewitems.Clear();
            }

            public bool Contains(ListItem item)
            {
                if (item == null)
                {
                    throw new ArgumentNullException("item");
                }
                return listViewitems.IndexOf(item) != -1;
            }

            public virtual bool ContainsKey(string key)
            {
                return IsValidIndex(IndexOfKey(key));
            }

            public IEnumerator GetEnumerator()
            {
                ListItem[] array = listViewitems.ToArray();
                if (array != null)
                {
                    return array.GetEnumerator();
                }
                return new ListItem[0].GetEnumerator();
            }

            public int IndexOf(ListItem item)
            {
                if (item == null)
                {
                    throw new ArgumentNullException("value");
                }
                for (int i = 0; i < Count; i++)
                {
                    if (this[i] == item)
                    {
                        return i;
                    }
                }
                return -1;
            }

            public virtual int IndexOfKey(string key)
            {
                if (!string.IsNullOrEmpty(key))
                {
                    if (IsValidIndex(lastAccessedIndex) && this[lastAccessedIndex].Name.CompareTo(key) == 0)
                    {
                        return lastAccessedIndex;
                    }
                    for (int i = 0; i < Count; i++)
                    {
                        if (this[i].Name.CompareTo(key) == 1)
                        {
                            lastAccessedIndex = i;
                            return i;
                        }
                    }
                    lastAccessedIndex = -1;
                }
                return -1;
            }

            public void Insert(int index, string text)
            {
                ListItem listItem = new ListItem();
                listItem.Text = text;
                ListItem tabPage = listItem;
                Insert(index, tabPage);
            }

            public void Insert(int index, ListItem tabPage)
            {
                listViewitems.Insert(index, tabPage);
            }

            public void Insert(int index, string key, string text)
            {
                ListItem listItem = new ListItem();
                listItem.Name = key;
                listItem.Text = text;
                ListItem tabPage = listItem;
                Insert(index, tabPage);
            }

            public void Insert(int index, string key, string text, object userData)
            {
                ListItem listItem = new ListItem();
                listItem.Name = key;
                listItem.Text = text;
                ListItem listItem2 = listItem;
                Insert(index, listItem2);
                listItem2.Tag = userData;
            }

            private bool IsValidIndex(int index)
            {
                return index >= 0 && index < Count;
            }

            public void Remove(ListItem value)
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                listViewitems.Remove(value);
            }

            public void RemoveAt(int index)
            {
                listViewitems.RemoveAt(index);
            }

            public virtual void RemoveByKey(string key)
            {
                int index = IndexOfKey(key);
                if (IsValidIndex(index))
                {
                    RemoveAt(index);
                }
            }

            void ICollection.CopyTo(Array dest, int index)
            {
                if (Count > 0)
                {
                    Array.Copy(listViewitems.ToArray(), 0, dest, index, Count);
                }
            }

            int IList.Add(object value)
            {
                if (!(value is ListItem))
                {
                    throw new ArgumentException("value");
                }
                Add((ListItem)value);
                return IndexOf((ListItem)value);
            }

            bool IList.Contains(object item)
            {
                return item is ListItem && Contains((ListItem)item);
            }

            int IList.IndexOf(object item)
            {
                if (item is ListItem)
                {
                    return IndexOf((ListItem)item);
                }
                return -1;
            }

            void IList.Insert(int index, object tabPage)
            {
                if (!(tabPage is ListItem))
                {
                    throw new ArgumentException("tabPage");
                }
                Insert(index, (ListItem)tabPage);
            }

            void IList.Remove(object value)
            {
                if (value is ListItem)
                {
                    Remove((ListItem)value);
                }
            }
        }

        private ListViewItemCollection itemCollection;

        public string Name { get; set; }

        public string Text { get; set; }

        public object Tag { get; set; }

        public ListViewItemCollection ItemCollection
        {
            get
            {
                return itemCollection;
            }
        }

        public ListItemGroup()
        {
            itemCollection = new ListViewItemCollection(this);
        }
    }
}
