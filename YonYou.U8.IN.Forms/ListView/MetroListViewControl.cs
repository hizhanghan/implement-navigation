﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms
{
    public partial class MetroListViewControl : UserControl
    {
        internal ListView listView;
        internal TextBox box;
        public MetroListViewControl()
        {
            InitializeComponent();
        }
        public void InitListView(ItemGroupCollection collection)
        {
            if (listView == null)
            {
                listView = new ListView();
                listView.ItemClick += OnItemClick;
                listView.LayoutUpdated += cmdListView_LayoutUpdated;
            }
            listView.Size = new Size(base.ClientRectangle.Width, base.ClientRectangle.Height);
            listView.InitListView(collection);
            listView.Location = new Point(0, 0);
            if (box == null)
            {
                box = new TextBox();
            }
            box.Height = 0;
            box.Width = 0;
            base.Controls.Add(box);
            base.Controls.Add(listView);
        }
    }
}
