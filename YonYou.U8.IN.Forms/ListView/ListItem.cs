﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-05-28 02:05:01
// Email:hanlilong2004@163.com
// Description:
**************************************/

namespace YonYou.U8.IN.Forms
{
    public class ListItem
    {
        public string Name { get; set; }

        public string Text { get; set; }

        public object Tag { get; set; }
    }
}