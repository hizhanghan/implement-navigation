﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-05-28 02:06:09
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms
{
    public class MetroListView : ListView
    {

        public void InitListView(ItemGroupCollection collection)
        {
            if (listView == null)
            {
                listView = new MetroListView(style);
                listView.ItemClick += OnItemClick;
                listView.LayoutUpdated += cmdListView_LayoutUpdated;
            }
            listView.Size = new Size(base.ClientRectangle.Width, base.ClientRectangle.Height);
            listView.InitListView(collection);
            listView.Location = new Point(0, 0);
            if (box == null)
            {
                box = new TextBox();
            }
            box.Height = 0;
            box.Width = 0;
            base.Controls.Add(box);
            base.Controls.Add(listView);
        }
    }
}
