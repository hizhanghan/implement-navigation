﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-05-28 02:05:42
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YonYou.U8.IN.Forms
{
    public class ItemGroupCollection : IList, ICollection, IEnumerable
    {
        private int lastAccessedIndex = -1;

        private List<ListItemGroup> listViewitems;

        public int Count
        {
            get
            {
                return listViewitems.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        public virtual ListItemGroup this[int index]
        {
            get
            {
                return listViewitems[index];
            }
            set
            {
                listViewitems.Insert(index, value);
            }
        }

        public virtual ListItemGroup this[string key]
        {
            get
            {
                if (!string.IsNullOrEmpty(key))
                {
                    int index = IndexOfKey(key);
                    if (IsValidIndex(index))
                    {
                        return this[index];
                    }
                }
                return null;
            }
        }

        bool ICollection.IsSynchronized
        {
            get
            {
                return false;
            }
        }

        object ICollection.SyncRoot
        {
            get
            {
                return this;
            }
        }

        bool IList.IsFixedSize
        {
            get
            {
                return false;
            }
        }

        object IList.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                if (!(value is ListItemGroup))
                {
                    throw new ArgumentException("value");
                }
                this[index] = (ListItemGroup)value;
            }
        }

        public ItemGroupCollection()
        {
            listViewitems = new List<ListItemGroup>();
        }

        public void Add(string text)
        {
            ListItemGroup listItemGroup = new ListItemGroup();
            listItemGroup.Text = text;
            ListItemGroup item = listItemGroup;
            listViewitems.Add(item);
        }

        public void Add(ListItemGroup value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            listViewitems.Add(value);
        }

        public void Add(string key, string text)
        {
            ListItemGroup listItemGroup = new ListItemGroup();
            listItemGroup.Name = key;
            listItemGroup.Text = text;
            ListItemGroup value = listItemGroup;
            Add(value);
        }

        public void Add(string key, string text, object userData)
        {
            ListItemGroup listItemGroup = new ListItemGroup();
            listItemGroup.Name = key;
            listItemGroup.Text = text;
            listItemGroup.Tag = userData;
            ListItemGroup value = listItemGroup;
            Add(value);
        }

        public void AddRange(ListItemGroup[] items)
        {
            if (items == null)
            {
                throw new ArgumentNullException("items");
            }
            foreach (ListItemGroup value in items)
            {
                Add(value);
            }
        }

        public virtual void Clear()
        {
            listViewitems.Clear();
        }

        public bool Contains(ListItemGroup item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            return listViewitems.IndexOf(item) != -1;
        }

        public virtual bool ContainsKey(string key)
        {
            return IsValidIndex(IndexOfKey(key));
        }

        public IEnumerator GetEnumerator()
        {
            ListItemGroup[] array = listViewitems.ToArray();
            if (array != null)
            {
                return array.GetEnumerator();
            }
            return new ListItemGroup[0].GetEnumerator();
        }

        public int IndexOf(ListItemGroup item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("value");
            }
            for (int i = 0; i < Count; i++)
            {
                if (this[i] == item)
                {
                    return i;
                }
            }
            return -1;
        }

        public virtual int IndexOfKey(string key)
        {
            if (!string.IsNullOrEmpty(key))
            {
                if (IsValidIndex(lastAccessedIndex) && this[lastAccessedIndex].Name.CompareTo(key) == 0)
                {
                    return lastAccessedIndex;
                }
                for (int i = 0; i < Count; i++)
                {
                    if (this[i].Name.CompareTo(key) == 1)
                    {
                        lastAccessedIndex = i;
                        return i;
                    }
                }
                lastAccessedIndex = -1;
            }
            return -1;
        }

        public void Insert(int index, string text)
        {
            ListItemGroup listItemGroup = new ListItemGroup();
            listItemGroup.Text = text;
            ListItemGroup tabPage = listItemGroup;
            Insert(index, tabPage);
        }

        public void Insert(int index, ListItemGroup tabPage)
        {
            listViewitems.Insert(index, tabPage);
        }

        public void Insert(int index, string key, string text)
        {
            ListItemGroup listItemGroup = new ListItemGroup();
            listItemGroup.Name = key;
            listItemGroup.Text = text;
            ListItemGroup tabPage = listItemGroup;
            Insert(index, tabPage);
        }

        public void Insert(int index, string key, string text, object userData)
        {
            ListItemGroup listItemGroup = new ListItemGroup();
            listItemGroup.Name = key;
            listItemGroup.Text = text;
            ListItemGroup listItemGroup2 = listItemGroup;
            Insert(index, listItemGroup2);
            listItemGroup2.Tag = userData;
        }

        private bool IsValidIndex(int index)
        {
            return index >= 0 && index < Count;
        }

        public void Remove(ListItemGroup value)
        {
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            listViewitems.Remove(value);
        }

        public void RemoveAt(int index)
        {
            listViewitems.RemoveAt(index);
        }

        public virtual void RemoveByKey(string key)
        {
            int index = IndexOfKey(key);
            if (IsValidIndex(index))
            {
                RemoveAt(index);
            }
        }

        void ICollection.CopyTo(Array dest, int index)
        {
            if (Count > 0)
            {
                Array.Copy(listViewitems.ToArray(), 0, dest, index, Count);
            }
        }

        int IList.Add(object value)
        {
            if (!(value is ListItemGroup))
            {
                throw new ArgumentException("value");
            }
            Add((ListItemGroup)value);
            return IndexOf((ListItemGroup)value);
        }

        bool IList.Contains(object item)
        {
            return item is ListItemGroup && Contains((ListItemGroup)item);
        }

        int IList.IndexOf(object item)
        {
            if (item is ListItemGroup)
            {
                return IndexOf((ListItemGroup)item);
            }
            return -1;
        }

        void IList.Insert(int index, object tabPage)
        {
            if (!(tabPage is ListItemGroup))
            {
                throw new ArgumentException("tabPage");
            }
            Insert(index, (ListItemGroup)tabPage);
        }

        void IList.Remove(object value)
        {
            if (value is ListItemGroup)
            {
                Remove((ListItemGroup)value);
            }
        }
    }
}
