﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms
{
    public  class MetroPanel : Panel
    {
        public MetroPanel()
        {
            base.SetStyle(ControlStyles.ContainerControl | ControlStyles.UserPaint 
                | ControlStyles.ResizeRedraw | ControlStyles.Selectable 
                | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
            base.BackColor = ColorTranslator.FromHtml("#E9F3FE");
        }
        [Description("设置边框颜色.")]
        [Category("Metro")]
        public Color BorderColor
        {
            get
            {
                return this._borderColor;
            }
            set
            {
                this._borderColor = value;
                base.Invalidate();
            }
        }

        [Category("Metro")]
        [Description("设置边框位置.")]
        public MetroPanel.BorderPosition BorderDrawPosition
        {
            get
            {
                return this._Position;
            }
            set
            {
                this._Position = value;
                base.Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            Rectangle clientRectangle = base.ClientRectangle;
            using (Brush brush = new SolidBrush(this.BorderColor))
            {
                using (Pen pen = new Pen(brush))
                {
                    if (this._Position == MetroPanel.BorderPosition.Bottom)
                    {
                        e.Graphics.DrawLine(pen, clientRectangle.X, clientRectangle.Bottom - 1, clientRectangle.Right, clientRectangle.Bottom - 1);
                    }
                    else if (this._Position == MetroPanel.BorderPosition.Top)
                    {
                        e.Graphics.DrawLine(pen, clientRectangle.X, clientRectangle.Y, clientRectangle.Right, clientRectangle.Y);
                    }
                }
            }
        }

        private Color _borderColor = ColorTranslator.FromHtml("#AFC3D9");

        private MetroPanel.BorderPosition _Position = MetroPanel.BorderPosition.Bottom;

        public enum BorderPosition
        {
            Left,
            Top,
            Right,
            Bottom,
            All,
            None
        }
    }
}
