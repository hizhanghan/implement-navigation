﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms
{
    public  class MetroSplit : SplitContainer
    {
        public MetroSplit()
        {
            base.Orientation = Orientation.Vertical;
            base.BorderStyle = BorderStyle.None;
            base.BackColor = Color.Transparent;
            base.Panel1.BackColor = Color.Transparent;
            base.Panel2.BackColor = Color.Transparent;
            base.SetStyle(ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
            base.SplitterMoved += this.MetroSplit_SplitterMoved;
        }

		protected override void OnPaint(PaintEventArgs e)
		{
			base.OnPaint(e);
			Graphics graphics = e.Graphics;
			graphics.CompositingQuality = CompositingQuality.HighQuality;
			graphics.SmoothingMode = SmoothingMode.AntiAlias;
			graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
			Rectangle splitterRectangle = base.SplitterRectangle;
			int x = splitterRectangle.X;
			int y = splitterRectangle.Y;
			int right = splitterRectangle.Right;
			int bottom = splitterRectangle.Bottom;
			using (Brush brush = new SolidBrush(Color.Silver))
			{
				using (Pen pen = new Pen(brush, 1.5f))
				{
					graphics.DrawLine(pen, x, y, x, bottom);
				}
			}
		}

		private void MetroSplit_SplitterMoved(object sender, SplitterEventArgs e)
		{
			base.Panel1.Focus();
		}
	}
}
