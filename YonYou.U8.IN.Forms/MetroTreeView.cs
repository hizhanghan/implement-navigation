﻿/*************************************
// Copyright (c) 2018 hanlilong,All rights reserved.
// Author:hanlilong
// Created:2023-03-18 00:44:09
// Email:hanlilong2004@163.com
// Description:
**************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.Win32;

namespace YonYou.U8.IN.Forms
{
    public class MetroTreeView : TreeView
    {
        public MetroTreeView()
        {
            base.HideSelection = false;
        }

        [Description("隐藏滚动条设置.")]
        [Category("Metro")]
        public MetroHideScrollBar HideScrollBarType
        {
            get
            {
                return this._HideScrollBar;
            }
            set
            {
                this._HideScrollBar = value;
                base.Invalidate();
            }
        }

        private void HideScrollBar(ref Message m)
        {
            int windowLong = NativeMethods.GetWindowLong(base.Handle, Constants.UF_GWL_STYLE);
            switch (this._HideScrollBar)
            {
                case MetroHideScrollBar.HorizontalScroll:
                    if ((windowLong & Constants.UF_WS_HSCROLL) == Constants.UF_WS_HSCROLL)
                    {
                        NativeMethods.ShowScrollBar(base.Handle, (int)this._HideScrollBar, 0);
                    }
                    break;
                case MetroHideScrollBar.VerticalScroll:
                    if ((windowLong & Constants.UF_WS_VSCROLL) == Constants.UF_WS_VSCROLL)
                    {
                        NativeMethods.ShowScrollBar(base.Handle, (int)this._HideScrollBar, 0);
                    }
                    break;
            }
        }

        protected override void WndProc(ref Message m)
        {
            this.HideScrollBar(ref m);
            base.WndProc(ref m);
        }

        private void MetroTreeView_DrawNode(object sender, DrawTreeNodeEventArgs e)
        {
            if ((e.State & TreeNodeStates.Selected) != (TreeNodeStates)0)
            {
                e.Graphics.FillRectangle(Brushes.DarkBlue, e.Node.Bounds);
                Font font = e.Node.NodeFont;
                if (font == null)
                {
                    font = this.Font;
                }
                e.Graphics.DrawString(e.Node.Text, font, Brushes.White, Rectangle.Inflate(e.Bounds, 2, 0));
            }
            else if ((e.State & TreeNodeStates.Focused) != (TreeNodeStates)0)
            {
                using (Pen pen = new Pen(Color.LightGray))
                {
                    pen.DashStyle = DashStyle.Dot;
                    Rectangle bounds = e.Node.Bounds;
                    bounds.Size = new Size(bounds.Width - 1, bounds.Height - 1);
                    e.Graphics.DrawRectangle(pen, bounds);
                }
            }
            else
            {
                e.DrawDefault = true;
            }
        }

        private MetroHideScrollBar _HideScrollBar = MetroHideScrollBar.All;

        public enum MetroHideScrollBar
        {
            HorizontalScroll,
            VerticalScroll,
            All
        }
    
    
    }
}
