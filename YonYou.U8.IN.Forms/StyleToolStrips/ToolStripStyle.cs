﻿namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public enum ToolStripStyle : short
	{
		StyleToolStrip = 1,
		StandardToolStrip,
		ContextMenuStrip,
		InnerToolStrip,
		ComboBoxStrip
	}
}