﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.Properties;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class StyleRendererFlat : ToolStripRenderer
	{

		internal const int PADDING = 4;
		internal const int TRASPADDING = 0;
		private int Radius = 2;
		protected void DrawBackgroundFlat(Graphics g, ToolStrip toolStrip)
		{
			Rectangle clientRectangle = toolStrip.ClientRectangle;
			using (SolidBrush solidBrush = new SolidBrush(ColorTable.ContextMenuStripBackgroundColor))
			{
				g.FillRectangle(solidBrush, new Rectangle(Point.Empty, toolStrip.Size));
			}
			using (Pen pen = new Pen(ColorTable.ContextMenuVerticalLineColor))
			{
				g.DrawLine(pen, toolStrip.DisplayRectangle.Left, 0, toolStrip.DisplayRectangle.Left, toolStrip.Height);
			}
		}

		protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
		{
			base.OnRenderToolStripBackground(e);
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			ToolStrip toolStrip = e.ToolStrip;
			if (toolStrip is MenuStrip || toolStrip is ToolStripDropDown)
			{
				this.DrawBackgroundFlat(e.Graphics, toolStrip);
			}
			else
			{
				Rectangle rect = new Rectangle(0, 0, e.ToolStrip.Width, e.ToolStrip.Height);
				using (SolidBrush solidBrush = new SolidBrush(ColorTable.ToolStripBackgroundColor))
				{
					e.Graphics.FillRectangle(solidBrush, rect);
				}
				using (Pen pen = new Pen(ColorTable.ToolStripBottomBorderColor))
				{
					e.Graphics.DrawLine(pen, rect.Left, rect.Height - 1, rect.Width, rect.Height - 1);
				}
			}
		}

		protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e)
		{
			ToolStrip toolStrip = e.ToolStrip;
			Graphics graphics = e.Graphics;
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			if (toolStrip is MenuStrip || toolStrip is ToolStripDropDown)
			{
				using (Pen pen = new Pen(ColorTable.ContextMenuStripBorderColor))
				{
					Rectangle rect = new Rectangle(0, 0, toolStrip.Width - 1, toolStrip.Height - 1);
					graphics.DrawRectangle(pen, rect);
				}
			}
			else if (toolStrip == null)
			{
				if (toolStrip is ContextMenuStrip)
				{
					using (Pen pen2 = new Pen(ColorTable.BottomBorder))
					{
						Rectangle rect = new Rectangle(Point.Empty, toolStrip.Size);
						GraphicsPath path = GraphicsPathHelper.CreatePath(rect, 4, RoundStyle.All, true);
						graphics.DrawPath(pen2, path);
					}
				}
				else
				{
					base.OnRenderToolStripBorder(e);
				}
			}
		}

		protected override void OnRenderItemText(ToolStripItemTextRenderEventArgs e)
		{
			if (e.Item is ToolStripSplitDownButtonEX)
			{
				(e.Item as ToolStripSplitDownButtonEX).DrawText(e.Graphics);
			}
			else if (e.Item is ToolStripButtonEX)
			{
				(e.Item as ToolStripButtonEX).DrawText(e.Graphics);
			}
			else if (e.Item is ToolStripDropDownButtonEX)
			{
				(e.Item as ToolStripDropDownButtonEX).DrawText(e.Graphics);
			}
			else if (e.Item is ToolStripMenuItem)
			{
				if (e.Item.Selected)
				{
					e.TextColor = e.Item.ForeColor;
				}
				ToolStripMenuItem toolStripMenuItem = e.Item as ToolStripMenuItem;
				if (!toolStripMenuItem.Enabled)
				{
					e.TextColor = ColorTable.ToolStripItemGrayColor;
				}
				using (Brush brush = new SolidBrush(e.TextColor))
				{
					e.Graphics.DrawString(e.Text, e.Item.Font, brush, new Point(e.TextRectangle.Left + 10, (e.Item.Height - e.Item.Font.Height) / 2));
				}
			}
			else
			{
				e.TextFont = e.ToolStrip.Font;
				if (!e.Item.Enabled)
				{
					TextRenderer.DrawText(e.Graphics, e.Text, e.TextFont, e.TextRectangle, ColorTable.ToolStripItemGrayColor);
				}
				else
				{
					base.OnRenderItemText(e);
				}
			}
		}

		protected override void OnRenderItemImage(ToolStripItemImageRenderEventArgs e)
		{
			if (e.Item is ToolStripSplitDownButtonEX)
			{
				ToolStripSplitDownButtonEX toolStripSplitDownButton = e.Item as ToolStripSplitDownButtonEX;
				if (toolStripSplitDownButton.Image != null)
				{
					int left = toolStripSplitDownButton.ButtonBounds.Left + (toolStripSplitDownButton.ButtonBounds.Width - toolStripSplitDownButton.Image.Width) / 2;
					int top = toolStripSplitDownButton.ButtonBounds.Top + (toolStripSplitDownButton.ButtonBounds.Height - toolStripSplitDownButton.Image.Height) / 2;
					this.DrawImage(e, left, top, toolStripSplitDownButton.Image.Width, toolStripSplitDownButton.Image.Height);
				}
			}
			else if (e.Item is ToolStripButtonEX)
			{
				ToolStripButtonEX toolStripButton = e.Item as ToolStripButtonEX;
				if (toolStripButton.Image != null)
				{
					int left = toolStripButton.ImageRectangle.Left + (toolStripButton.ImageRectangle.Width - toolStripButton.Image.Width) / 2;
					int top = toolStripButton.ImageRectangle.Top + (toolStripButton.ImageRectangle.Height - toolStripButton.Image.Height) / 2 + 2;
					this.DrawImage(e, left, top, toolStripButton.Image.Width, toolStripButton.Image.Height);
				}
			}
			else if (e.Item is ToolStripDropDownButtonEX)
			{
				ToolStripDropDownButtonEX toolStripDropDownButton = e.Item as ToolStripDropDownButtonEX;
				if (toolStripDropDownButton.Image != null)
				{
					int left = toolStripDropDownButton.ImageRectangle.Left + (toolStripDropDownButton.ImageRectangle.Width - toolStripDropDownButton.Image.Width) / 2;
					int top = toolStripDropDownButton.ImageRectangle.Top + (toolStripDropDownButton.ImageRectangle.Height - toolStripDropDownButton.Image.Height) / 2 + 2;
					this.DrawImage(e, left, top, toolStripDropDownButton.Image.Width, toolStripDropDownButton.Image.Height);
				}
			}
			else
			{
				base.OnRenderItemImage(e);
			}
		}

		private void DrawImage(ToolStripItemImageRenderEventArgs e, int left, int top, int width, int height)
		{
			if (e.Item.Enabled)
			{
				e.Graphics.DrawImage(e.Item.Image, left, top, e.Item.Image.Width, e.Item.Image.Height);
			}
			else
			{
				ControlPaintEx.DrawGrayImage2(e.Graphics, e.Item.Image, new Rectangle(left, top, e.Item.Image.Width, e.Item.Image.Height));
			}
		}

		protected virtual void OnRenderSplitDownButton(ToolStripItemRenderEventArgs e)
		{
			ToolStripSplitDownButtonEX toolStripSplitDownButton = e.Item as ToolStripSplitDownButtonEX;
			if (toolStripSplitDownButton != null)
			{
				Color color = Color.Empty;
				Color color2 = Color.Empty;
				if (toolStripSplitDownButton.Selected)
				{
					if (toolStripSplitDownButton.IsPointInButton)
					{
						color = ColorTable.ItemSelectedBackgroundColor;
					}
					if (toolStripSplitDownButton.IsPointInDropDown)
					{
						color2 = ColorTable.ItemSelectedBackgroundColor;
					}
				}
				if (toolStripSplitDownButton.Pressed)
				{
					if (toolStripSplitDownButton.IsPointInButton)
					{
						color = ColorTable.ItemPressedBackgroundColor;
					}
					else if (toolStripSplitDownButton.IsPointInDropDown)
					{
						color = ColorTable.ItemPressedBackgroundColor;
						color2 = ColorTable.ItemPressedBackgroundColor;
					}
					else
					{
						color = ColorTable.ItemPressedBackgroundColor;
						color2 = ColorTable.ItemPressedBackgroundColor;
					}
				}
				Graphics graphics = e.Graphics;
				this.DrawBackGround(graphics, color, toolStripSplitDownButton.ButtonBounds, e.Item);
				Rectangle rect = new Rectangle(toolStripSplitDownButton.DropDownButtonBounds.Left, toolStripSplitDownButton.DropDownButtonBounds.Top - 1, toolStripSplitDownButton.DropDownButtonBounds.Width, toolStripSplitDownButton.DropDownButtonBounds.Height + 1);
				this.DrawBackGround(graphics, color2, rect, e.Item);
			}
		}

		protected override void OnRenderButtonBackground(ToolStripItemRenderEventArgs e)
		{
			base.OnRenderButtonBackground(e);
			if (!(e.Item is ToolStripCheckBoxButtonEX))
			{
				Graphics graphics = e.Graphics;
				Rectangle rectangle = new Rectangle(Point.Empty, e.Item.Size);
				Color color = Color.Empty;
				Color color2 = Color.Empty;
				ToolStripButton toolStripButton = e.Item as ToolStripButton;
				if (toolStripButton.Pressed || toolStripButton.Checked)
				{
					color = ColorTable.ItemPressedBackgroundColor;
					color2 = ColorTable.ItemPressedBorderColor;
				}
				else if (toolStripButton.Selected)
				{
					color = ColorTable.ItemSelectedBackgroundColor;
					color2 = ColorTable.ItemSelectedBorderColor;
				}
				if ((toolStripButton.Selected || toolStripButton.Pressed || toolStripButton.Checked) && color != Color.Empty && color2 != Color.Empty)
				{
					bool flag = e.Item is ToolStripButtonEX || e.Item is ToolStripDropDownButton;
					int num = flag ? 2 : 0;
					int height = flag ? (toolStripButton.Height - num * 2) : toolStripButton.Height;
					this.DrawButtonBackground(graphics, num, height, toolStripButton.Width, color, color2);
				}
			}
		}

		private void DrawButtonBackground(Graphics g, int top, int height, int width, Color backgroundColor, Color borderColor)
		{
			Rectangle rect = new Rectangle(0, top, width, height);
			using (SolidBrush solidBrush = new SolidBrush(backgroundColor))
			{
				g.FillRectangle(solidBrush, rect);
			}
			rect.Inflate(-1, -1);
			using (Pen pen = new Pen(borderColor))
			{
				g.DrawRectangle(pen, rect);
			}
		}

		protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e)
		{
			Graphics graphics = e.Graphics;
			ToolStripItem item = e.Item;
			ToolStrip toolStrip = e.ToolStrip;
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			if (toolStrip is ContextMenuStrip)
			{
				this.RenderMenuItemBackground2(e);
			}
			else if (toolStrip is ToolStripDropDown)
			{
				this.RenderMenuItemBackground2(e);
			}
		}

		private void RenderMenuItemBackground2(ToolStripItemRenderEventArgs e)
		{
			Graphics graphics = e.Graphics;
			ToolStripItem item = e.Item;
			item.AutoSize = false;
			item.Height = 24;
			Color color = Color.Empty;
			Color color2 = Color.Empty;
			if (item.Pressed)
			{
				color = ColorTable.ContextMenuItemPressedBackgroundColor;
				color2 = ColorTable.ContextMenuItemPressedBorderColor;
			}
			else if (item.Selected)
			{
				color = ColorTable.ContextMenuItemSelectedBackgroundColor;
				color2 = ColorTable.ContextMenuItemSelectedBorderColor;
			}
			if (item.Pressed || item.Selected)
			{
				Rectangle rect = new Rectangle(new Point(0, 0), item.Size);
				using (SolidBrush solidBrush = new SolidBrush(color))
				{
					graphics.FillRectangle(solidBrush, rect);
				}
				using (Pen pen = new Pen(color2))
				{
					graphics.DrawLine(pen, new Point(0, 0), new Point(rect.Width, 0));
					graphics.DrawLine(pen, new Point(0, rect.Height - 1), new Point(rect.Width, rect.Height - 1));
				}
			}
		}

		protected override void OnRenderDropDownButtonBackground(ToolStripItemRenderEventArgs e)
		{
			base.OnRenderDropDownButtonBackground(e);
			ToolStripDropDownButton toolStripDropDownButton = e.Item as ToolStripDropDownButton;
			Graphics graphics = e.Graphics;
			Color color = Color.Empty;
			Color borderColor = Color.Empty;
			if (e.Item is ToolStripDropDownButton)
			{
				ToolStripDropDownButton toolStripDropDownButton2 = e.Item as ToolStripDropDownButton;
				if (toolStripDropDownButton2.Pressed)
				{
					color = ColorTable.ItemPressedBackgroundColor;
					borderColor = ColorTable.ItemPressedBorderColor;
				}
				else if (toolStripDropDownButton2.Selected)
				{
					color = ColorTable.ItemSelectedBackgroundColor;
					borderColor = ColorTable.ItemSelectedBorderColor;
				}
				if ((toolStripDropDownButton2.Selected || toolStripDropDownButton2.Pressed) && color != Color.Empty)
				{
					int num = 2;
					int height = toolStripDropDownButton2.Height - num * 2;
					this.DrawButtonBackground(graphics, num, height, toolStripDropDownButton2.Width, color, borderColor);
				}
			}
			else if (e.Item is ToolStripSplitDownButtonEX)
			{
				this.OnRenderSplitDownButton(e);
			}
			else
			{
				if (e.Item.Pressed)
				{
					color = ColorTable.ItemPressedBackgroundColor;
					borderColor = ColorTable.ItemPressedBorderColor;
				}
				else if (e.Item.Selected)
				{
					color = ColorTable.ItemSelectedBackgroundColor;
					borderColor = ColorTable.ItemSelectedBorderColor;
				}
				if ((e.Item.Selected || e.Item.Pressed) && color != Color.Empty)
				{
					int num = 0;
					int height = e.Item.Height;
					this.DrawButtonBackground(graphics, num, height, e.Item.Width, color, borderColor);
				}
			}
		}

		protected override void OnRenderSplitButtonBackground(ToolStripItemRenderEventArgs e)
		{
			base.OnRenderSplitButtonBackground(e);
			ToolStripSplitButtonEX toolStripSplitButtonEx = e.Item as ToolStripSplitButtonEX;
			Rectangle rectangle = new Rectangle(Point.Empty, e.Item.Size);
			Graphics graphics = e.Graphics;
			Color color = Color.Empty;
			Color color2 = Color.Empty;
			if (toolStripSplitButtonEx.ButtonSelected)
			{
				if (toolStripSplitButtonEx.IsButtonSelected)
				{
					color = ColorTable.ItemSelectedBackgroundColor;
				}
				if (toolStripSplitButtonEx.IsDropDownButtonSelected)
				{
					color2 = ColorTable.ItemSelectedBackgroundColor;
				}
			}
			if (toolStripSplitButtonEx.Pressed)
			{
				if (toolStripSplitButtonEx.DropDownButtonPressed)
				{
					color = ColorTable.ItemPressedBackgroundColor;
					color2 = ColorTable.ItemPressedBackgroundColor;
				}
				else if (toolStripSplitButtonEx.ButtonPressed)
				{
					color = ColorTable.ItemPressedBackgroundColor;
				}
			}
			Rectangle rect = new Rectangle(toolStripSplitButtonEx.ButtonBounds.Left, toolStripSplitButtonEx.ButtonBounds.Top, toolStripSplitButtonEx.ButtonBounds.Width + 2, toolStripSplitButtonEx.ButtonBounds.Height);
			this.DrawBackGround(graphics, color, rect, toolStripSplitButtonEx);
			this.DrawBackGround(graphics, color2, toolStripSplitButtonEx.DropDownButtonBounds, toolStripSplitButtonEx);
			float x = (float)(toolStripSplitButtonEx.DropDownButtonBounds.X + (toolStripSplitButtonEx.DropDownButtonBounds.Width - Resources.OverFlowButton.Width) / 2);
			float y = (float)((toolStripSplitButtonEx.DropDownButtonBounds.Height - Resources.OverFlowButton.Height) / 2);
			e.Graphics.DrawImage(Resources.OverFlowButton, x, y);
		}

		private void DrawBackGround(Graphics g, Color color, Rectangle rect, ToolStripItem item)
		{
			if (color != Color.Empty)
			{
				using (SolidBrush solidBrush = new SolidBrush(color))
				{
					g.FillRectangle(solidBrush, rect);
				}
			}
			if (item.Selected || item.Pressed)
			{
				using (Pen pen = new Pen(ColorTable.ItemSelectedBorderColor))
				{
					g.DrawRectangle(pen, new Rectangle(rect.Left, rect.Top, rect.Width - 1, rect.Height - 1));
				}
			}
		}

		protected override void OnRenderArrow(ToolStripArrowRenderEventArgs e)
		{
			if (!(e.Item is ToolStripDropDownButton) && !(e.Item is ToolStripSplitDownButtonEX))
			{
				if (e.Item is ToolStripCmbButtonEX)
				{
					float x = (float)(e.ArrowRectangle.Left + (e.ArrowRectangle.Width - Resources.OverFlowButton.Width) / 2);
					float y = (float)((e.ArrowRectangle.Height - Resources.OverFlowButton.Height) / 2);
					e.Graphics.DrawImage(Resources.OverFlowButton, x, y);
				}
				else if (e.Item is ToolStripDropDownButton)
				{
					float x = (float)(e.ArrowRectangle.Left + (e.ArrowRectangle.Width - Resources.OverFlowButton.Width) / 2);
					float y = (float)((e.ArrowRectangle.Height - Resources.OverFlowButton.Height) / 2);
					e.Graphics.DrawImage(Resources.OverFlowButton, x, y);
				}
				else if (e.Item is ToolStripMenuItem)
				{
					ToolStripMenuItem toolStripMenuItem = e.Item as ToolStripMenuItem;
					if (toolStripMenuItem != null && toolStripMenuItem.HasDropDownItems)
					{
						if (toolStripMenuItem.Selected)
						{
							Rectangle arrowRectangle = e.ArrowRectangle;
							arrowRectangle.Inflate(4, 1);
							ToolStripMenuItemEX toolStripMenuItemEx = e.Item as ToolStripMenuItemEX;
							if (toolStripMenuItemEx != null)
							{
								if (toolStripMenuItemEx.MenuItemStyle == MenuItemStyle.DropDownArrow)
								{
									Color buttonOnBeginColor = ColorTable.ButtonOnBeginColor;
									Color buttonOnEndColor = ColorTable.ButtonOnEndColor;
									using (Brush brush = new SolidBrush(ColorTable.MouseOnBorder))
									{
										GraphicsPath path = GraphicsPathHelper.CreatePath(arrowRectangle, 4, RoundStyle.All, false);
										e.Graphics.FillPath(brush, path);
									}
									using (Brush brush2 = new LinearGradientBrush(arrowRectangle, buttonOnBeginColor, buttonOnEndColor, LinearGradientMode.Vertical))
									{
										arrowRectangle.Inflate(-2, -2);
										e.Graphics.FillRectangle(brush2, arrowRectangle);
									}
									e.ArrowRectangle = arrowRectangle;
								}
							}
						}
						Image toolBar_OverFlow = Resources.ToolBar_OverFlow;
						if (toolBar_OverFlow != null)
						{
							int x2 = (e.ArrowRectangle.Width - toolBar_OverFlow.Width) / 2 + e.ArrowRectangle.Left;
							int y2 = (e.Item.Height - toolBar_OverFlow.Height) / 2;
							e.Graphics.DrawImage(toolBar_OverFlow, x2, y2, toolBar_OverFlow.Width, toolBar_OverFlow.Height);
						}
						e.ArrowColor = Color.Black;
					}
				}
				else
				{
					base.OnRenderArrow(e);
				}
			}
		}

		protected override void OnRenderOverflowButtonBackground(ToolStripItemRenderEventArgs e)
		{
			base.OnRenderOverflowButtonBackground(e);
			ToolStripOverflowButton toolStripOverflowButton = e.Item as ToolStripOverflowButton;
			if (toolStripOverflowButton != null)
			{
				Rectangle rect = new Rectangle(0, 0, 2, e.Item.Height);
				using (Brush brush = new LinearGradientBrush(rect, ColorTable.SeparatorBeginColor, ColorTable.SeparatorEndColor, 90f, true))
				{
					e.Graphics.FillRectangle(brush, rect);
					int y = (e.Item.Height - Resources.OverFlowButton.Height) / 2;
					int x = (e.Item.Width - Resources.OverFlowButton.Width) / 2;
					Rectangle rect2 = new Rectangle(x, y, Resources.OverFlowButton.Width, Resources.OverFlowButton.Height);
					e.Graphics.DrawImage(Resources.OverFlowButton, rect2);
				}
			}
		}

		protected override void OnRenderSeparator(ToolStripSeparatorRenderEventArgs e)
		{
			base.OnRenderSeparator(e);
			if (e.Item is ToolStripSeparator)
			{
				ToolStripSeparator toolStripSeparator = e.Item as ToolStripSeparator;
				Rectangle rect = new Rectangle(0, 0, e.Item.Width, e.Item.Height);
				if (e.ToolStrip != null)
				{
					using (LinearGradientBrush linearGradientBrush = new LinearGradientBrush(rect, Color.Transparent, Color.Transparent, 90f, true))
					{
						linearGradientBrush.InterpolationColors = ColorTable.ToolStripSeparatorColorBlend;
						Rectangle rect2 = new Rectangle(1, e.Item.ContentRectangle.Y, 1, e.Item.ContentRectangle.Height);
						e.Graphics.FillRectangle(linearGradientBrush, rect2);
					}
				}
				if (e.ToolStrip is ToolStripDropDown || e.ToolStrip is ContextMenuStrip)
				{
					using (Brush brush = new LinearGradientBrush(rect, ColorTable.SeparatorBeginColor, ColorTable.SeparatorEndColor, 90f, true))
					{
						Rectangle rect2 = new Rectangle(e.ToolStrip.DisplayRectangle.Left, e.Item.Height / 2, e.ToolStrip.Width, 1);
						e.Graphics.FillRectangle(brush, rect2);
					}
				}
			}
		}

	}
}