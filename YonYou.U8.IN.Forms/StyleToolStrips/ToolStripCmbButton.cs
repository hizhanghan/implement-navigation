﻿using System.Drawing;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class ToolStripCmbButton : ToolStripDropDownButton
	{
		public ToolStripCmbButton()
		{
		}

		public ToolStripCmbButton(string text, Image image)
			: base(text, image)
		{
		}
	}
}