﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using YonYou.U8.IN.Forms.Properties;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	[ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.All)]
	[DefaultProperty("Value")]
	public class ToolStripSplitDownButtonEX : System.Windows.Forms.ToolStripDropDownButton
	{
		public ToolStripSplitDownButtonEX()
		{
			this.Initialize(string.Empty, null);
		}

		public ToolStripSplitDownButtonEX(string text, Image image)
		{
			this.Initialize(text, image);
		}

		private void Initialize(string text, Image image)
		{
			this.Text = text;
			this.Image = image;
			base.ImageAlign = ContentAlignment.TopCenter;
			base.AutoSize = false;
			base.MouseMove += this.ToolStripSplitDownButton_MouseMove;
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			if (!this.IsPointInDropDown)
			{
				if (e.Button != MouseButtons.Left)
				{
					base.OnMouseDown(e);
				}
			}
			else if (!base.IsOnDropDown)
			{
				base.OnMouseDown(e);
			}
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.OnMouseUp(e);
			if (e.Button == MouseButtons.Left)
			{
				base.Invalidate();
				if (this.IsPointInButton)
				{
					if (this.ButtonClick != null)
					{
						this.ButtonClick(this, new EventArgs());
					}
				}
			}
		}

		private void ToolStripSplitDownButton_MouseMove(object sender, MouseEventArgs e)
		{
			base.Invalidate();
		}

		public Rectangle ButtonBounds
		{
			get
			{
				return new Rectangle(0, 3, base.Width, 36);
			}
		}

		public Rectangle DropDownButtonBounds
		{
			get
			{
				return new Rectangle(0, this.ButtonBounds.Bottom, base.Width, 34);
			}
		}

		public void DrawText(Graphics g)
		{
			if (this.splitText != null)
			{
				Color color = this.Enabled ? this.ForeColor : ColorTable.ToolStripItemGrayColor;
				using (SolidBrush solidBrush = new SolidBrush(color))
				{
					if (this.splitText.Length == 1)
					{
						Size size = TextRenderer.MeasureText(this.Text, base.Owner.Font);
						Point pt = new Point((this.DropDownButtonBounds.Width - size.Width) / 2 + this.Padding.Left, 3 + this.DropDownButtonBounds.Top);
						TextRenderer.DrawText(g, this.Text, this.Font, pt, color);
						int x = (this.DropDownButtonBounds.Width - Resources.OverFlowButton.Width) / 2 + this.DropDownButtonBounds.Left;
						int y = this.DropDownButtonBounds.Bottom - Resources.OverFlowButton.Height - 9;
						g.DrawImage(Resources.OverFlowButton, x, y);
					}
					else
					{
						SizeF sizeF = g.MeasureString(this.splitText[0], base.Owner.Font);
						Point pt = new Point((this.DropDownButtonBounds.Width - (int)Convert.ToInt16(sizeF.Width)) / 2 + this.Padding.Left, 4 + this.DropDownButtonBounds.Top);
						g.DrawString(this.splitText[0], base.Owner.Font, solidBrush, pt.X, pt.Y);
						sizeF = g.MeasureString(this.splitText[1], base.Owner.Font);
						sizeF.Width += Resources.OverFlowButton.Width;
						pt = new Point((this.DropDownButtonBounds.Width - Convert.ToInt16(sizeF.Width)) / 2 + this.Padding.Left, 4 + this.DropDownButtonBounds.Top);
						g.DrawString(this.splitText[1], base.Owner.Font, solidBrush, pt.X, pt.Y + sizeF.Height);
						int x = pt.X + (int)sizeF.Width - Resources.OverFlowButton.Width - 2;
						int y = pt.Y + ((int)sizeF.Height - Resources.OverFlowButton.Height) / 2 + (int)sizeF.Height - 1;
						g.DrawImage(Resources.OverFlowButton, x, y);
					}
				}
			}
		}

		public bool IsTextWrapped
		{
			get
			{
				return this.isTextWrapped;
			}
		}

		public new string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				this.splitText = base.Text.Split(new char[]
				{
					'#'
				});
				if (this.splitText.Length == 2)
				{
					base.Text = ((this.splitText[0].Length > this.splitText[1].Length) ? this.splitText[0] : this.splitText[1]);
					this.isTextWrapped = true;
				}
				base.Invalidate();
			}
		}

		public event EventHandler ButtonClick;

		public bool IsPointInButton
		{
			get
			{
				return this.IsPointIntRect(this.ButtonBounds);
			}
		}

		public bool IsPointInDropDown
		{
			get
			{
				return this.IsPointIntRect(this.DropDownButtonBounds);
			}
		}

		private bool IsPointIntRect(Rectangle rect)
		{
			Point point = Cursor.Position;
			point = base.Parent.PointToClient(point);
			point.X -= this.Bounds.Left;
			return rect.Contains(point);
		}

		private bool isTextWrapped = false;

		private string[] splitText;
	}
}