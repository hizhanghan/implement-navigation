﻿using System.Drawing;
using System.IO;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class ImageDescriptor
	{
		private Image _image;

		public ImageDescriptor()
		{
		}

		private ImageDescriptor(Image img)
		{
			_image = img;
		}

		public Image GetImage()
		{
			return _image;
		}

		public Image CreateImage()
		{
			return _image;
		}

		public static ImageDescriptor CreateFromPath(string path)
		{
			if (File.Exists(path))
			{
				return new ImageDescriptor(new Bitmap(path));
			}
			return null;
		}
	}
}