﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.Properties;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
    public class StyleRenderer : ToolStripRenderer
    {
        internal const int PADDING = 4;
        internal const int TRASPADDING = 0;
        private int Radius = 2;
        protected void DrawBorder(Graphics g, ToolStrip toolStrip)
        {
            Rectangle clientRectangle = toolStrip.ClientRectangle;
            GraphicsPath path = GraphicsPathHelper.CreatePath(clientRectangle, 10, RoundStyle.All, correction: false);
            toolStrip.Region = new Region(path);
            Image toolbar_dropdown_panel = Resources.toolbar_dropdown_panel;
            int num = 10;
            int num2 = 10;
            Rectangle srcRect = new Rectangle(0, 0, num, num2);
            Rectangle destRect = new Rectangle(0, 0, num, num2);
            g.DrawImage(toolbar_dropdown_panel, destRect, srcRect, GraphicsUnit.Pixel);
            Rectangle srcRect2 = new Rectangle(num, 0, 1, num2);
            Rectangle destRect2 = new Rectangle(num, 0, toolStrip.Width - num * 2, num2);
            g.DrawImage(toolbar_dropdown_panel, destRect2, srcRect2, GraphicsUnit.Pixel);
            Rectangle srcRect3 = new Rectangle(toolbar_dropdown_panel.Width - num, 0, num, num2);
            Rectangle destRect3 = new Rectangle(toolStrip.Width - num, 0, num, num2);
            g.DrawImage(toolbar_dropdown_panel, destRect3, srcRect3, GraphicsUnit.Pixel);
            int y = toolbar_dropdown_panel.Height - num;
            int y2 = toolStrip.Height - num;
            Rectangle srcRect4 = new Rectangle(0, y, num, num);
            Rectangle destRect4 = new Rectangle(0, y2, num, num);
            g.DrawImage(toolbar_dropdown_panel, destRect4, srcRect4, GraphicsUnit.Pixel);
            Rectangle srcRect5 = new Rectangle(num, y, 1, num);
            Rectangle destRect5 = new Rectangle(num, y2, toolStrip.Width - num * 2, num);
            g.DrawImage(toolbar_dropdown_panel, destRect5, srcRect5, GraphicsUnit.Pixel);
            Rectangle srcRect6 = new Rectangle(toolbar_dropdown_panel.Width - num, y, num, num);
            Rectangle destRect6 = new Rectangle(toolStrip.Width - num, y2, num, num);
            g.DrawImage(toolbar_dropdown_panel, destRect6, srcRect6, GraphicsUnit.Pixel);
            int height = toolStrip.Height - num - num2;
            Rectangle srcRect7 = new Rectangle(0, num2, num, 1);
            Rectangle destRect7 = new Rectangle(0, num2, num, height);
            g.DrawImage(toolbar_dropdown_panel, destRect7, srcRect7, GraphicsUnit.Pixel);
            Rectangle srcRect8 = new Rectangle(toolbar_dropdown_panel.Width - num, num2, num, 1);
            Rectangle destRect8 = new Rectangle(toolStrip.Width - num, num2, num, height);
            g.DrawImage(toolbar_dropdown_panel, destRect8, srcRect8, GraphicsUnit.Pixel);
            Rectangle srcRect9 = new Rectangle(toolbar_dropdown_panel.Width / 2, toolbar_dropdown_panel.Height / 2, 1, 1);
            Rectangle destRect9 = new Rectangle(num, num2, toolStrip.Width - num * 2, toolStrip.Height - num2 * 2);
            g.DrawImage(toolbar_dropdown_panel, destRect9, srcRect9, GraphicsUnit.Pixel);
            using (Pen pen = new Pen(Color.FromArgb(229, 229, 229)))
            {
                g.DrawLine(pen, toolStrip.DisplayRectangle.Left, 4, toolStrip.DisplayRectangle.Left, toolStrip.Height - 8);
            }
        }

        protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
        {
            base.OnRenderToolStripBackground(e);
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            ToolStrip toolStrip = e.ToolStrip;
            if (toolStrip is MenuStrip || toolStrip is ToolStripDropDown)
            {
                DrawBorder(e.Graphics, toolStrip);
                return;
            }

            Rectangle rect = new Rectangle(0, 0, e.ToolStrip.Width, e.ToolStrip.Height);
            using (TextureBrush brush = new TextureBrush(Resources.toolbar_Background))
            {
                e.Graphics.FillRectangle(brush, rect);
            }
        }

        protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e)
        {
            ToolStrip toolStrip = e.ToolStrip;
            Graphics graphics = e.Graphics;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            if (toolStrip is MenuStrip || toolStrip is ToolStripDropDown)
            {
                using (Pen pen = new Pen(ColorTable.BottomBorder))
                {
                    Rectangle rect = new Rectangle(Point.Empty, toolStrip.Size);
                    GraphicsPath path = GraphicsPathHelper.CreatePath(rect, 4, RoundStyle.All, correction: true);
                    graphics.DrawPath(pen, path);
                }
            }
            else
            {
                if (toolStrip != null)
                {
                    return;
                }

                if (toolStrip is ContextMenuStrip)
                {
                    using (Pen pen = new Pen(ColorTable.BottomBorder))
                    {
                        Rectangle rect = new Rectangle(Point.Empty, toolStrip.Size);
                        GraphicsPath path = GraphicsPathHelper.CreatePath(rect, 4, RoundStyle.All, correction: true);
                        graphics.DrawPath(pen, path);
                    }
                }
                else
                {
                    base.OnRenderToolStripBorder(e);
                }
            }
        }

        protected override void OnRenderItemText(ToolStripItemTextRenderEventArgs e)
        {
            if (e.Item is ToolStripSplitDownButtonEX)
            {
                (e.Item as ToolStripSplitDownButtonEX).DrawText(e.Graphics);
            }
            else if (e.Item is ToolStripButtonEX)
            {
                (e.Item as ToolStripButtonEX).DrawText(e.Graphics);
            }
            else if (e.Item is ToolStripDropDownButtonEX)
            {
                (e.Item as ToolStripDropDownButtonEX).DrawText(e.Graphics);
            }
            else if (e.Item is ToolStripMenuItem)
            {
                if (e.Item.Selected)
                {
                    e.TextColor = e.Item.ForeColor;
                }

                ToolStripMenuItem toolStripMenuItem = e.Item as ToolStripMenuItem;
                if (!toolStripMenuItem.Enabled)
                {
                    e.TextColor = ColorTable.ToolStripItemGrayColor;
                }

                using (Brush brush = new SolidBrush(e.TextColor))
                {
                    e.Graphics.DrawString(e.Text, e.Item.Font, brush, new Point(e.TextRectangle.Left + 10, (e.Item.Height - e.Item.Font.Height) / 2));
                }
            }
            else
            {
                e.TextFont = e.ToolStrip.Font;
                if (!e.Item.Enabled)
                {
                    TextRenderer.DrawText(e.Graphics, e.Text, e.TextFont, e.TextRectangle, ColorTable.ToolStripItemGrayColor);
                }
                else
                {
                    base.OnRenderItemText(e);
                }
            }
        }

        protected override void OnRenderItemImage(ToolStripItemImageRenderEventArgs e)
        {
            if (e.Item is ToolStripSplitDownButtonEX)
            {
                ToolStripSplitDownButtonEX toolStripSplitDownButton = e.Item as ToolStripSplitDownButtonEX;
                if (toolStripSplitDownButton.Image != null)
                {
                    int left = toolStripSplitDownButton.ButtonBounds.Left + (toolStripSplitDownButton.ButtonBounds.Width - toolStripSplitDownButton.Image.Width) / 2;
                    int top = toolStripSplitDownButton.ButtonBounds.Top + (toolStripSplitDownButton.ButtonBounds.Height - toolStripSplitDownButton.Image.Height) / 2;
                    DrawImage(e, left, top, toolStripSplitDownButton.Image.Width, toolStripSplitDownButton.Image.Height);
                }
            }
            else if (e.Item is ToolStripButtonEX)
            {
                ToolStripButtonEX toolStripButton = e.Item as ToolStripButtonEX;
                if (toolStripButton.Image != null)
                {
                    int left = toolStripButton.ImageRectangle.Left + (toolStripButton.ImageRectangle.Width - toolStripButton.Image.Width) / 2;
                    int top = toolStripButton.ImageRectangle.Top + (toolStripButton.ImageRectangle.Height - toolStripButton.Image.Height) / 2 + 2;
                    DrawImage(e, left, top, toolStripButton.Image.Width, toolStripButton.Image.Height);
                }
            }
            else if (e.Item is ToolStripDropDownButtonEX)
            {
                ToolStripDropDownButtonEX toolStripDropDownButton = e.Item as ToolStripDropDownButtonEX;
                if (toolStripDropDownButton.Image != null)
                {
                    int left = toolStripDropDownButton.ImageRectangle.Left + (toolStripDropDownButton.ImageRectangle.Width - toolStripDropDownButton.Image.Width) / 2;
                    int top = toolStripDropDownButton.ImageRectangle.Top + (toolStripDropDownButton.ImageRectangle.Height - toolStripDropDownButton.Image.Height) / 2 + 2;
                    DrawImage(e, left, top, toolStripDropDownButton.Image.Width, toolStripDropDownButton.Image.Height);
                }
            }
            else
            {
                base.OnRenderItemImage(e);
            }
        }

        private void DrawImage(ToolStripItemImageRenderEventArgs e, int left, int top, int width, int height)
        {
            if (e.Item.Enabled)
            {
                e.Graphics.DrawImage(e.Item.Image, left, top, e.Item.Image.Width, e.Item.Image.Height);
            }
            else
            {
                ControlPaintEx.DrawGrayImagePixel(e.Graphics, e.Item.Image, new Rectangle(left, top, e.Item.Image.Width, e.Item.Image.Height));
            }
        }

        protected virtual void OnRenderSplitDownButton(ToolStripItemRenderEventArgs e)
        {
            ToolStripSplitDownButtonEX toolStripSplitDownButton = e.Item as ToolStripSplitDownButtonEX;
            if (toolStripSplitDownButton == null)
            {
                return;
            }

            Image image = null;
            Image image2 = null;
            if (toolStripSplitDownButton.Selected)
            {
                image = Resources.toolbar_big_stroke_top;
                image2 = Resources.toolbar_big_stroke_bottom;
            }

            if (toolStripSplitDownButton.IsPointInButton)
            {
                image = Resources.toolbar_big_hover_top;
            }

            if (toolStripSplitDownButton.IsPointInDropDown)
            {
                image2 = Resources.toolbar_big_hover_bottom;
            }

            if (toolStripSplitDownButton.Pressed)
            {
                image = Resources.toolbar_big_hover_top;
                image2 = Resources.toolbar_big_select_bottom;
                if (toolStripSplitDownButton.IsPointInButton)
                {
                    image = Resources.toolbar_big_select_top;
                }
                else if (toolStripSplitDownButton.IsPointInDropDown)
                {
                    image = Resources.toolbar_big_hover_top;
                    image2 = Resources.toolbar_big_select_bottom;
                }
                else
                {
                    image = Resources.toolbar_big_hover_top;
                    image2 = Resources.toolbar_big_select_bottom;
                }
            }

            if (image != null && image2 != null)
            {
                Graphics graphics = e.Graphics;
                DrawBackGround(graphics, image, toolStripSplitDownButton.ButtonBounds, e.Item);
                DrawBackGround(graphics, image2, toolStripSplitDownButton.DropDownButtonBounds, e.Item);
            }
        }

        protected override void OnRenderButtonBackground(ToolStripItemRenderEventArgs e)
        {
            base.OnRenderButtonBackground(e);
            if (e.Item is ToolStripCheckBoxButtonEX)
            {
                return;
            }

            Graphics graphics = e.Graphics;
            Rectangle rectangle = new Rectangle(Point.Empty, e.Item.Size);
            Image image = null;
            ToolStripButton toolStripButton = e.Item as ToolStripButton;
            if (toolStripButton.Pressed || toolStripButton.Checked)
            {
                image = Resources.toolbar_small_select;
                if (toolStripButton.Height > image.Height)
                {
                    image = Resources.toolbar_big_select;
                }
            }
            else if (toolStripButton.Selected)
            {
                image = Resources.toolbar_small_hover;
                if (toolStripButton.Height > image.Height)
                {
                    image = Resources.toolbar_big_hover;
                }
            }

            if ((toolStripButton.Selected || toolStripButton.Pressed || toolStripButton.Checked) && image != null)
            {
                int top = (e.Item is ToolStripButtonEX || e.Item is ToolStripDropDownButtonEX) ? 2 : 0;
                int height = image.Height;
                DrawButtonBackground(graphics, top, height, toolStripButton.Width, image);
            }
        }

        private void DrawButtonBackground(Graphics g, int top, int height, int width, Image image)
        {
            Rectangle srcRect = new Rectangle(0, 0, Radius, height);
            Rectangle destRect = new Rectangle(0, top, Radius, height);
            g.DrawImage(image, destRect, srcRect, GraphicsUnit.Pixel);
            Rectangle srcRect2 = new Rectangle(Radius, 0, 1, height);
            Rectangle destRect2 = new Rectangle(Radius, top, width - Radius * 2, height);
            g.DrawImage(image, destRect2, srcRect2, GraphicsUnit.Pixel);
            Rectangle srcRect3 = new Rectangle(image.Width - Radius, 0, Radius, height);
            Rectangle destRect3 = new Rectangle(width - Radius, top, Radius, height);
            g.DrawImage(image, destRect3, srcRect3, GraphicsUnit.Pixel);
        }

        protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e)
        {
            Graphics graphics = e.Graphics;
            ToolStripItem item = e.Item;
            ToolStrip toolStrip = e.ToolStrip;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            if (toolStrip is ContextMenuStrip)
            {
                RenderMenuItemBackground2(e);
            }
            else if (toolStrip is ToolStripDropDown)
            {
                RenderMenuItemBackground2(e);
            }
        }

        private void RenderMenuItemBackground2(ToolStripItemRenderEventArgs e)
        {
            Graphics graphics = e.Graphics;
            ToolStripItem item = e.Item;
            item.AutoSize = false;
            item.Height = 24;
            SetMenuItemMargin(e);
            Image image = null;
            if (item.Pressed)
            {
                image = Resources.toolbar_dropdown_list_selected;
            }
            else if (item.Selected)
            {
                image = Resources.toolbar_dropdown_list_hover;
            }

            if (image != null)
            {
                Image image2 = new Bitmap(item.Width - 1, image.Height);
                Graphics graphics2 = Graphics.FromImage(image2);
                graphics2.FillRectangle(new TextureBrush(image, WrapMode.TileFlipX), 0, 0, image2.Width, image2.Height);
                graphics.DrawImage(image2, 1, 0, image2.Width, image2.Height);
                graphics2.Dispose();
            }
        }

        private void SetMenuItemMargin(ToolStripItemRenderEventArgs e)
        {
            int num = 4;
            if (e.ToolStrip.Items.Count == 1)
            {
                e.Item.Margin = new Padding(0, num, 0, num);
                return;
            }

            if (e.ToolStrip.Items.IndexOf(e.Item) == 0)
            {
                e.Item.Margin = new Padding(0, num, 0, 0);
            }

            if (e.ToolStrip.Items.IndexOf(e.Item) == e.ToolStrip.Items.Count - 1)
            {
                e.Item.Margin = new Padding(0, 0, 0, num);
            }
        }

        protected override void OnRenderDropDownButtonBackground(ToolStripItemRenderEventArgs e)
        {
            base.OnRenderDropDownButtonBackground(e);
            ToolStripDropDownButtonEX toolStripDropDownButton = e.Item as ToolStripDropDownButtonEX;
            Graphics graphics = e.Graphics;
            Image image = null;
            if (e.Item is ToolStripDropDownButtonEX)
            {
                ToolStripDropDownButtonEX toolStripDropDownButton2 = e.Item as ToolStripDropDownButtonEX;
                if (toolStripDropDownButton2.Pressed)
                {
                    image = Resources.toolbar_big_select;
                }
                else if (toolStripDropDownButton2.Selected)
                {
                    image = Resources.toolbar_big_hover;
                }

                if ((toolStripDropDownButton2.Selected || toolStripDropDownButton2.Pressed) && image != null)
                {
                    int top = 2;
                    int height = image.Height;
                    DrawButtonBackground(graphics, top, height, toolStripDropDownButton2.Width, image);
                }

                return;
            }

            if (e.Item is ToolStripSplitDownButtonEX)
            {
                OnRenderSplitDownButton(e);
                return;
            }

            if (toolStripDropDownButton.Pressed)
            {
                image = Resources.toolbar_small_select;
            }
            else if (toolStripDropDownButton.Selected)
            {
                image = Resources.toolbar_small_hover;
            }

            if (image != null)
            {
                int top = 0;
                int height = image.Height;
                Rectangle srcRect = new Rectangle(0, 0, Radius, height);
                Rectangle destRect = new Rectangle(0, top, Radius, height);
                graphics.DrawImage(image, destRect, srcRect, GraphicsUnit.Pixel);
                Rectangle srcRect2 = new Rectangle(Radius, 0, 1, height);
                Rectangle destRect2 = new Rectangle(Radius, top, toolStripDropDownButton.Width - Radius * 2, height);
                graphics.DrawImage(image, destRect2, srcRect2, GraphicsUnit.Pixel);
                Rectangle srcRect3 = new Rectangle(image.Width - Radius, 0, Radius, height);
                Rectangle destRect3 = new Rectangle(toolStripDropDownButton.Width - Radius, top, Radius, height);
                graphics.DrawImage(image, destRect3, srcRect3, GraphicsUnit.Pixel);
            }
        }

        protected override void OnRenderSplitButtonBackground(ToolStripItemRenderEventArgs e)
        {
            base.OnRenderSplitButtonBackground(e);
            ToolStripSplitButtonEX toolStripSplitButtonEx = e.Item as ToolStripSplitButtonEX;
            Rectangle rectangle = new Rectangle(Point.Empty, e.Item.Size);
            Graphics graphics = e.Graphics;
            Image image = null;
            Image image2 = null;
            if (toolStripSplitButtonEx.ButtonSelected)
            {
                image = Resources.toolbar_small_stroke_left;
                image2 = Resources.toolbar_small_stroke_right;
            }

            if (toolStripSplitButtonEx.IsButtonSelected)
            {
                image = Resources.toolbar_small_hover_left;
            }

            if (toolStripSplitButtonEx.IsDropDownButtonSelected)
            {
                image2 = Resources.toolbar_small_hover_right;
            }

            if (toolStripSplitButtonEx.DropDownButtonPressed)
            {
                image = Resources.toolbar_small_hover_left;
                image2 = Resources.toolbar_small_select_right;
            }

            if (toolStripSplitButtonEx.ButtonPressed)
            {
                image = Resources.toolbar_small_select_left;
                image2 = Resources.toolbar_small_hover_right;
            }

            if (image != null && image2 != null)
            {
                Rectangle rect = new Rectangle(toolStripSplitButtonEx.ButtonBounds.Left, toolStripSplitButtonEx.ButtonBounds.Top, toolStripSplitButtonEx.ButtonBounds.Width + 1, toolStripSplitButtonEx.ButtonBounds.Height);
                DrawBackGround(graphics, image, rect, toolStripSplitButtonEx);
                DrawBackGround(graphics, image2, toolStripSplitButtonEx.DropDownButtonBounds, toolStripSplitButtonEx);
            }

            float x = toolStripSplitButtonEx.DropDownButtonBounds.X + (toolStripSplitButtonEx.DropDownButtonBounds.Width - Resources.OverFlowButton.Width) / 2;
            float y = (toolStripSplitButtonEx.DropDownButtonBounds.Height - Resources.OverFlowButton.Height) / 2;
            e.Graphics.DrawImage(Resources.OverFlowButton, x, y);
        }

        private void DrawBackGround(Graphics g, Image image, Rectangle rect, ToolStripItem item)
        {
            Rectangle srcRect = new Rectangle(0, 0, Radius, rect.Height);
            Rectangle destRect = new Rectangle(rect.Left, rect.Top, Radius, rect.Height);
            g.DrawImage(image, destRect, srcRect, GraphicsUnit.Pixel);
            Rectangle srcRect2 = new Rectangle(Radius, 0, 1, rect.Height);
            Rectangle destRect2 = new Rectangle(rect.Left + Radius, rect.Top, rect.Width - 2 * Radius, rect.Height);
            g.DrawImage(image, destRect2, srcRect2, GraphicsUnit.Pixel);
            Rectangle srcRect3 = new Rectangle(image.Width - Radius, 0, Radius, rect.Height);
            Rectangle destRect3 = new Rectangle(rect.Right - Radius, rect.Top, Radius, rect.Height);
            g.DrawImage(image, destRect3, srcRect3, GraphicsUnit.Pixel);
        }

        protected override void OnRenderArrow(ToolStripArrowRenderEventArgs e)
        {
            if (e.Item is ToolStripDropDownButtonEX || e.Item is ToolStripSplitDownButtonEX)
            {
                return;
            }

            if (e.Item is ToolStripCmbButtonEX)
            {
                float x = e.ArrowRectangle.Left + (e.ArrowRectangle.Width - Resources.OverFlowButton.Width) / 2;
                float y = (e.ArrowRectangle.Height - Resources.OverFlowButton.Height) / 2;
                e.Graphics.DrawImage(Resources.OverFlowButton, x, y);
            }
            else if (e.Item is ToolStripDropDownButtonEX)
            {
                float x = e.ArrowRectangle.Left + (e.ArrowRectangle.Width - Resources.OverFlowButton.Width) / 2;
                float y = (e.ArrowRectangle.Height - Resources.OverFlowButton.Height) / 2;
                e.Graphics.DrawImage(Resources.OverFlowButton, x, y);
            }
            else if (e.Item is ToolStripMenuItem)
            {
                ToolStripMenuItem toolStripMenuItem = e.Item as ToolStripMenuItem;
                if (toolStripMenuItem == null || !toolStripMenuItem.HasDropDownItems)
                {
                    return;
                }

                if (toolStripMenuItem.Selected)
                {
                    Rectangle arrowRectangle = e.ArrowRectangle;
                    arrowRectangle.Inflate(4, 1);
                    ToolStripMenuItemEX toolStripMenuItemEx = e.Item as ToolStripMenuItemEX;
                    if (toolStripMenuItemEx != null && toolStripMenuItemEx.MenuItemStyle == MenuItemStyle.DropDownArrow)
                    {
                        Color buttonOnBeginColor = ColorTable.ButtonOnBeginColor;
                        Color buttonOnEndColor = ColorTable.ButtonOnEndColor;
                        using (Brush brush = new SolidBrush(ColorTable.MouseOnBorder))
                        {
                            GraphicsPath path = GraphicsPathHelper.CreatePath(arrowRectangle, 4, RoundStyle.All, correction: false);
                            e.Graphics.FillPath(brush, path);
                        }

                        using (Brush brush2 = new LinearGradientBrush(arrowRectangle, buttonOnBeginColor, buttonOnEndColor, LinearGradientMode.Vertical))
                        {
                            arrowRectangle.Inflate(-2, -2);
                            e.Graphics.FillRectangle(brush2, arrowRectangle);
                        }

                        e.ArrowRectangle = arrowRectangle;
                    }
                }

                Image toolBar_OverFlow = Resources.ToolBar_OverFlow;
                if (toolBar_OverFlow != null)
                {
                    int x2 = (e.ArrowRectangle.Width - toolBar_OverFlow.Width) / 2 + e.ArrowRectangle.Left;
                    int y2 = (e.Item.Height - toolBar_OverFlow.Height) / 2;
                    e.Graphics.DrawImage(toolBar_OverFlow, x2, y2, toolBar_OverFlow.Width, toolBar_OverFlow.Height);
                }

                e.ArrowColor = Color.Black;
            }
            else
            {
                base.OnRenderArrow(e);
            }
        }

        protected override void OnRenderOverflowButtonBackground(ToolStripItemRenderEventArgs e)
        {
            base.OnRenderOverflowButtonBackground(e);
            ToolStripOverflowButton toolStripOverflowButton = e.Item as ToolStripOverflowButton;
            if (toolStripOverflowButton != null)
            {
                Rectangle rect = new Rectangle(0, 0, 2, e.Item.Height);
                using (Brush brush = new LinearGradientBrush(rect, ColorTable.SeparatorBeginColor, ColorTable.SeparatorEndColor, 90f, isAngleScaleable: true))
                {
                    e.Graphics.FillRectangle(brush, rect);
                    int y = (e.Item.Height - Resources.OverFlowButton.Height) / 2;
                    int x = (e.Item.Width - Resources.OverFlowButton.Width) / 2;
                    Rectangle rect2 = new Rectangle(x, y, Resources.OverFlowButton.Width, Resources.OverFlowButton.Height);
                    e.Graphics.DrawImage(Resources.OverFlowButton, rect2);
                }
            }
        }

        protected override void OnRenderSeparator(ToolStripSeparatorRenderEventArgs e)
        {
            base.OnRenderSeparator(e);
            if (!(e.Item is ToolStripSeparator))
            {
                return;
            }

            ToolStripSeparator toolStripSeparator = e.Item as ToolStripSeparator;
            Rectangle rect = new Rectangle(0, 0, e.Item.Width, e.Item.Height);
            Rectangle rect2;
            if (e.ToolStrip != null)
            {
                using (LinearGradientBrush linearGradientBrush = new LinearGradientBrush(rect, Color.Transparent, Color.Transparent, 90f, isAngleScaleable: true))
                {
                    linearGradientBrush.InterpolationColors = ColorTable.ToolStripSeparatorColorBlend;
                    rect2 = new Rectangle(1, e.Item.ContentRectangle.Y, 1, e.Item.ContentRectangle.Height);
                    e.Graphics.FillRectangle(linearGradientBrush, rect2);
                }
            }

            if (e.ToolStrip is ToolStripDropDown || e.ToolStrip is ContextMenuStrip)
            {
                using (Brush brush = new LinearGradientBrush(rect, ColorTable.SeparatorBeginColor, ColorTable.SeparatorEndColor, 90f, isAngleScaleable: true))
                {
                    rect2 = new Rectangle(e.ToolStrip.DisplayRectangle.Left, e.Item.Height / 2, e.ToolStrip.Width, 1);
                    e.Graphics.FillRectangle(brush, rect2);
                }
            }
        }



    }
}
