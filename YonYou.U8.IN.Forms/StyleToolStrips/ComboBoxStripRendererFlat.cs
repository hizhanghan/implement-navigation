﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
    public class ComboBoxStripRendererFlat : StyleRendererFlat
    {
        protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
        {
            base.OnRenderToolStripBackground(e);
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            ToolStrip toolStrip = e.ToolStrip;
            if (toolStrip is MenuStrip || toolStrip is ToolStripDropDown)
            {
                Rectangle clientRectangle = toolStrip.ClientRectangle;
                using (SolidBrush solidBrush = new SolidBrush(ColorTable.ContextMenuStripBackgroundColor))
                {
                    e.Graphics.FillRectangle(solidBrush, new Rectangle(Point.Empty, toolStrip.Size));
                }
            }
            else
            {
                Rectangle clientRectangle = new Rectangle(0, 0, e.ToolStrip.Width, e.ToolStrip.Height);
                using (SolidBrush solidBrush2 = new SolidBrush(ColorTable.ToolStripBackgroundColor))
                {
                    e.Graphics.FillRectangle(solidBrush2, clientRectangle);
                }
                using (Pen pen = new Pen(ColorTable.ToolStripBottomBorderColor))
                {
                    e.Graphics.DrawLine(pen, clientRectangle.Left, clientRectangle.Height - 1, clientRectangle.Width, clientRectangle.Height - 1);
                }
            }
        }

        protected override void OnRenderItemText(ToolStripItemTextRenderEventArgs e)
        {
            if (e.Item is ToolStripMenuItem)
            {
                if (e.Item.Selected)
                {
                    e.TextColor = e.Item.ForeColor;
                }
                using (Brush brush = new SolidBrush(e.TextColor))
                {
                    e.Graphics.DrawString(e.Text, e.Item.Font, brush, new Point(10, (e.Item.Height - e.Item.Font.Height) / 2));
                }
            }
            else
            {
                e.TextFont = e.ToolStrip.Font;
                base.OnRenderItemText(e);
            }
        }
    }
}