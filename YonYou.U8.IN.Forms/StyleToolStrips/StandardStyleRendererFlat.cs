﻿using System.Windows.Forms;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class StandardStyleRendererFlat : StyleRendererFlat
	{
		protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
		{
			StandardToolStrip standardToolStrip = e.ToolStrip as StandardToolStrip;
			if (standardToolStrip != null)
			{
				if (!standardToolStrip.TransBackground)
				{
					base.OnRenderToolStripBackground(e);
				}
			}
			else
			{
				base.OnRenderToolStripBackground(e);
			}
		}
	}
}