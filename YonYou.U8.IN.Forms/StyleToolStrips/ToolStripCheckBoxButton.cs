﻿using System.ComponentModel;
using System;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using YonYou.U8.IN.Forms.Properties;
using System.Drawing;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	[DefaultProperty("Value")]
	[ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.All)]
	public class ToolStripCheckBoxButton : ToolStripButton
	{
		private ControlState _buttonState = ControlState.Normal;

		public override Image Image
		{
			get
			{
				return base.Image;
			}
			set
			{
				if (base.Image == null)
				{
					base.Image = value;
				}
			}
		}

		public ToolStripCheckBoxButton()
		{
			Initialize(string.Empty);
		}

		public ToolStripCheckBoxButton(string text)
		{
			Initialize(text);
		}

		private void Initialize(string text)
		{
			Text = text;
			Image = Resources.toolbar_checkbox;
			base.CheckedChanged += ToolStripCheckBoxButton_CheckedChanged;
		}

		private void ToolStripCheckBoxButton_CheckedChanged(object sender, EventArgs e)
		{
			UpdateButtonState(ControlState.Normal);
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				UpdateButtonState(ControlState.Pressed);
			}
			base.OnMouseDown(e);
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				UpdateButtonState(ControlState.Hover);
			}
			base.OnMouseUp(e);
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			base.OnMouseLeave(e);
			UpdateButtonState(ControlState.Normal);
		}

		protected override void OnMouseEnter(EventArgs e)
		{
			base.OnMouseEnter(e);
			UpdateButtonState(ControlState.Hover);
		}

		private void UpdateButtonState(ControlState state)
		{
			Image toolbar_checkbox = Resources.toolbar_checkbox;
			_buttonState = state;
			toolbar_checkbox = ((_buttonState == ControlState.Hover) ? ((base.CheckState != CheckState.Checked) ? Resources.toolbar_checkbox_hover : Resources.toolbar_checkbox_select_hover) : ((base.CheckState != CheckState.Checked) ? Resources.toolbar_checkbox : Resources.toolbar_checkbox_select));
			SetImage(toolbar_checkbox);
			Invalidate();
		}

		private void SetImage(Image image)
		{
			base.Image = image;
		}
	}
}