﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class ToolStripMenuItemEX : ToolStripMenuItem
	{
		public ToolStripMenuItemEX()
		{
		}

		public ToolStripMenuItemEX(string text) : base(text)
		{
		}

		public ToolStripMenuItemEX(Image image) : base(image)
		{
		}

		public ToolStripMenuItemEX(string text, Image image) : base(text, image)
		{
		}

		public ToolStripMenuItemEX(string text, Image image, EventHandler onClick) : base(text, image, onClick)
		{
		}

		public ToolStripMenuItemEX(string text, Image image, params ToolStripItem[] dropDownItems) : base(text, image, dropDownItems)
		{
		}

		public ToolStripMenuItemEX(string text, Image image, EventHandler onClick, string name) : base(text, image, onClick, name)
		{
		}

		public ToolStripMenuItemEX(string text, Image image, EventHandler onClick, string name, Keys shortcutKeys) : base(text, image, onClick, shortcutKeys)
		{
		}

		public MenuItemStyle MenuItemStyle { get; set; }

		public Color BackColor2 { get; set; }
	}
}