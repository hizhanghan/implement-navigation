﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.Win32;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
    public class StandardToolStrip : ToolStrip
    {
		private ToolStripToolTipHelper toolTipHelper;
		private const int IMAGE_RIGHT_SPACE = 3;
		private const int TEXT_RIGHT_SPACE = 5;
		private const int IMAGE_WIDTH = 16;
		private const int ITEM_HEIGHT = 22;
		private Padding itemPadding = new Padding(5, 0, 5, 0);
		private bool transBackground = false;
		public StandardToolStrip()
		{
			this.Renderer = StyleRenderFactory.GetStyleRender(ToolStripStyle.StandardToolStrip);
			this.AutoSize = false;
			this.MinimumSize = new Size(20, 20);
			this.Padding = new Padding(3, 0, 3, 0);
			this.GripStyle = ToolStripGripStyle.Hidden;
			this.Height = 31;
			this.toolTipHelper = new ToolStripToolTipHelper(this);
			this.ForeColor = ColorTable.ToolStripForeColor;
		}
		#region 属性
		public Padding ItemPadding
		{
			set
			{
				this.itemPadding = value;
			}
		}
		public bool TransBackground
		{
			get
			{
				return this.transBackground;
			}
			set
			{
				this.transBackground = value;
			}
		}
		#endregion
		protected override void WndProc(ref Message msg)
		{
			if (msg.Msg == 33)
			{
				NativeMethods.SetActiveWindow(NativeMethods.GetAncestor(this.Handle, 2));
			}
			base.WndProc(ref msg);
		}
		protected override void OnMouseMove(MouseEventArgs mea)
		{
			base.OnMouseMove(mea);
			this.toolTipHelper.ShowToolTip(mea);
		}
		protected override void OnLayout(LayoutEventArgs e)
		{
			using (Graphics graphics = base.CreateGraphics())
			{
				this.SetToolStripItemSize(graphics);
			}
			base.OnLayout(e);
		}
		private void SetToolStripItemSize(Graphics g)
		{
			foreach (ToolStripItem toolStripItem in this.Items)
			{
				if (toolStripItem is ToolStripControlHost || toolStripItem is ToolStripSeparator)
				{
					toolStripItem.Height = 22;
				}
				else if (toolStripItem is ToolStripComboBoxButtonEX)
				{
					toolStripItem.AutoSize = false;
					toolStripItem.Height = 22;
				}
				else if (toolStripItem is ToolStripCmbButtonEX)
				{
					toolStripItem.AutoSize = false;
					toolStripItem.Height = 22;
					toolStripItem.Width = 112;
					toolStripItem.Image = null;
					toolStripItem.TextAlign = ContentAlignment.MiddleLeft;
					toolStripItem.ImageAlign = ContentAlignment.MiddleLeft;
				}
				else
				{
					int num = TextRenderer.MeasureText(toolStripItem.Text, this.Font).Width + ((toolStripItem.Image != null) ? 16 : 0) + 3 + 5;
					int num2 = num;
					ToolStripSplitButton toolStripSplitButton = toolStripItem as ToolStripSplitButton;
					if (toolStripSplitButton != null)
					{
						num2 += toolStripSplitButton.DropDownButtonBounds.Width;
					}
					ToolStripDropDownButton toolStripDropDownButton = toolStripItem as ToolStripDropDownButton;
					if (toolStripDropDownButton != null)
					{
						num2 += 11;
					}
					toolStripItem.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
					toolStripItem.AutoSize = false;
					toolStripItem.Size = new Size(num2, 22);
					toolStripItem.TextAlign = ContentAlignment.MiddleLeft;
					toolStripItem.ImageAlign = ContentAlignment.MiddleLeft;
					toolStripItem.Margin = this.itemPadding;
				}
			}
		}
	}
}
