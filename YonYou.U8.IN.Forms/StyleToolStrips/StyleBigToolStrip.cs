﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.Win32;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class StyleBigToolStrip : ToolStrip, IDisposable
	{
		public bool DesignTime
		{
			get
			{
				return this.designTime;
			}
			set
			{
				this.designTime = value;
			}
		}

		public StyleBigToolStrip()
		{
			base.Renderer = StyleRenderFactory.GetStyleRender(ToolStripStyle.StyleToolStrip);
			this.AutoSize = false;
			this.MinimumSize = new Size(100, 20);
			base.Padding = new Padding(3, 0, 3, 0);
			base.GripStyle = ToolStripGripStyle.Hidden;
			base.Height = 75;
			this.toolTipHelper = new ToolStripToolTipHelper(this);
			base.ForeColor = ColorTable.ToolStripForeColor;
		}

		public ToolStripToolTipHelper ToolTipHelper
		{
			get
			{
				return this.toolTipHelper;
			}
		}

		protected override void WndProc(ref Message msg)
		{
			if (msg.Msg == 33)
			{
				NativeMethods.SetActiveWindow(NativeMethods.GetAncestor(base.Handle, 2));
			}
			base.WndProc(ref msg);
		}

		protected override void OnLayoutCompleted(EventArgs e)
		{
			using (Graphics graphics = base.CreateGraphics())
			{
				this.SetToolStripItemSize(graphics);
			}
			base.OnLayoutCompleted(e);
		}

		protected override void OnItemAdded(ToolStripItemEventArgs e)
		{
			base.OnItemAdded(e);
			using (Graphics graphics = base.CreateGraphics())
			{
				this.SetToolStripItemSize(graphics);
			}
		}

		private void SetToolStripItemSize(Graphics g)
		{
			foreach (object obj in this.Items)
			{
				ToolStripItem toolStripItem = (ToolStripItem)obj;
				if (!(toolStripItem is ToolStripItemGroupEX))
				{
					if (toolStripItem is ToolStripSeparator)
					{
						toolStripItem.AutoSize = false;
						toolStripItem.Width = 4;
						toolStripItem.Height = 75;
					}
					else
					{
						int num = TextRenderer.MeasureText(toolStripItem.Text, this.Font).Width + 6;
						toolStripItem.AutoSize = false;
						if (toolStripItem is ToolStripSplitDownButtonEX || toolStripItem is ToolStripButtonEX 
							|| toolStripItem is ToolStripDropDownButton)
						{
							num = (num < 42) ? 42 : num;
						}
						toolStripItem.Size = new Size(num, base.Height);
					}
				}
			}
		}

		protected override void OnMouseMove(MouseEventArgs mea)
		{
			base.OnMouseMove(mea);
			this.HideToolStip();
			this.toolTipHelper.ShowToolTip(mea);
		}

		internal void ShowToolTip(InnerToolStrip targetToolStrip, MouseEventArgs mea)
		{
			this.toolTipHelper.HideToolTip();
			foreach (object obj in this.Items)
			{
				ToolStripItem toolStripItem = (ToolStripItem)obj;
				ToolStripControlHost toolStripControlHost = toolStripItem as ToolStripControlHost;
				if (toolStripControlHost != null)
				{
					InnerToolStrip innerToolStrip = toolStripControlHost.Control as InnerToolStrip;
					if (innerToolStrip != null)
					{
						if (innerToolStrip == targetToolStrip)
						{
							targetToolStrip.ToolTipHelper.ShowToolTip(mea);
						}
						else
						{
							innerToolStrip.ToolTipHelper.HideToolTip();
						}
					}
				}
			}
		}

		private void HideToolStip()
		{
			foreach (object obj in this.Items)
			{
				ToolStripItem toolStripItem = (ToolStripItem)obj;
				ToolStripControlHost toolStripControlHost = toolStripItem as ToolStripControlHost;
				if (toolStripControlHost != null)
				{
					InnerToolStrip innerToolStrip = toolStripControlHost.Control as InnerToolStrip;
					if (innerToolStrip != null)
					{
						innerToolStrip.ToolTipHelper.HideToolTip();
					}
				}
			}
		}

		void IDisposable.Dispose()
		{
			this.toolTipHelper.clear();
			foreach (object obj in this.Items)
			{
				ToolStripItem toolStripItem = (ToolStripItem)obj;
				if (toolStripItem is ToolStripItemGroupEX)
				{
					((IDisposable)toolStripItem).Dispose();
				}
			}
			this.Items.Clear();
		}

		private const int IMAGE_RIGHT_SPACE = 3;

		private const int TEXT_SPACE = 3;

		private const int IMAGE_WIDTH = 16;

		private const int ITEM_HEIGHT = 22;

		private bool designTime = false;

		private ToolStripToolTipHelper toolTipHelper;
	}
}
