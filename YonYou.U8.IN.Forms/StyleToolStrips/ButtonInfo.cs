﻿using System.Drawing;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class ButtonInfo
	{
		private Rectangle boundsCore;

		private Image imageCore;

		private ObjectState stateCore;

		private int glyphIndex;

		public Rectangle Bounds
		{
			get
			{
				return boundsCore;
			}
		}

		public Image Image
		{
			get
			{
				return imageCore;
			}
		}

		public ObjectState State
		{
			get
			{
				return stateCore;
			}
			set
			{
				stateCore = value;
			}
		}

		public int GlyphIndex
		{
			get
			{
				return glyphIndex;
			}
			set
			{
				glyphIndex = value;
			}
		}

		public ButtonInfo(Rectangle bounds, Image image)
			: this(bounds, image, ObjectState.Normal, -1)
		{
		}

		public ButtonInfo(Rectangle bounds, Image image, ObjectState state)
			: this(bounds, image, ObjectState.Normal, -1)
		{
		}

		public ButtonInfo(Rectangle bounds, Image image, ObjectState state, int glyph)
		{
			boundsCore = bounds;
			imageCore = image;
			stateCore = state;
			glyphIndex = glyph;
		}

		public Size GetImageSize()
		{
			if (Image == null)
			{
				return Size.Empty;
			}
			return Image.Size;
		}

		public Rectangle GetImageBounds()
		{
			Rectangle result = Rectangle.Empty;
			Size imageSize = GetImageSize();
			if (!Bounds.IsEmpty && !imageSize.IsEmpty)
			{
				Size size = Bounds.Size - imageSize;
				result = new Rectangle(Bounds.Left + size.Width / 2, Bounds.Top + size.Height / 2, imageSize.Width, imageSize.Height);
			}
			return result;
		}

		public static int CalcSortFilterButtonImageIndex(ObjectState state)
		{
			int result = 0;
			if (state == ObjectState.Hot)
			{
				result = 1;
			}
			if (state == ObjectState.Pressed)
			{
				result = 2;
			}
			if (state == ObjectState.Selected)
			{
				result = 3;
			}
			return result;
		}
	}
}