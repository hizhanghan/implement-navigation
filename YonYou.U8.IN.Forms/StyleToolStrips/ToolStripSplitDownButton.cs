﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms.Design;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.Properties;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	[DefaultProperty("Value")]
	[ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.All)]
	public class ToolStripSplitDownButton : System.Windows.Forms.ToolStripDropDownButton
	{
		private bool isTextWrapped = false;

		private string[] splitText;

		public Rectangle ButtonBounds
		{
			get
			{
				return new Rectangle(0, 3, base.Width, 36);
			}
		}

		public Rectangle DropDownButtonBounds
		{
			get
			{
				return new Rectangle(0, ButtonBounds.Bottom, base.Width, 34);
			}
		}

		public bool IsTextWrapped
		{
			get
			{
				return isTextWrapped;
			}
		}

		public new string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				splitText = base.Text.Split('#');
				if (splitText.Length == 2)
				{
					base.Text = ((splitText[0].Length > splitText[1].Length) ? splitText[0] : splitText[1]);
					isTextWrapped = true;
				}
				Invalidate();
			}
		}

		public bool IsPointInButton
		{
			get
			{
				return IsPointIntRect(ButtonBounds);
			}
		}

		public bool IsPointInDropDown
		{
			get
			{
				return IsPointIntRect(DropDownButtonBounds);
			}
		}

		public event EventHandler ButtonClick;

		public ToolStripSplitDownButton()
		{
			Initialize(string.Empty, null);
		}

		public ToolStripSplitDownButton(string text, Image image)
		{
			Initialize(text, image);
		}

		private void Initialize(string text, Image image)
		{
			Text = text;
			Image = image;
			base.ImageAlign = ContentAlignment.TopCenter;
			base.AutoSize = false;
			base.MouseMove += ToolStripSplitDownButton_MouseMove;
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			if (!IsPointInDropDown)
			{
				if (e.Button != MouseButtons.Left)
				{
					base.OnMouseDown(e);
				}
			}
			else if (!base.IsOnDropDown)
			{
				base.OnMouseDown(e);
			}
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			base.OnMouseUp(e);
			if (e.Button == MouseButtons.Left)
			{
				Invalidate();
				if (IsPointInButton && this.ButtonClick != null)
				{
					this.ButtonClick(this, new EventArgs());
				}
			}
		}

		private void ToolStripSplitDownButton_MouseMove(object sender, MouseEventArgs e)
		{
			Invalidate();
		}

		public void DrawText(Graphics g)
		{
			if (splitText == null)
			{
				return;
			}
			Color color = (Enabled ? ForeColor : ColorTable.ToolStripItemGrayColor);
			using (SolidBrush brush = new SolidBrush(color))
			{
				if (splitText.Length == 1)
				{
					Size size = TextRenderer.MeasureText(Text, base.Owner.Font);
					Point pt = new Point((DropDownButtonBounds.Width - size.Width) / 2 + Padding.Left, 3 + DropDownButtonBounds.Top);
					TextRenderer.DrawText(g, Text, Font, pt, color);
					int x = (DropDownButtonBounds.Width - Resources.OverFlowButton.Width) / 2 + DropDownButtonBounds.Left;
					int y = DropDownButtonBounds.Bottom - Resources.OverFlowButton.Height - 9;
					g.DrawImage(Resources.OverFlowButton, x, y);
				}
				else
				{
					SizeF sizeF = g.MeasureString(splitText[0], base.Owner.Font);
					Point point = new Point((DropDownButtonBounds.Width - Convert.ToInt16(sizeF.Width)) / 2 + Padding.Left, 4 + DropDownButtonBounds.Top);
					g.DrawString(splitText[0], base.Owner.Font, brush, point.X, point.Y);
					sizeF = g.MeasureString(splitText[1], base.Owner.Font);
					sizeF.Width += Resources.OverFlowButton.Width;
					point = new Point((DropDownButtonBounds.Width - Convert.ToInt16(sizeF.Width)) / 2 + Padding.Left, 4 + DropDownButtonBounds.Top);
					g.DrawString(splitText[1], base.Owner.Font, brush, point.X, (float)point.Y + sizeF.Height);
					int x2 = point.X + (int)sizeF.Width - Resources.OverFlowButton.Width - 2;
					int y2 = point.Y + ((int)sizeF.Height - Resources.OverFlowButton.Height) / 2 + (int)sizeF.Height - 1;
					g.DrawImage(Resources.OverFlowButton, x2, y2);
				}
			}
		}

		private bool IsPointIntRect(Rectangle rect)
		{
			Point point = Cursor.Position;
			point = base.Parent.PointToClient(point);
			point.X -= Bounds.Left;
			return rect.Contains(point);
		}
	}
}
