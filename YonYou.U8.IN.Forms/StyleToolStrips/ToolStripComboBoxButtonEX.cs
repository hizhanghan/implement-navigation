﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	[DefaultProperty("Value")]
	[ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.All)]
	public class ToolStripComboBoxButtonEX : ToolStripControlHost
	{
		public ToolStripComboBoxButtonEX() : base(new ToolStripComboBoxControl())
		{
			this.Initialize(string.Empty);
		}

		public ToolStripComboBoxButtonEX(string text) : base(new ToolStripComboBoxControl())
		{
			this.Initialize(text);
		}

		private void Initialize(string text)
		{
			this.Text = text;
			this.ToolStripComboBox.Owner = this;
		}

		private ToolStripComboBoxControl ToolStripComboBox
		{
			get
			{
				if (this.toolStripComboBox == null)
				{
					this.toolStripComboBox = (base.Control as ToolStripComboBoxControl);
				}
				return this.toolStripComboBox;
			}
		}

		protected override void OnSubscribeControlEvents(Control c)
		{
			base.OnSubscribeControlEvents(c);
			this.ToolStripComboBox.SelectedIndexChanged += this.OnSelectedIndexChanged;
		}

		protected override void OnUnsubscribeControlEvents(Control c)
		{
			base.OnUnsubscribeControlEvents(c);
			this.ToolStripComboBox.SelectedIndexChanged -= this.OnSelectedIndexChanged;
		}

		public event EventHandler SelectedIndexChanged;

		private void OnSelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.SelectedIndexChanged != null)
			{
				this.SelectedIndexChanged(this.ToolStripComboBox, e);
			}
		}

		public ComboBox.ObjectCollection Items
		{
			get
			{
				return this.ToolStripComboBox.Items;
			}
		}

		private ToolStripComboBoxControl toolStripComboBox;

		private class ToolStripComboBoxControl : ComboBoxEx
		{
			public ToolStripComboBoxControl()
			{
				base.DropDownStyle = ComboBoxStyle.DropDownList;
				base.Width = 120;
				base.Height = 22;
			}

			public ToolStripComboBoxButtonEX Owner
			{
				get
				{
					return this.owner;
				}
				set
				{
					this.owner = value;
				}
			}

			private ToolStripComboBoxButtonEX owner;
		}
	}
}