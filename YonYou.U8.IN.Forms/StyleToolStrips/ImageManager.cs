﻿using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System;
using System.Drawing;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class ImageManager
	{
		private delegate void PreLoadIconsDelegate();

		public const string ICONS_PATH = "icons\\";

		public const string ICON_U8 = "u8.gif";

		public const string ICON_HELP = "help.gif";

		public const string ICON_CLOSERED = "closered2.gif";

		public const string ICON_CLOSEBLUE = "closeblue2.gif";

		public const string ICON_OPENBLUE = "openblue2.gif";

		public const string ICON_OPENRED = "openred2.gif";

		public const string ICON_PERSP = "default_persp.gif";

		public const string ICON_AREA = "showchild_mode.gif";

		public const string ICON_PRODUCT = "51.gif";

		public const string ICON_BUSIMAM = "49.gif";

		public const string ICON_CUST = "11.gif";

		public const string ICON_FOLDER = "folder.gif";

		public const string ICON_ORDER = "file_obj.gif";

		public const string ICON_DEPT = "filenav_nav.gif";

		public const string ICON_DESKTOP = "desktop.gif";

		public const string ICON_TIME = "waiting.gif";

		public const string ICON_ITEM = "activity.gif";

		public const string ICON_ORDERITEM = "step_done.gif";

		public const string ICON_ADDBK = "bkmrk_tsk.gif";

		public const string ICON_NEW = "add_obj.gif";

		public const string ICON_DELETE = "delete_obj.gif";

		public const string ICON_REFRESH = "refresh.gif";

		public const string ICON_NEWWIZ = "new_wiz.gif";

		public const string ICON_NEWWIZICON = "addtsk_tsk.gif";

		public const string IMG_LOGIN_CHS = "login_chs.jpg";

		public const string IMG_LOGIN_ENG = "login_banner.gif";

		public const string ICON_ROLE = "WB_ShowUponRole.bmp";

		public const string ICON_SCENARIO = "WB_Workbench.bmp";

		public const string ICON_NEWFOLDER = "newfolder_wiz.gif";

		public const string ICON_PVIEW = "pview.gif";

		public const string ICON_RELOGON = "relogin.gif";

		public const string ICON_EXIT = "exit.gif";

		public const string ICON_ASSIGN = "assign.gif";

		public const string ICON_WORKINGCENTER = "working center.gif";

		public const string ICON_PERSONLITY = "personlity.gif";

		public const string ICON_DETAILS = "details.gif";

		public const string ICON_DEFAULTSETTING = "defultsetting.gif";

		public const string ICON_FIRST_PAGE = "first page.gif";

		public const string ICON_PREVIOUS_PAGE = "previous page.gif";

		public const string ICON_NEXT_PAGE = "next page.gif";

		public const string ICON_LAST_PAGE = "last page.gif";

		public const string ICON_REMOTE_SERVICE = "remote_service.ico";

		public const string ICON_SAVE = "save.gif";

		public const string ICON_SAVE_ALL = "saveall.GIF";

		private static Dictionary<string, ImageDescriptor> imageDesps = new Dictionary<string, ImageDescriptor>();

		private static Dictionary<string, Image> imageDic = new Dictionary<string, Image>();

		private static readonly string imagePath = GlobalParams.ApplicationRunPath + "\\icons\\";

		private static void dopreloadicons()
		{
			string[] array = new string[55]
			{
			"SmallIcon\\Print.png", "SmallIcon\\Output.png", "SmallIcon\\Copy.png", "SmallIcon\\Draft.png", "SmallIcon\\Modify.png", "SmallIcon\\Delete.png", "SmallIcon\\accessories.png", "SmallIcon\\Cancel.png", "SmallIcon\\Submit.png", "SmallIcon\\ReSubmit.png",
			"SmallIcon\\UnSubmit.png", "SmallIcon\\approval query.png", "SmallIcon\\Unapprove.png", "SmallIcon\\lock down.png", "SmallIcon\\unlock.png", "SmallIcon\\Relate Person.png", "SmallIcon\\Settlement.png", "SmallIcon\\reserved.png", "SmallIcon\\vary.png", "SmallIcon\\close.png",
			"SmallIcon\\open.png", "SmallIcon\\Notes.png", "SmallIcon\\Discuss.png", "SmallIcon\\Notify.png", "SmallIcon\\Query Log.png", "SmallIcon\\QueryUP.png", "SmallIcon\\Querydown.png", "SmallIcon\\format.png", "SmallIcon\\SaveFormat.png", "SmallIcon\\ShowTemplate.png",
			"SmallIcon\\PrintTemplate.png", "SmallIcon\\Previous Page.png", "SmallIcon\\Next page.png", "SmallIcon\\Release.png", "SmallIcon\\auto.png", "BigIcon\\Approve.png", "BigIcon\\Save.png", "BigIcon\\Add.png", "BigIcon\\Refresh.png", "BigIcon\\Last page.png",
			"BigIcon\\Advanced.png", "BigIcon\\insert row.png", "BigIcon\\close.png", "BigIcon\\open.png", "BigIcon\\Split Row.png", "BigIcon\\Delete a row.png", "BigIcon\\batchmodify.png", "BigIcon\\Discount.png", "BigIcon\\Credit.png", "BigIcon\\ATP.png",
			"BigIcon\\matching.png", "BigIcon\\execute.png", "BigIcon\\Relate query.png", "BigIcon\\sorting.png", "BigIcon\\displayFormat.png"
			};
			string[] array2 = array;
			foreach (string image in array2)
			{
				PreloadImage(image);
			}
			//DBG.DbgTrace("preload icons:{0}", imageDic.Count);
		}

		public static void PreLoadIcons()
		{
			PreLoadIconsDelegate preLoadIconsDelegate = dopreloadicons;
			preLoadIconsDelegate.BeginInvoke(null, null);
		}

		public static ImageDescriptor GetImageDesc(string id)
		{
			if (imageDesps.Count < 1)
			{
				InitImages();
			}
			if (!imageDesps.ContainsKey(id))
			{
				DeclareImage(id);
			}
			return imageDesps[id];
		}

		public static Image GetImage(string Id)
		{
			try
			{
				ImageDescriptor imageDesc = GetImageDesc(Id);
				if (imageDesc != null)
				{
					return imageDesc.CreateImage();
				}
				return null;
			}
			catch (Exception ex)
			{
				//DBG.DbgTrace("门户加载图片失败：图片名称:{0} callStacks:{1}", Id, ex.Message + ex.StackTrace);
			}
			return null;
		}

		private static void InitImages()
		{
		}

		private static void DeclareImage(string imageName)
		{
			DeclareImage(imageName, "icons\\" + imageName);
		}

		private static void DeclareImage(string key, string path)
		{
			try
			{
				string path2 = GlobalParams.ApplicationRunPath + "\\" + path;
				ImageDescriptor desc = ImageDescriptor.CreateFromPath(path2);
				DeclareImage(key, desc);
			}
			catch (Exception ex)
			{
				//DBG.DbgTrace(ex.Message + ex.StackTrace);
			}
		}

		public static Image GetImage2(string relativePath)
		{
			return GetImage2(relativePath, false);
		}

		public static Image GetImage2(string relativePath, bool isAbsolutPath)
		{
			try
			{
				string text = (isAbsolutPath ? relativePath : (imagePath + relativePath));
				if (File.Exists(text))
				{
					if (imageDic.ContainsKey(text))
					{
						return imageDic[text];
					}
					imageDic.Add(text, Image.FromFile(text));
					return imageDic[text];
				}
				return null;
			}
			catch (Exception ex)
			{
				//DBG.DbgTrace(ex.Message + ex.StackTrace);
				return null;
			}
		}

		public static Image GetImage2(string relativePath, string ImageName)
		{
			try
			{
				Image image = GetImage2(relativePath, false);
				if (image != null)
				{
					return image;
				}
				return GetImage2(ImageName, false);
			}
			catch (Exception ex)
			{
				//DBG.DbgTrace(ex.Message + ex.StackTrace);
				return null;
			}
		}

		private static void PreloadImage(string image)
		{
			try
			{
				string text = imagePath + image;
				if (File.Exists(text) && !imageDic.ContainsKey(text))
				{
					imageDic.Add(text, Image.FromFile(text));
				}
			}
			catch (Exception ex)
			{
				//DBG.DbgTrace(ex.Message + ex.StackTrace);
			}
		}

		public static void DeclareImage(string imageName, ImageDescriptor desc)
		{
			if (!imageDesps.ContainsKey(imageName))
			{
				imageDesps.Add(imageName, desc);
			}
		}

		public static string ConvertToBase64(Image img, ImageFormat format)
		{
			if (img == null)
			{
				return null;
			}
			MemoryStream memoryStream = new MemoryStream();
			img.Save(memoryStream, format);
			byte[] inArray = memoryStream.ToArray();
			memoryStream.Close();
			return Convert.ToBase64String(inArray);
		}

		public static Image ConvertToImage(string base64str)
		{
			if (string.IsNullOrEmpty(base64str))
			{
				return null;
			}
			byte[] array = Convert.FromBase64String(base64str);
			MemoryStream memoryStream = new MemoryStream();
			memoryStream.Write(array, 0, array.Length);
			Image result = Image.FromStream(memoryStream);
			memoryStream.Close();
			return result;
		}

		public static byte[] ConvertToByteArray(Image img, ImageFormat format)
		{
			if (img == null)
			{
				return null;
			}
			MemoryStream memoryStream = new MemoryStream();
			img.Save(memoryStream, format);
			byte[] result = memoryStream.ToArray();
			memoryStream.Close();
			return result;
		}

		public static Image ConvertToImage(byte[] imageBytes)
		{
			if (imageBytes == null || imageBytes.Length <= 0)
			{
				return null;
			}
			MemoryStream memoryStream = new MemoryStream();
			memoryStream.Write(imageBytes, 0, imageBytes.Length);
			Image result = Image.FromStream(memoryStream);
			memoryStream.Close();
			return result;
		}
	}
}