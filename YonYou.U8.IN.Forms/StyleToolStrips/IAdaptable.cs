﻿using System;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public interface IAdaptable
	{
		object GetAdapter(Type targetType);
	}
}