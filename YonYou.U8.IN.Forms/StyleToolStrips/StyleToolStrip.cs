﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.Win32;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
    public partial class StyleToolStrip : ToolStrip, IDisposable
    {
        private bool designTime = false;
        private ToolStripToolTipHelper toolTipHelper;
        private const int IMAGE_RIGHT_SPACE = 3;
        private const int TEXT_SPACE = 3;
        private const int IMAGE_WIDTH = 16;
        private const int ITEM_HEIGHT = 22;
        #region 属性
        public bool DesignTime
        {
            get
            {
                return this.designTime;
            }
            set
            {
                this.designTime = value;
            }
        }
        public ToolStripToolTipHelper ToolTipHelper
        {
            get
            {
                return toolTipHelper;
            }
        }
        #endregion
        public StyleToolStrip()
        {
            base.Renderer = StyleRenderFactory.GetStyleRender(ToolStripStyle.StyleToolStrip);
            AutoSize = false;
            MinimumSize = new Size(100, 20);
            base.Padding = new Padding(3, 0, 3, 0);
            base.GripStyle = ToolStripGripStyle.Hidden;
            base.Height = 75;
            toolTipHelper = new ToolStripToolTipHelper(this);
            base.ForeColor = ColorTable.ToolStripForeColor;
        }
        protected override void OnLayoutCompleted(EventArgs e)
        {
            using(Graphics graphics = base.CreateGraphics())
            {
                this.SetToolStripItemSize(graphics);
            }
            base.OnLayoutCompleted(e);
        }
        
        protected override void OnItemAdded(ToolStripItemEventArgs e)
        {
            base.OnItemAdded(e);
            using(Graphics graphics = base.CreateGraphics())
            {
                this.SetToolStripItemSize(graphics);
            }
        }
        
        private void SetToolStripItemSize(Graphics g)
        {
            foreach(object obj in this.Items)
            {
                ToolStripItem toolStripItem = (ToolStripItem)obj;
                if(!(toolStripItem is ToolStripItemGroup))
                {
                    if(toolStripItem is ToolStripSeparator)
                    {
                        toolStripItem.AutoSize = false;
                        toolStripItem.Width = 4;
                        toolStripItem.Height = 75;
                    }
                    else
                    {
                        int num = TextRenderer.MeasureText(toolStripItem.Text, this.Font).Width + 6;
                        toolStripItem.AutoSize = false;
                        if(toolStripItem is ToolStripSplitDownButton || toolStripItem is ToolStripButton2 || toolStripItem is ToolStripDropDownButton)
                        {
                            num = ((num < 42) ? 42 : num);
                        }
                        toolStripItem.Size = new Size(num, base.Height);
                    }
                }
            }
        }
        
        protected override void WndProc(ref Message msg)
        {
            if(msg.Msg == 33)
            {
                NativeMethods.SetActiveWindow(NativeMethods.GetAncestor(base.Handle, 2));
            }
            base.WndProc(ref msg);
        }
        
        protected override void OnMouseMove(MouseEventArgs mea)
        {
            base.OnMouseMove(mea);
            HideToolStip();
            toolTipHelper.ShowToolTip(mea);
        }

        internal void ShowToolTip(InnerToolStrip targetToolStrip, MouseEventArgs mea)
        {
            toolTipHelper.HideToolTip();
            foreach (ToolStripItem item in Items)
            {
                ToolStripControlHost toolStripControlHost = item as ToolStripControlHost;
                if (toolStripControlHost == null)
                {
                    continue;
                }
                InnerToolStrip innerToolStrip = toolStripControlHost.Control as InnerToolStrip;
                if (innerToolStrip != null)
                {
                    if (innerToolStrip == targetToolStrip)
                    {
                        targetToolStrip.ToolTipHelper.ShowToolTip(mea);
                    }
                    else
                    {
                        innerToolStrip.ToolTipHelper.HideToolTip();
                    }
                }
            }
        }
        private void HideToolStip()
        {
            foreach (ToolStripItem item in Items)
            {
                ToolStripControlHost toolStripControlHost = item as ToolStripControlHost;
                if (toolStripControlHost != null)
                {
                    InnerToolStrip innerToolStrip = toolStripControlHost.Control as InnerToolStrip;
                    if (innerToolStrip != null)
                    {
                        innerToolStrip.ToolTipHelper.HideToolTip();
                    }
                }
            }
        }
        void IDisposable.Dispose()
        {
            toolTipHelper.clear();
            foreach (ToolStripItem item in Items)
            {
                if (item is ToolStripItemGroup)
                {
                    ((IDisposable)item).Dispose();
                }
            }
            Items.Clear();
        }

    }
}
