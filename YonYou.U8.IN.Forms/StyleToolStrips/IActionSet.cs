﻿using System.Collections;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public interface IActionSet : IAction
	{
		ArrayList Actions { get; }

		event ActionEventHandler ActionAdded;

		event ActionEventHandler ActionRemoved;
	}
}