﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.StyleToolStrips;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class StyleContextMenuStrip : ContextMenuStrip
	{
		public StyleContextMenuStrip()
		{
			base.Renderer = StyleRenderFactory.GetStyleRender(ToolStripStyle.ContextMenuStrip);
			this.AutoSize = true;
			base.Margin = new Padding(0, 0, 0, 0);
			base.Padding = new Padding(10);
			base.LayoutStyle = ToolStripLayoutStyle.VerticalStackWithOverflow;
			base.GripStyle = ToolStripGripStyle.Hidden;
			this.Dock = DockStyle.Fill;
			base.SetStyle(ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
			base.BackColor = Color.Transparent;
			base.UpdateStyles();
		}
	}
}
