﻿namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public interface IActionDelegate
	{
		void Run(IAction action);

		void SelectionChanged(IAction action, ISelection selection);
	}
}