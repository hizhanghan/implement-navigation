﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.Win32;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class ToolStripBuilder
	{
		private enum RowSpanFlag
		{
			Multi = -1,
			Single = 0
		}

		private class RowSpanTag
		{
			private List<IAction> items = new List<IAction>();

			private RowSpanFlag rowSpanFlag;

			public List<IAction> Items
			{
				get
				{
					return items;
				}
			}

			public RowSpanFlag RowSpanFlag
			{
				get
				{
					return rowSpanFlag;
				}
			}

			public RowSpanTag(RowSpanFlag rowSpanFlag)
			{
				this.rowSpanFlag = rowSpanFlag;
			}
		}

		private const int ROWSPAN = 3;

		private const string DEFAULT_GROUP = "129CE38E-11CB-4CCD-A97E-42C3A6034214";

		private static ToolStripBuilder instance;

		private int LimitLen = 8;

		private bool isNeedRecord = true;

		public static ToolStripBuilder Instance
		{
			get
			{
				if (instance == null)
				{
					instance = new ToolStripBuilder();
				}
				return instance;
			}
		}

		public bool IsNeedRecord
		{
			get
			{
				return isNeedRecord;
			}
			set
			{
				isNeedRecord = value;
			}
		}

		private ToolStripBuilder()
		{
		}

		public void Reset()
		{
			instance = null;
		}

		private ToolStrip CreateToolStrip()
		{
			ToolStrip toolStrip = new StyleToolStrip();
			toolStrip.Height = 90;
			toolStrip.Dock = DockStyle.Fill;
			toolStrip.Padding = new Padding(10, 0, 10, 0);
			toolStrip.Font = new Font("宋体", 9f);
			return toolStrip;
		}

		public ToolStrip BuildToolStrip(string btnsString)
		{
			ToolStrip toolStrip = CreateToolStrip();
			AbstractFormInput.ActionHashtable actions = GetActions(btnsString);
			toolStrip.Items.Clear();
			Instance.BuildToolStrip(actions, toolStrip);
			return toolStrip;
		}

		private AbstractFormInput.ActionHashtable GetActions(string btnsString)
		{
			//IL_00f6: Unknown result type (might be due to invalid IL or missing references)
			ButtonInfoCollection buttonInfoCollection = new ButtonInfoCollection(btnsString, ButtonType.PressButton);
			AbstractFormInput.ActionHashtable actionHashtable = new AbstractFormInput.ActionHashtable();
			int count = buttonInfoCollection.Count;
			for (int i = 0; i < count - 1; i++)
			{
				ButtonInfoWrap buttonInfoWrap = buttonInfoCollection.GetButtonInfoWrap(i);
				if (actionHashtable.IsExistKey(buttonInfoWrap.get_ID()))
				{
					actionHashtable[buttonInfoWrap.get_ID()].IsEnabled = buttonInfoWrap.get_Enabled();
					actionHashtable[buttonInfoWrap.get_ID()].IsVisible = buttonInfoWrap.get_Visible();
					actionHashtable[buttonInfoWrap.get_ID()].IsHaveAuth = buttonInfoWrap.get_HaveAuth();
					actionHashtable[buttonInfoWrap.get_ID()].Text = buttonInfoWrap.get_Caption();
					actionHashtable[buttonInfoWrap.get_ID()].ToolTipText = buttonInfoWrap.get_Tooltip();
					actionHashtable[buttonInfoWrap.get_ID()].IsChecked = buttonInfoWrap.get_Checked();
				}
				else
				{
					IActionContentProvider actionContentProvider = new ButtonActionContentProvider((ButtonInfo)(object)buttonInfoWrap);
					if (buttonInfoWrap != null)
					{
						IAction action = new VBAction(VBActionDelegate.Instance, actionContentProvider);
						actionHashtable.Add(action.Id, action, buttonInfoWrap.Style);
					}
				}
			}
			return actionHashtable;
		}

		private ToolStripItem GetToolStripItem(int style, IAction action)
		{
			ToolStripItem toolStripItem = null;
			switch (style)
			{
				case 8:
				case 24:
				case 131072:
					if (action.RowSpan == 3)
					{
						toolStripItem = new ToolStripSplitDownButton(action.Text, action.Image);
						toolStripItem.ImageScaling = ToolStripItemImageScaling.None;
						toolStripItem.Enabled = action.IsEnabled;
						(toolStripItem as ToolStripSplitDownButton).DropDownOpening += Dynmic_DropDownOpening;
						(toolStripItem as ToolStripSplitDownButton).ButtonClick += itemDefault_Click;
					}
					else
					{
						toolStripItem = new ToolStripSplitButtonEx(action.Text, action.Image);
						(toolStripItem as ToolStripSplitButton).DropDownOpening += Dynmic_DropDownOpening;
						(toolStripItem as ToolStripSplitButton).ButtonClick += itemDefault_Click;
					}
					break;
				case 128:
					if (action.RowSpan == 3)
					{
						toolStripItem = new UFIDA.U8.Portal.Controls.StyleToolStrips.ToolStripDropDownButton(action.Text, action.Image);
						(toolStripItem as UFIDA.U8.Portal.Controls.StyleToolStrips.ToolStripDropDownButton).DropDownOpening += Dynmic_DropDownOpening;
					}
					else
					{
						toolStripItem = new System.Windows.Forms.ToolStripDropDownButton(action.Text, action.Image);
						toolStripItem.Enabled = action.IsEnabled;
						(toolStripItem as System.Windows.Forms.ToolStripDropDownButton).DropDownOpening += Dynmic_DropDownOpening;
					}
					break;
				case 55:
				case 65536:
					toolStripItem = new ToolStripCmbButton(action.Text, action.Image);
					FillDropDown(toolStripItem as ToolStripCmbButton, action as VBAction);
					break;
				case 2:
				case 18:
					toolStripItem = new ToolStripCheckBoxButton(action.Text);
					(toolStripItem as ToolStripCheckBoxButton).Checked = action.IsChecked;
					toolStripItem.Tag = action;
					(toolStripItem as ToolStripCheckBoxButton).Click += ToolStripBuilder_Click;
					break;
				default:
					if (action.Text == "-")
					{
						ToolStripSeparator toolStripSeparator = new ToolStripSeparator();
						toolStripSeparator.Padding = new Padding(4, 0, 3, 0);
						toolStripItem = toolStripSeparator;
					}
					if (action.RowSpan == 3)
					{
						toolStripItem = new ToolStripButton2(action.Text, action.Image);
						toolStripItem.Click += item_Click;
					}
					else
					{
						toolStripItem = new ToolStripButton(action.Text, action.Image);
						toolStripItem.Click += item_Click;
					}
					break;
			}
			SetButtonCheckStyle(toolStripItem, action);
			toolStripItem.Tag = action;
			RefreshButton(toolStripItem, action);
			return toolStripItem;
		}

		private void FillDropDown(ToolStripItem item, VBAction action)
		{
			//IL_0087: Unknown result type (might be due to invalid IL or missing references)
			if (item == null)
			{
				return;
			}
			//DBG.DbgTrace("填充Combobox", new object[0]);
			object tag = item.Tag;
			action.Actions.Clear();
			AbstractFormInput.ActionHashtable actionHashtable = new AbstractFormInput.ActionHashtable();
			ButtonInfoCollection comboBoxItems = action.ToolbarSubject.GetComboBoxItems(action.Key);
			ActionController actionController = new ActionController(action.ToolbarSubject);
			int count = comboBoxItems.Count;
			for (int i = 0; i < count; i++)
			{
				ButtonInfoWrap buttonInfoWrap = comboBoxItems.GetButtonInfoWrap(i);
				IAction action2 = actionController.GetAction(buttonInfoWrap);
				if (!actionHashtable.IsExistKey(action2.Id))
				{
					actionHashtable.Add(action2.Id, action2, buttonInfoWrap.Style);
					action.Add(action2);
				}
			}
			ToolStripDropDownItem toolStripDropDownItem = item as ToolStripDropDownItem;
			if (toolStripDropDownItem == null)
			{
				return;
			}
			toolStripDropDownItem.DropDownItems.Clear();
			toolStripDropDownItem.DropDownDirection = ToolStripDropDownDirection.BelowRight;
			foreach (IActionSet action3 in action.Actions)
			{
				if (action3.Id != "-1")
				{
					ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem();
					toolStripMenuItem.Tag = action3;
					toolStripMenuItem.Text = action3.Text;
					toolStripMenuItem.ToolTipText = (string.IsNullOrEmpty(action3.ToolTipText) ? action3.Text : action3.ToolTipText);
					toolStripMenuItem.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
					toolStripMenuItem.Click += menuItem_Click;
					toolStripDropDownItem.DropDownItems.Add(toolStripMenuItem);
				}
				else
				{
					action.Text = action3.Text;
					action.ToolTipText = action3.Text;
					toolStripDropDownItem.Text = action3.Text;
					toolStripDropDownItem.ToolTipText = action3.Text;
				}
			}
		}

		private void menuItem_Click(object sender, EventArgs e)
		{
			ToolStripMenuItem toolStripMenuItem = sender as ToolStripMenuItem;
			if (toolStripMenuItem == null)
			{
				return;
			}
			IAction action = toolStripMenuItem.Tag as IAction;
			if (action == null || action.AtDesignTime)
			{
				return;
			}
			action.Run();
			action.IsChecked = true;
			toolStripMenuItem.OwnerItem.ToolTipText = action.Text;
			toolStripMenuItem.OwnerItem.Text = action.Text;
			foreach (ToolStripMenuItem dropDownItem in (toolStripMenuItem.OwnerItem as System.Windows.Forms.ToolStripDropDownButton).DropDownItems)
			{
				dropDownItem.Checked = false;
			}
			toolStripMenuItem.Checked = true;
		}

		private void ToolStripBuilder_DropDownOpening(object sender, EventArgs e)
		{
			//IL_00a0: Unknown result type (might be due to invalid IL or missing references)
			ToolStripItem toolStripItem = sender as ToolStripItem;
			if (toolStripItem == null)
			{
				return;
			}
			//DBG.DbgTrace("填充Combobox", new object[0]);
			IActionSet actionSet = toolStripItem.Tag as IActionSet;
			VBAction vBAction = actionSet as VBAction;
			vBAction.Actions.Clear();
			AbstractFormInput.ActionHashtable actionHashtable = new AbstractFormInput.ActionHashtable();
			ButtonInfoCollection comboBoxItems = vBAction.ToolbarSubject.GetComboBoxItems(vBAction.Key);
			ActionController actionController = new ActionController(vBAction.ToolbarSubject);
			int count = comboBoxItems.Count;
			for (int i = 0; i < count; i++)
			{
				ButtonInfoWrap buttonInfoWrap = comboBoxItems.GetButtonInfoWrap(i);
				IAction action = actionController.GetAction(buttonInfoWrap);
				if (!actionHashtable.IsExistKey(action.Id))
				{
					actionHashtable.Add(action.Id, action, buttonInfoWrap.Style);
					vBAction.Add(action);
				}
			}
			ToolStripItem[] array = new ToolStripItem[vBAction.Actions.Count];
			int num = 0;
			foreach (IActionSet action2 in vBAction.Actions)
			{
				ToolStripMenuItem toolStripMenuItem = (ToolStripMenuItem)(array[num] = CreateToolStripMenuItemEx(action2, true));
				num++;
			}
			ToolStripDropDownItem toolStripDropDownItem = toolStripItem as ToolStripDropDownItem;
			if (toolStripDropDownItem != null)
			{
				toolStripDropDownItem.DropDownItems.Clear();
				toolStripDropDownItem.DropDownDirection = ToolStripDropDownDirection.BelowLeft;
				toolStripDropDownItem.DropDownItems.AddRange(array);
			}
		}

		private void ToolStripBuilder_Click(object sender, EventArgs e)
		{
			NativeMethods.ReleaseNetCapture();
			IAction action = (IAction)((ToolStripItem)sender).Tag;
			if (action != null && !action.AtDesignTime)
			{
				UpdateCheckState(action);
				if (action != null)
				{
					action.Run();
				}
			}
		}

		private void UpdateCheckState(IAction action)
		{
			if (action.Style == 2)
			{
				action.IsChecked = !action.IsChecked;
			}
		}

		private void ToolbarManagerController_SelectedIndexChanged(object sender, EventArgs e)
		{
			ComboBox comboBox = sender as ComboBox;
			if (comboBox != null)
			{
				IAction action = comboBox.SelectedItem as IAction;
				if (action != null && !action.AtDesignTime)
				{
					action.Run();
				}
			}
		}

		private void item_Click(object sender, EventArgs e)
		{
			ToolStripItem toolStripItem = sender as ToolStripItem;
			if (toolStripItem != null)
			{
				IAction action = (IAction)((ToolStripItem)sender).Tag;
				NativeMethods.ReleaseNetCapture();
				if (action != null && !action.AtDesignTime)
				{
					UpdateCheckState(action);
					action.Run();
					Record(action as NetAction);
				}
			}
		}

		private void itemDefault_Click(object sender, EventArgs e)
		{
			DBG.DbgTrace("0000", new object[0]);
			NativeMethods.ReleaseNetCapture();
			IAction action = (IAction)((ToolStripItem)sender).Tag;
			if (action.AtDesignTime)
			{
				return;
			}
			DBG.DbgTrace("11000", new object[0]);
			DBG.DbgTrace("开始执行默认功能  buttonkey={0},text='{1}", new object[2]
			{
			GetActionKey(action),
			action.Text
			});
			AbstractFormInput.ActionHashtable actionHashtable = InitDynmaicActions(action);
			ToolStripDefaultMenu defaultMenu = ToolStripDatasource.Instance.GetDefaultMenu(GetActionFormCode(action), GetActionKey(action));
			for (int i = 0; i < actionHashtable.Count; i++)
			{
				IAction action2 = FindActionByKey(defaultMenu.DefaultMenu, actionHashtable[i] as IActionSet);
				if (action2 != null && action2.IsVisible && action2.IsEnabled)
				{
					action2.Run();
					return;
				}
			}
			DBG.DbgTrace("结束执行默认功能  buttonkey={0},text='{1}", new object[2]
			{
			GetActionKey(action),
			action.Text
			});
			action.Run();
			DBG.DbgTrace("22222执行默认功能 {0}", new object[1] { action.Text });
			Record(action as NetAction);
		}

		private void Record(NetAction netAction)
		{
			if (IsNeedRecord && netAction != null)
			{
				MenubarRecorderFactory.Execute(GetActionFormCode(netAction), netAction.Id, netAction.Text);
			}
		}

		private IAction FindActionByKey(string destKey, IActionSet action)
		{
			if (string.IsNullOrEmpty(destKey))
			{
				return null;
			}
			string actionKey = GetActionKey(action);
			if (destKey.ToLower() == actionKey.ToLower())
			{
				return action;
			}
			IAction action2 = null;
			foreach (IActionSet action3 in action.Actions)
			{
				action2 = FindActionByKey(destKey, action3);
				if (action2 != null)
				{
					return action2;
				}
			}
			return action2;
		}

		private void itemForCheck_Click(object sender, EventArgs e)
		{
			NativeMethods.ReleaseNetCapture();
			ToolStripMenuItemEx toolStripMenuItemEx = sender as ToolStripMenuItemEx;
			if (toolStripMenuItemEx == null)
			{
				return;
			}
			IAction action = toolStripMenuItemEx.OwnerItem.OwnerItem.Tag as IAction;
			if (action == null || action.AtDesignTime)
			{
				return;
			}
			toolStripMenuItemEx.Checked = !toolStripMenuItemEx.Checked;
			ToolStripDefaultMenu defaultMenu = ToolStripDatasource.Instance.GetDefaultMenu(GetActionFormCode(action), GetActionKey(action));
			if (defaultMenu != null)
			{
				defaultMenu.DefaultMenu = GetActionKey(toolStripMenuItemEx.Tag as IAction);
				if (!toolStripMenuItemEx.Checked)
				{
					defaultMenu.DefaultMenu = string.Empty;
				}
				ToolStripDatasource.Instance.UpdateDefaultCheck(defaultMenu);
			}
		}

		private AbstractFormInput.ActionHashtable InitDynmaicActions(object actionOrg)
		{
			AbstractFormInput.ActionHashtable actionHashtable = new AbstractFormInput.ActionHashtable();
			if (actionOrg.GetType().IsSubclassOf(typeof(VBAction)) || actionOrg.GetType() == typeof(VBAction))
			{
				VBAction action = actionOrg as VBAction;
				actionHashtable = GetVBActionsInLevel(action);
			}
			else if (actionOrg.GetType().IsSubclassOf(typeof(NetAction)) || actionOrg.GetType() == typeof(NetAction))
			{
				NetAction netAction = actionOrg as NetAction;
				foreach (IAction action2 in netAction.Actions)
				{
					if (!actionHashtable.IsExistKey(action2.Id))
					{
						actionHashtable.Add(action2.Id, action2);
					}
				}
			}
			IActionSet actionSet = actionOrg as IActionSet;
			actionSet.Actions.Clear();
			for (int i = 0; i < actionHashtable.Count; i++)
			{
				actionSet.Actions.Add(actionHashtable[i]);
			}
			return actionHashtable;
		}

		private AbstractFormInput.ActionHashtable GetVBActions(VBAction action)
		{
			//IL_005f: Unknown result type (might be due to invalid IL or missing references)
			AbstractFormInput.ActionHashtable actionHashtable = new AbstractFormInput.ActionHashtable();
			ButtonInfoCollection popMenus = action.ToolbarSubject.GetPopMenus(action.Key);
			ActionController actionController = new ActionController(action.ToolbarSubject);
			int count = popMenus.Count;
			for (int i = 0; i < count; i++)
			{
				ButtonInfoWrap buttonInfoWrap = popMenus.GetButtonInfoWrap(i);
				IAction action2 = actionController.GetAction(buttonInfoWrap);
				if (!actionHashtable.IsExistKey(action2.Id))
				{
					actionHashtable.Add(action2.Id, action2, buttonInfoWrap.Style);
				}
			}
			return actionHashtable;
		}

		private AbstractFormInput.ActionHashtable GetVBActionsInLevel(VBAction action)
		{
			AbstractFormInput.ActionHashtable vBActions = GetVBActions(action);
			List<VBAction> subVBAction = GetSubVBAction(action.Key, vBActions, false);
			foreach (VBAction item in subVBAction)
			{
				List<VBAction> subVBAction2 = GetSubVBAction(item.Key, vBActions, true);
				ValidateVisible(item, subVBAction2);
				item.Actions.AddRange(subVBAction2);
			}
			AbstractFormInput.ActionHashtable actionHashtable = new AbstractFormInput.ActionHashtable();
			foreach (VBAction item2 in subVBAction)
			{
				actionHashtable.Add(item2.Id, item2);
			}
			return actionHashtable;
		}

		private List<VBAction> GetSubVBAction(string parentKey, AbstractFormInput.ActionHashtable actions, bool recru)
		{
			List<VBAction> list = new List<VBAction>();
			for (int i = 0; i < actions.Count; i++)
			{
				VBAction vBAction = actions[i] as VBAction;
				if (parentKey == vBAction.ParentKey)
				{
					list.Add(vBAction);
					if (recru)
					{
						List<VBAction> subVBAction = GetSubVBAction(vBAction.Key, actions, recru);
						ValidateVisible(vBAction, subVBAction);
						vBAction.Actions.AddRange(subVBAction);
					}
				}
			}
			return list;
		}

		private void ValidateVisible(VBAction vbaction, List<VBAction> temp)
		{
			int num = 0;
			foreach (VBAction item in temp)
			{
				if (!item.IsVisible)
				{
					num++;
				}
			}
			if (temp.Count > 0 && num == temp.Count)
			{
				vbaction.IsVisible = false;
			}
		}

		private void Dynmic_DropDownOpening(object sender, EventArgs e)
		{
			ToolStripItem toolStripItem = sender as ToolStripItem;
			if (toolStripItem != null)
			{
				IActionSet actionSet = toolStripItem.Tag as IActionSet;
				InitDynmaicActions(actionSet);
				ContextMenuStrip contextMenu = GetContextMenu(actionSet);
				ToolStripItem[] array = new ToolStripItem[contextMenu.Items.Count];
				contextMenu.Items.CopyTo(array, 0);
				ToolStripDropDownItem toolStripDropDownItem = toolStripItem as ToolStripDropDownItem;
				if (toolStripDropDownItem != null)
				{
					toolStripDropDownItem.DropDownItems.Clear();
					toolStripDropDownItem.DropDownDirection = ToolStripDropDownDirection.BelowRight;
					toolStripDropDownItem.DropDownItems.AddRange(array);
				}
			}
		}

		private string GetActionKey(IAction action)
		{
			if (!(action is VBAction))
			{
				return action.Id;
			}
			return (action as VBAction).Key;
		}

		private string GetActionFormCode(IAction action)
		{
			if (!(action is VBAction))
			{
				return ToolbarManagerController.Instance.GetFormCode();
			}
			return (action as VBAction).ToolbarSubject.get_FormCode();
		}

		private ContextMenuStrip GetContextMenu(IActionSet action)
		{
			ContextMenuStrip contextMenuStrip = new StyleContextMenuStrip();
			ToolStripDefaultMenu defaultMenu = null;
			ToolStripMenuItemEx toolStripMenuItemEx = null;
			if (action.Style == 24 || action.Style == 8)
			{
				if (!(action is VBAction))
				{
					string id = action.Id;
				}
				else
				{
					string key = (action as VBAction).Key;
				}
				defaultMenu = ToolStripDatasource.Instance.GetDefaultMenu(GetActionFormCode(action), GetActionKey(action));
				toolStripMenuItemEx = CreateToolStripMenuItemEx(ToolStripDatasource.Instance.GetSetDefaltAction() as IActionSet);
				RecursizeBuildDefault(toolStripMenuItemEx.DropDownItems, action.Actions, string.Empty, defaultMenu);
			}
			RecursizeBuildItems(contextMenuStrip.Items, action.Actions, defaultMenu);
			if (toolStripMenuItemEx != null && toolStripMenuItemEx.DropDownItems.Count > 0)
			{
				contextMenuStrip.Items.Add(new ToolStripSeparator());
				contextMenuStrip.Items.Add(toolStripMenuItemEx);
			}
			return contextMenuStrip;
		}

		private void RecursizeBuildDefault(ToolStripItemCollection dest, ICollection src, string levelText, ToolStripDefaultMenu defaultMenu)
		{
			if (dest == null || src == null)
			{
				return;
			}
			try
			{
				foreach (IActionSet item in src)
				{
					bool flag = item.Actions.Count > 0 || item.Style == 6;
					if (!item.IsEnabled)
					{
						continue;
					}
					if (!flag)
					{
						if (!IsSpearator(item))
						{
							ToolStripMenuItemEx toolStripMenuItemEx = CreateToolStripMenuItemEx(item, true);
							toolStripMenuItemEx.Image = null;
							dest.Add(toolStripMenuItemEx);
							if (!string.IsNullOrEmpty(levelText))
							{
								toolStripMenuItemEx.Text = levelText + "\\" + toolStripMenuItemEx.Text;
							}
							if (defaultMenu != null && GetActionKey(item) == defaultMenu.DefaultMenu)
							{
								toolStripMenuItemEx.Checked = true;
							}
						}
					}
					else if (item.IsVisible)
					{
						RecursizeBuildDefault(dest, item.Actions, item.Text, defaultMenu);
					}
				}
			}
			catch (Exception ex)
			{
				DBG.DbgTrace("构建默认功能菜单错误{0}", new object[1] { ex.Message });
			}
		}

		private bool IsSpearator(IAction ac)
		{
			if (!(ac.Text == "-"))
			{
				return ac.Style == 3;
			}
			return true;
		}

		private ToolStripMenuItemEx CreateToolStripMenuItemEx(IActionSet ast)
		{
			return CreateToolStripMenuItemEx(ast, false);
		}

		private ToolStripMenuItemEx CreateToolStripMenuItemEx(IActionSet ast, bool forCheck)
		{
			bool flag = ast.Actions.Count > 0 || ast.Style == 6;
			ToolStripMenuItemEx toolStripMenuItemEx = new ToolStripMenuItemEx();
			toolStripMenuItemEx.Tag = ast;
			toolStripMenuItemEx.Text = ast.Text;
			toolStripMenuItemEx.Image = ast.Image;
			toolStripMenuItemEx.Enabled = ast.IsEnabled;
			toolStripMenuItemEx.Visible = ast.IsVisible;
			toolStripMenuItemEx.BackColor2 = ast.BackgroundColor;
			toolStripMenuItemEx.ForeColor = ast.ForegroundColor;
			toolStripMenuItemEx.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
			ast.Properties["CTL"] = toolStripMenuItemEx;
			if (ast.Style == 2)
			{
				toolStripMenuItemEx.Checked = ast.IsChecked;
			}
			if (!flag)
			{
				if (!forCheck)
				{
					toolStripMenuItemEx.Click += item_Click;
				}
				else
				{
					toolStripMenuItemEx.Click += itemForCheck_Click;
				}
			}
			return toolStripMenuItemEx;
		}

		private long RecursizeBuildItems(ToolStripItemCollection dest, ICollection src, ToolStripDefaultMenu defaultMenu)
		{
			if (dest == null || src == null)
			{
				return 0L;
			}
			try
			{
				foreach (IActionSet item in src)
				{
					if (!item.IsVisible)
					{
						continue;
					}
					if (IsSpearator(item))
					{
						dest.Add(new ToolStripSeparator());
						continue;
					}
					bool flag = item.Actions.Count > 0 || item.Style == 6;
					ToolStripMenuItemEx toolStripMenuItemEx = CreateToolStripMenuItemEx(item);
					if (defaultMenu != null && defaultMenu.DefaultMenu == GetActionKey(item))
					{
						toolStripMenuItemEx.Checked = true;
					}
					dest.Add(toolStripMenuItemEx);
					if (flag)
					{
						RecursizeBuildItems(toolStripMenuItemEx.DropDownItems, item.Actions, defaultMenu);
					}
				}
			}
			catch (Exception ex)
			{
				DBG.DbgTrace("RecursizeBuildItems got an Exception: {0}", new object[1] { ex.Message });
			}
			return 0L;
		}

		private void RefreshButton(ToolStripItem button, IAction action)
		{
			button.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
			button.Tag = action;
			string text = FilterActionText(action.Text.Replace("*", ""));
			if (button is ToolStripSplitDownButton)
			{
				(button as ToolStripSplitDownButton).Text = text;
			}
			else if (button is UFIDA.U8.Portal.Controls.StyleToolStrips.ToolStripDropDownButton)
			{
				(button as UFIDA.U8.Portal.Controls.StyleToolStrips.ToolStripDropDownButton).Text = text;
			}
			else if (button is ToolStripButton2)
			{
				(button as ToolStripButton2).Text = text;
			}
			else if (!(button is ToolStripCmbButton))
			{
				button.Text = text;
			}
			if (action.Image != null)
			{
				button.Image = action.Image;
			}
			if (button is ToolStripButton)
			{
				(button as ToolStripButton).Checked = action.IsChecked;
			}
			button.Enabled = action.IsEnabled;
			if (string.IsNullOrEmpty(action.ToolTipText))
			{
				button.ToolTipText = action.Text.Replace("*", "");
			}
			else
			{
				button.ToolTipText = action.ToolTipText;
			}
		}

		private string FilterActionText(string sourceText)
		{
			string empty = string.Empty;
			if (Thread.CurrentThread.CurrentCulture.Name.ToLower() == "zh-cn" || Thread.CurrentThread.CurrentCulture.Name.ToLower() == "zh-tw")
			{
				int num = (sourceText.Contains("#") ? (LimitLen + 1) : LimitLen);
				return (sourceText.Length > num) ? (sourceText.Substring(0, num) + "...") : sourceText;
			}
			int num2 = (sourceText.Contains("#") ? (LimitLen * 2 + 1) : (LimitLen * 2));
			return (sourceText.Length > num2) ? (sourceText.Substring(0, num2) + "...") : sourceText;
		}

		private void SetButtonCheckStyle(ToolStripItem item, IAction action)
		{
			ToolStripButton toolStripButton = item as ToolStripButton;
			if (toolStripButton != null)
			{
				toolStripButton.CheckOnClick = action.Style == 18 || action.Style == 2 || action.Style == 6 || action.Style == 262144;
				toolStripButton.Checked = action.IsChecked;
				toolStripButton.Enabled = action.IsEnabled;
				toolStripButton.ToolTipText = action.ToolTipText;
			}
		}

		private void FillComboBox(ToolStripComboBoxButton item, VBAction action)
		{
			//IL_007c: Unknown result type (might be due to invalid IL or missing references)
			if (item == null)
			{
				return;
			}
			DBG.DbgTrace("填充Combobox", new object[0]);
			object tag = item.Tag;
			AbstractFormInput.ActionHashtable actionHashtable = new AbstractFormInput.ActionHashtable();
			ButtonInfoCollection comboBoxItems = action.ToolbarSubject.GetComboBoxItems(action.Key);
			ActionController actionController = new ActionController(action.ToolbarSubject);
			int count = comboBoxItems.Count;
			for (int i = 0; i < count; i++)
			{
				ButtonInfoWrap buttonInfoWrap = comboBoxItems.GetButtonInfoWrap(i);
				IAction action2 = actionController.GetAction(buttonInfoWrap);
				if (!actionHashtable.IsExistKey(action2.Id))
				{
					actionHashtable.Add(action2.Id, action2, buttonInfoWrap.Style);
				}
			}
			for (int j = 0; j < actionHashtable.Count; j++)
			{
				if (actionHashtable[j].Id != "-1")
				{
					item.Items.Add(actionHashtable[j]);
				}
				else
				{
					item.Text = actionHashtable[j].Text;
				}
			}
		}

		public Dictionary<string, List<ToolStripItem>> Dispatch2Group(Dictionary<string, ToolStripItem> toolStripItemDic)
		{
			Dictionary<string, List<ToolStripItem>> dictionary = new Dictionary<string, List<ToolStripItem>>();
			foreach (ToolStripItem value in toolStripItemDic.Values)
			{
				string text = (value.Tag as IAction).Catalog;
				if (string.IsNullOrEmpty(text))
				{
					text = "129CE38E-11CB-4CCD-A97E-42C3A6034214";
				}
				if (!dictionary.ContainsKey(text))
				{
					List<ToolStripItem> list = new List<ToolStripItem>();
					list.Add(value);
					dictionary.Add(text, list);
				}
				else
				{
					dictionary[text].Add(value);
				}
			}
			return dictionary;
		}

		private ToolStripItemGroup GetToolStripGroup()
		{
			ToolStripItemGroup toolStripItemGroup = new ToolStripItemGroup();
			toolStripItemGroup.Padding = new Padding(0, 0, 0, 0);
			toolStripItemGroup.Margin = new Padding(0);
			toolStripItemGroup.Tag = 0;
			toolStripItemGroup.Dock = DockStyle.Fill;
			return toolStripItemGroup;
		}

		public void BuildSmallToolStrip(AbstractFormInput.ActionHashtable buttons, out List<ToolStripItem> result)
		{
			result = new List<ToolStripItem>();
			FilterStar(buttons);
			PerformanceTimer.TimeStart();
			Dictionary<string, List<IAction>> dictionary = Dispatch2Group(buttons);
			PerformanceTimer.TimeEnd("分组工具栏", new object[0]);
			int num = 0;
			foreach (string key in dictionary.Keys)
			{
				bool flag = false;
				foreach (IAction item in dictionary[key])
				{
					item.RowSpan = 1;
					ToolStripItem toolStripItem = GetToolStripItem(item.Style, item);
					if (toolStripItem != null)
					{
						flag = true;
						result.Add(toolStripItem);
					}
				}
				if (flag && num != dictionary.Keys.Count - 1)
				{
					result.Add(new ToolStripSeparator());
				}
				num++;
			}
			List<ToolStripItem> collection = AttachHidden(buttons);
			result.AddRange(collection);
		}

		public void BuildSmallToolStrip(AbstractFormInput.ActionHashtable buttons, ToolStrip bar)
		{
			List<ToolStripItem> result = new List<ToolStripItem>();
			BuildSmallToolStrip(buttons, out result);
			ToolStripItem[] array = new ToolStripItem[result.Count];
			result.CopyTo(array);
			bar.Items.AddRange(array);
		}

		public void BuildToolStrip(AbstractFormInput.ActionHashtable buttons, ToolStrip bar)
		{
			List<ToolStripItem> toolStripItems = null;
			BuildToolStrip(buttons, out toolStripItems);
			ToolStripItem[] array = new ToolStripItem[toolStripItems.Count];
			toolStripItems.CopyTo(array);
			bar.Items.AddRange(array);
		}

		public void BuildToolStrip(AbstractFormInput.ActionHashtable buttons, out List<ToolStripItem> toolStripItems)
		{
			FilterStar(buttons);
			PerformanceTimer.TimeStart();
			Dictionary<string, List<IAction>> groups = Dispatch2Group(buttons);
			PerformanceTimer.TimeEnd("分组工具栏", new object[0]);
			PerformanceTimer.TimeStart();
			toolStripItems = new List<ToolStripItem>();
			BuildToolStripByGroup(groups, ref toolStripItems);
			PerformanceTimer.TimeEnd("构建工具栏结束", new object[0]);
			List<ToolStripItem> list = AttachHidden(buttons);
			if (list != null)
			{
				toolStripItems.AddRange(list);
			}
		}

		private List<ToolStripItem> AttachHidden(AbstractFormInput.ActionHashtable buttons)
		{
			List<ToolStripItem> list = new List<ToolStripItem>();
			for (int i = 0; i < buttons.Count; i++)
			{
				IAction action = buttons[i];
				if (action != null && action.Id.Length > 0 && !(action.Text == "-") && !(action.Pos != "P") && (!action.IsVisible || action.Catalog.ToLower() == "hidden"))
				{
					ToolStripItem toolStripItem = GetToolStripItem(action.Style, action);
					if (toolStripItem != null)
					{
						toolStripItem.Visible = false;
						list.Add(toolStripItem);
					}
				}
			}
			return list;
		}

		private void FilterStar(AbstractFormInput.ActionHashtable buttons)
		{
			for (int i = 0; i < buttons.Count; i++)
			{
				IAction action = buttons[i];
				if (action != null)
				{
					action.Text = action.Text.Replace("*", string.Empty);
				}
			}
		}

		private void BuildToolStripByGroup(Dictionary<string, List<IAction>> groups, ref List<ToolStripItem> toolStripItems)
		{
			foreach (string key in groups.Keys)
			{
				if (groups[key].Count != 0)
				{
					ReLayoutToolStipItem(groups[key], toolStripItems, key);
					toolStripItems.Add(new ToolStripSeparator());
				}
			}
		}

		private void ReLayoutToolStipItem(List<IAction> items, List<ToolStripItem> toolStripItems, string group)
		{
			PerformanceTimer.TimeStart();
			Dictionary<string, List<IAction>> setGroups = GetSetGroups(items);
			PerformanceTimer.TimeEnd("Group:“{0}”组又分排版组", new object[1] { group });
			foreach (string key in setGroups.Keys)
			{
				List<IAction> list = setGroups[key];
				if (list.Count == 1)
				{
					if (list[0].RowSpan == 3)
					{
						ToolStripItem toolStripItemBigButton = GetToolStripItemBigButton(list[0]);
						toolStripItems.Add(toolStripItemBigButton);
						continue;
					}
					ToolStripItem toolStripItem = BuildSetGroup(list);
					if (toolStripItem != null)
					{
						toolStripItems.Add(toolStripItem);
					}
				}
				else
				{
					ToolStripItem toolStripItem2 = BuildSetGroup(list);
					if (toolStripItem2 != null)
					{
						toolStripItems.Add(toolStripItem2);
					}
				}
			}
		}

		private ToolStripItemGroup BuildSetGroup(List<IAction> items)
		{
			ToolStripItemGroup toolStripGroup = GetToolStripGroup();
			int num = 0;
			foreach (IAction item in items)
			{
				ToolStripItem toolStripItem = GetToolStripItem(item.Style, item);
				toolStripGroup.Items.Add(toolStripItem);
				num++;
				if (num == 3)
				{
					return toolStripGroup;
				}
			}
			return toolStripGroup;
		}

		private ToolStripItem GetToolStripItemBigButton(IAction action)
		{
			ToolStripItem toolStripItem = GetToolStripItem(action.Style, action);
			toolStripItem.ImageScaling = ToolStripItemImageScaling.None;
			toolStripItem.TextImageRelation = TextImageRelation.ImageAboveText;
			toolStripItem.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
			if (toolStripItem is ToolStripButton)
			{
				toolStripItem.ImageAlign = ContentAlignment.MiddleCenter;
				toolStripItem.TextAlign = ContentAlignment.BottomCenter;
			}
			return toolStripItem;
		}

		private Dictionary<string, List<IAction>> Dispatch2Group(AbstractFormInput.ActionHashtable buttons)
		{
			Dictionary<string, List<IAction>> dictionary = new Dictionary<string, List<IAction>>();
			for (int i = 0; i < buttons.Count; i++)
			{
				IAction action = buttons[i];
				if (action != null && action.Id.Length > 0 && !(action.Text == "-") && !(action.Pos != "P") && action.IsVisible && !(action.Catalog.ToLower() == "hidden"))
				{
					string key = (string.IsNullOrEmpty(action.Catalog) ? "129CE38E-11CB-4CCD-A97E-42C3A6034214" : action.Catalog);
					if (!dictionary.ContainsKey(key))
					{
						List<IAction> list = new List<IAction>();
						dictionary.Add(key, list);
						list.Add(action);
					}
					else
					{
						dictionary[key].Add(action);
					}
				}
			}
			return dictionary;
		}

		private Dictionary<string, List<IAction>> GetSetGroups(List<IAction> actions)
		{
			Dictionary<string, List<IAction>> dictionary = new Dictionary<string, List<IAction>>();
			int count = actions.Count;
			int num = 0;
			do
			{
				IAction action = actions[num];
				if (action.RowSpan == 3)
				{
					string key = Guid.NewGuid().ToString();
					dictionary.Add(key, new List<IAction>());
					dictionary[key].Add(actions[num]);
					num++;
					continue;
				}
				bool flag = string.IsNullOrEmpty(action.SetGroup);
				string text = (flag ? Guid.NewGuid().ToString() : action.SetGroup);
				if (!dictionary.ContainsKey(text))
				{
					dictionary.Add(text, new List<IAction>());
				}
				else
				{
					DBG.DbgTrace("预置数据错误:组:'{0}'包含了超过三个按钮:'{1}'", new object[2]
					{
					text,
					actions[num]
					});
				}
				dictionary[text].Add(actions[num]);
				num++;
				int i;
				for (i = 0; num + i < count && i < 2 && actions[num + i].RowSpan != 3; i++)
				{
					if (flag)
					{
						if (!string.IsNullOrEmpty(actions[num + i].SetGroup))
						{
							break;
						}
					}
					else if (string.IsNullOrEmpty(actions[num + i].SetGroup) || text != actions[num + i].SetGroup)
					{
						break;
					}
					dictionary[text].Add(actions[num + i]);
				}
				num += i;
			}
			while (num < count);
			return dictionary;
		}

		private void UpdateActionRowSpan(Dictionary<string, List<IAction>> actionDic)
		{
			foreach (string key in actionDic.Keys)
			{
				if (actionDic[key].Count == 1)
				{
					actionDic[key][0].RowSpan = 3;
				}
			}
		}

		public void UpdateComboBox(ToolStrip toolStrip, AbstractFormInput.ActionHashtable buttons, Dictionary<string, ToolStripItem> toolStripItemDic)
		{
			SuspendLayout(toolStrip);
			if (buttons.Count > 0)
			{
				for (int i = 0; i < buttons.Count; i++)
				{
					IAction action = buttons[i];
					if (action != null && action.Id.Length > 0 && toolStripItemDic.ContainsKey(action.Id) && (action.Style == 55 || action.Style == 65536))
					{
						FillDropDown(toolStripItemDic[action.Id], action as VBAction);
					}
				}
			}
			ResumeLayout(toolStrip);
		}

		private void SuspendLayout(ToolStrip bar)
		{
			bar.SuspendLayout();
			foreach (ToolStripItem item in bar.Items)
			{
				if (item is ToolStripItemGroup)
				{
					(item as ToolStripItemGroup).SuspendLayout();
				}
			}
		}

		private void ResumeLayout(ToolStrip bar)
		{
			foreach (ToolStripItem item in bar.Items)
			{
				if (item is ToolStripItemGroup)
				{
					(item as ToolStripItemGroup).ResumeLayout();
				}
			}
			bar.ResumeLayout();
		}

		public void UpdateToolStrip(ToolStrip toolStrip, AbstractFormInput.ActionHashtable buttons, Dictionary<string, ToolStripItem> toolStripItemDic)
		{
			SuspendLayout(toolStrip);
			if (buttons.Count > 0)
			{
				for (int i = 0; i < buttons.Count; i++)
				{
					IAction action = buttons[i];
					if (action != null && action.Id.Length > 0 && toolStripItemDic.ContainsKey(action.Id))
					{
						RefreshButton(toolStripItemDic[action.Id], action);
						if (action.Style == 55 || action.Style == 65536)
						{
							FillDropDown(toolStripItemDic[action.Id], action as VBAction);
						}
					}
				}
			}
			ResumeLayout(toolStrip);
		}
	}
}
