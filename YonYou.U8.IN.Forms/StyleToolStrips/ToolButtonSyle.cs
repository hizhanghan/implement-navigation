﻿namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public enum ToolButtonSyle
	{
		Normal = 0,
		Separator = 1,
		Check = 2,
		CheckGroup = 6,
		DropDown = 8,
		AutoSize = 16,
		DropDownArrow = 128,
		PlaceHolder = 8192,
		ComboBox = 55,
		ComboBox2 = 65536,
		DropDownArrowNoDefault = 131072,
		ToggleButton = 262144
	}
}