﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class ColorTable
	{
		public static ColorBlend ToolStripButtonSelectedColorBlend
		{
			get
			{
				if (_toolStripButtonSelectedColorBlend == null)
				{
                    _toolStripButtonSelectedColorBlend = new ColorBlend
					{
						Colors = new Color[]
						{
							Color.FromArgb(255, 253, 238, 179),
							Color.FromArgb(255, 253, 227, 136),
							Color.FromArgb(255, 252, 229, 140),
							Color.FromArgb(255, 255, 255, 255)
						},
						Positions = new float[]
						{
							0f,
							0.19f,
							0.81f,
							1f
						}
					};
				}
				return _toolStripButtonSelectedColorBlend;
			}
		}

		public static ColorBlend ToolStripButtonPressedColorBlend
		{
			get
			{
				if (_toolStripButtonPressedColorBlend == null)
				{
                    _toolStripButtonPressedColorBlend = new ColorBlend
					{
						Colors = new Color[]
						{
							Color.FromArgb(255, 255, 229, 117),
							Color.FromArgb(255, 255, 216, 107),
							Color.FromArgb(255, 255, 244, 128)
						},
						Positions = new float[]
						{
							0f,
							0.67f,
							1f
						}
					};
				}
				return _toolStripButtonPressedColorBlend;
			}
		}

		public static ColorBlend ToolStripSeparatorColorBlend
		{
			get
			{
				if (_toolStripSeparatorColorBlend == null)
				{
                    _toolStripSeparatorColorBlend = new ColorBlend
					{
						Colors = new Color[]
						{
							Color.FromArgb(255, 242, 242, 242),
							Color.FromArgb(255, 188, 188, 188),
							Color.FromArgb(255, 204, 204, 204)
						},
						Positions = new float[]
						{
							0f,
							0.49f,
							1f
						}
					};
				}
				return _toolStripSeparatorColorBlend;
			}
		}

		public static ColorBlend ToolStripBackgroundColorBlend
		{
			get
			{
				if (_toolStripBackgroundColorBlend == null)
				{
                    _toolStripBackgroundColorBlend = new ColorBlend
					{
						Colors = new Color[]
						{
							Color.FromArgb(255, 255, 255, 255),
							Color.FromArgb(255, 246, 248, 249),
							Color.FromArgb(255, 229, 233, 238)
						},
						Positions = new float[]
						{
							0f,
							0.55f,
							1f
						}
					};
				}
				return _toolStripBackgroundColorBlend;
			}
		}

		public static ColorBlend SpliDownActiveColorBlend
		{
			get
			{
				if (_spliDownActiveColorBlend == null)
				{
                    _spliDownActiveColorBlend = new ColorBlend
					{
						Colors = new Color[]
						{
							Color.FromArgb(255, 255, 253, 219),
							Color.FromArgb(255, 255, 231, 148),
							Color.FromArgb(255, 255, 215, 70)
						},
						Positions = new float[]
						{
							0f,
							0.7f,
							1f
						}
					};
				}
				return _spliDownActiveColorBlend;
			}
		}

		public static ColorBlend SpliDownButtonColorBlend
		{
			get
			{
				if (spliDownButtonColorBlend == null)
				{
                    spliDownButtonColorBlend = new ColorBlend
					{
						Colors = new Color[]
						{
							Color.FromArgb(255, 255, 241, 200),
							Color.FromArgb(255, 255, 247, 219)
						},
						Positions = new float[]
						{
							0f,
							1f
						}
					};
				}
				return spliDownButtonColorBlend;
			}
		}

		public static ColorBlend SplitDownPressColorBlend
		{
			get
			{
				if (splitDownPressColorBlend == null)
				{
                    splitDownPressColorBlend = new ColorBlend
					{
						Colors = new Color[]
						{
                            ButtonPressBeginColor,
                            ButtonPressEndColor
                        },
						Positions = new float[]
						{
							0f,
							1f
						}
					};
				}
				return splitDownPressColorBlend;
			}
		}

		public static Color CheckBoxPeripheryColor
		{
			get
			{
				return checkBoxPeripheryColor;
			}
		}

		public const int TOOLSTRIP_HEIGHT = 75;

		public const int BIG_BUTTON_MARGGING_TOP = 2;

		public const int BIG_BUTTON_HEIGHT = 70;

		public const int BIG_BUTTON_MINIMUM_WIDHT = 42;

		public const int BIG_BUTTON_TOP_SPACE = 3;

		public const int BIG_BUTTON_LEFT_SPACE = 3;

		public const int BIG_BUTTON_RIGHT_SPACE = 3;

		public const int BIG_BUTTON_IMAGE_TEXT_SPACE = 6;

		public const int BIG_BUTTON_TEXT_TEXT_SPACE = 15;

		public const int SPLITDOWN_BUTTON_BTN_HEIGHT = 36;

		public const int SPLITDOWN_BUTTON_DROPDOWN_HEIGHT = 34;

		public const int SPLITDOWN_BUTTON_DROPDOWN_TOP_SPACE = 3;

		public const int SPLITDOWN_BUTTON_DROPDOWN_ARROW_BOTTOM_SPACE = 9;

		public const int SPLITDOWN_BUTTON_DROPDOWN_ARROW_RIGHT_SPACE = 4;

		public const int SMALL_BTN_TWO_TOP_SPACE = 12;

		public const int SMALL_BTN_TWO_BOTTOM_SPACE = 12;

		public const int SMALL_BTN_THREE_TOP_SPACE = 3;

		public const int SMALL_BTN_LEFT_SPACE = 3;

		public const int SMALL_BTN_RIGHT_SPACE = 3;

		public const int SMALL_BTN_IMAGE_BETWEEN_TEXT_SPACE = 3;

		public const int SMALL_BTN_IMAGE_WIDHT = 16;

		public const int SMALL_BTN_TEXT_RIGHT_SPACE = 3;

		public const int SMALL_BTN_HEIGHT = 22;

		public const int SMALL_BTN_DROPDOWNBUTTON_WIDHT = 11;

		public const int SMALL_BTN_PADDING_LEFT = 2;

		public const int SMALL_BTN_PADDING_RIGHT = 2;

		public const int SMALL_SEPARATOR_WIDTH = 4;

		public static Color MouseOnBorder = Color.FromArgb(241, 202, 88);

		public static Color ButtonPressBeginColor = Color.FromArgb(254, 128, 62);

		public static Color ButtonPressEndColor = Color.FromArgb(255, 223, 154);

		public static Color ButtonOnBeginColor = Color.FromArgb(255, 255, 222);

		public static Color ButtonOnEndColor = Color.FromArgb(255, 203, 136);

		public static readonly Color TopBorder = Color.FromArgb(135, 135, 135);

		public static readonly Color BottomBorder = Color.FromArgb(153, 153, 153);

		public static readonly Color BeginColor = Color.White;

		public static readonly Color EndColor = Color.FromArgb(229, 233, 238);

		public static readonly Color ToolStripBackgroundColor = Color.FromArgb(248, 252, 255);

		public static readonly Color ToolStripBottomBorderColor = Color.FromArgb(175, 195, 217);

		public static readonly Color ToolStripTopBorder = Color.FromArgb(102, 147, 205);

		public static readonly Color SeparatorBeginColor = Color.FromArgb(239, 239, 239);

		public static readonly Color SeparatorMiddleColor = Color.FromArgb(239, 239, 239);

		public static readonly Color SeparatorEndColor = Color.FromArgb(204, 204, 204);

		public static Color OverFlowButtonBackGroundColor = Color.FromArgb(229, 233, 238);

		public static Color StatusStripGradientBeginColor = Color.FromArgb(233, 240, 251);

		public static Color StatusStripGradientEndColor = Color.FromArgb(195, 212, 227);

		public static Color ToolStripComboBoxBorderColor = Color.FromArgb(204, 204, 204);

		public static Color ContextMenuStripBackgroundColor = Color.FromArgb(255, 255, 255);

		public static Color ContextMenuStripBorderColor = Color.FromArgb(186, 186, 186);

		public static Color ContextMenuItemSelectedBackgroundColor = Color.FromArgb(255, 242, 204);

		public static Color ContextMenuItemSelectedBorderColor = Color.FromArgb(238, 207, 158);

		public static Color ContextMenuItemPressedBackgroundColor = Color.FromArgb(255, 234, 158);

		public static Color ContextMenuItemPressedBorderColor = Color.FromArgb(221, 188, 133);

		public static Color ContextMenuVerticalLineColor = Color.FromArgb(229, 229, 229);

		public static Color ItemSelectedBackgroundColor = Color.FromArgb(255, 234, 158);

		public static Color ItemSelectedBorderColor = Color.FromArgb(221, 188, 133);

		public static Color ItemPressedBackgroundColor = Color.FromArgb(245, 218, 131);

		public static Color ItemPressedBorderColor = Color.FromArgb(207, 157, 68);

		public static Color ToolStripButtonSelectedBorderColor = Color.FromArgb(255, 241, 202, 88);

		private static ColorBlend _toolStripButtonSelectedColorBlend = null;

		public static Color ToolStripButtonPressedBorderColor = Color.FromArgb(255, 194, 138, 48);

		private static ColorBlend _toolStripButtonPressedColorBlend = null;

		private static ColorBlend _toolStripSeparatorColorBlend = null;

		private static ColorBlend _toolStripBackgroundColorBlend = null;

		private static ColorBlend _spliDownActiveColorBlend;

		private static ColorBlend spliDownButtonColorBlend;

		private static ColorBlend splitDownPressColorBlend;

		private static Color checkBoxPeripheryColor = Color.FromArgb(241, 202, 88);

		public static Color ToolStripForeColor = Color.FromArgb(59, 59, 59);

		public static Color ToolStripItemGrayColor = Color.FromArgb(152, 152, 152);

		public static Color PageToolStripBackGroundColor = Color.Transparent;

		public static Color PageToolStripBorderColor = Color.Transparent;

		public static Color PageToolStripButtonBackGroundColor = Color.Transparent;

		public static Color PageToolStripButtonMouseOverColor = Color.FromArgb(100, 255, 255, 255);

		public static Color PageToolStripButtonMousePressColor = Color.FromArgb(100, 255, 255, 255);
	}
}
