﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class ToolStripButton2 : ToolStripButton
	{
		private bool isTextWrapped = false;

		private string[] splitText;

		public Rectangle ImageRectangle
		{
			get
			{
				return new Rectangle(0, 0, base.Width, 36);
			}
		}

		public Rectangle TextRectangle
		{
			get
			{
				return new Rectangle(0, ImageRectangle.Bottom, base.Width, 34);
			}
		}

		public bool IsTextWrapped
		{
			get
			{
				return isTextWrapped;
			}
		}

		public new string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				splitText = base.Text.Split('#');
				if (splitText.Length == 2)
				{
					base.Text = ((splitText[0].Length > splitText[1].Length) ? splitText[0] : splitText[1]);
					isTextWrapped = true;
				}
				Invalidate();
			}
		}

		public ToolStripButton2()
		{
			Init(string.Empty);
		}

		public ToolStripButton2(string text)
			: base(text)
		{
			Init(text);
		}

		public ToolStripButton2(Image image)
			: base(image)
		{
			Init(string.Empty);
		}

		public ToolStripButton2(string text, Image image)
			: base(text, image)
		{
			Init(text);
		}

		public ToolStripButton2(string text, Image image, EventHandler onClick)
			: base(text, image, onClick)
		{
			Init(text);
		}

		public ToolStripButton2(string text, Image image, EventHandler onClick, string name)
			: base(text, image, onClick, name)
		{
			Init(text);
		}

		private void Init(string text)
		{
			Text = text;
			base.Margin = new Padding(0, 2, 0, 3);
		}

		public void DrawText(Graphics g)
		{
			if (splitText == null)
			{
				return;
			}
			Color color = (Enabled ? ForeColor : ColorTable.ToolStripItemGrayColor);
			using (SolidBrush brush = new SolidBrush(color))
			{
				if (splitText.Length == 1)
				{
					int x = (base.Width - TextRenderer.MeasureText(Text, Font).Width) / 2;
					int y = TextRectangle.Top + 6;
					g.DrawString(Text, Font, brush, new Point(x, y));
					return;
				}
				Size size = TextRenderer.MeasureText(splitText[0], base.Owner.Font);
				Point point = new Point((TextRectangle.Width - size.Width) / 2 + Padding.Left, 4 + TextRectangle.Top);
				g.DrawString(splitText[0], base.Owner.Font, brush, point.X, point.Y);
				size = TextRenderer.MeasureText(splitText[1], base.Owner.Font);
				point = new Point((TextRectangle.Width - size.Width) / 2 + Padding.Left, 4 + TextRectangle.Top);
				g.DrawString(splitText[1], base.Owner.Font, brush, point.X, point.Y + size.Height);
			}
		}
	}

}
