﻿using System;
using System.Drawing;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.Win32;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class InnerToolStrip : ToolStrip, IDisposable
	{
		public InnerToolStrip()
		{
			base.Renderer = StyleRenderFactory.GetStyleRender(ToolStripStyle.InnerToolStrip);
			this.AutoSize = true;
			this.Dock = DockStyle.Fill;
			base.Padding = new Padding(2, 0, 2, 0);
			base.Margin = new Padding(0, 0, 0, 0);
			base.LayoutStyle = ToolStripLayoutStyle.VerticalStackWithOverflow;
			base.GripStyle = ToolStripGripStyle.Hidden;
			this.Dock = DockStyle.Fill;
			base.SetStyle(ControlStyles.SupportsTransparentBackColor | ControlStyles.OptimizedDoubleBuffer, true);
			base.BackColor = Color.Transparent;
			this.toolTipHelper = new ToolStripToolTipHelper(this);
		}

		public ToolStripToolTipHelper ToolTipHelper
		{
			get
			{
				return this.toolTipHelper;
			}
		}

		protected override void WndProc(ref Message msg)
		{
			if (msg.Msg == 33)
			{
				NativeMethods.SetActiveWindow(NativeMethods.GetAncestor(base.Handle, 2));
			}
			base.WndProc(ref msg);
		}

		protected override void OnMouseMove(MouseEventArgs mea)
		{
			base.OnMouseMove(mea);
			StyleBigToolStrip styleToolStrip = base.Parent as StyleBigToolStrip;
			if (styleToolStrip != null)
			{
				styleToolStrip.ShowToolTip(this, mea);
			}
			this.toolTipHelper.ShowToolTip(mea);
		}

		protected override void OnLayoutCompleted(EventArgs e)
		{
			foreach (object obj in this.Items)
			{
				ToolStripItem toolStripItem = (ToolStripItem)obj;
				toolStripItem.Margin = new Padding(0, 0, 0, 0);
			}
			using (Graphics graphics = base.CreateGraphics())
			{
				this.SetToolStripItemSize(graphics);
			}
			base.OnLayoutCompleted(e);
		}

		public bool DesignTime
		{
			get
			{
				return this.designTime;
			}
			set
			{
				this.designTime = value;
			}
		}

		private void SetToolStripItemSize(Graphics g)
		{
			int num = 0;
			foreach (object obj in this.Items)
			{
				ToolStripItem toolStripItem = (ToolStripItem)obj;
				if (toolStripItem is ToolStripComboBoxButtonEX)
				{
					toolStripItem.AutoSize = false;
					toolStripItem.Height = 22;
				}
				else if (toolStripItem is ToolStripCmbButtonEX)
				{
					toolStripItem.AutoSize = false;
					toolStripItem.Height = 22;
					toolStripItem.Width = 112;
					toolStripItem.Image = null;
					toolStripItem.TextAlign = ContentAlignment.MiddleLeft;
					toolStripItem.ImageAlign = ContentAlignment.MiddleLeft;
				}
				else
				{
					int num2 = TextRenderer.MeasureText(toolStripItem.Text, this.Font).Width + ((toolStripItem.Image != null) ? 16 : 0) + 3 + 3;
					ToolStripSplitButton toolStripSplitButton = toolStripItem as ToolStripSplitButton;
					if (toolStripSplitButton != null)
					{
						num2 += toolStripSplitButton.DropDownButtonBounds.Width;
					}
					ToolStripDropDownButton toolStripDropDownButton = toolStripItem as ToolStripDropDownButton;
					if (toolStripDropDownButton != null)
					{
						num2 += 11;
					}
					toolStripItem.Width = num2;
					toolStripItem.Height = 22;
					toolStripItem.TextAlign = ContentAlignment.MiddleLeft;
					toolStripItem.ImageAlign = ContentAlignment.MiddleLeft;
					num = ((num2 > num) ? num2 : num);
				}
			}
			if (this.Items.Count == 2 || this.Items.Count == 1)
			{
				base.Padding = new Padding(base.Padding.Left, 3, base.Padding.Right, 4);
				foreach (object obj2 in this.Items)
				{
					ToolStripItem toolStripItem = (ToolStripItem)obj2;
					toolStripItem.Margin = new Padding(toolStripItem.Padding.Left, 6, toolStripItem.Padding.Right, 6);
				}
			}
			if (this.Items.Count == 3)
			{
				base.Padding = new Padding(base.Padding.Left, 4, base.Padding.Right, 5);
				foreach (object obj3 in this.Items)
				{
					ToolStripItem toolStripItem = (ToolStripItem)obj3;
					toolStripItem.Margin = new Padding(0);
				}
			}
			if (this.Items.Count == 2 || this.Items.Count == 1)
			{
				int x = 0;
				int num3 = 12;
				base.SetItemLocation(this.Items[0], new Point(x, num3));
				if (this.Items.Count == 2)
				{
					base.SetItemLocation(this.Items[1], new Point(x, 46));
				}
			}
			else if (this.Items.Count == 3)
			{
				int x = 0;
				int num3 = 3;
				foreach (object obj4 in this.Items)
				{
					ToolStripItem toolStripItem = (ToolStripItem)obj4;
					base.SetItemLocation(toolStripItem, new Point(x, num3));
					num3 += toolStripItem.Height;
				}
			}
		}

		void IDisposable.Dispose()
		{
			this.toolTipHelper = null;
		}

		private ToolStripToolTipHelper toolTipHelper;

		private bool designTime = false;
	}
}