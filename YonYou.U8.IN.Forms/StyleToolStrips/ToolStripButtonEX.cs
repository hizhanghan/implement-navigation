﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class ToolStripButtonEX : ToolStripButton
	{
		public ToolStripButtonEX()
		{
			this.Init(string.Empty);
		}

		public ToolStripButtonEX(string text) : base(text)
		{
			this.Init(text);
		}

		public ToolStripButtonEX(Image image) : base(image)
		{
			this.Init(string.Empty);
		}

		public ToolStripButtonEX(string text, Image image) : base(text, image)
		{
			this.Init(text);
		}

		public ToolStripButtonEX(string text, Image image, EventHandler onClick) : base(text, image, onClick)
		{
			this.Init(text);
		}

		public ToolStripButtonEX(string text, Image image, EventHandler onClick, string name) : base(text, image, onClick, name)
		{
			this.Init(text);
		}

		private void Init(string text)
		{
			this.Text = text;
			base.Margin = new Padding(0, 2, 0, 3);
		}

		public Rectangle ImageRectangle
		{
			get
			{
				return new Rectangle(0, 0, base.Width, 36);
			}
		}

		public Rectangle TextRectangle
		{
			get
			{
				return new Rectangle(0, this.ImageRectangle.Bottom, base.Width, 34);
			}
		}

		public void DrawText(Graphics g)
		{
			if (this.splitText != null)
			{
				Color color = this.Enabled ? this.ForeColor : ColorTable.ToolStripItemGrayColor;
				using (SolidBrush solidBrush = new SolidBrush(color))
				{
					if (this.splitText.Length == 1)
					{
						int x = (base.Width - TextRenderer.MeasureText(this.Text, this.Font).Width) / 2;
						int y = this.TextRectangle.Top + 6;
						g.DrawString(this.Text, this.Font, solidBrush, new Point(x, y));
					}
					else
					{
						Size size = TextRenderer.MeasureText(this.splitText[0], base.Owner.Font);
						Point point = new Point((this.TextRectangle.Width - size.Width) / 2 + this.Padding.Left, 4 + this.TextRectangle.Top);
						g.DrawString(this.splitText[0], base.Owner.Font, solidBrush, (float)point.X, (float)point.Y);
						size = TextRenderer.MeasureText(this.splitText[1], base.Owner.Font);
						point = new Point((this.TextRectangle.Width - size.Width) / 2 + this.Padding.Left, 4 + this.TextRectangle.Top);
						g.DrawString(this.splitText[1], base.Owner.Font, solidBrush, (float)point.X, (float)(point.Y + size.Height));
					}
				}
			}
		}

		public bool IsTextWrapped
		{
			get
			{
				return this.isTextWrapped;
			}
		}

		public new string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				this.splitText = base.Text.Split(new char[]
				{
					'#'
				});
				if (this.splitText.Length == 2)
				{
					base.Text = ((this.splitText[0].Length > this.splitText[1].Length) ? this.splitText[0] : this.splitText[1]);
					this.isTextWrapped = true;
				}
				base.Invalidate();
			}
		}

		private bool isTextWrapped = false;

		private string[] splitText;
	}
}