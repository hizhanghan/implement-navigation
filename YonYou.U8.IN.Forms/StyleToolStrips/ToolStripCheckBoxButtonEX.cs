﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using YonYou.U8.IN.Forms.Properties;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	[DefaultProperty("Value")]
	[ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.All)]
	public class ToolStripCheckBoxButtonEX : ToolStripButton
	{
		public ToolStripCheckBoxButtonEX()
		{
			this.Initialize(string.Empty);
		}

		public ToolStripCheckBoxButtonEX(string text)
		{
			this.Initialize(text);
		}

		private void Initialize(string text)
		{
			this.Text = text;
			this.Image = Resources.toolbar_checkbox;
			base.CheckedChanged += this.ToolStripCheckBoxButton_CheckedChanged;
		}

		private void ToolStripCheckBoxButton_CheckedChanged(object sender, EventArgs e)
		{
			this.UpdateButtonState(ControlState.Normal);
		}

		protected override void OnMouseDown(MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				this.UpdateButtonState(ControlState.Pressed);
			}
			base.OnMouseDown(e);
		}

		protected override void OnMouseUp(MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				this.UpdateButtonState(ControlState.Hover);
			}
			base.OnMouseUp(e);
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			base.OnMouseLeave(e);
			this.UpdateButtonState(ControlState.Normal);
		}

		protected override void OnMouseEnter(EventArgs e)
		{
			base.OnMouseEnter(e);
			this.UpdateButtonState(ControlState.Hover);
		}

		private void UpdateButtonState(ControlState state)
		{
			Image image = Resources.toolbar_checkbox;
			this._buttonState = state;
			if (this._buttonState == ControlState.Hover)
			{
				if (base.CheckState == CheckState.Checked)
				{
					image = Resources.toolbar_checkbox_select_hover;
				}
				else
				{
					image = Resources.toolbar_checkbox_hover;
				}
			}
			else if (base.CheckState == CheckState.Checked)
			{
				image = Resources.toolbar_checkbox_select;
			}
			else
			{
				image = Resources.toolbar_checkbox;
			}
			this.SetImage(image);
			base.Invalidate();
		}

		private void SetImage(Image image)
		{
			base.Image = image;
		}

		public override Image Image
		{
			get
			{
				return base.Image;
			}
			set
			{
				if (base.Image == null)
				{
					base.Image = value;
				}
			}
		}

		private ControlState _buttonState = ControlState.Normal;
	}
}