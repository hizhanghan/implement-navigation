﻿using Microsoft.Win32;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public sealed class GlobalParams
	{
		private static string CurrentInstPath = "SOFTWARE\\Ufsoft\\WF\\V8.700\\Install\\CurrentInstPath";

		public static string ApplicationRunPath
		{
			get
			{
				return Registry.LocalMachine.OpenSubKey(CurrentInstPath).GetValue(string.Empty).ToString();
			}
		}

		public static string ProductRootRegPath
		{
			get
			{
				return "HKEY_LOCAL_MACHINE\\SOFTWARE\\Ufsoft\\WF\\V8.700";
			}
		}

		private GlobalParams()
		{
		}
	}
}