﻿using System;
using System.Drawing;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class ButtonActionContentProvider : IActionContentProvider, IAdaptable
	{
		private ButtonInfo m_ButtonInfo;

		public object Element
		{
			get
			{
				return m_ButtonInfo;
			}
		}

		public ButtonActionContentProvider(ButtonInfo buttonInfo)
		{
			m_ButtonInfo = buttonInfo;
		}

		public void RegisterButtonInfo(ButtonInfo buttonInfo)
		{
			m_ButtonInfo = buttonInfo;
		}

		public int GetAccelerator(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return 0;
		}

		public Image GetImage(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			int rowSpan = GetRowSpan(element);
			string path = "SmallIcon\\";
			if (rowSpan == 3)
			{
				path = "BigIcon\\";
			}
			return GetImage(CastToBeButtonInfoWrap(element).get_Image(), path);
		}

		private Image GetImage(string id, string path)
		{
			if (id == "")
			{
				return null;
			}
			try
			{
				Image image = ImageManager.GetImage2(string.Format("{0}{1}.png", path, id));
				if (image == null)
				{
					image = ImageManager.GetImage(string.Format("{0}{1}.png", path, "unknown"));
				}
				return image;
			}
			catch (Exception innerException)
			{
				//Exception ex = new Exception(SR.GetString("s.iconnotfound") + "\n" + id + ".gif", innerException);
				//UFIDA.U8.Portal.Common.Utils.Trace.WriteLine(ex.Message + ex.StackTrace);
				return null;
			}
		}

		public string GetID(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).get_ID();
		}

		public string GetKey(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).get_Key();
		}

		public string GetText(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).get_Caption();
		}

		public string GetToolTipText(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).get_Tooltip();
		}

		public string GetActionGroup(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).get_Group();
		}

		public bool IsChecked(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).get_Checked();
		}

		public bool IsEnabled(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).get_Enabled();
		}

		public bool IsVisible(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).get_Visible();
		}

		public bool IsHaveAuth(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).get_HaveAuth();
		}

		public short GetIndex(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).Index;
		}

		public object GetAdapter(Type targetType)
		{
			return this;
		}

		private void ShouldCastToBeButtonInfoWrap(object element)
		{
			System.Diagnostics.Trace.Assert(IsNotNull(CastToBeButtonInfoWrap(element)), "期望遇到类型：" + "UFIDA.U8.Portal.Proxy.UFPortalProxy.ButtonInfo", element.ToString() + "不能转化为" + "UFIDA.U8.Portal.Proxy.UFPortalProxy.ButtonInfo");
		}

		private ButtonInfoWrap CastToBeButtonInfoWrap(object element)
		{
			return element as ButtonInfoWrap;
		}

		private bool IsNotNull(object element)
		{
			return !IsNull(element);
		}

		private bool IsNull(object element)
		{
			if (element != null)
			{
				return false;
			}
			return true;
		}

		public string GetPos(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).get_Pos();
		}

		public int GetRowSpan(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).get_RowSpan();
		}

		public string GetParentKey(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).ParentKey;
		}

		public string GetBackgroundColor(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).BackgroundColor;
		}

		public string GetForegroundColor(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).ForegroundColor;
		}

		public string GetSetGroup(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).SetGroup;
		}

		public short GetSetGroupRow(object element)
		{
			ShouldCastToBeButtonInfoWrap(element);
			return CastToBeButtonInfoWrap(element).SetGroupRow;
		}

		public int GetMenuItemStyle(object element)
		{
			//IL_000e: Unknown result type (might be due to invalid IL or missing references)
			//IL_0014: Expected I4, but got Unknown
			ShouldCastToBeButtonInfoWrap(element);
			return (int)CastToBeButtonInfoWrap(element).Style;
		}
	}
}