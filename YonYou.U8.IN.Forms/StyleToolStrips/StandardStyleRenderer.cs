﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.Properties;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class StandardStyleRenderer : StyleRenderer
	{
		protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
		{
			StandardToolStrip standardToolStrip = e.ToolStrip as StandardToolStrip;
			if (standardToolStrip != null)
			{
				if (!standardToolStrip.TransBackground)
				{
					Rectangle rect = new Rectangle(0, 0, e.ToolStrip.Width, e.ToolStrip.Height);
					using (TextureBrush textureBrush = new TextureBrush(Resources.StandardToolbar_Background))
					{
						e.Graphics.FillRectangle(textureBrush, rect);
					}
				}
			}
			else
			{
				base.OnRenderToolStripBackground(e);
			}
		}
	}
}
