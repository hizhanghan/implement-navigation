﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class ToolStripSplitButtonEX : ToolStripSplitButton, IDisposable
	{
		public ToolStripSplitButtonEX()
		{
		}

		public ToolStripSplitButtonEX(string text, Image image) : base(text, image)
		{
		}

		public ToolStripSplitButtonEX(string text) : this(text, null)
		{
		}

		public ToolStripSplitButtonEX(Image image) : this(string.Empty, image)
		{
		}

		public bool IsButtonSelected
		{
			get
			{
				return this.isButotnSelected;
			}
			set
			{
				this.isButotnSelected = value;
			}
		}

		public bool IsDropDownButtonSelected
		{
			get
			{
				return this.isDropDownButtonSelected;
			}
			set
			{
				this.isDropDownButtonSelected = value;
			}
		}

		protected override void OnMouseMove(MouseEventArgs mea)
		{
			if (base.ButtonBounds.Contains(mea.Location))
			{
				this.isButotnSelected = true;
				this.IsDropDownButtonSelected = false;
			}
			else
			{
				this.isButotnSelected = false;
				this.IsDropDownButtonSelected = true;
			}
			base.Invalidate();
			base.OnMouseMove(mea);
		}

		protected override void OnMouseLeave(EventArgs e)
		{
			this.IsButtonSelected = false;
			this.IsDropDownButtonSelected = false;
			base.Invalidate();
			base.OnMouseLeave(e);
		}

		void IDisposable.Dispose()
		{
			if (this.Image != null)
			{
				this.Image.Dispose();
			}
			base.Dispose();
		}

		private bool isButotnSelected = false;

		private bool isDropDownButtonSelected = false;
	}
}