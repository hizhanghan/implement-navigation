﻿using System.Collections;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public interface ISelection
	{
		string Context { get; set; }

		bool IsEmpty { get; }

		object FirstElement { get; }

		object[] ToArray();

		IList ToList();
	}
}