﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class InnerRenderer : StyleRenderer
	{
		protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e)
		{
			ToolStrip toolStrip = e.ToolStrip;
			Graphics graphics = e.Graphics;
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			if (toolStrip is MenuStrip || toolStrip is ToolStripDropDown)
			{
				using (Pen pen = new Pen(ColorTable.BottomBorder))
				{
					GraphicsPath path = GraphicsPathHelper.CreatePath(toolStrip.ClientRectangle, 0, RoundStyle.None, true);
					graphics.DrawPath(pen, path);
				}
			}
			else if (toolStrip == null)
			{
				base.OnRenderToolStripBorder(e);
			}
		}

		protected override void OnRenderToolStripBackground(ToolStripRenderEventArgs e)
		{
			base.OnRenderToolStripBackground(e);
			e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
			ToolStrip toolStrip = e.ToolStrip;
			if (toolStrip is ContextMenuStrip)
			{
				Rectangle rect = new Rectangle(0, 0, e.ToolStrip.Width, e.ToolStrip.Height);
				using (LinearGradientBrush linearGradientBrush = new LinearGradientBrush(new Point(0, 0), new Point(0, e.ToolStrip.Height), Color.Transparent, Color.Transparent))
				{
					e.Graphics.FillRectangle(linearGradientBrush, rect);
				}
			}
		}
	}
}