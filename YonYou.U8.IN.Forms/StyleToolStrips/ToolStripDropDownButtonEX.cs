﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using YonYou.U8.IN.Forms.Properties;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	[ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.All)]
	[DefaultProperty("Value")]
	[ToolboxBitmap(typeof(ToolStripDropDownButtonEX), "xzstyle.ico")]
	public class ToolStripDropDownButtonEX : System.Windows.Forms.ToolStripDropDownButton
	{
		public ToolStripDropDownButtonEX()
		{
			this.Initialize(string.Empty, null);
		}

		public ToolStripDropDownButtonEX(string text, Image image)
		{
			this.Initialize(text, image);
		}

		private void Initialize(string text, Image image)
		{
			this.Text = text;
			this.Image = image;
			base.Margin = new Padding(0, 2, 0, 3);
			base.AutoSize = false;
			base.MouseMove += this.ToolStripDropDownButton_MouseMove;
		}

		public Rectangle ImageRectangle
		{
			get
			{
				return new Rectangle(0, 0, base.Width, 36);
			}
		}
		public Rectangle TextRectangle
		{
			get
			{
				return new Rectangle(0, this.ImageRectangle.Bottom, base.Width, 34);
			}
		}
		private void ToolStripDropDownButton_MouseMove(object sender, MouseEventArgs e)
		{
			base.Invalidate();
		}

		public void DrawText(Graphics g)
		{
			if (this.splitText != null)
			{
				Color color = this.Enabled ? this.ForeColor : ColorTable.ToolStripItemGrayColor;
				using (SolidBrush solidBrush = new SolidBrush(color))
				{
					if (this.splitText.Length == 1)
					{
						int x = (base.Width - TextRenderer.MeasureText(this.Text, this.Font).Width) / 2;
						int y = this.TextRectangle.Top + 6;
						g.DrawString(this.Text, this.Font, solidBrush, new Point(x, y));
						int x2 = (this.TextRectangle.Width - Resources.OverFlowButton.Width) / 2 + this.TextRectangle.Left;
						int y2 = this.TextRectangle.Bottom - Resources.OverFlowButton.Height - 9;
						g.DrawImage(Resources.OverFlowButton, x2, y2);
					}
					else
					{
						Size size = TextRenderer.MeasureText(this.splitText[0], base.Owner.Font);
						Point point = new Point((this.TextRectangle.Width - size.Width) / 2 + this.Padding.Left, 4 + this.TextRectangle.Top);
						g.DrawString(this.splitText[0], base.Owner.Font, solidBrush, (float)point.X, (float)point.Y);
						size = TextRenderer.MeasureText(this.splitText[1], base.Owner.Font);
						size.Width += Resources.OverFlowButton.Width;
						point = new Point((this.TextRectangle.Width - size.Width) / 2 + this.Padding.Left, 4 + this.TextRectangle.Top);
						g.DrawString(this.splitText[1], base.Owner.Font, solidBrush, (float)point.X, (float)(point.Y + size.Height));
						int x2 = point.X + size.Width - Resources.OverFlowButton.Width - 2;
						int y2 = point.Y + (size.Height - Resources.OverFlowButton.Height) / 2 + size.Height - 1;
						g.DrawImage(Resources.OverFlowButton, x2, y2);
					}
				}
			}
		}

		public bool IsTextWrapped
		{
			get
			{
				return this.isTextWrapped;
			}
		}

		public new string Text
		{
			get
			{
				return base.Text;
			}
			set
			{
				base.Text = value;
				this.splitText = base.Text.Split(new char[]
				{
					'#'
				});
				if (this.splitText.Length == 2)
				{
					base.Text = ((this.splitText[0].Length > this.splitText[1].Length) ? this.splitText[0] : this.splitText[1]);
					this.isTextWrapped = true;
				}
				base.Invalidate();
			}
		}

		private bool IsPointIntRect(Rectangle rect)
		{
			Point point = Cursor.Position;
			point = base.Parent.PointToClient(point);
			return rect.Contains(point);
		}

		private bool isTextWrapped = false;

		private string[] splitText;
	}
}
