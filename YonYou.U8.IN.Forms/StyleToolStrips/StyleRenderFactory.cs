﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YonYou.U8.IN.Forms.StyleForms;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class StyleRenderFactory
	{
		public static ToolStripRenderer GetStyleRender(ToolStripStyle tooStripStyle)
		{
			ToolStripRenderer result = null;
			switch (tooStripStyle)
			{
				case ToolStripStyle.StyleToolStrip:
					if (SkinFormStyleTable.Style == WindowStyle.Modern)
					{
						result = new StyleRenderer();
					}
					else
					{
						result = new StyleRendererFlat();
					}
					break;
				case ToolStripStyle.StandardToolStrip:
					if (SkinFormStyleTable.Style == WindowStyle.Modern)
					{
						result = new StandardStyleRenderer();
					}
					else
					{
						result = new StandardStyleRendererFlat();
					}
					break;
				case ToolStripStyle.ContextMenuStrip:
					if (SkinFormStyleTable.Style == WindowStyle.Modern)
					{
						result = new ContextMenuStripRenderer();
					}
					else
					{
						result = new ContextMenuStripRendererFlat();
					}
					break;
				case ToolStripStyle.InnerToolStrip:
					if (SkinFormStyleTable.Style == WindowStyle.Modern)
					{
						result = new InnerRenderer();
					}
					else
					{
						result = new InnerRendererFlat();
					}
					break;
				case ToolStripStyle.ComboBoxStrip:
					if (SkinFormStyleTable.Style == WindowStyle.Modern)
						result = new ComboBoxStripRenderer();
					else
						result = new ComboBoxStripRendererFlat();
					break;
			}
			return result;
		}
	}
}
