﻿using System.Xml;
using System;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class FormInputExtend
	{
		private string formCode = string.Empty;

		private bool isSmallButton;

		private string csubid = string.Empty;

		private string cprojectno = string.Empty;

		private XmlDocument doc;

		private IntPtr handle = IntPtr.Zero;

		public string FormCode
		{
			get
			{
				if (string.IsNullOrEmpty(formCode))
				{
					formCode = GetAttribute("cformcode");
				}
				return formCode;
			}
			set
			{
				formCode = value;
			}
		}

		public bool IsSmallButton
		{
			get
			{
				string attribute = GetAttribute("bsmallbutton");
				if (!string.IsNullOrEmpty(attribute))
				{
					isSmallButton = ((attribute == "1") ? true : false);
				}
				return isSmallButton;
			}
			set
			{
				isSmallButton = value;
			}
		}

		public string CSubId
		{
			get
			{
				if (string.IsNullOrEmpty(csubid))
				{
					csubid = GetAttribute("csubid");
				}
				if (string.IsNullOrEmpty(csubid))
				{
					csubid = "AA";
				}
				return csubid;
			}
		}

		public string CProjectNo
		{
			get
			{
				if (string.IsNullOrEmpty(cprojectno))
				{
					cprojectno = GetAttribute("cprojectno");
				}
				if (string.IsNullOrEmpty(cprojectno))
				{
					cprojectno = "AA";
				}
				return cprojectno;
			}
		}

		public IntPtr Handle
		{
			get
			{
				return handle;
			}
			set
			{
				handle = value;
			}
		}

		public FormInputExtend()
		{
		}

		public FormInputExtend(string xml)
		{
			if (!string.IsNullOrEmpty(xml))
			{
				try
				{
					doc = new XmlDocument();
					doc.LoadXml(xml);
				}
				catch (XmlException ex)
				{
					//DBG.DbgTrace("Xml文档格式不正确:{0}", ex.Message);
				}
			}
		}

		private string GetAttribute(string attriName)
		{
			if (doc != null && doc.DocumentElement != null && doc.DocumentElement.FirstChild != null)
			{
				XmlNode xmlNode = doc.DocumentElement.FirstChild.Attributes[attriName];
				if (xmlNode != null)
				{
					return xmlNode.Value;
				}
			}
			return string.Empty;
		}
	}
}