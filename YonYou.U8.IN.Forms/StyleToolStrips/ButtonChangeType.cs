﻿namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public enum ButtonChangeType
	{
		Any = 0,
		Add = 1,
		Remove = 2,
		Modify = 3,
		ModifyComboBox = 4
	}

}