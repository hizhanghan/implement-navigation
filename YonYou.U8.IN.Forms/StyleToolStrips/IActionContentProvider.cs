﻿using System.Drawing;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public interface IActionContentProvider : IAdaptable
	{
		object Element { get; }

		int GetAccelerator(object element);

		Image GetImage(object element);

		string GetID(object element);

		string GetKey(object element);

		string GetText(object element);

		string GetToolTipText(object element);

		string GetActionGroup(object element);

		bool IsChecked(object element);

		bool IsEnabled(object element);

		bool IsVisible(object element);

		bool IsHaveAuth(object element);

		short GetIndex(object element);

		string GetPos(object element);

		int GetRowSpan(object element);

		string GetParentKey(object element);

		string GetBackgroundColor(object element);

		string GetForegroundColor(object element);

		string GetSetGroup(object element);

		short GetSetGroupRow(object element);

		int GetMenuItemStyle(object element);
	}

}