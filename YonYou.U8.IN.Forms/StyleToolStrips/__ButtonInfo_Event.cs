﻿using System.Runtime.InteropServices;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
    public delegate void __ButtonInfo_PropertyChangedEventHandler([In][Out][MarshalAs(UnmanagedType.Struct)] ref object sPropName, [In][Out][MarshalAs(UnmanagedType.Struct)] ref object vVal);
    public interface __ButtonInfo_Event
    {
        event __ButtonInfo_PropertyChangedEventHandler PropertyChanged;
    }
}