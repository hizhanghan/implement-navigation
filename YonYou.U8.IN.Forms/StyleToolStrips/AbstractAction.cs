﻿using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public abstract class AbstractAction : IActionSet, IAction
	{
		protected IActionDelegate dele;

		private int accelerator;

		private Image image;

		private string id = string.Empty;

		private string text = string.Empty;

		private string toolTipText = string.Empty;

		private bool isChecked;

		private bool isEnabled = true;

		private bool isVisible = true;

		private bool isHaveAuth = true;

		private int style = 1;

		private int dispstyle = 4;

		private object tag;

		private Control econtrol;

		private IComponent associatedbar;

		private string catalog = string.Empty;

		private Hashtable properties = new Hashtable();

		protected ArrayList childrenActions = new ArrayList();

		private int rowSpan = 1;

		private string pos = "P";

		private Color foregroundColor = Color.Black;

		private Color backgroundColor = Color.Transparent;

		private string setGroup = string.Empty;

		private int setGroupRow = 3;

		private bool atDesignTime;

		public Control EControl
		{
			get
			{
				return econtrol;
			}
			set
			{
				econtrol = value;
			}
		}

		public ArrayList Actions
		{
			get
			{
				return childrenActions;
			}
		}

		public Hashtable Properties
		{
			get
			{
				return properties;
			}
		}

		public object Tag
		{
			get
			{
				return tag;
			}
			set
			{
				tag = value;
				FirePropertyChanged(this);
			}
		}

		public string Catalog
		{
			get
			{
				return catalog;
			}
			set
			{
				catalog = value;
				FirePropertyChanged(this);
			}
		}

		public int Accelerator
		{
			get
			{
				return accelerator;
			}
			set
			{
				accelerator = value;
				FirePropertyChanged(this);
				OnSelectChanged();
			}
		}

		public Image Image
		{
			get
			{
				return image;
			}
			set
			{
				image = value;
				FirePropertyChanged(this);
				OnSelectChanged();
			}
		}

		public string Id
		{
			get
			{
				return id;
			}
			set
			{
				id = value;
				FirePropertyChanged(this);
			}
		}

		public string Text
		{
			get
			{
				return text;
			}
			set
			{
				text = value;
				FirePropertyChanged(this);
				OnSelectChanged();
			}
		}

		public string ToolTipText
		{
			get
			{
				return toolTipText;
			}
			set
			{
				toolTipText = value;
				FirePropertyChanged(this);
				OnSelectChanged();
			}
		}

		public bool IsChecked
		{
			get
			{
				return isChecked;
			}
			set
			{
				isChecked = value;
				FirePropertyChanged(this);
				OnSelectChanged();
			}
		}

		public bool IsEnabled
		{
			get
			{
				return isEnabled;
			}
			set
			{
				isEnabled = value;
				FirePropertyChanged(this);
				OnSelectChanged();
			}
		}

		public bool IsVisible
		{
			get
			{
				return isVisible;
			}
			set
			{
				isVisible = value;
				FirePropertyChanged(this);
				OnSelectChanged();
			}
		}

		public bool IsHaveAuth
		{
			get
			{
				return isHaveAuth;
			}
			set
			{
				isHaveAuth = value;
				FirePropertyChanged(this);
				OnSelectChanged();
			}
		}

		public int Style
		{
			get
			{
				return style;
			}
			set
			{
				style = value;
				FirePropertyChanged(this);
				OnSelectChanged();
			}
		}

		public int DisplayStyle
		{
			get
			{
				return dispstyle;
			}
			set
			{
				dispstyle = value;
				FirePropertyChanged(this);
				OnSelectChanged();
			}
		}

		public IComponent AssociatedBar
		{
			get
			{
				return associatedbar;
			}
			set
			{
				associatedbar = value;
				FirePropertyChanged(this);
			}
		}

		public int RowSpan
		{
			get
			{
				return rowSpan;
			}
			set
			{
				rowSpan = value;
				FirePropertyChanged(this);
			}
		}

		public string Pos
		{
			get
			{
				return pos;
			}
			set
			{
				pos = value;
			}
		}

		public Color ForegroundColor
		{
			get
			{
				return foregroundColor;
			}
			set
			{
				foregroundColor = value;
			}
		}

		public Color BackgroundColor
		{
			get
			{
				return backgroundColor;
			}
			set
			{
				backgroundColor = value;
			}
		}

		public string SetGroup
		{
			get
			{
				return setGroup;
			}
			set
			{
				setGroup = value;
			}
		}

		public int SetGroupRow
		{
			get
			{
				return setGroupRow;
			}
			set
			{
				if (value > 0)
				{
					setGroupRow = value;
				}
			}
		}

		public bool AtDesignTime
		{
			get
			{
				return atDesignTime;
			}
			set
			{
				atDesignTime = value;
			}
		}

		public event ActionEventHandler PropertyChanged;

		public event ActionEventHandler ActionAdded;

		public event ActionEventHandler ActionRemoved;

		public AbstractAction(string id)
			: this(id, null)
		{
		}

		public AbstractAction(string id, IActionDelegate dele)
		{
			this.id = id;
			this.dele = dele;
		}

		public virtual void Run(IActionDelegate dele)
		{
			if (dele != null)
			{
				dele.Run(this);
			}
		}

		public virtual string GetTypeName()
		{
			return "AbstractAction";
		}

		public int Add(IAction action)
		{
			int result = -1;
			if (!childrenActions.Contains(action))
			{
				result = childrenActions.Add(action);
				FireActionAdded(action);
			}
			return result;
		}

		public void Remove(IAction action)
		{
			if (childrenActions.Contains(action))
			{
				childrenActions.Remove(action);
				FireActionRemoved(action);
			}
		}

		public void Remove(int index)
		{
			if (index >= 0 && index < childrenActions.Count)
			{
				IAction action = childrenActions[index] as IAction;
				childrenActions.RemoveAt(index);
				FireActionRemoved(action);
			}
		}

		protected void OnSelectChanged()
		{
			if (dele != null)
			{
				dele.SelectionChanged(this, null);
			}
		}

		internal void FirePropertyChanged(IAction action)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(action, new ActionEventArgs(action));
			}
		}

		internal void FireActionAdded(IAction action)
		{
			if (this.ActionAdded != null)
			{
				this.ActionAdded(action, new ActionEventArgs(action));
			}
		}

		internal void FireActionRemoved(IAction action)
		{
			if (this.ActionAdded != null)
			{
				this.ActionRemoved(action, new ActionEventArgs(action));
			}
		}

		public virtual void Run()
		{
			Run(dele);
		}

		public override string ToString()
		{
			return text;
		}
	}

}