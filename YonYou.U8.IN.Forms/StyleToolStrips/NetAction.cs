﻿using System;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class NetAction : RetargetAction
	{
		public enum NetActionType
		{
			Normal = 0,
			Edit = 1,
			Oprate = 2,
			Query = 3
		}

		private NetActionType _type;

		public NetActionType ActionType
		{
			set
			{
				_type = value;
			}
		}

		public NetAction(string id, IActionDelegate dele)
			: base(id, dele)
		{
		}

		public override string GetTypeName()
		{
			return GetType(_type);
		}

		private string GetType(NetActionType type)
		{
			switch (Convert.ToInt32(type))
			{
				case 0:
					return "通用";
				case 1:
					return "编辑";
				case 2:
					return "处理";
				case 3:
					return "查询";
				default:
					return "通用";
			}
		}
	}
}