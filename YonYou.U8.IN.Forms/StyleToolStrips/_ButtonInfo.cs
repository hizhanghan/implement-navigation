﻿namespace YonYou.U8.IN.Forms.StyleToolStrips
{
    public interface _ButtonInfo
    {
        string id { get; set; }
        string Key { get; set; }
        string Caption { get; set; }
        int MsgDispatcherHandle { get; set; }
        string Tooltip { get; set; }
        ToolButtonSyle Style { get; set; }
        bool Visible { get; set; }
        bool Enabled { get; set; }
        bool Checked { get; set; }
        string Tag { get; set; }
        string Image { get; set; }
        string Index { get; set; }
        ButtonInfos Buttons { get; set; }
        string ToolbarPos { get; set; }
        int ButtonHeight { get; set; }
        string actionSet { get; set; }
        string ParentKey { get; set; }
        string ForegroundColor { get; set; }
        string BackgroundColor { get; set; }
        string SetGroup { get; set; }
        short SetGroupRow { get; set; }
        string ToStringEx();
    }
}