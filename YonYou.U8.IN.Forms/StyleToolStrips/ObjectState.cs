﻿using System;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	[Flags]
	public enum ObjectState
	{
		Normal = 0,
		Hot = 1,
		Pressed = 2,
		Disabled = 4,
		Selected = 8
	}
}