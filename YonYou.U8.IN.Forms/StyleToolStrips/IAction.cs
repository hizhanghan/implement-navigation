﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public delegate void ActionEventHandler(object sender, ActionEventArgs e);
	public class ActionEventArgs : EventArgs
	{
		private IAction action;

		private int flags;

		public IAction Action
		{
			get
			{
				return action;
			}
		}

		public ActionEventArgs(IAction action)
			: this(action, 0)
		{
		}

		public ActionEventArgs(IAction action, int flags)
		{
			this.action = action;
			this.flags = flags;
		}
	}
	public interface IAction
	{
		int Accelerator { get; set; }

		Image Image { get; set; }

		string ToolTipText { get; set; }

		bool IsChecked { get; set; }

		bool IsEnabled { get; set; }

		bool IsVisible { get; set; }

		bool IsHaveAuth { get; set; }

		int Style { get; set; }

		int DisplayStyle { get; set; }

		Control EControl { get; set; }

		IComponent AssociatedBar { get; set; }

		Hashtable Properties { get; }

		string Catalog { get; set; }

		string Id { get; }

		string Text { get; set; }

		object Tag { get; set; }

		int RowSpan { get; set; }

		string Pos { get; set; }

		Color ForegroundColor { get; set; }

		Color BackgroundColor { get; set; }

		string SetGroup { get; set; }

		int SetGroupRow { get; set; }

		bool AtDesignTime { get; set; }

		event ActionEventHandler PropertyChanged;

		void Run();
	}
}