﻿using System;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public abstract class AbstractFormInput : IEditorInput
	{
		public class ActionHashtable
		{
			private const int MaxCount = 200;

			private string[] _actionkey;

			protected IAction[] _actions;

			private ToolButtonSyle[] _actionStyle;

			private int curIndex;

			public int Count
			{
				get
				{
					return curIndex;
				}
			}

			public IAction this[string key]
			{
				get
				{
					for (int i = 0; i < 200; i++)
					{
						if (_actionkey[i] == key)
						{
							return _actions[i];
						}
					}
					return null;
				}
				set
				{
					for (int i = 0; i < 200; i++)
					{
						if (_actionkey[i] == key)
						{
							_actions[i] = value;
							break;
						}
					}
				}
			}

			public IAction this[int Index]
			{
				get
				{
					return _actions[Index];
				}
				set
				{
					_actions[Index] = value;
				}
			}

			public ActionHashtable()
			{
				_actionkey = new string[200];
				_actions = new IAction[200];
				_actionStyle = (ToolButtonSyle[])(object)new ToolButtonSyle[200];
				curIndex = 0;
			}

			public bool IsExistKey(string key)
			{
				for (int i = 0; i < 200; i++)
				{
					if (_actionkey[i] == key)
					{
						return true;
					}
					if (_actionkey[i] == null)
					{
						return false;
					}
				}
				return false;
			}

			public void Add(string key, IAction action, ToolButtonSyle tbs)
			{
				//IL_0036: Unknown result type (might be due to invalid IL or missing references)
				//IL_0038: Expected I4, but got Unknown
				if (curIndex < 200)
				{
					_actionkey[curIndex] = key;
					_actions[curIndex] = action;
					_actionStyle[curIndex] = (ToolButtonSyle)(int)tbs;
					curIndex++;
				}
			}

			public void Add(string key, IAction action)
			{
				if (action.GetType().IsSubclassOf(typeof(NetAction)) || action.GetType() == typeof(NetAction))
				{
					if (((NetAction)action).Actions.Count > 0)
					{
						Add(key, action, (ToolButtonSyle)8);
					}
					else if (Enum.IsDefined(typeof(ToolButtonSyle), action.Style))
					{
						Add(key, action, (ToolButtonSyle)action.Style);
					}
					else
					{
						Add(key, action, (ToolButtonSyle)0);
					}
				}
				else
				{
					Add(key, action, (ToolButtonSyle)0);
				}
			}

			public IAction[] getActions()
			{
				return _actions;
			}

			public void Remove(string key)
			{
				for (int i = 0; i < 200; i++)
				{
					if (_actionkey[i] == key)
					{
						_actions[i] = null;
						curIndex--;
					}
				}
			}

			public void Clear()
			{
				for (int i = 0; i < 200; i++)
				{
					_actionkey[i] = null;
					_actions[i] = null;
					_actionStyle[curIndex] = (ToolButtonSyle)0;
				}
				curIndex = 0;
			}

			public ToolButtonSyle GetActionStyle(string key)
			{
				for (int i = 0; i < 200; i++)
				{
					if (_actionkey[i] == key)
					{
						return _actionStyle[i];
					}
				}
				return (ToolButtonSyle)0;
			}
		}

		private string formCode = string.Empty;

		private object m_Tag;

		protected ActionHashtable _actions = new ActionHashtable();

		private string _editorID = "";

		private string _key = string.Empty;

		public string FormCode
		{
			get
			{
				return formCode;
			}
			set
			{
				formCode = value;
			}
		}

		public virtual FormInputExtend FormInputExtend
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		public object Tag
		{
			get
			{
				return m_Tag;
			}
			set
			{
				m_Tag = value;
			}
		}

		public string Key
		{
			get
			{
				return _key;
			}
			set
			{
				_key = value;
			}
		}

		public ActionHashtable Actions
		{
			get
			{
				return _actions;
			}
		}

		public string EditorID
		{
			get
			{
				return _editorID;
			}
			set
			{
				_editorID = value;
			}
		}

		public event ButtonChange ButtonChange;

		public event ActionsChange ActionsChange;

		public event TransferMessageHandler TransferMessage;

		public AbstractFormInput(string key)
		{
			_key = key;
		}

		protected void OnButtonChange()
		{
		}

		public void OnActionsChange(ButtonChangeType changeType, ActionHashtable actions)
		{
			if (this.ActionsChange != null)
			{
				this.ActionsChange(this, changeType, actions);
			}
		}

		protected void OnTransferMessage(string sKey, string sMessageXml)
		{
			if (this.TransferMessage != null)
			{
				this.TransferMessage(this, sKey, sMessageXml);
			}
		}
	}
	public delegate void TransferMessageHandler(object sender, string sKey, string sMessageXml);
	public delegate void ActionsChange(object sender, ButtonChangeType changeType, AbstractFormInput.ActionHashtable actions);

	public delegate void ButtonChange(object sender, ButtonChangeType changeType, IAction action);
}