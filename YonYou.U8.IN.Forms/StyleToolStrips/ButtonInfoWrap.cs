﻿using System.Threading;
using System;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	public class ButtonInfoWrap : ButtonInfo, _ButtonInfo, __ButtonInfo_Event
	{
		private ButtonInfo _btnInfo;

		private ButtonType _btnType;

		private string _Image = string.Empty;

		private short _Index;

		private int _MsgDispatcherHandle;

		private string _Key = string.Empty;

		private string _ID = string.Empty;

		private bool _Visible;

		private bool _Enabled;

		private bool _Checked;

		private string _Caption = string.Empty;

		private string _Tooltip = string.Empty;

		private string _Tag = string.Empty;

		private int _RowSpan = 1;

		private string _Pos = "P";

		private string _ParentKey = string.Empty;

		private string _Group = string.Empty;

		private ToolButtonSyle _Style;

		private string _BackgroundColor = string.Empty;

		private string _ForegroundColor = string.Empty;

		private string _SetGroup = string.Empty;

		private short _SetGroupRow = 3;

		private bool _HaveAuth;

		public ButtonInfo ButtonInfo
		{
			get
			{
				return _btnInfo;
			}
		}

		public ButtonType ButtonType
		{
			get
			{
				return _btnType;
			}
		}

		public ToolButtonSyle Style
		{
			get
			{
				return _Style;
			}
			set
			{
				_Style = value;
			}
		}

		public int ButtonHeight
		{
			get
			{
				return _RowSpan;
			}
			set
			{
				_RowSpan = value;
			}
		}

		public string ToolbarPos
		{
			get
			{
				return _Pos;
			}
			set
			{
				_Pos = value;
			}
		}

		public string actionSet
		{
			get
			{
				return _Group;
			}
			set
			{
				_Group = value;
			}
		}

		public string ParentKey
		{
			get
			{
				return _ParentKey;
			}
			set
			{
				_ParentKey = value;
			}
		}

		public string BackgroundColor
		{
			get
			{
				return _BackgroundColor;
			}
			set
			{
				_BackgroundColor = value;
			}
		}

		public string ForegroundColor
		{
			get
			{
				return _ForegroundColor;
			}
			set
			{
				_ForegroundColor = value;
			}
		}

		public string SetGroup
		{
			get
			{
				return _SetGroup;
			}
			set
			{
				_SetGroup = value;
			}
		}

		public short SetGroupRow
		{
			get
			{
				return _SetGroupRow;
			}
			set
			{
				_SetGroupRow = value;
			}
		}

        public string id { get; set; }
        public string Key { get; set; }
        public string Caption { get; set; }
        public int MsgDispatcherHandle { get; set; }
        public string Tooltip { get; set; }
        public bool Visible { get; set; }
        public bool Enabled { get; set; }
        public bool Checked { get; set; }
        public string Tag { get; set; }
        string _ButtonInfo.Image { get; set; }
        public string Index { get; set; }
        public ButtonInfos Buttons { get; set; }

        public event __ButtonInfo_PropertyChangedEventHandler PropertyChanged
		{
			add
			{
				__ButtonInfo_PropertyChangedEventHandler _ButtonInfo_PropertyChangedEventHandler = this.PropertyChanged;
				__ButtonInfo_PropertyChangedEventHandler _ButtonInfo_PropertyChangedEventHandler2;
				do
				{
					_ButtonInfo_PropertyChangedEventHandler2 = _ButtonInfo_PropertyChangedEventHandler;
					__ButtonInfo_PropertyChangedEventHandler value2 = (__ButtonInfo_PropertyChangedEventHandler)(object)Delegate.Combine((Delegate)(object)_ButtonInfo_PropertyChangedEventHandler2, (Delegate)(object)value);
					_ButtonInfo_PropertyChangedEventHandler = Interlocked.CompareExchange(ref this.PropertyChanged, value2, _ButtonInfo_PropertyChangedEventHandler2);
				}
				while ((object)_ButtonInfo_PropertyChangedEventHandler != _ButtonInfo_PropertyChangedEventHandler2);
			}
			remove
			{
				__ButtonInfo_PropertyChangedEventHandler _ButtonInfo_PropertyChangedEventHandler = this.PropertyChanged;
				__ButtonInfo_PropertyChangedEventHandler _ButtonInfo_PropertyChangedEventHandler2;
				do
				{
					_ButtonInfo_PropertyChangedEventHandler2 = _ButtonInfo_PropertyChangedEventHandler;
					__ButtonInfo_PropertyChangedEventHandler value2 = (__ButtonInfo_PropertyChangedEventHandler)(object)Delegate.Remove((Delegate)(object)_ButtonInfo_PropertyChangedEventHandler2, (Delegate)(object)value);
					_ButtonInfo_PropertyChangedEventHandler = Interlocked.CompareExchange(ref this.PropertyChanged, value2, _ButtonInfo_PropertyChangedEventHandler2);
				}
				while ((object)_ButtonInfo_PropertyChangedEventHandler != _ButtonInfo_PropertyChangedEventHandler2);
			}
		}

		public ButtonInfoWrap(ButtonInfo btnInfo, ButtonType btnType)
		{
			_btnInfo = btnInfo;
			_btnType = btnType;
		}

		public ButtonInfoWrap(string sBtnInfo, ButtonType btnType)
		{
			_btnType = btnType;
			Refresh(sBtnInfo);
		}

		public void Refresh(string sBtnInfo)
		{
			string[] array = sBtnInfo.Split('^');
			_ID = array[0];
			_Key = array[1];
			_Caption = array[2];
			_Tooltip = array[3];
			_Image = array[4];
			_Tag = array[5];
			_Enabled = bool.Parse(array[6]);
			_Visible = bool.Parse(array[7]);
			_Checked = bool.Parse(array[8]);
			_MsgDispatcherHandle = int.Parse(array[9]);
			_Style = (ToolButtonSyle)int.Parse(array[10]);
			if (_ID == "" || _ID == string.Empty)
			{
				_ID = _Key;
			}
			_RowSpan = int.Parse(array[11]);
			if (!string.IsNullOrEmpty(array[12]))
			{
				_Pos = array[12];
			}
			_Group = array[13];
			_ParentKey = array[14];
			_ForegroundColor = array[15];
			_BackgroundColor = array[16];
			_SetGroup = array[17];
			short.TryParse(array[18], out _SetGroupRow);
			_HaveAuth = bool.Parse(array[19]);
		}

		private string GetNullString(object o)
		{
			return string.Empty;
		}















		public bool get_HaveAuth()
		{
			return _HaveAuth;
		}

		public int get_RowSpan()
		{
			return _RowSpan;
		}

		public string get_Pos()
		{
			return _Pos;
		}

		public string get_Group()
		{
			return _Group;
		}



















		public string ToStringEx()
		{
			return "";
		}



	}
}