﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace YonYou.U8.IN.Forms.StyleToolStrips
{
	[DefaultProperty("Value")]
	[ToolStripItemDesignerAvailability(ToolStripItemDesignerAvailability.All)]
	public class ToolStripItemGroupEX : ToolStripControlHost, IDisposable
	{
		internal InnerToolStrip ToolStrip
		{
			get
			{
				return base.Control as InnerToolStrip;
			}
		}

		public ToolStripItemGroupEX() : base(new InnerToolStrip())
		{
			this.ToolStrip.Margin = new Padding(5, 0, 0, 0);
		}

		public ToolStripItemCollection Items
		{
			get
			{
				return this.ToolStrip.Items;
			}
		}

		public void SuspendLayout()
		{
			this.ToolStrip.SuspendLayout();
		}

		public void ResumeLayout(bool perfomLayout)
		{
			this.ToolStrip.ResumeLayout(perfomLayout);
		}

		public void ResumeLayout()
		{
			this.ToolStrip.ResumeLayout();
		}

		void IDisposable.Dispose()
		{
			this.ToolStrip.Items.Clear();
			((IDisposable)this.ToolStrip).Dispose();
			base.Control.Dispose();
		}
	}
}