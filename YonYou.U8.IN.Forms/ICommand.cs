﻿namespace YonYou.U8.IN.Forms
{
	public interface ICommand
	{
		object Execute(params object[] args);
	}

}