﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;

namespace YonYou.U8.IN.Forms
{
    public partial class MetroSkin : Component
    {
        private Color _BorderColor = ColorTranslator.FromHtml("#5DA3DA");

        private Color _BackColor = ColorTranslator.FromHtml("#5DA3DA");

        private Color _StatusBackColor = ColorTranslator.FromHtml("#5DA3DA");
        public MetroSkin()
        {
            InitializeComponent();
        }

        public MetroSkin(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        
        [Category("外观")]
        public virtual Color BorderColor
        {
            get
            {
                return this._BorderColor;
            }
            set
            {
                this._BorderColor = value;
            }
        }
        [Category("外观")]
        public virtual Color BackColor
        {
            get
            {
                return this._BackColor;
            }
            set
            {
                this._BackColor = value;
            }
        }

        [Category("外观")]
        public virtual Color StatusBackColor
        {
            get
            {
                return this._StatusBackColor;
            }
            set
            {
                this._StatusBackColor = value;
            }
        }

    }
}
