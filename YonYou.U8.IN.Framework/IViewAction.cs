﻿namespace YonYou.U8.IN.Framework
{
	public interface IViewAction
	{
		object Execute(params object[] args);
	}
}