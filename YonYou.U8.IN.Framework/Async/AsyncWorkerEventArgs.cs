﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YonYou.U8.IN.Framework.Async
{
	public class AsyncWorkerEventArgs : EventArgs
	{
		public WorkerState State { get; set; }

		public object Argument { get; private set; }

		public object Tag { get; set; }

		public int Count { get; set; }

		public int Num { get; set; }

		public AsyncWorkerEventArgs(object argument)
		{
			Argument = argument;
		}
	}
}
