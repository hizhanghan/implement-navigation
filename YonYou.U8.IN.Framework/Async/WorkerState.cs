﻿namespace YonYou.U8.IN.Framework.Async
{
	public enum WorkerState
	{
		None = 0,
		Completed = 1,
		Canceled = 2,
		Error = 3
	}

}