﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YonYou.U8.IN.Framework.Async
{
    public partial class Loading : Form
    {
        public Loading(string info)
        {
            InitializeComponent();
			lblInfo.Text = info;
		}

		public string LoadingInfo
		{
			get
			{
				return lblInfo.Text;
			}
			set
			{
				lblInfo.Text = value;
			}
		}
	}
}
