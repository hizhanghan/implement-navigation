﻿namespace YonYou.U8.IN.Framework
{
	public enum LayoutStyle
	{
		ToolBar = 0,
		Editor = 1,
		StatusBar = 2
	}
}