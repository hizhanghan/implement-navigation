﻿namespace YonYou.U8.IN.Framework
{
	public interface INetLoader
	{
		bool IsEmbed { get; }

		bool Load(BizContext context, string cMenuId, string subMenuId);

		void ShowEmbedControl(INetControl control, string key, bool singleton);
	}
}