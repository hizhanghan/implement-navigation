﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YonYou.U8.IN.Forms;
using YonYou.U8.IN.Framework.Views;
using YonYou.U8.IN.Utility;

namespace YonYou.U8.IN.Framework.Internals
{
    public partial class ViewBenchForm : MetroForm
    {
		private LayoutView layoutView;
		private ToolBarView toolBarView;
		public ViewBenchForm()
        {
            InitializeComponent();
            this.Initialize();
            SkinManager.Instance.Start();
        }

		public LayoutView LayoutView
		{
			get
			{
				if (this.layoutView == null)
				{
					this.layoutView = new LayoutView(this.ToolBarContainer, this.EditorContainer, this.StatusContainer);
				}
				return this.layoutView;
			}
		}

		public ToolBarView ToolBarView
		{
			get
			{
				if (this.toolBarView == null)
				{
					string name = Path.Combine(PathUtils.INPath, "ImplementNavigation\\icons\\qi_logo.png");
					this.toolBar.Logo = ImageUtils.GetImageFromLocal(name);
					this.toolBarView = new ToolBarView(this.toolBar);
				}
				return this.toolBarView;
			}
		}

		private void Initialize()
		{
			this.helpProvider.HelpNamespace = Application.StartupPath + "/Help/实施导航.chm";
			this.helpProvider.SetHelpNavigator(this, HelpNavigator.Topic);
			base.FormClosing += this.TriggerFormClosing;
		}

		internal void SetHelpUrl(string url)
		{
			this.helpProvider.SetHelpNavigator(this, HelpNavigator.Topic);
			this.helpProvider.SetShowHelp(this, true);
			this.helpProvider.SetHelpKeyword(this, url);
		}

		private void TriggerFormClosing(object sender, FormClosingEventArgs e)
		{
			ViewAdvisor.Instance.OnExiting(e);
		}




	}
}
