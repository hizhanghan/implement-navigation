﻿using System.Drawing;
using System.Windows.Forms;

namespace YonYou.U8.IN.Framework.Internals
{
    public class ViewContainer : ContainerControl
	{
		public ViewContainer()
		{
			SetStyle(ControlStyles.SupportsTransparentBackColor 
				| ControlStyles.AllPaintingInWmPaint 
				| ControlStyles.OptimizedDoubleBuffer, true);
			BackColor = Color.Transparent;
		}
	}
}
