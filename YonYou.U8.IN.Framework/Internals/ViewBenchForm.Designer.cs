﻿
namespace YonYou.U8.IN.Framework.Internals
{
    partial class ViewBenchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ViewBenchForm));
            this.ToolBarContainer = new System.Windows.Forms.Panel();
            this.toolBar = new YonYou.U8.IN.Forms.MetroToolBar();
            this.StatusContainer = new System.Windows.Forms.Panel();
            this.EditorContainer = new System.Windows.Forms.Panel();
            this.helpProvider = new System.Windows.Forms.HelpProvider();
            this.metroSkin1 = new YonYou.U8.IN.Forms.MetroSkin(this.components);
            this.ToolBarContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolBarContainer
            // 
            this.ToolBarContainer.BackColor = System.Drawing.Color.Transparent;
            this.ToolBarContainer.Controls.Add(this.toolBar);
            this.ToolBarContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolBarContainer.Location = new System.Drawing.Point(1, 30);
            this.ToolBarContainer.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ToolBarContainer.Name = "ToolBarContainer";
            this.ToolBarContainer.Size = new System.Drawing.Size(898, 72);
            this.ToolBarContainer.TabIndex = 0;
            // 
            // toolBar
            // 
            this.toolBar.BackColor = System.Drawing.Color.Transparent;
            this.toolBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolBar.ItemSpace = 10;
            this.toolBar.Location = new System.Drawing.Point(0, 0);
            this.toolBar.Logo = ((System.Drawing.Image)(resources.GetObject("toolBar.Logo")));
            this.toolBar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.toolBar.MinimumSize = new System.Drawing.Size(0, 72);
            this.toolBar.Name = "toolBar";
            this.toolBar.Size = new System.Drawing.Size(898, 72);
            this.toolBar.TabIndex = 4;
            this.toolBar.Text = "metroToolBar1";
            // 
            // StatusContainer
            // 
            this.StatusContainer.BackColor = System.Drawing.Color.Transparent;
            this.StatusContainer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.StatusContainer.Location = new System.Drawing.Point(1, 579);
            this.StatusContainer.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.StatusContainer.Name = "StatusContainer";
            this.StatusContainer.Size = new System.Drawing.Size(898, 20);
            this.StatusContainer.TabIndex = 1;
            // 
            // EditorContainer
            // 
            this.EditorContainer.BackColor = System.Drawing.Color.White;
            this.EditorContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditorContainer.Location = new System.Drawing.Point(1, 102);
            this.EditorContainer.Margin = new System.Windows.Forms.Padding(0);
            this.EditorContainer.Name = "EditorContainer";
            this.EditorContainer.Size = new System.Drawing.Size(898, 477);
            this.EditorContainer.TabIndex = 2;
            // 
            // metroSkin1
            // 
            this.metroSkin1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(218)))));
            this.metroSkin1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(218)))));
            this.metroSkin1.StatusBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(93)))), ((int)(((byte)(163)))), ((int)(((byte)(218)))));
            // 
            // ViewBenchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(900, 600);
            this.Controls.Add(this.EditorContainer);
            this.Controls.Add(this.StatusContainer);
            this.Controls.Add(this.ToolBarContainer);
            this.MetroSkin = this.metroSkin1;
            this.Name = "ViewBenchForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.SysButton = YonYou.U8.IN.Forms.SysButton.Normal;
            this.Text = "ViewBenchForm";
            this.TitleBarEndColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TitleBarStartColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ToolBarContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel ToolBarContainer;
        private System.Windows.Forms.Panel StatusContainer;
        private System.Windows.Forms.Panel EditorContainer;
        private System.Windows.Forms.HelpProvider helpProvider;
        private Forms.MetroToolBar toolBar;
        private Forms.MetroSkin metroSkin1;
    }
}