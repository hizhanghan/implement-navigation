﻿using System.Runtime.InteropServices;
using System;
using System.Text;

namespace YonYou.U8.IN.Framework
{
	public class Win32Api
	{
		public const uint SWP_FRAMECHANGED = 32u;

		public const uint SWP_HIDEWINDOW = 128u;

		public const uint SWP_NOACTIVATE = 16u;

		public const uint SWP_NOCOPYBITS = 256u;

		public const uint SWP_NOMOVE = 2u;

		public const uint SWP_NOOWNERZORDER = 512u;

		public const uint SWP_NOREDRAW = 8u;

		public const uint SWP_NOSENDCHANGING = 1024u;

		public const uint SWP_NOSIZE = 1u;

		public const uint SWP_NOZORDER = 4u;

		public const uint SWP_SHOWWINDOW = 64u;

		public const uint TOPMOST_FLAGS = 3u;

		public const int SWP_MAX = 3;

		public const int SWP_MIN = 2;

		public const int SWP_NORMAL = 1;

		public const int WM_COPYDATA = 74;

		public const int WM_SYSCOMMAND = 274;

		public const int SC_MAXIMIZE = 61488;

		public const int SC_RESTORE = 61728;

		public static readonly IntPtr HWND_NOTOPMOST = new IntPtr(-2);

		public static readonly IntPtr HWND_TOP = new IntPtr(0);

		public static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);

		[DllImport("User32.dll")]
		public static extern int SendMessage(IntPtr hWnd, int wMsg, uint wParam, uint lParam);

		[DllImport("User32.dll")]
		public static extern int PostMessage(IntPtr hWnd, int wMsg, uint wParam, uint lParam);

		[DllImport("user32.dll", SetLastError = true)]
		public static extern IntPtr SetActiveWindow(IntPtr hWnd);

		[DllImport("user32")]
		public static extern int SetForegroundWindow(IntPtr hwnd);

		[DllImport("user32.dll")]
		public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

		[DllImport("user32.dll", CharSet = CharSet.Auto)]
		public static extern int ShowWindow(IntPtr hwnd, int nCmdShow);


        [DllImport("Shell32.DLL")]
        public static extern int SHGetSpecialFolderLocation(IntPtr hwndOwner, int nFolder, out IntPtr ppidl);

        [StructLayout(LayoutKind.Sequential, Pack = 8)]
        public struct BROWSEINFO
        {
            public IntPtr hwndOwner;

            public IntPtr pidlRoot;

            public IntPtr pszDisplayName;

            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpszTitle;

            public int ulFlags;

            [MarshalAs(UnmanagedType.FunctionPtr)]
            public BFFCALLBACK lpfn;

            public IntPtr lParam;

            public int iImage;
        }

        public delegate int BFFCALLBACK(IntPtr hwnd, uint uMsg, IntPtr lParam, IntPtr lpData);

        [DllImport("Shell32.DLL", CharSet = CharSet.Auto)]
        public static extern IntPtr SHBrowseForFolder(ref BROWSEINFO bi);


        [DllImport("Shell32.DLL")]
        public static extern int SHGetPathFromIDList(IntPtr pidl, StringBuilder Path);

        [Guid("00000002-0000-0000-C000-000000000046")]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        public interface IMalloc
        {
            [PreserveSig]
            IntPtr Alloc([In] int cb);

            [PreserveSig]
            IntPtr Realloc([In] IntPtr pv, [In] int cb);

            [PreserveSig]
            void Free([In] IntPtr pv);

            [PreserveSig]
            int GetSize([In] IntPtr pv);

            [PreserveSig]
            int DidAlloc(IntPtr pv);

            [PreserveSig]
            void HeapMinimize();
        }

        [DllImport("Shell32.DLL")]
        public static extern int SHGetMalloc(out IMalloc ppMalloc);


    }
}