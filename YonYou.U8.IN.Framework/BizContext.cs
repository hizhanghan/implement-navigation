﻿using System.Collections;
using UFSoft.U8.Framework.Login.UI;
using UFSoft.U8.Framework.LoginContext;
using YonYou.U8.IN.Model;

namespace YonYou.U8.IN.Framework
{
	public abstract class BizContext
	{
		private static Hashtable _BizCache;

		public clsLogin LoginObject
		{
			get
			{
				return LoginEntity.LoginObject;
			}
		}

		public UserData UserData
		{
			get
			{
				return LoginEntity.UserData;
			}
		}

		public LoginEntity LoginEntity { get; internal set; }

		static BizContext()
		{
			_BizCache = new Hashtable();
		}

		public static void PutCache<T>(string key, T value)
		{
			if (_BizCache.ContainsKey(key))
			{
				_BizCache[key] = value;
			}
			else
			{
				_BizCache.Add(key, value);
			}
		}

		public static void RemoveCache(string key)
		{
			if (_BizCache.ContainsKey(key))
			{
				_BizCache.Remove(key);
			}
		}

		public static T QueryCache<T>(string key)
		{
			if (_BizCache.ContainsKey(key))
			{
				return (T)_BizCache[key];
			}
			return default(T);
		}

		public abstract BizMenuNode QueryMenuNode(string cMenuId);
	}
}