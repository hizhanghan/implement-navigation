﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YonYou.U8.IN.Framework.Events
{
	public class NetLoaderEventArgs : EventArgs
	{
		public INetControl Control { get; private set; }

		public string Key { get; private set; }

		public bool Singleton { get; private set; }

		internal INetLoader Loader { get; set; }

		public NetLoaderEventArgs(INetControl control, string key, bool singleton)
		{
			Control = control;
			Key = key;
			Singleton = singleton;
		}
	}
}
