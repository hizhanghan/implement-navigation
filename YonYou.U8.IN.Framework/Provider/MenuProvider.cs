﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YonYou.U8.IN.Framework.Repositories;
using YonYou.U8.IN.Model;

namespace YonYou.U8.IN.Framework.Provider
{
	public class MenuProvider : IProvider<BizMenuNode>, IProvider
	{
		private static BizMenuNode _BizMenuNode;

		public BizMenuNode Get(params object[] args)
		{
			if (_BizMenuNode == null)
			{
				if (args.Length != 1)
				{
					throw new ArgumentException("args");
				}
				string text = args[0].ToString();
				if (string.IsNullOrEmpty(text))
				{
					throw new ArgumentNullException("connectionStr");
				}
				MenuRepository menuRepository = new MenuRepository(text);
				IList<BizMenu> list = menuRepository.Query();
                BizMenu bizMenu = new BizMenu
                {
                    Id = "IN",
                    AuthId = "IN",
                    Name = "业务导航",
                    Order = 0
                };
                BizMenuNode bizMenuNode = new BizMenuNode(bizMenu);
				IDictionary<string, BizMenuNode> dictionary = new Dictionary<string, BizMenuNode>();
				if (list != null)
				{
					foreach (BizMenu item in list)
					{
						BizMenuNode bizMenuNode2 = new BizMenuNode(item);
						if (string.IsNullOrEmpty(item.ParentId))
						{
							if (!dictionary.ContainsKey(item.Id))
							{
								bizMenuNode.Append(bizMenuNode2);
								dictionary.Add(item.Id, bizMenuNode2);
							}
						}
						else if (dictionary.ContainsKey(item.ParentId))
						{
							dictionary[item.ParentId].Append(bizMenuNode2);
						}
					}
				}
				_BizMenuNode = bizMenuNode;
			}
			return _BizMenuNode;
		}

		private BizMenuNode SearchParent(BizMenuNode root, BizMenu menu, IList<BizMenu> menus, IDictionary<string, BizMenuNode> cache)
		{
			if (menu == null)
			{
				return null;
			}
			BizMenuNode bizMenuNode = new BizMenuNode(menu);
			if (cache.ContainsKey(menu.ParentId))
			{
				if (!cache.ContainsKey(menu.Id))
				{
					cache[menu.ParentId].Append(bizMenuNode);
					cache.Add(menu.Id, bizMenuNode);
				}
				return bizMenuNode;
			}
			if (string.IsNullOrEmpty(menu.ParentId))
			{
				if (!cache.ContainsKey(menu.Id))
				{
					root.Append(bizMenuNode);
					cache.Add(menu.Id, bizMenuNode);
				}
				return bizMenuNode;
			}
			BizMenu bizMenu = menus.Where((BizMenu p) => p.Id == menu.ParentId).FirstOrDefault();
			if (bizMenu == null)
			{
				return null;
			}
			BizMenuNode bizMenuNode2 = SearchParent(root, bizMenu, menus, cache);
			if (bizMenuNode2 != null)
			{
				bizMenuNode2.Append(bizMenuNode);
				cache.Add(menu.Id, bizMenuNode);
				return bizMenuNode;
			}
			return null;
		}

		public object GetObject(params object[] args)
		{
			return Get(args);
		}
	}
}
