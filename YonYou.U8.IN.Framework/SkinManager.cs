﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YonYou.U8.IN.Framework
{
	internal class SkinManager : IDisposable
	{
		private static bool _loadedResources;

		private static SkinManager _instance;

		//private SkinSE_Net _skinSE;

		public static SkinManager Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new SkinManager();
				}
				return _instance;
			}
		}

		private SkinManager()
		{
			_loadedResources = false;
		}

		public void Start()
		{
			//IL_0009: Unknown result type (might be due to invalid IL or missing references)
			//IL_0013: Expected O, but got Unknown
			try
			{
				//if (_skinSE == null)
				//{
				//	_skinSE = new SkinSE_Net();
				//}
				if (!_loadedResources)
				{
					//_skinSE.Start();
					_loadedResources = true;
				}
			}
			catch
			{
			}
		}

		public void Dispose()
		{
			try
			{
				//_skinSE.Clear();
			}
			catch
			{
			}
		}
	}
}
