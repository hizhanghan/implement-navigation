﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YonYou.U8.IN.Framework
{
	public interface IProvider
	{
		object GetObject(params object[] args);
	}

	public interface IProvider<T> : IProvider where T : class, new()
	{
		T Get(params object[] args);
	}
}
