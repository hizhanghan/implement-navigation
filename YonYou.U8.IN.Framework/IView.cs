﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YonYou.U8.IN.Framework
{
	public interface IView
	{
		Q ExecuteAction<T, Q>(params object[] args) where T : IViewAction;
	}
}
