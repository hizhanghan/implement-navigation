﻿using System.Windows.Forms;

namespace YonYou.U8.IN.Framework
{
	public class MessageBoxUtils
	{
		public static void ShowTip(string message)
		{
			MessageBox.Show(message);
		}

		public static void ShowTip(string format, params object[] args)
		{
			MessageBox.Show(string.Format(format, args));
		}

		public static void ShowTipWithTitle(string message, string caption)
		{
			MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.None);
		}

		public static void ShowError(string message, string caption)
		{
			MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Hand);
		}
	}
}