﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YonYou.U8.IN.Framework
{
	public class ObjectTable<T> : Hashtable
	{
		public T this[string key]
		{
			get
			{
				if (ContainsKey(key))
				{
					return (T)base[key];
				}
				return default(T);
			}
		}
	}
}
