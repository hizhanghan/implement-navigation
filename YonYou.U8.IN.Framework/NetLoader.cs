﻿using System;
using YonYou.U8.IN.Framework.Events;

namespace YonYou.U8.IN.Framework
{
	public class NetLoader : INetLoader
	{
		private EventHandler<NetLoaderEventArgs> _OpenEmbedControl;

		public virtual bool IsEmbed
		{
			get
			{
				return false;
			}
		}

		internal bool IsInternalEmbed { get; set; }

		public event EventHandler<NetLoaderEventArgs> OpenEmbedControl
		{
			add
			{
				_OpenEmbedControl = (EventHandler<NetLoaderEventArgs>)Delegate.Combine(_OpenEmbedControl, value);
			}
			remove
			{
				_OpenEmbedControl = (EventHandler<NetLoaderEventArgs>)Delegate.Remove(_OpenEmbedControl, value);
			}
		}

		public virtual bool Load(BizContext context, string cMenuId, string subMenuId)
		{
			return true;
		}

		public void ShowEmbedControl(INetControl control, string key, bool singleton)
		{
			NetLoaderEventArgs netLoaderEventArgs = new NetLoaderEventArgs(control, key, singleton);
			netLoaderEventArgs.Loader = this;
			OnOpenEmbedControl(netLoaderEventArgs);
		}

		protected virtual void OnOpenEmbedControl(NetLoaderEventArgs e)
		{
			if (_OpenEmbedControl != null)
			{
				_OpenEmbedControl(this, e);
			}
		}
	}
}