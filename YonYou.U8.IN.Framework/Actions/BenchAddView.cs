﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YonYou.U8.IN.Framework.Internals;

namespace YonYou.U8.IN.Framework.Actions
{
	internal class BenchAddView : IViewAction
	{
		public object Execute(params object[] args)
		{
			if (args.Count() != 2)
			{
				throw new ArgumentException("args");
			}
			Execute(args[0] as ViewBenchForm, args[1] as AbstractView);
			return null;
		}

		protected virtual void Execute(ViewBenchForm viewBench, AbstractView view)
		{
			if (viewBench == null)
			{
				throw new ArgumentNullException("viewBench");
			}
			if (view == null)
			{
				throw new ArgumentNullException("view");
			}
			viewBench.Controls.Add(view.Control);
		}
	}
}
