﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YonYou.U8.IN.Framework.Internals;

namespace YonYou.U8.IN.Framework.Actions
{
	internal class BenchInitialize : IViewAction
	{
		public object Execute(params object[] args)
		{
			if (args.Count() != 1)
			{
				throw new ArgumentException("args");
			}
			Execute(args[0] as ViewBenchForm);
			return null;
		}

		protected virtual void Execute(ViewBenchForm viewBench)
		{
			if (viewBench == null)
			{
				throw new ArgumentNullException("viewBench");
			}
			viewBench.SuspendLayout();
			viewBench.Text = "U8+实施导航工作台";
			viewBench.ResumeLayout(false);
		}
	}
}
