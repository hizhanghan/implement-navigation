﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YonYou.U8.IN.Framework.Views;

namespace YonYou.U8.IN.Framework.Actions
{
	internal class LayoutAddView : IViewAction
	{
		public object Execute(params object[] args)
		{
			if (args.Count() != 3)
			{
				throw new ArgumentException("args");
			}
			Execute(args[0] as LayoutView, (LayoutStyle)args[1], args[2] as AbstractView);
			return null;
		}

		protected virtual void Execute(LayoutView layoutView, LayoutStyle style, AbstractView view)
		{
			if (layoutView == null)
			{
				throw new ArgumentNullException("viewBench");
			}
			if (view != null || view.Control != null)
			{
				switch (style)
				{
					case LayoutStyle.ToolBar:
						layoutView.ToolBarContainer.Controls.Add(view.Control);
						break;
					case LayoutStyle.Editor:
						layoutView.EditorContainer.Controls.Clear();
						layoutView.EditorContainer.Controls.Add(view.Control);
						view.Control.Dock = DockStyle.Fill;
						break;
					case LayoutStyle.StatusBar:
						layoutView.StatusContainer.Controls.Clear();
						layoutView.StatusContainer.Controls.Add(view.Control);
						layoutView.StatusContainer.Visible = true;
						break;
				}
			}
		}
	}
}
