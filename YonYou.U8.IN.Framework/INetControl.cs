﻿using System.Windows.Forms;

namespace YonYou.U8.IN.Framework
{
	public interface INetControl
	{
		bool ShowStatusBar { get; }

		Control CreateControl(BizContext context);
	}
}