﻿using System;
using System.Threading;
using System.Windows.Forms;
using UFSoft.U8.Framework.Login.UI;
using UFSoft.U8.Framework.LoginContext;
using YonYou.U8.IN.Framework;
using YonYou.U8.IN.Model;
using YonYou.U8.IN.Utility;
namespace YonYou.U8.IN.Framework
{
    public class Loginable
    {
        private static Loginable _instance;

        private Mutex _mutex;

        private string userToken = string.Empty;

        private LoginEntity loginEntity;

        private clsLogin loginObject;

        public static Loginable Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Loginable();
                }
                return _instance;
            }
        }

        public LoginEntity LoginEntity
        {
            get
            {
                if (loginEntity == null)
                {
                    loginEntity = new LoginEntity(loginObject);
                }
                return loginEntity;
            }
        }

        public string LanguageId { get; private set; }

        private Loginable()
        {
        }

        public bool Login(string[] args)
        {
            //new clsLogin();
            if (args != null && args.Length == 0)
            {
                return InternalLogin();
            }
            return InternalLogin(args);
        }

        internal bool InternalLogin(string[] args)
        {
            try
            {
                clsLogin clsLogin = new clsLogin();
                if (args != null && args.Length > 0)
                {
                    string userId = args[0].ToString().Replace("/u:", "");
                    string text = args[1].ToString().Replace("/t:", "");
                    string pwd = args[2].ToString().Replace("/p:", "");
                    string langId = args[3].ToString().Replace("/show:", "").Trim()
                        .Replace("/culture:", "");
                    UserData loginInfo = clsLogin.GetLoginInfo(text);
                    if (loginInfo == null)
                    {
                        MessageBoxUtils.ShowTip(GetString("in_001", langId));
                        return false;
                    }
                    clsLogin.LanguageID = loginInfo.LanguageID;
                    if (!clsLogin.login("QI", userId, pwd, loginInfo.AppServer, loginInfo.operDate, loginInfo.DataSource, loginInfo.WorkStationSerial, false))
                    {
                        MessageBoxUtils.ShowTip(clsLogin.ErrDescript);
                        return false;
                    }
                    clsLogin.SubLogin("QI");
                    loginObject = clsLogin;
                    return true;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("[实施导航]-[Loginable->LoginWithArgs]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
                MessageBoxUtils.ShowTipWithTitle(ex.Message, "登录失败");
            }
            return false;
        }

        private string GetString(string id, string langId)
        {
            switch (langId)
            {
                case "zh-cn":
                    {
                        string text3;
                        if ((text3 = id) != null && text3 == "in_001")
                        {
                            return "登陆失败,请稍后再试.";
                        }
                        return "";
                    }
                case "zh-tw":
                    {
                        string text2;
                        if ((text2 = id) != null && text2 == "in_001")
                        {
                            return "登陸失敗,請稍後再試.";
                        }
                        return "";
                    }
                case "en-us":
                    {
                        string text;
                        if ((text = id) != null && text == "in_001")
                        {
                            return "Login failed,Please try again later.";
                        }
                        return "";
                    }
                default:
                    return "登陸失敗,請稍後再試.";
            }
        }

        internal bool InternalLogin()
        {
            clsLogin clsLogin = new clsLogin();
            try
            {
                if (clsLogin.login("QI"))
                {
                    loginObject = clsLogin;
                    userToken = clsLogin.userToken;
                    LanguageId = loginObject.GetLoginInfo().LanguageID;
                    clsLogin.SubLogin("QI");
                    return true;
                }
                MessageBoxUtils.ShowTipWithTitle(clsLogin.ErrDescript, "登录失败");
                return false;
            }
            catch (Exception ex)
            {
                MessageBoxUtils.ShowTipWithTitle(ex.Message, "登录失败");
            }
            return false;
        }

        public bool CheckIsLogin()
        {
            _mutex = new Mutex(false, "SINGLE_INSTANCE_MUTEX_IN2");
            if (!_mutex.WaitOne(0, false))
            {
                _mutex.Close();
                _mutex = null;
                MessageBox.Show("U8快速实施导航工具已运行！");
                return true;
            }
            return false;
        }

        public void Release()
        {
            _mutex = null;
        }
    }
}