﻿using System.Linq;
using System.Windows.Forms;
using YonYou.U8.IN.Framework.Actions;
using YonYou.U8.IN.Framework.Internals;

namespace YonYou.U8.IN.Framework.Views
{
	public class BenchView : AbstractView
	{
		private ViewBenchForm _BenchForm;

		public override Control Control
		{
			get
			{
				return (Control)(object)_BenchForm;
			}
		}

		public BenchView()
		{
			_BenchForm = new ViewBenchForm();
			ExecuteAction<BenchInitialize>(new object[0]);
		}

		protected override T InternalExecuteAction<T>(IViewAction Action, params object[] args)
		{
			object[] array = null;
			if (args != null)
			{
				array = new object[args.Count() + 1];
				array[0] = _BenchForm;
				for (int i = 1; i <= args.Length; i++)
				{
					array[i] = args[i - 1];
				}
				return (T)Action.Execute(array);
			}
			return (T)Action.Execute(_BenchForm);
		}

		protected override void InitializeActionTypes()
		{
			RegisteActionType<BenchAddView>();
			RegisteActionType<BenchInitialize>();
		}
	}
}