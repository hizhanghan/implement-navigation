﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YonYou.U8.IN.Framework.Views
{
	public class EditorView : AbstractView
	{
		private Control _Control;

		public override Control Control
		{
			get
			{
				return _Control;
			}
		}

		public EditorView(Control control)
		{
			_Control = control;
		}

		protected override T InternalExecuteAction<T>(IViewAction Action, params object[] args)
		{
			return default(T);
		}

		protected override void InitializeActionTypes()
		{
		}
	}
}
