﻿using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using YonYou.U8.IN.Forms;

namespace YonYou.U8.IN.Framework.Views
{
	public class StatusBarView : AbstractView
	{
		private MetroStatusStrip _StatusBar;

		private ToolStripStatusLabel _Status;

		public override Control Control
		{
			get
			{
				return _StatusBar;
			}
		}

		public StatusBarView()
		{
			InitializeContainer();
		}

		protected override T InternalExecuteAction<T>(IViewAction Action, params object[] args)
		{
			object[] array = null;
			if (args != null)
			{
				array = new object[args.Count() + 1];
				array[0] = _StatusBar;
				for (int i = 1; i <= args.Length; i++)
				{
					array[i] = args[i - 1];
				}
				return (T)Action.Execute(array);
			}
			return (T)Action.Execute(_StatusBar);
		}

		protected override void InitializeActionTypes()
		{
		}

		protected void InitializeContainer()
		{
			_StatusBar = new MetroStatusStrip();
			_Status = new ToolStripStatusLabel();
			_StatusBar.SuspendLayout();
			_StatusBar.AutoSize = false;
			_StatusBar.Items.AddRange(new ToolStripItem[1] { _Status });
			_StatusBar.Dock = DockStyle.Fill;
			_StatusBar.BackColor = Color.FromArgb(0, 122, 204);
			_StatusBar.Location = new Point(1, 547);
			_StatusBar.Name = "StatusBar";
			_StatusBar.Size = new Size(847, 24);
			_StatusBar.SizingGrip = false;
			_StatusBar.TabIndex = 0;
			_StatusBar.Text = "StatusBar";
			if (ViewAdvisor.Instance.WorkBench.Skin != null)
			{
				_StatusBar.BackColor = (Color)ViewAdvisor.Instance.WorkBench.Skin.BackColor;
			}
			_Status.Name = "Status";
			_Status.Size = new Size(131, 17);
			_Status.Text = "";
			_StatusBar.ResumeLayout(false);
		}
	}
}