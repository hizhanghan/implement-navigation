﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YonYou.U8.IN.Framework.Actions;

namespace YonYou.U8.IN.Framework.Views
{
	public class LayoutView : AbstractView
	{
		public LayoutView()
		{
			this.InitializeContainer();
		}

		public LayoutView(Control toolBarContainer, Control editorContainer, Control statusContainer) : this()
		{
			this._ToolBarContainer = toolBarContainer;
			this._EditorContainer = editorContainer;
			this._StatusContainer = statusContainer;
		}

		protected override T InternalExecuteAction<T>(IViewAction Action, params object[] args)
		{
			T result;
			if (args != null)
			{
				object[] array = new object[args.Count<object>() + 1];
				array[0] = this;
				for (int i = 1; i <= args.Length; i++)
				{
					array[i] = args[i - 1];
				}
				result = (T)Action.Execute(array);
			}
			else
			{
				result = (T)Action.Execute(new object[]
                {
                    this
                });
			}
			return result;
		}

		protected override void InitializeActionTypes()
		{
			base.RegisteActionType<LayoutAddView>();
		}

		internal void InitializeContainer()
		{
		}

		internal Control ToolBarContainer
		{
			get
			{
				return this._ToolBarContainer;
			}
		}

		internal Control EditorContainer
		{
			get
			{
				return this._EditorContainer;
			}
		}

		internal Control StatusContainer
		{
			get
			{
				return this._StatusContainer;
			}
		}

		public override Control Control
		{
			get
			{
				throw new InvalidOperationException();
			}
		}

		private Control _ToolBarContainer;

		private Control _EditorContainer;

		private Control _StatusContainer;
	}
}
