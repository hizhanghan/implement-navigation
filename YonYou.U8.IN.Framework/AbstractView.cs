﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YonYou.U8.IN.Framework
{
	public abstract class AbstractView : IView
	{
		public AbstractView()
		{
			this._ActionTypes = new List<Type>();
			this.InitializeActionTypes();
		}

		public Q ExecuteAction<T, Q>(params object[] args) where T : IViewAction
		{
			try
			{
				Type typeFromHandle = typeof(T);
				if (this._ActionTypes.Contains(typeFromHandle))
				{
					IViewAction viewAction = Activator.CreateInstance<T>();
					if (viewAction != null)
					{
						return this.InternalExecuteAction<Q>(viewAction, args);
					}
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine("[实施导航]-[AbstractView->ExecuteAction]-[StackTrace:{0}]-[Message:{1}]", new object[]
				{
					ex.StackTrace,
					ex.Message
				});
			}
			return default(Q);
		}

		public void ExecuteAction<T>(params object[] args) where T : IViewAction
		{
			try
			{
				Type typeFromHandle = typeof(T);
				if (this._ActionTypes.Contains(typeFromHandle))
				{
					IViewAction viewAction = Activator.CreateInstance<T>();
					if (viewAction != null)
					{
						this.InternalExecuteAction<object>(viewAction, args);
					}
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine("[实施导航]-[AbstractView->ExecuteAction]-[StackTrace:{0}]-[Message:{1}]", new object[]
				{
					ex.StackTrace,
					ex.Message
				});
			}
		}

		protected abstract T InternalExecuteAction<T>(IViewAction Action, params object[] args);

		protected void RegisteActionType<T>() where T : IViewAction
		{
			Type typeFromHandle = typeof(T);
			if (!this._ActionTypes.Contains(typeFromHandle))
			{
				this._ActionTypes.Add(typeFromHandle);
			}
		}

		protected void UnregisteActionType<T>() where T : IViewAction
		{
			Type typeFromHandle = typeof(T);
			if (this._ActionTypes.Contains(typeFromHandle))
			{
				this._ActionTypes.Remove(typeFromHandle);
			}
		}

		protected abstract void InitializeActionTypes();

		public virtual Control Control
		{
			get
			{
				return null;
			}
		}

		private IList<Type> _ActionTypes;
	}
}
