﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YonYou.U8.IN.Model;

namespace YonYou.U8.IN.Framework
{
	internal class FrameworkContext : BizContext
	{
		private BizMenuNode _RootMenuNode;

		internal BizMenuNode RootMenuNode { get; set; }

		public FrameworkContext(BizMenuNode root)
		{
			if (root != null && root.Parent != null)
			{
				throw new ArgumentException("root");
			}
			_RootMenuNode = root;
		}

		public FrameworkContext()
		{
		}

		public override BizMenuNode QueryMenuNode(string cMenuId)
		{
			if (_RootMenuNode == null)
			{
				return null;
			}
			return _RootMenuNode.QueryNode(cMenuId);
		}
	}
}
