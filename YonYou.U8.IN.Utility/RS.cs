﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;

namespace YonYou.U8.IN.Utility
{
	public class RS
	{
		private static ResourceManager _resourceManager;

		public RS(string baseName, Assembly assembly)
		{
			try
			{
				_resourceManager = new ResourceManager(baseName, assembly);
			}
			catch (MissingManifestResourceException ex)
			{
				Debug.WriteLine("[实施导航]-[RS]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
			}
			catch (Exception ex2)
			{
				Debug.WriteLine("[实施导航]-[RS]-[StackTrace:{0}]-[Message:{1}]", ex2.StackTrace, ex2.Message);
			}
		}

		public string GetString(string sourcename)
		{
			try
			{
				string @string = _resourceManager.GetString(sourcename, Thread.CurrentThread.CurrentUICulture);
				return (@string != null) ? @string : sourcename;
			}
			catch (MissingManifestResourceException ex)
			{
				Debug.WriteLine("[实施导航]-[RS->GetString]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
				return sourcename;
			}
			catch (Exception ex2)
			{
				Debug.WriteLine("[实施导航]-[RS->GetString]-[StackTrace:{0}]-[Message:{1}]", ex2.StackTrace, ex2.Message);
				return sourcename;
			}
		}

		public string GetString(string sourcename, string languageId)
		{
			CultureInfo culture = new CultureInfo(languageId);
			try
			{
				string @string = _resourceManager.GetString(sourcename, culture);
				return (@string != null) ? @string : sourcename;
			}
			catch (MissingManifestResourceException ex)
			{
				Debug.WriteLine("[实施导航]-[RS->GetString(string sourcename,string languageId)]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
				return sourcename;
			}
			catch (Exception ex2)
			{
				Debug.WriteLine("[实施导航]-[RS->GetString(string sourcename,string languageId)]-[StackTrace:{0}]-[Message:{1}]", ex2.StackTrace, ex2.Message);
				return sourcename;
			}
		}

		public static string GetResourceString(string fileName, Assembly assembly)
		{
			if (string.IsNullOrEmpty(fileName))
			{
				throw new ArgumentNullException("fileName");
			}
			if (assembly == null)
			{
				throw new ArgumentNullException("assembly");
			}
			try
			{
				Stream manifestResourceStream = assembly.GetManifestResourceStream(fileName);
				StreamReader streamReader = new StreamReader(manifestResourceStream, Encoding.Default);
				string text = string.Empty;
				while (streamReader.Peek() > -1)
				{
					string text2 = streamReader.ReadLine();
					text = text + text2 + Environment.NewLine;
				}
				manifestResourceStream.Close();
				streamReader.Close();
				return text;
			}
			catch (Exception ex)
			{
				Debug.WriteLine("[实施导航]-[RS->GetResourceString(string fileName, Assembly assembly)]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
			}
			return null;
		}

		public static string GetResourceString(string fileName)
		{
			if (string.IsNullOrEmpty(fileName))
			{
				throw new ArgumentNullException("fileName");
			}
			try
			{
				Assembly callingAssembly = Assembly.GetCallingAssembly();
				Stream manifestResourceStream = callingAssembly.GetManifestResourceStream(fileName);
				StreamReader streamReader = new StreamReader(manifestResourceStream, Encoding.Default);
				string text = string.Empty;
				while (streamReader.Peek() > -1)
				{
					string text2 = streamReader.ReadLine();
					text = text + text2 + Environment.NewLine;
				}
				manifestResourceStream.Close();
				streamReader.Close();
				return text;
			}
			catch (Exception ex)
			{
				Debug.WriteLine("[实施导航]-[RS->GetResourceString]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
			}
			return null;
		}

		public static Stream GetResourceStream(string fileName)
		{
			if (string.IsNullOrEmpty(fileName))
			{
				throw new ArgumentNullException("fileName");
			}
			try
			{
				Assembly callingAssembly = Assembly.GetCallingAssembly();
				return callingAssembly.GetManifestResourceStream(fileName);
			}
			catch (Exception ex)
			{
				Debug.WriteLine("[实施导航]-[RS->GetResourceStream]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
			}
			return null;
		}

		public static Stream GetResourceStream(string fileName, Assembly assembly)
		{
			if (string.IsNullOrEmpty(fileName))
			{
				throw new ArgumentNullException("fileName");
			}
			if (assembly == null)
			{
				throw new ArgumentNullException("assembly");
			}
			try
			{
				return assembly.GetManifestResourceStream(fileName);
			}
			catch (Exception ex)
			{
				Debug.WriteLine("[实施导航]-[RS->GetResourceStream(string fileName,Assembly assembly)]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
			}
			return null;
		}
	}
}
