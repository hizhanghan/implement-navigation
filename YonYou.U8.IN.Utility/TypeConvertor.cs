﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YonYou.U8.IN.Utility
{
	public class TypeConvertor
	{
		public static T ToEnumValue<T>(string value, T defaultValue) where T : struct
		{
			try
			{
				if (string.IsNullOrEmpty(value))
				{
					return defaultValue;
				}
				return (T)Enum.Parse(typeof(T), value);
			}
			catch (Exception ex)
			{
				Debug.WriteLine("[实施导航]-[TypeConvertor->ToEnumValue]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
				return defaultValue;
			}
		}
	}
}
