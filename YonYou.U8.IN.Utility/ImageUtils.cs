﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace YonYou.U8.IN.Utility
{
	public class ImageUtils
	{
		public static Image GetImageFromAssembly(string name)
		{
			try
			{
				Image result = null;
				if (string.IsNullOrEmpty(name))
				{
					throw new ArgumentNullException("name");
				}
				Assembly callingAssembly = Assembly.GetCallingAssembly();
				string name2 = callingAssembly.GetName().Name;
				StringBuilder stringBuilder = new StringBuilder();
				if (name[0] != '.')
				{
					stringBuilder.Append(name2 + "." + name);
				}
				else
				{
					stringBuilder.Append(name2 + name);
				}
				using (Stream stream = callingAssembly.GetManifestResourceStream(stringBuilder.ToString()))
				{
					result = Image.FromStream(stream);
				}
				return result;
			}
			catch (Exception ex)
			{
				Debug.WriteLine("[实施导航]-[ImageUtils->GetEmbeddedImage]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
				throw ex;
			}
		}

		public static Image GetImageFromAssemblyByName(string fullName)
		{
			try
			{
				Image result = null;
				if (string.IsNullOrEmpty(fullName))
				{
					throw new ArgumentNullException("name");
				}
				Assembly callingAssembly = Assembly.GetCallingAssembly();
				using (Stream stream = callingAssembly.GetManifestResourceStream(fullName))
				{
					result = Image.FromStream(stream);
				}
				return result;
			}
			catch (Exception ex)
			{
				Debug.WriteLine("[实施导航]-[ImageUtils->GetEmbeddedImage]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
				throw ex;
			}
		}

		public static Image GetImageFromLocal(string name)
		{
			try
			{
				if (string.IsNullOrEmpty(name))
				{
					throw new ArgumentNullException("name");
				}
				if (!File.Exists(name))
				{
					throw new FileNotFoundException(name);
				}
				return Image.FromFile(name);
			}
			catch (Exception ex)
			{
				Debug.WriteLine("[实施导航]-[ImageUtils->GetEmbeddedImage]-[StackTrace:{0}]-[Message:{1}]", ex.StackTrace, ex.Message);
				throw ex;
			}
		}
	}
}
