﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace YonYou.U8.IN.Utility
{
	public class Debug
	{
		[DllImport("Kernel32", CharSet = CharSet.Auto)]
		private static extern void OutputDebugString(string msg);

		public static void WriteLine(string format, params object[] args)
		{
			try
			{
				DateTime now = DateTime.Now;
				if (args != null && args.Length > 0)
				{
					OutputDebugString("YonYou.U8.Visio:[" + now.ToString() + ":" + now.Millisecond + "]-" + string.Format(format, args) + "\r\n");
				}
				else
				{
					OutputDebugString("YonYou.U8.Visio:[" + now.ToString() + ":" + now.Millisecond + "]-" + format + "\r\n");
				}
			}
			catch
			{
			}
		}

		public static void Write(string format, params object[] args)
		{
			try
			{
				DateTime now = DateTime.Now;
				if (args != null && args.Length > 0)
				{
					OutputDebugString("YonYou.U8.Visio:[" + now.ToString() + ":" + now.Millisecond + "]-" + string.Format(format, args));
				}
				else
				{
					OutputDebugString("YonYou.U8.Visio:[" + now.ToString() + ":" + now.Millisecond + "]-" + format);
				}
			}
			catch
			{
			}
		}
	}
}
