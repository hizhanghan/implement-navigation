﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YonYou.U8.IN.Utility
{
	public static class CommonUtils
	{
		public static string Group(int[] values, char separator)
		{
			if (values.Length == 0)
			{
				return string.Empty;
			}
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < values.Length; i++)
			{
				if (i != values.Length - 1)
				{
					stringBuilder.AppendFormat("{0}{1}", values[i], separator);
				}
				else
				{
					stringBuilder.AppendFormat("{0}", values[i]);
				}
			}
			return stringBuilder.ToString();
		}

		public static string Group(string[] values, char separator)
		{
			if (values.Length == 0)
			{
				return string.Empty;
			}
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < values.Length; i++)
			{
				if (i != values.Length - 1)
				{
					stringBuilder.AppendFormat("{0}{1}", values[i], separator);
				}
				else
				{
					stringBuilder.AppendFormat("{0}", values[i]);
				}
			}
			return stringBuilder.ToString();
		}

		public static string Group(string[] values, string formate, char separator)
		{
			if (values.Length == 0)
			{
				return string.Empty;
			}
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < values.Length; i++)
			{
				if (i != values.Length - 1)
				{
					stringBuilder.AppendFormat(formate + "{1}", values[i], separator);
				}
				else
				{
					stringBuilder.AppendFormat(formate, values[i]);
				}
			}
			return stringBuilder.ToString();
		}

		public static bool EndsWith(this string str, char value)
		{
			int length = str.Length;
			if (length != 0)
			{
				return str[length - 1] == value;
			}
			return false;
		}
	}
}
